<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class PDSController extends REST_Controller
{
    public $tokenData = array();
    function __construct() {
        parent::__construct();
        $this->load->model('User');
        
        if(!$this->checkToken()){
            $this->errorUnAuthorized();
            return;
        }
        $this->tokenData = $this->checkToken();
        
    }
    
    function checkToken(){
        $headers = $this->input->request_headers();
        if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            //Lakukan pengecekan token ke database, hanya boleh token valid
            $isValidToken = $this->User->isValidAccessToken($token->access_token);
            if($isValidToken){
                return $token;
            }
            return FALSE;
        }else{
            $this->errorForbbiden();
            return;
        }
    }
    
    public function getToken()
    {
        return $this->tokenData;
    }
            
    

    function errorUnAuthorized(){
        $response = [
            'status' => 401,
            'message' => 'Unauthorized',
        ];
        $this->set_response($response, 401);
        return;
    }

    function errorForbbiden(){
        $response = [
            'status' => 403,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, 403);
        return;
    }
}
