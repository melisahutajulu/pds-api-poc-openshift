<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';
require_once APPPATH . 'helpers/message_helper.php';

class Gadai extends CorePegadaian
{
  /// Constanta Nama database
  const Perhiasan = 'gadai_perhiasan';
  const LogamMulia = 'gadai_logam_mulia';
  const Handphone = 'gadai_handphone';
  const Elektronik = 'gadai_elektronik';
  const Kendaraan = 'gadai_kendaraan';
  const Laptop = 'gadai_laptop';
  const Efek = 'gadai_efek';

  // Konstanta Start Value Kode Booking
  const PerhiasanStartValue = '100001';
  const LogamMuliaStartValue = '200001';
  const HandphoneStartValue = '300001';
  const ElektronikStartValue = '400001';
  const KendaraanStartValue = '500001';
  const LaptopStartValue = '600001';
  const EfekStartValue = '700001';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('GadaiModel', 'User', 'NotificationModel', 'ConfigModel', 'BankModel'));
        $this->load->model('MasterModel');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->service('restGosend_service');
        $this->load->service('gadai_service');
    }

  private function uploadFoto()
  {

    $uploadDir = $this->config->item('upload_dir');
    // Upload Foto gadai
    $config = [];
    $config['upload_path'] = $uploadDir . '/user/gadai/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
    $config['max_size'] = 0;
    $config['max_width'] = 0;
    $config['max_height'] = 0;
    $config['encrypt_name'] = true;

    $this->load->library('upload');

    $foto = [];
    $errors = [];
    $jmlFoto = count($_FILES['foto']['name']);
    for ($i = 0; $i < $jmlFoto; $i++) :
      $_FILES['userfile']['name'] = $_FILES['foto']['name'][$i];
      $_FILES['userfile']['type'] = $_FILES['foto']['type'][$i];
      $_FILES['userfile']['tmp_name'] = $_FILES['foto']['tmp_name'][$i];
      $_FILES['userfile']['error'] = $_FILES['foto']['error'][$i];
      $_FILES['userfile']['size'] = $_FILES['foto']['size'][$i];

      $this->upload->initialize($config);
      if (!$this->upload->do_upload()) :
        $errors[] = $this->upload->display_errors();
      else :
        $foto[]['nama_foto'] = $this->upload->data()['file_name'];
      endif;
    endfor;

    return array(
      'foto' => $foto,
      'error' => $errors
    );
  }

  /**
   * Simplified send mail
   * @param string $nama Nama Pengguna
   * @param string $email Email Pengguna
   * @param string $kodeBooking Kode booking yang sudah di generate
   * @param string $kodeOutlet Kode Outlet Pegadaian
   * @param integer $jadwalKedatangan Jadwal Kedatangan User
   */
  private function sendEmail($idUser, $nama, $email, $kodeBooking, $kodeOutlet, $jadwalKedatangan, $noTel)
  {

    $dataCabang = $this->MasterModel->getSingleCabang($kodeOutlet);
    $tanggalKedatangan = date('d-m-Y', $jadwalKedatangan);
    $waktuKedatangan = date('H:i', $jadwalKedatangan);
    $detailGadai = $this->GadaiModel->getDetailGadaiByKodeBooking($kodeBooking);
    $user = $this->User->getUser($idUser);

    $templateData = array(
      'nama' => $user->nama,
      'noPengajuan' => $kodeBooking,
      'namaOutlet' => $dataCabang->nama,
      'alamatOutlet' => $dataCabang->alamat . ', ' . $dataCabang->kelurahan . ', ' . $dataCabang->kecamatan . ', ' . $dataCabang->kabupaten . ', ' . $dataCabang->provinsi,
      'teleponOutlet' => $dataCabang->telepon,
      'tanggalDatang' => $tanggalKedatangan,
      'waktuDatang' => $waktuKedatangan,
      'detailGadai' => $detailGadai
    );

    $subjekEmail = 'Pengajuan Gadai Berhasil (Kode Booking ' . $kodeBooking . ')';

    $content = $this->load->view('mail/notif_kca', $templateData, TRUE);

    $template = $this->load->view('mail/email_template_top', array('title' => $subjekEmail), true);
    $template = $template . $content;
    $template = $template . $this->load->view('mail/email_template_bottom', array(), true);

    $mobile = $this->load->view('notification/top_template', array('title' => $subjekEmail), true);
    $mobile = $mobile . $content;
    $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);


    Message::sendEmail($email, $subjekEmail, $template);

    //Simpan notifikasi baru
    $idNotif = $this->NotificationModel->add(
      $idUser,
      NotificationModel::TYPE_GADAI,
      NotificationModel::CONTENT_TYPE_HTML,
      "Pengajuan Gadai",
      "Kode Booking " . $kodeBooking,
      $mobile,
      $content,
      "GD"
    );

    Message::sendFCMNotif(
      $this->User->getFCMToken($idUser),
      [
        "id" => $idNotif,
        "tipe" => "GD",
        "title" => "Pengajuan Gadai",
        "tagline" => "Kode Booking " . $kodeBooking,
        "kodeBooking" => $kodeBooking,
        "content" => '',
        "token" => $noTel
      ]
    );
  }

  private function formatKeUNIXTimestamp($tanggal, $waktu)
  {
    // Format date to unixtimestamp
    $waktuDatang = $tanggal . ' ' . $waktu;
    return strtotime($waktuDatang);
  }

  /**
   * Menghitunga Prosentase UP Terhadap Taksiran (Ref. DiskusiGadaiOnline 04Sept2017.xls)
   * @param integer $taksiran hasil dari perhitungan taksiran
   * @return integer presentase taksiran
   */
  private function prosentaseUP($taksiran)
  {
    if ($taksiran >= 50000 && $taksiran <= 500000) {
      return 0.95;
    } else if ($taksiran >= 510000 && $taksiran <= 20000000) {
      return 0.92;
    } else if ($taksiran >= 20010000) {
      return 0.93;
    } else {
      return 0.92;
    }
  }

  private function pembulatanPinj($totalPinj)
  {
    if ($totalPinj >= 500000 && $totalPinj <= 20000000) {
      return 10000;
    } else if ($totalPinj > 20000000) {
      return 100000;
    } else {
      return 10000;
    }
  }

  /**
   * Pembulantan kebawah nilai Gadai berdasarkan rule
   * @param integer $num Jumlah Pinjaman
   * @param integer $divisor banyak pembulatan
   * @return integer Hasil Pembulatan
   */
  function roundDown($num, $divisor)
  {
    $diff = $num % $divisor;
    if ($diff == 0)
      return $num;
    elseif ($diff >= ceil($divisor / 2))
      return $num - $diff + $divisor;
    else
      return $num - $diff;
  }

  /**
   * Perhitungan Gadai
   */
  function hitung_perhiasan_post()
  {
    $this->form_validation->set_rules('kadar', "Kadar", 'required|numeric');
    $this->form_validation->set_rules('berat_bersih', 'Berat Bersih', 'numeric|required');

    if (!$this->form_validation->run()) {
      $this->response(array(
        'code' => 101,
        'status' => 'error',
        'message' => 'Invalid Input',
        'errors' => $this->form_validation->error_array()
      ), 200);
    } else {
      $cekSTL = $this->getHargaSTL();

      if ($cekSTL->responseCode == '00') {
        $data = json_decode($cekSTL->data);

        $kadar = $this->post('kadar');
        $beratBersih = $this->post('berat_bersih');

        // Diketahui
        $kadarAtas = $kadar;
        $kadarBawah = $kadar - 4;
        $stlPerhiasan = $data->hargaEmas;

        // Todo: Hitung range tertinggi
        $stlAtas = ($kadarAtas / 24) * $stlPerhiasan;
        $taksiranAtas = $stlAtas * $beratBersih;
        $pinjamanAtas = $this->prosentaseUP($taksiranAtas) * $taksiranAtas;
        $totalPinjamanAtas = $this->roundDown($pinjamanAtas, $this->pembulatanPinj($pinjamanAtas));

        // Todo Hitung range terendah
        $stlBawah = ($kadarBawah / 24) * $stlPerhiasan;
        $taksiranBawah = $stlBawah * $beratBersih;
        $pinjamanBawah = $this->prosentaseUP($taksiranBawah) * $taksiranBawah;
        $totalPinjamanBawah = $this->roundDown($pinjamanBawah, $this->pembulatanPinj($pinjamanBawah));

        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => array(
            'taksiran_atas' => floor($totalPinjamanAtas),
            'taksiran_bawah' => floor($totalPinjamanBawah),
          )
        ), 200);
      } else {
        $this->errorUnAuthorized();
      }
    }
  }

  function hitung_perhiasan_god_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'kadar' =>  $this->post('kadar'),
        'berat_bersih' => $this->post('berat_bersih')
      );
      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('kadar[]', "Kadar", 'required|numeric');
      $this->form_validation->set_rules('berat_bersih[]', 'Berat Bersih', 'numeric|required');
      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $cekSTL = $this->getHargaSTL();
        if ($cekSTL->responseCode == '00') {
          $data = json_decode($cekSTL->data);

          $kadar = $this->post('kadar');
          $beratBersih = $this->post('berat_bersih');
          $totalKadar = 0;
          $totalBeratBersih = 0;

          $presentaseAtas = 0.90;
          $presentaseBawah = 0.20;
          $stlPerhiasan = $data->hargaEmas;
          $pinjamanAtas = 0;
          $pinjamanBawah = 0;
          $TotalNilaiBarang = 0;

          foreach ($kadar as $key => $val) {
            $TotalNilaiBarang = $TotalNilaiBarang + (($val / 24) * $beratBersih[$key] * $stlPerhiasan);
          }
          $TotalNilaiBarang = floor($TotalNilaiBarang);
          $pinjamanAtas = floor($TotalNilaiBarang * 0.9);
          $pinjamanBawah = floor($TotalNilaiBarang * 0.2);
          $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => array(
              'taksiran_atas' => $pinjamanAtas,
              'taksiran_bawah' => $pinjamanBawah,
              'total_nilai' => $TotalNilaiBarang
            )
          ), 200);
        } else {
          $this->set_response([
            'status' => 'error',
            'message' => 'RC ' . $cekSTL->responseCode . ': ' . $cekSTL->responseDesc,
            'code' => 103
          ]);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function hitung_logam_mulia_post()
  {
    // Karena nanti bisa hitung lewat home, tidak perlu AccessToken
    $this->form_validation->set_rules('berat_keping[]', 'Berat Keping', 'required|numeric');

    if (!$this->form_validation->run()) {
      $this->response(array(
        'code' => 101,
        'nama' => $this->put('nama'),
        'status' => 'error',
        'message' => 'Invalid input',
        'errors' => $this->form_validation->error_array()
      ), 200);
    } else {
      $cekSTL = $this->getHargaSTL();
      if ($cekSTL->responseCode == '00') {
        $data = json_decode($cekSTL->data);
        $stlLantakan = $data->hargaLantakanEmas;
        $stlPerhiasan = $data->hargaEmas;
        // Get All Keping
        $arKeping = $this->post('berat_keping');
        $totalBeratKeping = 0;
        foreach ($arKeping as $val) {
          $totalBeratKeping += $val;
        }

        // Diketahui
        $taksiranAtas = $stlLantakan * $totalBeratKeping;
        $taksiranBawah = $stlPerhiasan * $totalBeratKeping;

        $pinjamanAtas = $this->prosentaseUP($taksiranBawah) * $taksiranBawah;
        $pinjamanBawah = $this->prosentaseUP($taksiranAtas) * $taksiranAtas;

        $pinjamanAtas = $this->roundDown($pinjamanAtas, $this->pembulatanPinj($pinjamanAtas));
        $pinjamanBawah = $this->roundDown($pinjamanBawah, $this->pembulatanPinj($pinjamanBawah));

        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => array(
            'taksiran_atas' => floor($pinjamanAtas),
            'taksiran_bawah' => floor($pinjamanBawah)
          )
        ));
      } else {
        $this->set_response([
          'status' => 'error',
          'message' => 'RC ' . $cekSTL->responseCode . ': ' . $cekSTL->responseDesc,
          'code' => 103
        ]);
      }
    }
  }

  function hitung_logam_mulia_god_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'berat_keping' =>  $this->post('berat_keping')
      );
      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('berat_keping[]', 'Berat Keping', 'required|numeric');
      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'nama' => $this->put('nama'),
          'status' => 'error',
          'message' => 'Invalid input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $cekSTL = $this->getHargaSTL();
        if ($cekSTL->responseCode == '00') {
          $data = json_decode($cekSTL->data);
          $stlLantakan = $data->hargaLantakanEmas;
          $stlPerhiasan = $data->hargaEmas;
          // Get All Keping
          $arKeping = $this->post('berat_keping');
          $totalBeratKeping = 0;
          foreach ($arKeping as $val) {
            $totalBeratKeping += $val;
          }

          // Diketahui
          $taksiranAtas = floor($stlPerhiasan * $totalBeratKeping);
          $taksiranBawah = floor($stlPerhiasan * $totalBeratKeping);

          $pinjamanAtas = floor($taksiranAtas * 0.90);
          $pinjamanBawah = floor($taksiranBawah * 0.20);

          $totalPinjamanAtas = $taksiranAtas;
          $TotalNilaiBarang = $totalPinjamanAtas;
          $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => array(
              'taksiran_atas' => $pinjamanAtas,
              'taksiran_bawah' => $pinjamanBawah,
              'total_nilai' => $TotalNilaiBarang
            )
          ));
        } else {
          $this->set_response([
            'status' => 'error',
            'message' => 'RC ' . $cekSTL->responseCode . ': ' . $cekSTL->responseDesc,
            'code' => 103
          ]);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function nominal_pengajuan_nasabah_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'pengajuan_nasabah' =>  $this->post('pengajuan_nasabah')
      );
      $this->form_validation->set_data($setData);
      $data = json_decode($this->session->item);
      $this->form_validation->set_rules('pengajuan_nasabah', 'Nominal Pengajuan Nasabah', 'required|numeric');
      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'pengajuan_nasabah' => $this->put('pengajuan_nasabah'),
          'status' => 'error',
          'message' => 'Invalid input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        if (!empty($data)) {
          $pengajuan_nasabah = $this->input->post('pengajuan_nasabah');
          $idpengajuan = $this->GadaiModel->saveNominalLogamMulia(
            $data_kode_booking['kode_booking'],
            $pengajuan_nasabah
          );
          $this->set_response(
            array(
              'status' => 'success',
              'message' => '',
              'data' => array(
                'taksiran_atas' => $data->taksiran_atas,
                'taksiran_bawah' => $data->taksiran_bawah,
                'pengajuan_nasabah' => $this->input->post('pengajuan_nasabah'),
                'user_AIID' => $token->id,
                'kode_booking' => $data
              )
            )
          );
        } else {
          $this->set_response([
            'status' => 'error',
            'message' => 'RC ',
            'code' => 103
          ]);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function pembatalan_pinjaman_post()
  {
    $token = $this->getToken();
    $setData = array(
      'jenis_gadai' => $this->post('jenis_gadai'),
      'kode_booking' => $this->post('kode_booking')
    );
    $this->form_validation->set_data($setData);
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');
    $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
    if ($token) {
      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {

        $kodeBookingBatal = $this->post('kode_booking');
        $jenisGadai = $this->post('jenis_gadai');
        $data = $this->db->select('kode_outlet, jenis_layanan')
          ->where('kode_booking', $kodeBookingBatal)
          ->get('gadai_' . $jenisGadai)->result();
        if ($jenisGadai == 'perhiasan') {
          $status = $this->GadaiModel->getStatusPerhiasanByKodeBooking($kodeBookingBatal);
        } else if ($jenisGadai == 'logam_mulia') {
          $status = $this->GadaiModel->getStatusLogamMuliaByKodeBooking($kodeBookingBatal);
        }

        if ($status->respon_nasabah  == 2 || $status->status  == 4) {
          $this->set_response(array(
            'status' => 'error',
            'message' => 'Kode Booking ' . $kodeBookingBatal . ' Sudah ada response Sebelumnya',
            'data' => array(
              'kode_booking' => $kodeBookingBatal,
              'status' => $status->status
            )
          ), 400);
        } else {
          if (!empty($data)) {
            foreach ($data as $value) {
              // parsing data send to core
              $dataSend = [
                'kodeBooking' => $kodeBookingBatal,
                'branchCode' => $value->kode_outlet,
                'jenisTransaksi' => 'OP',
                'flag' => 'K'
              ];
              //send data to core saat di tolak dari sisi mobile
              //(Notes : end point core untuk cancel order gojek sama dengan yang dari admin cabang.)
              $tolakPost = $this->notifAdminTolak($dataSend);
              log_message('debug', 'Response GoD Cancel dari core (Mobile)'.json_encode($tolakPost));

              $user_AIID = $token->id;
              $status = $this->GadaiModel->simpanResponNasabah($user_AIID, $kodeBookingBatal, $jenisGadai);
              $this->NotificationModel->addAdminNotificaion(
                $token->id,
                NotificationModel::TYPE_GADAI,
                NotificationModel::CONTENT_TYPE_TEXT,
                "Pegadaian Digital Service",
                "Pengajuan dibatakan oleh Nasabah",
                "",
                "",
                $value->jenis_layanan,
                $value->kode_outlet,
                $kodeBookingBatal
              );
            }

            //response pembatalan
            $this->set_response(array(
              'status' => 'success',
              'message' => 'Pinjaman dengan kode booking ' . $kodeBookingBatal . ' dibatalkan',
              'data' => $status
            ), 200);
          }
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function terima_pinjaman_post()
  {

    $token = $this->getToken();
    $setData = array(
      'jenis_gadai' => $this->post('jenis_gadai'),
      'kode_booking' => $this->post('kode_booking')
    );
    $this->form_validation->set_data($setData);
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');
    $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
    if ($token) {
      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $kodeBooking = $this->post('kode_booking');
        $jenisGadai = $this->post('jenis_gadai');
        $data = $this->db->select('kode_outlet, jenis_layanan')
          ->where('kode_booking', $kodeBooking)
          ->get('gadai_' . $jenisGadai)->result();
        if ($jenisGadai == 'perhiasan') {
          $status = $this->GadaiModel->getStatusPerhiasanByKodeBooking($kodeBooking);
        } else if ($jenisGadai == 'logam_mulia') {
          $status = $this->GadaiModel->getStatusLogamMuliaByKodeBooking($kodeBooking);
        }
        if ($status->respon_nasabah  == 2 || $status->status == 4) {
          $this->set_response(array(
            'status' => 'error',
            'message' => 'Kode Booking ' . $kodeBooking . ' Sudah ada response Sebelumnya',
            'data' => array(
              'kode_booking' => $kodeBooking,
              'status' => $status->status
            )
          ), 400);
        } else {
          foreach ($data as $value) {
            $user_AIID = $token->id;
            $status = $this->GadaiModel->simpanApproveNasabah($user_AIID, $kodeBooking, $jenisGadai);
            $this->NotificationModel->addAdminNotificaion(
              $token->id,
              NotificationModel::TYPE_GADAI,
              NotificationModel::CONTENT_TYPE_TEXT,
              "Pegadaian Digital Service",
              "Pengajuan diterima oleh Nasabah",
              "",
              "",
              $value->jenis_layanan,
              $value->kode_outlet,
              $kodeBooking
            );
          }
          $this->set_response(array(
            'status' => 'success',
            'message' => 'Pinjaman dengan kode booking ' . $kodeBooking,
            'data' => $status
          ), 200);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function notif_admin_terima_post()
  {
    $setData = array(
      'jenis_gadai' => $this->post('jenis_gadai'),
      'kode_booking' => $this->post('kode_booking')
    );
    $this->form_validation->set_data($setData);
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');
    $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
    if (!$this->form_validation->run()) {
      $this->response(array(
        'code' => 101,
        'status' => 'error',
        'message' => 'Invalid Input',
        'errors' => $this->form_validation->error_array()
      ), 200);
    } else {
      $kodeBooking = $this->post('kode_booking');
      $jenisGadai = $this->post('jenis_gadai');

      if ($jenisGadai == 'logam_mulia') {
        $data = array();
        $data = $this->db->select('nominal_pengajuan_nasabah,taksiran_atas, kode_outlet, user_AIID, jenis_layanan, kode_outlet, up')
          ->where('kode_booking', $kodeBooking)
          ->get('gadai_' . $jenisGadai)->row();
        $idNotif = $this->NotificationModel->add(
          $data->user_AIID,
          NotificationModel::TYPE_GADAI,
          NotificationModel::CONTENT_TYPE_HTML,
          "Selamat pinjaman anda sudah disetujui",
          "Kode Booking " . $kodeBooking,
          "",
          "",
          "GOD"
        );
        Message::sendFCMNotif(
          $this->User->getFCMToken($data->user_AIID),
          [
            "id" => $idNotif,
            "tipe" => "PengajuanDisetujuiLM",
            "title" => "Pegadaian Digital Service #" . $kodeBooking . "#1",
            "tagline" => "Selamat pinjaman anda sudah disetujui " . $kodeBooking,
            "action_url" => "PengajuanDisetujuiLM",
            "kode_booking" => $kodeBooking,
            "taksiran_atas" => $data->taksiran_atas,
            "nominal_pengajuan" => $data->nominal_pengajuan_nasabah,
            "up" => $data->up,
            "jenis_gadai" => $jenisGadai
          ]
        );
      } elseif ($jenisGadai == 'perhiasan') {
        $data = array();
        $data = $this->db->select('nominal_pengajuan_nasabah,taksiran_atas, kode_outlet, user_AIID, jenis_layanan, kode_outlet, up')
          ->where('kode_booking', $kodeBooking)
          ->get('gadai_' . $jenisGadai)->row();
        $idNotif = $this->NotificationModel->add(
          $data->user_AIID,
          NotificationModel::TYPE_GADAI,
          NotificationModel::CONTENT_TYPE_HTML,
          "Selamat pinjaman anda sudah disetujui",
          "Kode Booking " . $kodeBooking,
          "",
          "",
          "GOD"
        );

        Message::sendFCMNotif(
          $this->User->getFCMToken($data->user_AIID),
          [
            "id" => $idNotif,
            "tipe" => "PengajuanDisetujui",
            "title" => "Pegadaian Digital Service #" . $kodeBooking . "#2",
            "tagline" => "Selamat pinjaman anda sudah disetujui " . $kodeBooking,
            "action_url" => "PengajuanDisetujui",
            "kode_booking" => $kodeBooking,
            "taksiran_atas" => $data->taksiran_atas,
            "nominal_pengajuan" => $data->nominal_pengajuan_nasabah,
            "up" => $data->up,
            "jenis_gadai" => $jenisGadai
          ]
        );
      }

      $response = array(
        'status' => 'success',
        'message' => 'Notifikasi terkirim',
        'data' => [
          'kode_booking' => $kodeBooking,
          'taksiran_atas' => $data->taksiran_atas,
          'nominal_pengajuan' => $data->nominal_pengajuan_nasabah
        ]
      );
      $this->set_response($response, 200);
      return;
    }
  }

  function notif_admin_tolak_post()
  {
    $setData = array(
      'jenis_gadai' => $this->post('jenis_gadai'),
      'kode_booking' => $this->post('kode_booking')
    );
    $this->form_validation->set_data($setData);
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');
    $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
    if (!$this->form_validation->run()) {
      $this->response(array(
        'code' => 101,
        'status' => 'error',
        'message' => 'Invalid Input',
        'errors' => $this->form_validation->error_array()
      ), 200);
    } else {

      $kodeBooking = $this->post('kode_booking');
      $jenisGadai = $this->post('jenis_gadai');

      if ($jenisGadai == 'logam_mulia') {
        $data = array();
        $data = $this->db->select('nominal_pengajuan_nasabah,taksiran_atas, kode_outlet, user_AIID, jenis_layanan, kode_outlet, no_order_gosend')
          ->where('kode_booking', $kodeBooking)
          ->get('gadai_' . $jenisGadai)->row();

        // parsing data send to core
        $dataSend = [
          'kodeBooking' => $kodeBooking,
          'branchCode' => $data->kode_outlet,
          'jenisTransaksi' => 'OP',
          'flag' => 'K'
        ];
        //send data to core
        $tolakPost = $this->notifAdminTolak($dataSend);

        //falidasi jika sukses
        if ($tolakPost->responseCode == '00') {
          log_message('debug', 'Response GoD Cancel dari core (Admin Cabang)'.json_encode($tolakPost));
          $idNotif = $this->NotificationModel->add(
            $data->user_AIID,
            NotificationModel::TYPE_GADAI,
            NotificationModel::CONTENT_TYPE_TEXT,
            "Pegadaian Digital Service",
            "Maaf Pinjaman anda belum disetujui",
            "",
            "",
            $data->jenis_layanan,
            $data->kode_outlet,
            $kodeBooking
          );

          Message::sendFCMNotif(
            $this->User->getFCMToken($data->user_AIID),
            [
              "id" => $idNotif,
              "tipe" => "PengajuanTidakDisetujuiLM",
              "title" => "Pegadaian Digital Service #" . $kodeBooking . "#1",
              "tagline" => "Maaf Pinjaman anda belum disetujui " . $kodeBooking,
              "action_url" => "PengajuanTidakDisetujuiLM",
              "kode_booking" => $kodeBooking,
              "taksiran_atas" => $data->taksiran_atas,
              "nominal_pengajuan" => $data->nominal_pengajuan_nasabah,
              "no_order_gosend" => $data->no_order_gosend
            ]
          );
        } else {
          log_message('debug', 'Response GoD Cancel dari core (Admin Cabang)'.json_encode($tolakPost));
          $this->set_response([
            'status' => 'error',
            'message' => '',
            'responseDesc' => $tolakPost->responseDesc,
            'data' => $tolakPost->data
          ]);
        }
      }
      if ($jenisGadai == 'perhiasan') {
        $data = array();
        $data = $this->db->select('nominal_pengajuan_nasabah,taksiran_atas, kode_outlet, user_AIID, jenis_layanan, kode_outlet, no_order_gosend')
          ->where('kode_booking', $kodeBooking)
          ->get('gadai_' . $jenisGadai)->row();

          // parsing data send to core
          $dataSend = [
            'kodeBooking' => $kodeBooking,
            'branchCode' => $data->kode_outlet,
            'jenisTransaksi' => 'OP',
            'flag' => 'K'
          ];

          //send data to core
          $tolakPost = $this->notifAdminTolak($dataSend);
          //falidasi jika sukses
          
          if ($tolakPost->responseCode == '00') {
            log_message('debug', 'Response GoD Cancel dari core (Admin Cabang)'.json_encode($tolakPost));
            $idNotif = $this->NotificationModel->add(
              $data->user_AIID,
              NotificationModel::TYPE_GADAI,
              NotificationModel::CONTENT_TYPE_HTML,
              "Maaf Pinjaman anda belum disetujui",
              "Kode Booking " . $kodeBooking,
              "",
              "",
              "GOD"
            );
            Message::sendFCMNotif(
              $this->User->getFCMToken($data->user_AIID),
              [
                "id" => $idNotif,
                "tipe" => "PengajuanTidakDisetujui",
                "title" => "Pegadaian Digital Service #" . $kodeBooking . "#2",
                "tagline" => "Maaf Pinjaman anda belum disetujui" . $kodeBooking,
                "action_url" => "PengajuanTidakDisetujui",
                "kode_booking" => $kodeBooking,
                "taksiran_atas" => $data->taksiran_atas,
                "nominal_pengajuan" => $data->nominal_pengajuan_nasabah,
                "no_order_gosend" => $data->no_order_gosend
              ]
            );
          }else {
            log_message('debug', 'Response GoD Cancel dari core (Admin Cabang)'.json_encode($tolakPost));
            $this->set_response([
              'status' => 'error',
              'message' => '',
              'responseDesc' => $tolakPost->responseDesc,
              'data' => $tolakPost->data
            ]);
          }
      }
      $response = array(
        'status' => 'success',
        'message' => 'Notifikasi terkirim',
        'data' => [
          'kode_booking' => $kodeBooking,
          'taksiran_atas' => $data->taksiran_atas,
          'nominal_pengajuan' => $data->nominal_pengajuan_nasabah
        ]
      );
      $this->set_response($response, 200);
      return;
    }
  }

  function hitung_gadai_post()
  {
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');
    $this->form_validation->set_rules('perkiraan_harga', 'Perkiraan Harga', 'required|integer');

    if (!$this->form_validation->run()) {
      $this->response(array(
        'code' => 101,
        'status' => 'error',
        'message' => 'Invalid Input',
        'errors' => $this->form_validation->error_array()
      ), 200);
    } else {
      $jenisGadai = $this->post('jenis_gadai');
      $perkiraanHarga = $this->post('perkiraan_harga');

      $presentase = $this->GadaiModel->getPresentase($jenisGadai);

      // Hitung Gadai
      $batasAtas = $presentase[1] * $perkiraanHarga;
      $batasBawah = $presentase[0] * $perkiraanHarga;

      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => array(
          'taksiran_atas' => floor($batasAtas),
          'taksiran_bawah' => floor($batasBawah),
        )
      ), 200);
    }
  }

  function status_gosend_post()
  {
    $token = $this->getToken();
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');
    $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
    $this->form_validation->set_rules('order_gosend', 'No Order Gosend', 'required');
    if ($token) {
      $kodeBooking = $this->post('kode_booking');
      $jenisGadai = $this->post('jenis_gadai');
      $orderGosend = $this->post('order_gosend');
      if ($jenisGadai == 'perhiasan') {
        $status = $this->GadaiModel->getStatusPerhiasanByKodeBooking($kodeBooking);
      } else if ($jenisGadai == 'logam_mulia') {
        $status = $this->GadaiModel->getStatusLogamMuliaByKodeBooking($kodeBooking);
      }
      if ($orderGosend == '') {
        $this->set_response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'belum mendapatkan driver',
          'data' => array(
            'kode_booking' => $kodeBooking,
            'status' => $status->status
          )
        ), 400);
      } else {
        $user_AIID = $token->id;
        $status = $this->GadaiModel->simpanStatusGosend(
          $user_AIID,
          $kodeBooking,
          $jenisGadai,
          $orderGosend
        );

        $data = $this->db->select('kode_outlet, user_AIID, jenis_layanan, no_order_gosend')
          ->where('kode_booking', $kodeBooking)
          ->get('gadai_' . $jenisGadai)->row();
        $idNotif = $this->NotificationModel->addAdminNotificaion(
          $user_AIID,
          NotificationModel::TYPE_GADAI,
          NotificationModel::CONTENT_TYPE_TEXT,
          "Pegadaian Digital Service",
          "Nasabah memesan kurir",
          "",
          "",
          $data->jenis_layanan,
          $data->kode_outlet,
          $kodeBooking
        );

        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => array(
            'kode_booking' => $kodeBooking,
            'no_order_gosend' => $orderGosend
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function change_status_otp_post()
  {
    $token = $this->getToken();
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');
    $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
    $this->form_validation->set_rules('kode_otp', 'Kode OTP', 'required|numeric');
    if ($token) {
      $kodeBooking = $this->post('kode_booking');
      $jenisGadai = $this->post('jenis_gadai');
      $kodeOtp = $this->post('kode_otp');
      $user_AIID = $token->id;
      if ($jenisGadai == 'perhiasan') {
        $status = $this->GadaiModel->getStatusPerhiasanByKodeBooking($kodeBooking);
      } else if ($jenisGadai == 'logam_mulia') {
        $status = $this->GadaiModel->getStatusLogamMuliaByKodeBooking($kodeBooking);
      }
      if ($kodeOtp == '') {
        $this->set_response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'otp masih kosong',
          'data' => array(
            'kode_booking' => $kodeBooking,
            'status' => $status->status
          )
        ), 400);
      } else {
        $status = $this->GadaiModel->simpanStatusOtp(
          $user_AIID,
          $kodeBooking,
          $jenisGadai,
          $kodeOtp
        );

        $data = $this->db->select('kode_outlet, user_AIID, jenis_layanan, no_order_gosend')
          ->where('kode_booking', $kodeBooking)
          ->get('gadai_' . $jenisGadai)->row();
        $idNotif = $this->NotificationModel->addAdminNotificaion(
          $data->user_AIID,
          NotificationModel::TYPE_GADAI,
          NotificationModel::CONTENT_TYPE_TEXT,
          "Pegadaian Digital Service",
          "Nasabah telah melakukan verifikasi otp",
          "",
          "",
          $data->jenis_layanan,
          $data->kode_outlet,
          $kodeBooking
        );

        $this->set_response(array(
          'status' => 'success',
          'message' => 'OTP telah di verifikasi ',
          'data' => array(
            'kode_otp' => $kodeOtp,
            'kode_booking' => $kodeBooking
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function un_completed_logam_mulia_get()
  {
    $token = $this->getToken();
    if ($token) {
      $user_AIID = $token->id;
      $data = $this->GadaiModel->getDataGadaiUnCompletedLogamMulia($user_AIID);
      if ($data->num_rows() > 0) {
        if ($data->row()->status == 1) {
          $screen_name = 'PickupAndDeliveryServiceLM';
        } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 0) {
          $screen_name = 'LookingDriverLM';
        } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 1) {
          $screen_name = 'SerahTerimaBarangLM';
        } elseif ($data->row()->status == 6) {
          $screen_name = 'DetailTransaksiAfterGopayLM';
        } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 1) {
          $screen_name = 'PengajuanDisetujuiLM';
        } elseif ($data->row()->status == 8) {
          $screen_name = 'LookingDriverLM';
        } elseif ($data->row()->status == 9) {
          $screen_name = 'DetailTransaksiAfterGopayLM';
        } elseif ($data->row()->status == 92) {
          $screen_name = 'PengembalianLM';
        } elseif ($data->row()->status == 94) {
          $screen_name = 'PengembalianLM';
        } elseif ($data->row()->status == 0) {
          $screen_name = 'PengembalianLM';
        } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 2 && $data->row()->otp == 0) {
          $screen_name = 'VerifyOTPLM';
        } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 2 && $data->row()->otp == 1) {
          $screen_name = 'PilihNoRekeningPenerimaLM';
        }
        $transaksi = TRUE;

        $data->row()->lokasi_nasabah = json_decode($data->row()->lokasi_nasabah);
        $keping = json_decode($data->row()->keping_json);
        $jumlah_keping = json_decode($data->row()->jumlah_keping);
        $harga_satuan = json_decode($data->row()->harga_satuan_keping);
        $totalNilai = 0;
        $result = array();
        for ($i = 0; $i < count($keping); $i++) {
          $LogamMulia['berat_keping'] = $keping[$i]->berat_keping;
          $LogamMulia['jumlah_keping'] = $jumlah_keping[$i]->jumlah_keping;
          if (empty($harga_satuan[$i]->harga_satuan)) {
            $LogamMulia['hargaSatuan'] = '';
          } else {
            $LogamMulia['hargaSatuan'] = (int)$harga_satuan[$i]->harga_satuan;
          }
          $totalNilai += $harga_satuan[$i]->harga_satuan;
          $result[] = $LogamMulia;
        }
        $data->row()->dataLogamMulia = $result;
        $data->row()->totalNilai = $totalNilai;
        $pickupDestination = array(
          'address' => $data->row()->alamat,
          'latitude' => $data->row()->latitude,
          'longitude' => $data->row()->longitude,
          'jarak' => $data->row()->jarak_to_outlet,
          'kodeOutlet' => $data->row()->kode_outlet,
          'nama' => $data->row()->nama_outlet,
          'phone' => $data->row()->telepon
        );
        $data->row()->pickupDestination = $pickupDestination;

        $this->set_response(array(
          'status' => 'success',
          'message' => ' ',
          'uncompleted' => true,
          'data' => $data->row(),
          'screen_name' => $screen_name,
          'no_order_gosend' => $data->row()->no_order_gosend
        ), 200);
      } else {
        $this->set_response(array(
          'status' => 'success',
          'message' => ' ',
          'uncompleted' => false
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function un_completed_logam_mulia_by_kode_booking_get()
  {
    $token = $this->getToken();
    if ($token) {

      $user_AIID = $token->id;
      //get kode booking
      $getBookingCode = $this->db->select('kode_booking')
        ->where('user_AIID', $user_AIID)
        ->like('kode_booking', '2', 'after')
        ->order_by('kode_booking', 'desc')
        ->limit(1)
        ->get('gadai_logam_mulia')->row();
      if (isset($getBookingCode->kode_booking)) {
        $data = $this->GadaiModel->getDataGadaiUnCompletedLogamMuliaByKodeBooking($getBookingCode->kode_booking);
        if ($data->num_rows() > 0) {
          if ($data->row()->status == 1) {
            $screen_name = 'PickupAndDeliveryServiceLM';
          } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 0) {
            $screen_name = 'LookingDriverLM';
          } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 1) {
            $screen_name = 'SerahTerimaBarangLM';
          } elseif ($data->row()->status == 6) {
            $screen_name = 'DetailTransaksiAfterGopayLM';
          } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 1) {
            $screen_name = 'PengajuanDisetujuiLM';
          } elseif ($data->row()->status == 8) {
            $screen_name = 'LookingDriverLM';
          } elseif ($data->row()->status == 9) {
            $screen_name = 'DetailTransaksiAfterGopayLM';
          } elseif ($data->row()->status == 92) {
            $screen_name = 'PengembalianLM';
          } elseif ($data->row()->status == 94) {
            $screen_name = 'PengembalianLM';
          } elseif ($data->row()->status == 0) {
            $screen_name = 'PengembalianLM';
          } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 2 && $data->row()->otp == 0) {
            $screen_name = 'VerifyOTPLM';
          } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 2 && $data->row()->otp == 1) {
            $screen_name = 'PilihNoRekeningPenerimaLM';
          }
          $transaksi = TRUE;

          $data->row()->lokasi_nasabah = json_decode($data->row()->lokasi_nasabah);
          $keping = json_decode($data->row()->keping_json);
          $jumlah_keping = json_decode($data->row()->jumlah_keping);
          $harga_satuan = json_decode($data->row()->harga_satuan_keping);
          $totalNilai = 0;
          $result = array();
          for ($i = 0; $i < count($keping); $i++) {
            $LogamMulia['berat_keping'] = $keping[$i]->berat_keping;
            $LogamMulia['jumlah_keping'] = $jumlah_keping[$i]->jumlah_keping;
            if (empty($harga_satuan[$i]->harga_satuan)) {
              $LogamMulia['hargaSatuan'] = '';
            } else {
              $LogamMulia['hargaSatuan'] = (int)$harga_satuan[$i]->harga_satuan;
            }
            $totalNilai += $harga_satuan[$i]->harga_satuan;
            $result[] = $LogamMulia;
          }
          $data->row()->dataLogamMulia = $result;
          $data->row()->totalNilai = $totalNilai;
          $pickupDestination = array(
            'address' => $data->row()->alamat,
            'latitude' => $data->row()->latitude,
            'longitude' => $data->row()->longitude,
            'jarak' => $data->row()->jarak_to_outlet,
            'kodeOutlet' => $data->row()->kode_outlet,
            'nama' => $data->row()->nama_outlet,
            'phone' => $data->row()->telepon
          );
          $data->row()->pickupDestination = $pickupDestination;

          $this->set_response(array(
            'status' => 'success',
            'message' => 'Data is not complete',
            'uncompleted' => true,
            'data' => $data->row(),
            'screen_name' => $screen_name,
            'no_order_gosend' => $data->row()->no_order_gosend
          ), 200);
        } else {
          $this->set_response(array(
            'status' => 'success',
            'message' => 'There is no data uncompleted',
            'uncompleted' => false
          ), 200);
        }
      } else {
        $this->set_response(array(
          'status' => 'success',
          'message' => 'There is no data uncompleted',
          'uncompleted' => false
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function logam_mulia_by_kode_booking_get()
  {
    $token = $this->getToken();
    if ($token) {
      $user_AIID = $token->id;
      $kodebooking = $this->uri->segment(3);
      if (isset($kodebooking)) {
        $data = $this->GadaiModel->getByKodeBookingLogamMulia($kodebooking);
        $screen_name = '';
        if (isset($data)) {
          if ($data->status == 1) {
            $screen_name = 'PickupAndDeliveryServiceLM';
          } elseif ($data->status == 2) {
            $screen_name = 'DetailGadaiLM';
          } elseif ($data->status == 4 || $data->status == 0) {
            $screen_name = 'PengambilanBarangLM';
          } elseif ($data->status == 5 && $data->status_fcm == 0) {
            $screen_name = 'LookingDriverLM';
          } elseif ($data->status == 5 && $data->status_fcm == 1) {
            $screen_name = 'SerahTerimaBarangLM';
          } elseif ($data->status == 6) {
            $screen_name = 'DetailTransaksiAfterGopayLM';
          } elseif ($data->status == 7 && $data->respon_nasabah == 1) {
            $screen_name = 'PengajuanDisetujuiLM';
          } elseif ($data->status == 8) {
            $screen_name = 'LookingDriverLM';
          } elseif ($data->status == 9) {
            $screen_name = 'DetailTransaksiAfterGopayLM';
          } elseif ($data->status == 92) {
            $screen_name = 'PengambilanBarangLM';
          } elseif ($data->status == 94) {
            $screen_name = 'PengambilanBarangLM';
          } elseif ($data->status == 7 && $data->respon_nasabah == 2 && $data->otp == 0) {
            $screen_name = 'VerifyOTPLM';
          } elseif ($data->status == 7 && $data->respon_nasabah == 2 && $data->otp == 1) {
            $screen_name = 'PilihNoRekeningPenerimaLM';
          }
          $transaksi = TRUE;
          // print_r($data);
          $data->lokasi_nasabah = json_decode($data->lokasi_nasabah);
          $keping = json_decode($data->keping_json);
          $jumlah_keping = json_decode($data->jumlah_keping);
          $harga_satuan = json_decode($data->harga_satuan_keping);
          $totalNilai = 0;
          $result = array();
          for ($i = 0; $i < count($keping); $i++) {
            $LogamMulia['berat_keping'] = $keping[$i]->berat_keping;
            $LogamMulia['jumlah_keping'] = $jumlah_keping[$i]->jumlah_keping;
            if (empty($harga_satuan[$i]->harga_satuan)) {
              $LogamMulia['hargaSatuan'] = '';
            } else {
              $LogamMulia['hargaSatuan'] = (int)$harga_satuan[$i]->harga_satuan;
            }
            $totalNilai += $harga_satuan[$i]->harga_satuan;
            $result[] = $LogamMulia;
          }
          $data->dataLogamMulia = $result;
          $data->totalNilai = $totalNilai;
          $pickupDestination = array(
            'address' => $data->alamat,
            'latitude' => $data->latitude,
            'longitude' => $data->longitude,
            'jarak' => $data->jarak_to_outlet,
            'kodeOutlet' => $data->kode_outlet,
            'nama' => $data->nama_outlet,
            'phone' => $data->telepon
          );
          $data->pickupDestination = $pickupDestination;


          $this->set_response(array(
            'status' => 'success',
            'message' => 'Data is not complete',
            'uncompleted' => true,
            'data' => $data,
            'screen_name' => $screen_name,
            'no_order_gosend' => $data->no_order_gosend
          ), 200);
        } else {
          $this->set_response(array(
            'status' => 'success',
            'message' => 'There is no data uncompleted',
            'uncompleted' => false
          ), 200);
        }
      } else {
        $this->set_response(array(
          'status' => 'success',
          'message' => 'There is no data uncompleted',
          'uncompleted' => false
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function un_completed_perhiasan_get()
  {
    $token = $this->getToken();
    if ($token) {
      $user_AIID = $token->id;
      $data = $this->GadaiModel->getDataGadaiUnCompletedPerhiasan($user_AIID);
      if ($data->num_rows() > 0) {
        if ($data->row()->status == 1) {
          $screen_name = 'PickupAndDeliveryService';
        } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 0) {
          $screen_name = 'LookingDriver';
        } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 1) {
          $screen_name = 'SerahTerimaBarang';
        } elseif ($data->row()->status == 6) {
          $screen_name = 'DetailTransaksiAfterGopay';
        } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 1) {
          $screen_name = 'PengajuanDisetujui';
        } elseif ($data->row()->status == 8) {
          $screen_name = 'LookingDriver';
        } elseif ($data->row()->status == 9) {
          $screen_name = 'DetailTransaksiAfterGopay';
        } elseif ($data->row()->status == 92) {
          $screen_name = 'Pengembalian';
        } elseif ($data->row()->status == 94) {
          $screen_name = 'Pengembalian';
        } elseif ($data->row()->status == 0) {
          $screen_name = 'Pengembalian';
        } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 2 && $data->row()->otp == 0) {
          $screen_name = 'VerifyOTP';
        } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 2 && $data->row()->otp == 1) {
          $screen_name = 'PilihNoRekeningPenerima';
        }
        $data->row()->lokasi_nasabah = json_decode($data->row()->lokasi_nasabah);

        //begin list item perhiasan
        $jenis_perhiasan_god = json_decode($data->row()->jenis_perhiasan_god);
        $kadar_emas_god = json_decode($data->row()->kadar_emas_god);
        $berat_bersih = json_decode($data->row()->berat_bersih_god);
        $catatan = json_decode($data->row()->catatan);
        $harga_satuan = json_decode($data->row()->harga_satuan_keping);
        $totalNilai = 0;

        $perhiasan = array();
        $result = array();
        for ($i = 0; $i < count($jenis_perhiasan_god); $i++) {
          $perhiasan['kadar'] = $kadar_emas_god[$i]->kadar;
          $perhiasan['berat_bersih'] = $berat_bersih[$i]->beratBersih;
          $perhiasan['jenis_perhiasan'] = $jenis_perhiasan_god[$i]->jenis_perhiasan;
          $perhiasan['catatan'] = $catatan[$i]->catatan;
          $perhiasan['hargaSatuan'] = (int)$harga_satuan[$i]->hargaSatuan;
          $totalNilai += $harga_satuan[$i]->hargaSatuan;
          $result[] = $perhiasan;
        }

        $data->row()->listItem = $result;
        $data->row()->totalNilai = $totalNilai;
        //end list item perhiasan

        $pickupDestination = array(
          'address' => $data->row()->alamat,
          'latitude' => $data->row()->latitude,
          'longitude' => $data->row()->longitude,
          'jarak' => $data->row()->jarak_to_outlet,
          'kodeOutlet' => $data->row()->kode_outlet,
          'nama' => $data->row()->nama_outlet,
          'phone' => $data->row()->telepon
        );
        $data->row()->pickupDestination = $pickupDestination;


        $this->set_response(array(
          'status' => 'success',
          'message' => 'Data is not complete',
          'uncompleted' => true,
          'data' => $data->row(),
          'screen_name' => $screen_name,
          'no_order_gosend' => $data->row()->no_order_gosend
        ), 200);
      } else {
        $this->set_response(array(
          'status' => 'success',
          'message' => 'There is no data uncompleted',
          'uncompleted' => false
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  //un_completed perhiasan by kode booking
  function un_completed_perhiasan_by_kode_booking_get()
  {
    $token = $this->getToken();
    if ($token) {
      $user_AIID = $token->id;
      //get kode booking
      $getBookingCode = $this->db->select('kode_booking')
        ->where('user_AIID', $user_AIID)
        ->like('kode_booking', '1', 'after')
        ->order_by('kode_booking', 'desc')
        ->limit(1)
        ->get('gadai_perhiasan')->row();
      if (isset($getBookingCode->kode_booking)) {
        $data = $this->GadaiModel->getDataGadaiUnCompletedPerhiasanByKodeBooking($getBookingCode->kode_booking);
        if ($data->num_rows() > 0) {
          if ($data->row()->status == 1) {
            $screen_name = 'PickupAndDeliveryService';
          } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 0) {
            $screen_name = 'LookingDriver';
          } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 1) {
            $screen_name = 'SerahTerimaBarang';
          } elseif ($data->row()->status == 6) {
            $screen_name = 'DetailTransaksiAfterGopay';
          } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 1) {
            $screen_name = 'PengajuanDisetujui';
          } elseif ($data->row()->status == 8) {
            $screen_name = 'LookingDriver';
          } elseif ($data->row()->status == 9) {
            $screen_name = 'DetailTransaksiAfterGopay';
          } elseif ($data->row()->status == 92) {
            $screen_name = 'Pengembalian';
          } elseif ($data->row()->status == 94) {
            $screen_name = 'Pengembalian';
          } elseif ($data->row()->status == 0) {
            $screen_name = 'PengembalianLM';
          } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 2 && $data->row()->otp == 0) {
            $screen_name = 'VerifyOTP';
          } elseif ($data->row()->status == 7 && $data->row()->respon_nasabah == 2 && $data->row()->otp == 1) {
            $screen_name = 'PilihNoRekeningPenerima';
          }
          $transaksi = TRUE;
          $data->row()->lokasi_nasabah = json_decode($data->row()->lokasi_nasabah);

          //begin list item perhiasan
          $jenis_perhiasan_god = json_decode($data->row()->jenis_perhiasan_god);
          $kadar_emas_god = json_decode($data->row()->kadar_emas_god);
          $berat_bersih = json_decode($data->row()->berat_bersih_god);
          $catatan = json_decode($data->row()->catatan);
          $harga_satuan = json_decode($data->row()->harga_satuan_keping);
          $totalNilai = 0;

          $perhiasan = array();
          $result = array();
          for ($i = 0; $i < count($jenis_perhiasan_god); $i++) {
            $perhiasan['kadar'] = $kadar_emas_god[$i]->kadar;
            $perhiasan['berat_bersih'] = $berat_bersih[$i]->beratBersih;
            $perhiasan['jenis_perhiasan'] = $jenis_perhiasan_god[$i]->jenis_perhiasan;
            $perhiasan['catatan'] = $catatan[$i]->catatan;
            $perhiasan['hargaSatuan'] = (int)$harga_satuan[$i]->hargaSatuan;
            $totalNilai += $harga_satuan[$i]->hargaSatuan;
            $result[] = $perhiasan;
          }

          $data->row()->listItem = $result;
          $data->row()->totalNilai = $totalNilai;
          //end list item perhiasan

          $pickupDestination = array(
            'address' => $data->row()->alamat,
            'latitude' => $data->row()->latitude,
            'longitude' => $data->row()->longitude,
            'jarak' => $data->row()->jarak_to_outlet,
            'kodeOutlet' => $data->row()->kode_outlet,
            'nama' => $data->row()->nama_outlet,
            'phone' => $data->row()->telepon
          );
          $data->row()->pickupDestination = $pickupDestination;


          $this->set_response(array(
            'status' => 'success',
            'message' => 'Data is not complete',
            'uncompleted' => true,
            'data' => $data->row(),
            'screen_name' => $screen_name,
            'no_order_gosend' => $data->row()->no_order_gosend
          ), 201);
        } else {
          $this->set_response(array(
            'status' => 'success',
            'message' => 'There is no data uncompleted',
            'uncompleted' => false
          ), 200);
        }
      } else {
        $this->set_response(array(
          'status' => 'success',
          'message' => 'There is no data uncompleted',
          'uncompleted' => false
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function perhiasan_by_kode_booking_get()
  {
    $token = $this->getToken();
    if ($token) {
      $user_AIID = $token->id;
      $kodebooking = $this->uri->segment(3);
      if (isset($kodebooking)) {

        $data = $this->GadaiModel->getByKodeBookingPerhiasan($kodebooking);
        if (isset($data)) {
          $screen_name = '';
          if ($data->status == 1) {
            $screen_name = 'PickupAndDeliveryService';
          } elseif ($data->status == 2) {
            $screen_name = 'DetailGadai';
          } elseif ($data->status == 4 || $data->status == 0) {
            $screen_name = 'PengambilanBarang';
          } elseif ($data->status == 5 && $data->status_fcm == 0) {
            $screen_name = 'LookingDriver';
          } elseif ($data->status == 5 && $data->status_fcm == 1) {
            $screen_name = 'SerahTerimaBarang';
          } elseif ($data->status == 6) {
            $screen_name = 'DetailTransaksiAfterGopay';
          } elseif ($data->status == 7 && $data->respon_nasabah == 1) {
            $screen_name = 'PengajuanDisetujui';
          } elseif ($data->status == 8) {
            $screen_name = 'LookingDriver';
          } elseif ($data->status == 9) {
            $screen_name = 'DetailTransaksiAfterGopay';
          } elseif ($data->status == 92) {
            $screen_name = 'PengambilanBarang';
          } elseif ($data->status == 94) {
            $screen_name = 'PengambilanBarang';
          } elseif ($data->status == 7 && $data->respon_nasabah == 2 && $data->otp == 0) {
            $screen_name = 'VerifyOTP';
          } elseif ($data->status == 7 && $data->respon_nasabah == 2 && $data->otp == 1) {
            $screen_name = 'PilihNoRekeningPenerima';
          }

          $transaksi = TRUE;
          $data->lokasi_nasabah = json_decode($data->lokasi_nasabah);

          //begin list item perhiasan
          $jenis_perhiasan_god = json_decode($data->jenis_perhiasan_god);
          $kadar_emas_god = json_decode($data->kadar_emas_god);
          $berat_bersih = json_decode($data->berat_bersih_god);
          $catatan = json_decode($data->catatan);
          $harga_satuan = json_decode($data->harga_satuan_keping);
          $totalNilai = 0;

          $perhiasan = array();
          $result = array();
          for ($i = 0; $i < count($jenis_perhiasan_god); $i++) {
            $perhiasan['kadar'] = $kadar_emas_god[$i]->kadar;
            $perhiasan['berat_bersih'] = $berat_bersih[$i]->beratBersih;
            $perhiasan['jenis_perhiasan'] = $jenis_perhiasan_god[$i]->jenis_perhiasan;
            $perhiasan['catatan'] = $catatan[$i]->catatan;
            $perhiasan['hargaSatuan'] = (int)$harga_satuan[$i]->hargaSatuan;
            $totalNilai += $harga_satuan[$i]->hargaSatuan;
            $result[] = $perhiasan;
          }

          $data->listItem = $result;
          $data->totalNilai = $totalNilai;
          //end list item perhiasan

          $pickupDestination = array(
            'address' => $data->alamat,
            'latitude' => $data->latitude,
            'longitude' => $data->longitude,
            'jarak' => $data->jarak_to_outlet,
            'kodeOutlet' => $data->kode_outlet,
            'nama' => $data->nama_outlet,
            'phone' => $data->telepon
          );
          $data->pickupDestination = $pickupDestination;


          $this->set_response(array(
            'status' => 'success',
            'message' => 'Data is not complete',
            'uncompleted' => true,
            'data' => $data,
            'screen_name' => $screen_name,
            'no_order_gosend' => $data->no_order_gosend
          ), 201);
        } else {
          $this->set_response(array(
            'status' => 'success',
            'message' => 'There is no data uncompleted',
            'uncompleted' => false
          ), 200);
        }
      } else {
        $this->set_response(array(
          'status' => 'success',
          'message' => 'There is no data uncompleted',
          'uncompleted' => false
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function cancel_session_pay_post()
  {
    $token = $this->getToken();
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');
    $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
    if ($token) {
      $kodeBooking = $this->post('kode_booking');
      $jenisGadai = $this->post('jenis_gadai');
      if ($kodeBooking == "" || $jenisGadai == "") {
        $this->set_response(array(
          'status' => 'error',
          'message' => 'Data kosong'
        ), 406);
      } else {
        $status = $this->GadaiModel->simpanStatusSessionPay($kodeBooking, $jenisGadai);
        $this->set_response(array(
          'status' => 'success',
          'message' => 'Sesi anda telah berakhir ',
          'data' => array(
            'kode_booking' => $kodeBooking
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function cancel_timeout_post()
  {
    $token = $this->getToken();
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');
    $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
    if ($token) {
      $kodeBooking = $this->post('kode_booking');
      $jenisGadai = $this->post('jenis_gadai');
      if ($kodeBooking == "" || $jenisGadai == "") {
        $this->set_response(array(
          'status' => 'error',
          'message' => 'Data kosong'
        ), 406);
      } else {
        $status = $this->GadaiModel->simpanStatusTimeout($kodeBooking, $jenisGadai);
        $this->set_response(array(
          'status' => 'success',
          'message' => 'Sesi anda telah berakhir ',
          'data' => array(
            'kode_booking' => $kodeBooking
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  //add syukron
  function update_status_gopay_logam_mulia_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'kode_booking' =>  $this->post('kode_booking'),
        'amount' =>  $this->post('amount'),
        'jenis_pickup' =>  $this->post('jenis_pickup'),
        'id_order_midtrans' => $this->post('id_order_midtrans')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
      $this->form_validation->set_rules('amount', 'Amount', 'required|numeric');
      $this->form_validation->set_rules('jenis_pickup', 'Jenis Pickup', 'required|numeric');
      $this->form_validation->set_rules('id_order_midtrans', 'ID Order Midtrans', 'required');

      $kodeBooking = $this->post('kode_booking');
      $amount      = $this->post('amount');
      $jenisPickup = $this->post('jenis_pickup');
      $idOrderMidtrans = $this->post('id_order_midtrans');
      $user_AIID = $token->id;

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $this->session->item = ['kode_booking' => $kodeBooking];
      }
      $status = $this->GadaiModel->getStatusLogamMuliaByKodeBooking($kodeBooking);
      if ($status->status  == 8) {
        $this->set_response(array(
          'status' => 'error',
          'message' => 'Gopay Sudah dibayar Sebelumnya',
          'data' => false
        ), 400);
      } else {
        $this->GadaiModel->simpanStatusGopayLogamMulia(
          $user_AIID,
          $kodeBooking,
          $amount,
          $jenisPickup,
          $idOrderMidtrans
        );
        log_message('debug', 'simpan status gopay logam mulia: '.$kodeBooking);
        $detailGadai = $this->GadaiModel->getDetailGadaiByKodeBooking($kodeBooking);
        $user = $this->User->getUser($user_AIID);


        $templateData = array(
          'noPengajuan' => $kodeBooking,
          'detailGadai' => $detailGadai
        );

        $content = $this->load->view('notification/detail_transaksi_gopay', $templateData, TRUE);

        $mobile = $this->load->view('notification/top_template', array('title' => "Pembayaran Kurir Pickup Delivery Berhasil"), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        $idNotif = $this->NotificationModel->add(
          $user_AIID,
          NotificationModel::TYPE_GADAI,
          NotificationModel::CONTENT_TYPE_HTML,
          "Pembayaran Gopay Berhasil",
          "Kode Booking " . $kodeBooking,
          $mobile,
          $content,
          "GOD"
        );


        Message::sendFCMNotif(
          $this->User->getFCMToken($user_AIID),
          [
            "id" => $idNotif,
            "tipe" => "LookingDriverLM",
            "title" => "Pembayaran Gopay Berhasil #" . $kodeBooking . "#1",
            "tagline" => "Kode Booking " . $kodeBooking,
            "content" => '',
            "action_url" => 'LookingDriverLM',
            "token" => $token->no_hp
          ]
        );


        $this->set_response(array(
          'status' => 'success',
          'payment' => true,
          'message' => 'Gopay telah di bayar dengan Kode Booking ' . $kodeBooking . ' ',
          'data' => true
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function update_link_payment_logam_mulia_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'kode_booking' =>  $this->post('kode_booking'),
        'payment_transaction_id' =>  $this->post('payment_transaction_id'),
        'payment_link' =>  $this->post('payment_link')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
      $this->form_validation->set_rules('payment_transaction_id', 'Payment Transaction Id');
      $this->form_validation->set_rules('payment_link', 'Payment Link');

      $kodeBooking = $this->post('kode_booking');
      $payment_transaction_id = $this->post('payment_transaction_id');
      $payment_link = $this->post('payment_link');
      $user_AIID = $token->id;

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $this->GadaiModel->simpanLinkPaymentLogamMulia(
          $kodeBooking,
          $payment_transaction_id,
          $payment_link
        );

        $this->set_response(array(
          'status' => 'success',
          'payment' => true,
          'message' => 'Link payment berhasil disimpan ' . $kodeBooking . ' ',
          'data' => true
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function update_link_payment_perhiasan_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'kode_booking' =>  $this->post('kode_booking'),
        'payment_transaction_id' =>  $this->post('payment_transaction_id'),
        'payment_link' =>  $this->post('payment_link')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
      $this->form_validation->set_rules('payment_transaction_id', 'Payment Transaction Id');
      $this->form_validation->set_rules('payment_link', 'Payment Link');

      $kodeBooking = $this->post('kode_booking');
      $payment_transaction_id = $this->post('payment_transaction_id');
      $payment_link = $this->post('payment_link');
      $user_AIID = $token->id;

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $this->GadaiModel->simpanLinkPaymentPerhiasan(
          $kodeBooking,
          $payment_transaction_id,
          $payment_link
        );

        $this->set_response(array(
          'status' => 'success',
          'payment' => true,
          'message' => 'Link payment berhasil disimpan ' . $kodeBooking . ' ',
          'data' => true
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function update_status_gopay_perhiasan_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'kode_booking' =>  $this->post('kode_booking'),
        'amount' =>  $this->post('amount'),
        'jenis_pickup' =>  $this->post('jenis_pickup'),
        'id_order_midtrans' => $this->post('id_order_midtrans')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required|integer');
      $this->form_validation->set_rules('amount', 'Amount', 'required|numeric');
      $this->form_validation->set_rules('jenis_pickup', 'Jenis Pickup', 'required|numeric');
      $this->form_validation->set_rules('id_order_midtrans', 'ID Order Midtrans', 'required');

      $kodeBooking = $this->post('kode_booking');
      $amount      = $this->post('amount');
      $jenisPickup = $this->post('jenis_pickup');
      $idOrderMidtrans = $this->post('id_order_midtrans');
      $user_AIID = $token->id;

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $this->session->item = ['kode_booking' => $kodeBooking];
      }
      $status = $this->GadaiModel->getStatusPerhiasanByKodeBooking($kodeBooking);
      if ($status->status  == 8) {
        $this->set_response(array(
          'status' => 'error',
          'message' => 'Gopay Sudah dibayar Sebelumnya',
          'data' => false
        ), 400);
      } else {
        $this->GadaiModel->simpanStatusGopayPerhiasan(
          $user_AIID,
          $kodeBooking,
          $amount,
          $jenisPickup,
          $idOrderMidtrans
        );
        log_message('debug', 'simpan status gopay perhiasan: '.$kodeBooking);
        $user = $this->User->getUser($user_AIID);

        $detailGadai = $this->GadaiModel->getDetailGadaiByKodeBooking($kodeBooking);

        $templateData = array(
          'noPengajuan' => $kodeBooking,
          'detailGadai' => $detailGadai
        );

        $content = $this->load->view('notification/detail_transaksi_gopay', $templateData, TRUE);

        $mobile = $this->load->view('notification/top_template', array('title' => "Gopay"), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        $idNotif = $this->NotificationModel->add(
          $user_AIID,
          NotificationModel::TYPE_GADAI,
          NotificationModel::CONTENT_TYPE_HTML,
          "Pembayaran Gopay Berhasil",
          "Kode Booking " . $kodeBooking,
          $mobile,
          $content,
          "GOD"
        );

        Message::sendFCMNotif(
          $this->User->getFCMToken($user_AIID),
          [
            "id" => $idNotif,
            "tipe" => "LookingDriver",
            "title" => "Pembayaran Gopay Berhasil #" . $kodeBooking . "#2",
            "tagline" => "Kode Booking " . $kodeBooking,
            "content" => '',
            "action_url" => 'LookingDriver',
            "token" => $token->no_hp
          ]
        );

        $this->set_response(
          array(
            'status' => 'success',
            'payment' => true,
            'message' => 'Gopay telah di bayar dengan Kode Booking ' . $kodeBooking . ' ',
            'data' => true
          ),
          200
        );
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function detail_kontrak_post()
  {
    $token = $this->getToken();
    // check token validity
    if  (!$token) {
      $this->errorUnAuthorized();
      return;
    }

    // set request data and rules
    $reqData = array(
      'kode_booking' => $this->post('kode_booking'),
      'jenis_gadai' => $this->post('jenis_gadai'),
    );

    $this->form_validation->set_data($reqData);
    $this->form_validation->set_rules('kode_booking', 'kode_booking', 'required|integer');
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');

    // check validity request data
    if (!$this->form_validation->run()) {
      $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Invalid Input',
        'errors' => $this->form_validation->error_array()
      ];
      $this->response($response, 200);
      log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
      return;
    }

    // get detail gadai
    $detailGadai = $this->GadaiModel->getDetailKontrakGod($reqData['kode_booking'], $reqData['jenis_gadai']);

    // if detail gadai not exist
    if (!$detailGadai) {
      $response = [
        'code' => 99,
        'status' => 'error',
        'message' => 'Detail gadai tidak tersedia!',
      ];
      $this->response($response, 200);
      log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
      return;
    }

    $checkPayment = $this->GadaiModel->getPaymentGadaiByKodeBooking($reqData['kode_booking']);

    // generate gada qr code image
    include "phpqrcode/qrlib.php";
    $qr_img_dir = "uploaded/user/gadai/";

    if (!file_exists($qr_img_dir))
      mkdir($qr_img_dir);

    // set qr code data
    $isi_teks = array(
      'namaNasabah' => $detailGadai->nasabah,
      'cif' => $detailGadai->cif,
      'no_ktp' => $detailGadai->noIdentitas,
      'kodeOtp' => $detailGadai->kodeOtp,
      'tglBooking' => date('d-m-Y', strtotime($detailGadai->tglBooking))
    );

    $data = json_encode($isi_teks);
    $hash_kodeBooking = sha1($reqData['kode_booking']);
    $qr_img_file = $hash_kodeBooking . ".png";

    // save qrcode if not exist
    if (!file_exists($qr_img_dir . $qr_img_file)) {
      QRCode::png($data, $qr_img_dir . $qr_img_file, 'H', 5, 0);

      if ($reqData['jenis_gadai'] == '2') {
        $this->GadaiModel->simpanBarcode($token->id, $reqData['kode_booking'], $isi_teks, $qr_img_file);
      } else {
        $this->GadaiModel->simpanBarcodePerhiasan($token->id, $reqData['kode_booking'], $isi_teks, $qr_img_file);
      }
    }

    // set template data
    $templateData = array(
      'noPengajuan' => $reqData['kode_booking'],
      'barcode' => $qr_img_file,
      'tglLelang' => $detailGadai->tglLelang,
      'tglTransaksi' => $detailGadai->tglTransaksi,
      'administrasi' => $detailGadai->administrasi,
      'serialNumber' => $detailGadai->serialNumber,
      'sewaModal' => $detailGadai->sewaModal,
      'tglJatuhTempo' => $detailGadai->tglJatuhTempo,
      'biayaProsesLelang' => $detailGadai->biayaProsesLelang ?: '1',
      'biayaLelang' => $detailGadai->biayaLelang ?: '2',
      'nasabah' => $detailGadai->nasabah,
      'kodeNamaCabang' => $detailGadai->kodeNamaCabang,
      'biayaAdmBjpdl' => $detailGadai->biayaAdmBjpdl ?: '1',
      'biayaBjdplMax' => $detailGadai->biayaBjdplMax ?: '6',
      'tanggal_pembayaran' => $detailGadai->tanggal_pembayaran,
      'jumlahHariTarif' => $detailGadai->jumlahHariTarif,
      'taksir_ulang_admin' => $detailGadai->taksir_ulang_admin,
      'namaPetugas' => $detailGadai->nama,
      'noIdentitas' => $detailGadai->noIdentitas,
      'jalan' => $detailGadai->jalan,
      'rubrik' => $detailGadai->rubrik,
      'taksiran' => $detailGadai->taksiran,
      'up' => $detailGadai->up,
      'tglKredit' => $detailGadai->tglKredit,
      'cif' => $detailGadai->cif,
      'no_kredit' => $detailGadai->no_kredit,
      'nama_group' => $detailGadai->nama_group,
      'nama_cabang' => $detailGadai->nama_cabang,
      'namaPinca' => $detailGadai->namaPinca,
      'detailGadai' => $detailGadai,
      'checkPayment' => $checkPayment,
      'asuransi' => $detailGadai->asuransi,
      'diterimaNasabah' => $detailGadai->diterimaNasabah
    );

    // template for payment status
    if ($checkPayment->count > 0) {
      $content = $this->load->view('mail/lihat_detail_gadai', $templateData, TRUE);
    } else {
      $content = $this->load->view('mail/belum_pembayaran_detail_gadai', $templateData, TRUE);
    }

    // set add content for mobile
    $mobile = $this->load->view('notification/top_template', array('title' => "Detail Kontrak"), true);
    $mobile = $mobile . $content;
    $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

    // save notification
    $this->NotificationModel->add(
      $token->id,
      NotificationModel::TYPE_GADAI,
      NotificationModel::CONTENT_TYPE_HTML,
      "Detail Kontrak Berhasil",
      "Kode Booking " . $reqData['kode_booking'],
      $mobile,
      $content,
      "GOD"
    );

    // set response
    $response = [
      'status' => 'success',
      'message' => 'Lihat Detail Kontrak ',
      'data' => '',
      'html' => $content
    ];
    $this->set_response($response, 200);
    log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
  }

  function save_rekening_post()
  {
    $token = $this->getToken();
    if (!$token) {
      $this->errorUnAuthorized();
      return;
    }

    $setData = array(
      'nama' => $this->post('nama'),
      'jenis_identitas' => $this->post('jenis_identitas'),
      'no_identitas' => $this->post('no_identitas'),
      'tanggal_expired_identitas' => $this->post('tanggal_expired_identitas'),
      'tempat_lahir' => $this->post('tempat_lahir'),
      'tanggal_lahir' => $this->post('tanggal_lahir'),
      'no_hp' => $this->post('no_hp'),
      'jenis_kelamin' => $this->post('jenis_kelamin'),
      'status_kawin' => $this->post("status_kawin"),
      'kode_kelurahan' => $this->post("kode_kelurahan"),
      'jalan' => $this->post("jalan"),
      'ibu_kandung' => $this->post("ibu_kandung")
    );

    $this->form_validation->set_data($setData);
    $this->form_validation->set_rules('nama', 'Nama Nasabah', 'required');

    $this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required');
    $this->form_validation->set_rules('no_identitas', 'No Identitas', 'required');

    if ($this->post('jenis_identitas') == '12') {
      $this->form_validation->set_rules('tanggal_expired_identitas', 'tanggal_expired_identitas', 'required');
    }

    $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
    $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');

    $this->form_validation->set_rules('no_hp', 'No HP', 'required');

    $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
    $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required');

    $this->form_validation->set_rules('kode_kelurahan', 'Kode kelurahan', 'required');

    $this->form_validation->set_rules('jalan', 'Jalan', 'required');
    $this->form_validation->set_rules('ibu_kandung', 'Ibu Kandung', 'required');

    if ($this->form_validation->run() == FALSE) {
      $this->set_response(array(
        'status' => 'error',
        'code' => 201,
        'message' => '',
        'errros' => $this->form_validation->error_array()
      ), 200);
    } else {
      $nama = $this->post('nama');
      $jenis_identitas = $this->post('jenis_identitas');
      $no_identitas = $this->post('no_identitas');
      $tanggal_expired_identitas = $this->post('tanggal_expired_identitas');
      $tempat_lahir = $this->post('tempat_lahir');
      $tanggal_lahir = $this->post('tanggal_lahir');
      $no_hp = $this->post('no_hp');
      $jenis_kelamin = $this->post('jenis_kelamin');
      $status_kawin = $this->post("status_kawin");

      $kode_kelurahan = $this->post("kode_kelurahan");
      $jalan = $this->post("jalan");

      $ibu_kandung = $this->post("ibu_kandung");

      $namaFoto = '';
      // Cek jika ada data foto dari user
      if (!isset($_FILES['userfile']['name'])) {
        // get foto name from UserModel
        $namaFoto = $this->User->profile($token->id)->fotoKTP;

        if (!$namaFoto) {
          $this->response([
            'code' => 101,
            'status' => 'error',
            'message' => '',
            'errors' => 'Foto KTP Tidak boleh kosong'
          ], 200);
          return;
        }
      } else {

        // Update foto KTP User
        $uploadDir = $this->config->item('upload_dir');

        $config = [];
        $config['upload_path'] = $uploadDir .  '/user/ktp';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        $uploadData = null;
        if (!$this->upload->do_upload('userfile')) {
          $this->response([
            'code' => 101,
            'status' => 'error',
            'message' => '',
            'errors' => $this->upload->display_errors()
          ], 200);
          return;
        } else {
          //Delete previous user file
          $prevFile = $this->User->profile($token->id)->fotoKTP;

          if ($prevFile != null && file_exists($uploadDir . '/user/ktp/' . $prevFile)) {
            unlink($uploadDir . '/user/ktp/' . $prevFile);
          }

          $uploadData = $this->upload->data();
          $namaFoto = $uploadData['file_name'];
        }
      }

      //Save tabungan emas dan update data user
      $idRekening = $this->GadaiModel->saveRekening(
        $token->id,
        $nama,
        $jenis_identitas,
        $no_identitas,
        $tanggal_expired_identitas,
        $tempat_lahir,
        $tanggal_lahir,
        $no_hp,
        $jenis_kelamin,
        $status_kawin,
        $kode_kelurahan,
        $jalan,
        $ibu_kandung,
        $namaFoto
      );

      $this->set_response(array(
        'status' => 'success',
        'message' => 'Rekening Berhasil Dibuat',
        'data' => array(
          'id' => $idRekening
        )
      ), 200);
    }
  }

  function perhiasan_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'jenis_perhiasan' => $this->post('jenis_perhiasan'),
        'kadar' => $this->post('kadar'),
        'berat_bersih' => $this->post('berat_bersih'),
        'berat_kotor' => $this->post('berat_kotor'),
        'kode_outlet' => $this->post('kode_outlet'),
        'waktu_kedatangan' => $this->post('waktu_kedatangan'),
        'tanggal_kedatangan' => $this->post('tanggal_kedatangan'),
        'taksiran_atas' => $this->post('taksiran_atas'),
        'taksiran_bawah' => $this->post('taksiran_bawah'),
        'jenisPromo' => $this->post('jenisPromo')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('jenis_perhiasan', 'Jenis Perhiasan', 'required');
      $this->form_validation->set_rules('kadar', 'Kadar Emas', 'required|numeric');
      $this->form_validation->set_rules('berat_bersih', 'Berat Bersih', 'required|numeric');
      $this->form_validation->set_rules('berat_kotor', 'Berat Kotor', 'required|numeric');
      $this->form_validation->set_rules('kode_outlet', 'Kode Outlet', 'required');
      $this->form_validation->set_rules('waktu_kedatangan', 'Waktu Kedatangan', 'required');
      $this->form_validation->set_rules('tanggal_kedatangan', 'Tanggal Kedatangan', 'required');
      $this->form_validation->set_rules('taksiran_atas', 'taksiran_atas', 'required|integer');
      $this->form_validation->set_rules('taksiran_bawah', 'taksiran_bawah', 'required|integer');

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $jenisPerhiasan = $this->post('jenis_perhiasan');
        $kadar = $this->post('kadar');
        $beratKotor = $this->post('berat_kotor');
        $beratBersih = $this->post('berat_bersih');
        $kodeOutlet = $this->post('kode_outlet');
        $waktuKedatangan = $this->post('waktu_kedatangan');
        $tanggalKedatangan = $this->post('tanggal_kedatangan');
        $taksiranAtas = $this->post('taksiran_atas');
        $taksiranBawah = $this->post('taksiran_bawah');
        $jenisPromo = $this->post('jenis_promo');

        $foto = array('foto' => array());
        if (isset($_FILES['foto'])) {
          $foto = $this->uploadFoto();

          if (count($foto['error']) > 0) {
            $this->response(array(
              'code' => 101,
              'nama' => $this->put('nama'),
              'status' => 'error',
              'message' => 'Internal Server Error',
              'errors' => $foto['error']
            ), 200);
            return;
          }
        }

        // Format date to unixtimestamp
        $waktuDatang = $tanggalKedatangan . ' ' . $waktuKedatangan;
        $waktuDatang = strtotime($waktuDatang);

        $this->db->trans_start();

        $kodeBooking = $this->GadaiModel->generateBooking(self::Perhiasan, self::PerhiasanStartValue);

        $this->GadaiModel->saveGadaiPerhiasan(
          $token->id,
          $kodeBooking,
          $jenisPerhiasan,
          $kadar,
          $beratKotor,
          $beratBersih,
          $kodeOutlet,
          $waktuDatang,
          $foto['foto'],
          $taksiranAtas,
          $taksiranBawah,
          $jenisPromo
        );

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
          $this->dbTransError('Save Gadai Perhiasan');
        }

        $this->sendEmail($token->id, $token->nama, $token->email, $kodeBooking, $kodeOutlet, $waktuDatang, $token->no_hp);

        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => array(
            'kodeBooking' => $kodeBooking
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function logam_mulia_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'berat_keping', $this->post('berat_keping'),
        'kode_outlet' => $this->post('kode_outlet'),
        'waktu_kedatangan' => $this->post('waktu_kedatangan'),
        'tanggal_kedatangan' => $this->post('tanggal_kedatangan'),
        'taksiran_atas' => $this->post('taksiran_atas'),
        'taksiran_bawah' => $this->post('taksiran_bawah'),
        'jenisPromo' => $this->post('jenisPromo')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('berat_keping[]', 'Berat Keping', 'require|decimal');
      $this->form_validation->set_rules('kode_outlet', 'Kode Outlet', 'required|numeric');
      $this->form_validation->set_rules('waktu_kedatangan', 'Waktu Kedatangan', 'required');
      $this->form_validation->set_rules('tanggal_kedatangan', 'Tanggal Kedatangan', 'required');
      $this->form_validation->set_rules('taksiran_atas', 'taksiran_atas', 'required|integer');
      $this->form_validation->set_rules('taksiran_bawah', 'taksiran_bawah', 'required|integer');

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $beratKeping = $this->post('berat_keping');
        $kodeOutlet = $this->post('kode_outlet');
        $waktuKedatangan = $this->post('waktu_kedatangan');
        $tanggalKedatangan = $this->post('tanggal_kedatangan');
        $taksiranAtas = $this->post('taksiran_atas');
        $taksiranBawah = $this->post('taksiran_bawah');
        $jenisPromo = $this->post('jenis_promo');

        $totalBeratKeping = 0;
        $keping = array();
        foreach ($beratKeping as $val) {
          $keping[]['berat_keping'] = $val;
          $totalBeratKeping += $val;
        }

        $foto = array('foto' => array());
        if (isset($_FILES['foto'])) {
          $foto = $this->uploadFoto();

          if (count($foto['error']) > 0) {
            $this->response(array(
              'code' => 101,
              'nama' => $this->put('nama'),
              'status' => 'error',
              'message' => 'Internal Server Error',
              'errors' => $foto['error']
            ), 200);
            return;
          }
        }

        $jadwalKedatangan = $this->formatKeUNIXTimestamp($tanggalKedatangan, $waktuKedatangan);

        $this->db->trans_start();

        $kodeBooking = $this->GadaiModel->generateBooking(self::LogamMulia, self::LogamMuliaStartValue);

        $this->GadaiModel->simpanLogamMulia(
          $token->id,
          $keping,
          $totalBeratKeping,
          $foto['foto'],
          $kodeOutlet,
          $jadwalKedatangan,
          $kodeBooking,
          $taksiranAtas,
          $taksiranBawah,
          $jenisPromo
        );

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
          $this->dbTransError('TRANSACTION ERROR GADAI LOGAM MULIA');
        }

        $this->sendEmail($token->id, $token->nama, $token->email, $kodeBooking, $kodeOutlet, $jadwalKedatangan, $token->no_hp);

        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => array(
            'kodeBooking' => $kodeBooking,
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function perhiasan_god_post()
  {
    $token = $this->getToken();
    if ($token) {
      if ($this->post('destinationAddress') != '' && $this->post('destinationContactName') != '' && $this->post('destinationLatLong') != '' && $this->post('destinationName') != '') {
        $data = array('destinationAddress' => $this->post('destinationAddress'), 'destinationContactName' => $this->post('destinationContactName'), 'destinationLatLong' => $this->post('destinationLatLong'), 'destinationName' => $this->post('destinationName'));
        $data_lokasi_nasabah = json_encode($data);
      } else {
        $data = array('destinationAddress' => $this->post('destinationAddress'), 'destinationContactName' => $this->post('destinationContactName'), 'destinationLatLong' => $this->post('destinationLatLong'), 'destinationName' => $this->post('destinationName'));
      }

      $setData = array(
        'jenis_perhiasan' => $this->post('jenis_perhiasan'),
        'kadar' => $this->post('kadar'),
        'berat_bersih' => $this->post('berat_bersih'),
        'harga_satuan' => $this->post('harga_satuan'),
        'kode_outlet' => $this->post('kode_outlet'),
        // 'waktu_kedatangan' => $this->post('waktu_kedatangan'),
        // 'tanggal_kedatangan' => $this->post('tanggal_kedatangan'),
        'jarak_to_outlet' => $this->post('jarak_to_outlet'),
        'taksiran_atas' => $this->post('taksiran_atas'),
        'taksiran_bawah' => $this->post('taksiran_bawah'),
        'nominal_pengajuan_nasabah' => $this->post('nominal_pengajuan_nasabah'),
        'jenisPromo' => $this->post('jenisPromo'),
        'catatan' => $this->post('catatan'),
        'lokasi_nasabah' => $data_lokasi_nasabah
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('jenis_perhiasan[]', 'Jenis Perhiasan', 'required');
      $this->form_validation->set_rules('kadar[]', 'Kadar Emas', 'required|numeric');
      $this->form_validation->set_rules('berat_bersih[]', 'Berat Bersih', 'required|numeric');
      $this->form_validation->set_rules('harga_satuan[]', 'Harga Satuan', 'required|numeric');
      $this->form_validation->set_rules('kode_outlet', 'Kode Outlet', 'required');
      // $this->form_validation->set_rules('waktu_kedatangan', 'Waktu Kedatangan', 'required');
      // $this->form_validation->set_rules('tanggal_kedatangan', 'Tanggal Kedatangan', 'required');
      $this->form_validation->set_rules('jarak_to_outlet', 'jarak_to_outlet', 'required');
      $this->form_validation->set_rules('taksiran_atas', 'taksiran_atas', 'required|integer');
      $this->form_validation->set_rules('taksiran_bawah', 'taksiran_bawah', 'required|integer');
      $this->form_validation->set_rules('nominal_pengajuan_nasabah', 'pengajuan nasabah', 'integer');
      $this->form_validation->set_rules('catatan[]', 'Catatan');
      $this->form_validation->set_rules('lokasi_nasabah', 'Lokasi Nasabah', 'required');


      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $jenis_perhiasan = $this->post('jenis_perhiasan');
        $jmlPerhiasan = 0;
        $Jenis = array();
        foreach ($jenis_perhiasan as $val) {
          $Jenis[]['jenis_perhiasan'] = $val;
          $jmlPerhiasan++;
        }
        $kadar1 = $this->post('kadar');
        $kadar = array();
        foreach ($kadar1 as $val) {
          $kadar[]['kadar'] = $val;
        }
        $beratBersih1 = $this->post('berat_bersih');
        $beratBersih = array();
        foreach ($beratBersih1 as $val) {
          $beratBersih[]['beratBersih'] = $val;
        }

        $hargaSatuan1 = $this->post('harga_satuan');
        $hargaSatuan = array();
        foreach ($hargaSatuan1 as $val) {
          $hargaSatuan[]['hargaSatuan'] = $val;
        }

        $id_jaminan = array();
        $id = (string)1;
        foreach ($jenis_perhiasan as $key => $val) {
          $id_jaminan[]['id_jaminan'] = (string)$id;
          $id++;
        }

        $idJaminan = json_encode($id_jaminan);

        $kodeOutlet = $this->post('kode_outlet');
        $waktuKedatangan = $this->post('waktu_kedatangan');
        $tanggalKedatangan = $this->post('tanggal_kedatangan');
        $jarak_to_outlet = $this->post('jarak_to_outlet');
        $taksiranAtas = $this->post('taksiran_atas');
        $taksiranBawah = $this->post('taksiran_bawah');
        $jenisPromo = $this->post('jenis_promo');
        $nominal_pengajuan_nasabah = $this->post('nominal_pengajuan_nasabah');

        $catatan1 = $this->post('catatan');
        $catatan = array();
        foreach ($catatan1 as $val) {
          $catatan[]['catatan'] = $val;
        }

        $lokasi_nasabah = json_encode($data);

        $this->session->item = ['jumlah_perhiasan' => $jmlPerhiasan];
        //$foto = $this->uploadFotoPerhiasan_god();


        // Format date to unixtimestamp
        $jadwalKedatangan = $tanggalKedatangan . ' ' . $waktuKedatangan;
        $jadwalKedatangan = strtotime($jadwalKedatangan);

        $this->db->trans_start();
        $data_uncompleted = $this->GadaiModel->getDataGadaiUnCompletedPerhiasan($token->id);
        if ($data_uncompleted->num_rows() > 0) {
          if ($data_uncompleted->row()->status == 1) {
            $screen_name = 'PickupAndDeliveryService';
          } elseif ($data_uncompleted->row()->status == 5 && $data->row()->status_fcm == 0) {
            $screen_name = 'LookingDriver';
          } elseif ($data_uncompleted->row()->status == 5 && $data->row()->status_fcm == 1) {
            $screen_name = 'SerahTerimaBarang';
          } elseif ($data_uncompleted->row()->status == 6) {
            $screen_name = 'DetailTransaksiAfterGopay';
          } elseif ($data_uncompleted->row()->status == 7 && $data_uncompleted->row()->respon_nasabah == 1) {
            $screen_name = 'PengajuanDisetujui';
          } elseif ($data_uncompleted->row()->status == 8) {
            $screen_name = 'LookingDriver';
          } elseif ($data_uncompleted->row()->status == 9) {
            $screen_name = 'DetailTransaksiAfterGopay';
          } elseif ($data_uncompleted->row()->status == 92) {
            $screen_name = 'PengambilanBarang';
          } elseif ($data_uncompleted->row()->status == 7 && $data_uncompleted->row()->respon_nasabah == 2 && $data_uncompleted->row()->otp == 0) {
            $screen_name = 'VerifyOTP';
          } elseif ($data_uncompleted->row()->status == 7 && $data_uncompleted->row()->respon_nasabah == 2 && $data_uncompleted->row()->otp == 1) {
            $screen_name = 'PilihNoRekeningPenerima';
          }
          $this->set_response(array(
            'code' => 101,
            'status' => 'error',
            'message' => 'Please Completed Your Booking Number' . $data_uncompleted->row()->kode_booking,
            'data' => array(
              'detail_gadai' => $data_uncompleted->row(),
              'screen_name' => $screen_name,
              'no_order_gosend' => $data_uncompleted->row()->no_order_gosend
            )

          ), 400);
        } else {
          
          $kodeBooking = $this->GadaiModel->generateBooking(self::Perhiasan, self::PerhiasanStartValue);
          $this->GadaiModel->saveGadaiPerhiasanGod(
            $token->id,
            $kodeBooking,
            $Jenis,
            $kadar,
            $beratBersih,
            $hargaSatuan,
            $kodeOutlet,
            $jadwalKedatangan,
            $jarak_to_outlet,
            $taksiranAtas,
            $taksiranBawah,
            $jenisPromo,
            $nominal_pengajuan_nasabah,
            $catatan,
            $lokasi_nasabah,
            $idJaminan
          );
          log_message("debug", "perhiasan_god_post SUKSES: " . $kodeBooking);
          $dataCabang = $this->MasterModel->getSingleCabang($kodeOutlet);
          $tanggalKedatangan = date('d-m-Y', $jadwalKedatangan);
          $waktuKedatangan = date('H:i', $jadwalKedatangan);
          $detailGadai = $this->GadaiModel->getDetailGadaiByKodeBooking($kodeBooking);
          $user = $this->User->getUser($token->id);
          $templateData = array(
            'nama' => $user->nama,
            'noPengajuan' => $kodeBooking,
            'namaOutlet' => $dataCabang->nama,
            'alamatOutlet' => $dataCabang->alamat,
            'teleponOutlet' => $dataCabang->telepon,
            'tanggalDatang' => $tanggalKedatangan,
            'waktuDatang' => $waktuKedatangan,
            'detailGadai' => $detailGadai
          );

          $content = $this->load->view('mail/notif_kca', $templateData, TRUE);

          $mobile = $this->load->view('notification/top_template', array('title' => 'Pengajuan Gadai Berhasil Kode Booking ' . $kodeBooking . ' '), true);
          $mobile = $mobile . $content;
          $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

          $this->NotificationModel->addAdminNotificaion(
            $token->id,
            NotificationModel::TYPE_GADAI,
            NotificationModel::CONTENT_TYPE_TEXT,
            "Pengajuan Gadai",
            "Perhiasan",
            "",
            "",
            "",
            $kodeOutlet,
            $kodeBooking
          );


          //   $userOtp=$this->GadaiModel->getCountUserOtp($token->id);
          //   if($userOtp->count > 0){
          //     $updateData = array('otp'=> 0);
          //     $this->db->where('user_AIID', $token->id)->update('gadai_perhiasan', $updateData);
          //   }

          $this->db->trans_complete();
          if ($this->db->trans_status() === FALSE) {
            $this->dbTransError('Save Gadai Perhiasan');
          }

          $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => array(
              'kodeBooking' => $kodeBooking
            )
          ), 200);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function uploadfoto_perhiasan_god_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'kode_booking' => $this->post('kode_booking')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required');

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $kode_booking = $this->post('kode_booking');
        $checkKodeBooking = $this->GadaiModel->getCheckKodeBooking($kode_booking, 'gadai_perhiasan');

        if ($checkKodeBooking->count > 0) {
          $jmlFoto = count($_FILES['foto']['name']);
          if ($jmlFoto < 3) {
            $this->form_validation->set_rules('foto[]', 'Foto', 'required');
            $this->response(array(
              'code' => 101,
              'status' => 'error',
              'message' => 'photo must be 3 items'
            ), 200);
          } else {
            $foto = $this->uploadFoto();
            $this->db->trans_start();
            $this->GadaiModel->saveGadaiPerhiasanFotoGod(
              $token->id,
              $kode_booking,
              $foto['foto']
            );
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
              $this->dbTransError('Save Gadai Perhiasan');
            }

            $this->set_response(array(
              'status' => 'success',
              'message' => 'foto telah terupload',
              'data' => $foto['foto'],
              'upload_path' => '/user/gadai'
            ), 200);
          }
        } else {
          $this->response(array(
            'code' => 101,
            'status' => 'error',
            'message' => 'Kode Booking Tidak ada'
          ), 400);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }


  function logam_mulia_god_post()
  {
    $token = $this->getToken();
    if ($token) {
      if ($this->post('destinationAddress') != '' && $this->post('destinationContactName') != '' && $this->post('destinationLatLong') != '' && $this->post('destinationName') != '') {
        $data = array('destinationAddress' => $this->post('destinationAddress'), 'destinationContactName' => $this->post('destinationContactName'), 'destinationLatLong' => $this->post('destinationLatLong'), 'destinationName' => $this->post('destinationName'));
        $data_lokasi_nasabah = json_encode($data);
      } else {
        $data = array('destinationAddress' => $this->post('destinationAddress'), 'destinationContactName' => $this->post('destinationContactName'), 'destinationLatLong' => $this->post('destinationLatLong'), 'destinationName' => $this->post('destinationName'));

        $data_lokasi_nasabah = '';
        // $message='';
        // if ($data['destinationAddress']==''){
        //   $message= 'Destination Address null';
        // }else if($data['destinationContactName']==''){
        //   $message= 'Destination Contact Name null';
        // }else if($data['destinationLatLong']==''){
        //   $message= 'DestinationLatLong null';
        // }else if($data['destinationName']==''){
        //   $message= 'Destination Name null';
        // }
      }

      $setData = array(
        'berat_keping' => $this->post('berat_keping[]'),
        'jumlah_keping' => $this->post('jumlah_keping[]'),
        'harga_satuan' => $this->post('harga_satuan[]'),
        'kode_outlet' => $this->post('kode_outlet'),
        // 'waktu_kedatangan' => $this->post('waktu_kedatangan'),
        // 'tanggal_kedatangan' => $this->post('tanggal_kedatangan'),
        'taksiran_atas' => $this->post('taksiran_atas'),
        'taksiran_bawah' => $this->post('taksiran_bawah'),
        'nominal_pengajuan_nasabah' => $this->post('nominal_pengajuan_nasabah'),
        'jarak_to_outlet' => $this->post('jarak_to_outlet'),
        'lokasi_nasabah' => $data_lokasi_nasabah
      );


      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('berat_keping[]', 'Berat Keping', 'require|decimal');
      $this->form_validation->set_rules('jumlah_keping[]', 'Jumlah Keping', 'decimal');
      $this->form_validation->set_rules('harga_satuan[]', 'Harga Satuan Keping', 'decimal');
      $this->form_validation->set_rules('kode_outlet', 'Kode Outlet', 'required|numeric');
      // $this->form_validation->set_rules('waktu_kedatangan', 'Waktu Kedatangan', 'required');
      //$this->form_validation->set_rules('tanggal_kedatangan', 'Tanggal Kedatangan', 'required');
      $this->form_validation->set_rules('taksiran_atas', 'taksiran_atas', 'required|integer');
      $this->form_validation->set_rules('taksiran_bawah', 'taksiran_bawah', 'required|integer');
      $this->form_validation->set_rules('nominal_pengajuan_nasabah', 'nominal pengajuan nasabah', 'integer');
      $this->form_validation->set_rules('jarak_to_outlet', 'Jarak to outlet', 'required');
      $this->form_validation->set_rules('lokasi_nasabah', 'Lokasi Nasabah', 'required');


      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 400);
      } else {
        $totalBeratKeping = 0;
        $beratKeping = $this->post('berat_keping');
        foreach ($beratKeping as $val) {
          $keping[]['berat_keping'] = $val;
          $totalBeratKeping += $val;
        }
        $data_keping = json_encode($keping);

        $jumlahKeping = $this->post('jumlah_keping');
        foreach ($jumlahKeping as $val) {
          $jmlkeping[]['jumlah_keping'] = $val;
        }

        $banyak_keping = json_encode($jmlkeping);
        $harga = $this->post('harga_satuan');
        foreach ($harga as $val) {
          $harga_satuan[]['harga_satuan'] = $val;
        }

        $harga_satuan_keping = json_encode($harga_satuan);

        $id_jaminan = array();
        $id = (string)1;
        foreach ($beratKeping as $key => $val) {
          $id_jaminan[]['id_jaminan'] = (string)$id;
          $id++;
        }

        $idJaminan = json_encode($id_jaminan);

        $kodeOutlet = $this->post('kode_outlet');
        $waktuKedatangan = $this->post('waktu_kedatangan');
        $tanggalKedatangan = $this->post('tanggal_kedatangan');
        $taksiranAtas = $this->post('taksiran_atas');
        $taksiranBawah = $this->post('taksiran_bawah');
        $jenisPromo = $this->post('jenis_promo');
        $nominal_pengajuan_nasabah = $this->post('nominal_pengajuan_nasabah');
        $jarak_to_outlet = $this->post('jarak_to_outlet');
        $lokasi_nasabah = json_encode($data);

        $jadwalKedatangan = $this->formatKeUNIXTimestamp($tanggalKedatangan, $waktuKedatangan);
        // $this->db->trans_start();

        $data_uncompleted = $this->GadaiModel->getDataGadaiUnCompletedLogamMulia($token->id);

        if ($data_uncompleted->num_rows() > 0) {
          if ($data_uncompleted->row()->status == 1) {
            $screen_name = 'PickupAndDeliveryServiceLM';
          } elseif ($data_uncompleted->row()->status == 5 && $data->row()->status_fcm == 0) {
            $screen_name = 'LookingDriverLM';
          } elseif ($data_uncompleted->row()->status == 5 && $data->row()->status_fcm == 1) {
            $screen_name = 'SerahTerimaBarangLM';
          } elseif ($data_uncompleted->row()->status == 6) {
            $screen_name = 'DetailTransaksiAfterGopayLM';
          } elseif ($data_uncompleted->row()->status == 7 && $data_uncompleted->row()->respon_nasabah == 1) {
            $screen_name = 'PengajuanDisetujuiLM';
          } elseif ($data_uncompleted->row()->status == 8) {
            $screen_name = 'LookingDriverLM';
          } elseif ($data_uncompleted->row()->status == 9) {
            $screen_name = 'DetailTransaksiAfterGopayLM';
          } elseif ($data_uncompleted->row()->status == 92) {
            $screen_name = 'PengambilanBarangLM';
          } elseif ($data_uncompleted->row()->status == 7 && $data_uncompleted->row()->respon_nasabah == 2 && $data_uncompleted->row()->otp == 0) {
            $screen_name = 'VerifyOTPLM';
          } elseif ($data_uncompleted->row()->status == 7 && $data_uncompleted->row()->respon_nasabah == 2 && $data_uncompleted->row()->otp == 1) {
            $screen_name = 'PilihNoRekeningPenerimaLM';
          }
          $this->set_response(array(
            'code' => 101,
            'status' => 'error',
            'message' => 'Please Completed Your Booking Number ' . $data_uncompleted->row()->kode_booking,
            'data' => array(
              'detail_gadai' => $data_uncompleted->row(),
              'screen_name' => $screen_name,
              'no_order_gosend' => $data_uncompleted->row()->no_order_gosend
            )
          ), 400);
          log_message("debug", "logam_mulia_god ERROR: " . json_encode($data_uncompleted->row()));
        } else {
          $kodeBooking = $this->GadaiModel->generateBooking(self::LogamMulia, self::LogamMuliaStartValue);

          $this->session->item = ['kode_booking' => $kodeBooking];
          $this->GadaiModel->simpanLogamMuliaGod(
            $token->id,
            $data_keping,
            $banyak_keping,
            $harga_satuan_keping,
            $totalBeratKeping,
            $kodeOutlet,
            $jadwalKedatangan,
            $kodeBooking,
            $taksiranAtas,
            $taksiranBawah,
            $jenisPromo,
            $nominal_pengajuan_nasabah,
            $jarak_to_outlet,
            $lokasi_nasabah,
            $idJaminan
          );

          $dataCabang = $this->MasterModel->getSingleCabang($kodeOutlet);
          $tanggalKedatangan = date('d-m-Y', $jadwalKedatangan);
          $waktuKedatangan = date('H:i', $jadwalKedatangan);
          $detailGadai = $this->GadaiModel->getDetailGadaiByKodeBooking($kodeBooking);
          $user = $this->User->getUser($token->id);
          $templateData = array(
            'nama' => $user->nama,
            'noPengajuan' => $kodeBooking,
            'namaOutlet' => $dataCabang->nama,
            'alamatOutlet' => $dataCabang->alamat,
            'teleponOutlet' => $dataCabang->telepon,
            'tanggalDatang' => $tanggalKedatangan,
            'waktuDatang' => $waktuKedatangan,
            'detailGadai' => $detailGadai
          );

          $content = $this->load->view('mail/notif_kca', $templateData, TRUE);

          $mobile = $this->load->view('notification/top_template', array('title' => 'Pengajuan Gadai Berhasil Kode Booking ' . $kodeBooking . ' '), true);
          $mobile = $mobile . $content;
          $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

          //add syukron
          $this->NotificationModel->addAdminNotificaion(
            $token->id,
            NotificationModel::TYPE_GADAI,
            NotificationModel::CONTENT_TYPE_TEXT,
            "Pengajuan Gadai",
            "Logam Mulia",
            "",
            "",
            "",
            $kodeOutlet,
            $kodeBooking
          );

          //   $userOtp=$this->GadaiModel->getCountUserOtp($token->id);
          //   if($userOtp->count > 0){
          //     $updateData = array('otp'=> 0);
          //     $this->db->where('user_AIID', $token->id)->update('gadai_logam_mulia', $updateData);
          //   }

          $this->db->trans_complete();
          if ($this->db->trans_status() === FALSE) {
            $this->dbTransError('TRANSACTION ERROR GADAI LOGAM MULIA');
          }

          $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => array(
              'kodeBooking' => $kodeBooking,
            )
          ), 200);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function uploadfoto_god_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'kode_booking' => $this->post('kode_booking')
      );

      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('kode_booking', 'Kode Booking', 'required');
      if (empty($_FILES['foto']['name'])) {
        $this->form_validation->set_rules('foto[]', 'Foto', 'required');
      }
      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $kode_booking = $this->post('kode_booking');
        $checkKodeBooking = $this->GadaiModel->getCheckKodeBooking($kode_booking, 'gadai_logam_mulia');

        if ($checkKodeBooking->count > 0) {
          $jmlFoto = count($_FILES['foto']['name']);
          if ($jmlFoto < 3) {
            $this->form_validation->set_rules('foto[]', 'Foto', 'required');
            $this->response(array(
              'code' => 101,
              'status' => 'error',
              'message' => 'photo must be 3 items'
            ), 200);
          } else {
            $foto = $this->uploadFoto();
            $this->db->trans_start();
            $this->GadaiModel->simpanLogamMuliaFotoGod(
              $token->id,
              $kode_booking,
              $foto['foto']
            );
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
              $this->dbTransError('Save Gadai LogamMulia');
            }

            $this->set_response(array(
              'status' => 'success',
              'message' => 'foto telah terupload ',
              'data' => $foto['foto'],
              'upload_path' => '/user/gadai/'
            ), 200);
          }
        } else {
          $this->response(array(
            'code' => 101,
            'status' => 'error',
            'message' => 'Kode Booking Tidak ada'
          ), 400);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function elektronik_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'jenis' => $this->post('jenis'),
        'id_merk_elektronik' => $this->post('id_merk_elektronik'),
        'tipe' => $this->post('tipe'),
        'kondisi_barang' => $this->post('kondisi_barang'),
        'harga_jual' => $this->post('harga_jual'),
        'kode_outlet' => $this->post('kode_outlet'),
        'waktu_kedatangan' => $this->post('waktu_kedatangan'),
        'tanggal_kedatangan' => $this->post('tanggal_kedatangan'),
        'tipe_jaminan' => $this->post('tipe_jaminan'),
        'taksiran_atas' => $this->post('taksiran_atas'),
        'taksiran_bawah' => $this->post('taksiran_bawah'),
        'jenisPromo' => $this->post('jenisPromo')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('jenis', 'Jenis Elektronik', 'required');
      $this->form_validation->set_rules('id_merk_elektronik', 'Merek Elektronik', 'required');
      $this->form_validation->set_rules('tipe', 'Tipe Elektronik', 'required');
      $this->form_validation->set_rules('kondisi_barang', 'Kondisi Barang', 'required');
      $this->form_validation->set_rules('harga_jual', 'Harga Jual Sekarang', 'required|numeric');
      $this->form_validation->set_rules('kode_outlet', 'Kode Outlet', 'required');
      $this->form_validation->set_rules('waktu_kedatangan', 'Waktu Kedatangan', 'required');
      $this->form_validation->set_rules('tanggal_kedatangan', 'Tanggal Kedatangan', 'required');

      $this->form_validation->set_rules('tipe_jaminan', 'tipe_jaminan', 'required');
      $this->form_validation->set_rules('taksiran_atas', 'taksiran_atas', 'required|integer');
      $this->form_validation->set_rules('taksiran_bawah', 'taksiran_bawah', 'required|integer');



      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $jenis = $this->post('jenis');
        $merk = $this->post('id_merk_elektronik');
        $tipe = $this->post('tipe');
        $kondisiBarang = $this->post('kondisi_barang');
        $hargaJual = $this->post('harga_jual');
        $kodeOutlet = $this->post('kode_outlet');
        $waktuKedatangan = $this->post('waktu_kedatangan');
        $tanggalKedatangan = $this->post('tanggal_kedatangan');

        $tipeJaminan = $this->post('tipe_jaminan');
        $taksiranAtas = $this->post('taksiran_atas');
        $taksiranBawah = $this->post('taksiran_bawah');
        $jenisPromo = $this->post('jenis_promo');

        $foto = array('foto' => array());
        if (isset($_FILES['foto'])) {
          $foto = $this->uploadFoto();

          if (count($foto['error']) > 0) {
            $this->response(array(
              'code' => 101,
              'nama' => $this->put('nama'),
              'status' => 'error',
              'message' => 'Internal Server Error',
              'errors' => $foto['error']
            ), 200);
            return;
          }
        }

        $jadwalKedatangan = $this->formatKeUNIXTimestamp($tanggalKedatangan, $waktuKedatangan);

        $this->db->trans_start();

        $kodeBooking = $this->GadaiModel->generateBooking(self::Elektronik, self::ElektronikStartValue);

        $this->GadaiModel->simpanElektronik(
          $token->id,
          $jenis,
          $merk,
          $tipe,
          $kondisiBarang,
          $hargaJual,
          $foto['foto'],
          $kodeOutlet,
          $jadwalKedatangan,
          $kodeBooking,
          $tipeJaminan,
          $taksiranAtas,
          $taksiranBawah,
          $jenisPromo
        );

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
          $this->dbTransError('TRANSACTION ERROR GADAI ELEKTRONIK');
        }

        $this->sendEmail($token->id, $token->nama, $token->email, $kodeBooking, $kodeOutlet, $jadwalKedatangan, $token->no_hp);

        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => array(
            'kodeBooking' => $kodeBooking,
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function handphone_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'id_merk_smartphone' => $this->post('id_merk_smartphone'),
        'tipe' => $this->post('tipe'),
        'kondisi_smartphone' => $this->post('kondisi_smartphone'),
        'tahun_beli' => $this->post('tahun_beli'),
        'harga_jual' => $this->post('harga_jual'),
        'kode_outlet' => $this->post('kode_outlet'),
        'waktu_kedatangan' => $this->post('waktu_kedatangan'),
        'tanggal_kedatangan' => $this->post('tanggal_kedatangan'),
        'taksiran_atas' => $this->post('taksiran_atas'),
        'taksiran_bawah' => $this->post('taksiran_bawah'),
        'jenisPromo' => $this->post('jenisPromo')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('id_merk_smartphone', 'Merek Smartphone', 'required');
      $this->form_validation->set_rules('tipe', 'Tipe Smartphone', 'required');
      $this->form_validation->set_rules('kondisi_smartphone', 'Kondisi Smartphone', 'required');
      $this->form_validation->set_rules('tahun_beli', 'Tahun Beli Smartphone', 'required');
      $this->form_validation->set_rules('harga_jual', 'Harga Jual Sekarang', 'required|numeric');
      $this->form_validation->set_rules('kode_outlet', 'Kode Outlet', 'required');
      $this->form_validation->set_rules('waktu_kedatangan', 'Waktu Kedatangan', 'required');
      $this->form_validation->set_rules('tanggal_kedatangan', 'Tanggal Kedatangan', 'required');

      $this->form_validation->set_rules('taksiran_atas', 'taksiran_atas', 'required|integer');
      $this->form_validation->set_rules('taksiran_bawah', 'taksiran_bawah', 'required|integer');

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $merk = $this->post('id_merk_smartphone');
        $tipe = $this->post('tipe');
        $thBeli = $this->post('tahun_beli');
        $kondisiBarang = $this->post('kondisi_smartphone');
        $hargaJual = $this->post('harga_jual');
        $kodeOutlet = $this->post('kode_outlet');
        $waktuKedatangan = $this->post('waktu_kedatangan');
        $tanggalKedatangan = $this->post('tanggal_kedatangan');

        $taksiranAtas = $this->post('taksiran_atas');
        $taksiranBawah = $this->post('taksiran_bawah');
        $jenisPromo = $this->post('jenis_promo');


        $foto = array('foto' => array());
        if (isset($_FILES['foto'])) {
          $foto = $this->uploadFoto();

          if (count($foto['error']) > 0) {
            $this->response(array(
              'code' => 101,
              'nama' => $this->put('nama'),
              'status' => 'error',
              'message' => 'Internal Server Error',
              'errors' => $foto['error']
            ), 200);
            return;
          }
        }

        $jadwalKedatangan = $this->formatKeUNIXTimestamp($tanggalKedatangan, $waktuKedatangan);

        $this->db->trans_start();

        $kodeBooking = $this->GadaiModel->generateBooking(self::Handphone, self::HandphoneStartValue);

        $this->GadaiModel->simpanSmartphone(
          $token->id,
          $merk,
          $tipe,
          $kondisiBarang,
          $thBeli,
          $hargaJual,
          $foto['foto'],
          $kodeOutlet,
          $jadwalKedatangan,
          $kodeBooking,
          $taksiranAtas,
          $taksiranBawah,
          $jenisPromo
        );

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
          $this->dbTransError('TRANSACTION ERROR GADAI HANDPHONE');
        }


        $this->sendEmail($token->id, $token->nama, $token->email, $kodeBooking, $kodeOutlet, $jadwalKedatangan, $token->no_hp);

        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => array(
            'kodeBooking' => $kodeBooking,
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function kendaraan_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'jenis' => $this->post('jenis'),
        'id_merk_kendaraan' => $this->post('id_merk_kendaraan'),
        'tipe' => $this->post('tipe'),
        'deskripsi' => $this->post('deskripsi'),
        'isi_silinder' => $this->post('isi_silinder'),
        'tahun_pembuatan' => $this->post('tahun_pembuatan'),
        'harga_jual' => $this->post('harga_jual'),
        'kode_outlet' => $this->post('kode_outlet'),
        'waktu_kedatangan' => $this->post('waktu_kedatangan'),
        'tanggal_kedatangan' => $this->post('tanggal_kedatangan'),

        'no_polisi' => $this->post('no_polisi'),
        'no_bpkb' => $this->post('no_bpkb'),
        'nama_bpkb' => $this->post('nama_bpkb'),
        'no_stnk' => $this->post('no_stnk'),
        'no_mesin' => $this->post('no_mesin'),
        'taksiran' => $this->post('taksiran'),
        'no_rangka' => $this->post('no_rangka'),

        'tipe_jaminan' => $this->post('tipe_jaminan'),
        'taksiran_atas' => $this->post('taksiran_atas'),
        'taksiran_bawah' => $this->post('taksiran_bawah'),
        'jenisPromo' => $this->post('jenisPromo')

      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('jenis', 'Jenis Kendaraan', 'required');
      $this->form_validation->set_rules('id_merk_kendaraan', 'Merek Kendaraan', 'required');
      $this->form_validation->set_rules('tipe', 'Type Kendaraan', 'required');
      $this->form_validation->set_rules('deskripsi', 'Deskripsi Kendaraan', 'required');
      $this->form_validation->set_rules('isi_silinder', 'Isi Silinder', 'required|numeric');
      $this->form_validation->set_rules('tahun_pembuatan', 'Tahun Pembuatan', 'required|numeric');
      $this->form_validation->set_rules('harga_jual', 'Harga Jual Sekarang', 'required|numeric');
      $this->form_validation->set_rules('kode_outlet', 'Kode Outlet', 'required');
      $this->form_validation->set_rules('waktu_kedatangan', 'Waktu Kedatangan', 'required');
      $this->form_validation->set_rules('tanggal_kedatangan', 'Tanggal Kedatangan', 'required');

      $this->form_validation->set_rules('no_polisi', 'No Polisi', 'required');
      $this->form_validation->set_rules('no_bpkb', 'No BPKB', 'required');
      $this->form_validation->set_rules('nama_bpkb', 'Nama BPKB', 'required');
      $this->form_validation->set_rules('no_stnk', 'No STNK', 'required');
      $this->form_validation->set_rules('no_mesin', 'No Mesin', 'required');
      $this->form_validation->set_rules('no_rangka', 'No Rangka', 'required');

      $this->form_validation->set_rules('tipe_jaminan', 'tipe_jaminan', 'required');
      $this->form_validation->set_rules('taksiran_atas', 'taksiran_atas', 'required|integer');
      $this->form_validation->set_rules('taksiran_bawah', 'taksiran_bawah', 'required|integer');

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $jenis = $this->post('jenis');
        $merk = $this->post('id_merk_kendaraan');
        $tipe = $this->post('tipe');
        $deskripsi = $this->post('deskripsi');
        $isiSilinder = $this->post('isi_silinder');
        $tahunPembuatan = $this->post('tahun_pembuatan');
        $hargaJual = $this->post('harga_jual');
        $kodeOutlet = $this->post('kode_outlet');
        $waktuKedatangan = $this->post('waktu_kedatangan');
        $tanggalKedatangan = $this->post('tanggal_kedatangan');

        $no_polisi = $this->post('no_polisi');
        $no_bpkb = $this->post('no_bpkb');
        $nama_bpkb = $this->post('nama_bpkb');
        $no_stnk = $this->post('no_stnk');
        $no_mesin = $this->post('no_mesin');
        $taksiran = $this->post('taksiran');
        $no_rangka = $this->post('no_rangka');

        $tipeJaminan = $this->post('tipe_jaminan');
        $taksiranAtas = $this->post('taksiran_atas');
        $taksiranBawah = $this->post('taksiran_bawah');

        $jenisPromo = $this->post('jenis_promo');

        $foto = array('foto' => array());
        if (isset($_FILES['foto'])) {
          $foto = $this->uploadFoto();

          if (count($foto['error']) > 0) {
            $this->response(array(
              'code' => 101,
              'nama' => $this->put('nama'),
              'status' => 'error',
              'message' => 'Internal Server Error',
              'errors' => $foto['error']
            ), 200);
            return;
          }
        }

        $jadwalKedatangan = $this->formatKeUNIXTimestamp($tanggalKedatangan, $waktuKedatangan);

        $this->db->trans_start();

        $kodeBooking = $this->GadaiModel->generateBooking(self::Kendaraan, self::KendaraanStartValue);

        $this->GadaiModel->simpanKendaraan(
          $token->id,
          $jenis,
          $merk,
          $tipe,
          $deskripsi,
          $isiSilinder,
          $tahunPembuatan,
          $hargaJual,
          $foto['foto'],
          $kodeOutlet,
          $jadwalKedatangan,
          $kodeBooking,

          $no_polisi,
          $no_bpkb,
          $nama_bpkb,
          $no_stnk,
          $no_mesin,
          $taksiran,
          $no_rangka,
          $tipeJaminan,
          $taksiranAtas,
          $taksiranBawah,
          $jenisPromo
        );

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
          $this->dbTransError('TRANSACTION ERROR GADAI KENDARAAN');
        }

        $this->sendEmail($token->id, $token->nama, $token->email, $kodeBooking, $kodeOutlet, $jadwalKedatangan, $token->no_hp);

        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => array(
            'kodeBooking' => $kodeBooking
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function laptop_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'id_merk_laptop' => $this->post('id_merk_laptop'),
        'type' => $this->post('type'),
        'deskripsi' => $this->post('deskripsi'),
        'kapasitas_hdd' => $this->post('kapasitas_hdd'),
        'tahun_pembelian' => $this->post('tahun_pembelian'),
        'harga_jual' => $this->post('harga_jual'),
        'kode_outlet' => $this->post('kode_outlet'),
        'waktu_kedatangan' => $this->post('waktu_kedatangan'),
        'tanggal_kedatangan' => $this->post('tanggal_kedatangan'),
        'taksiran_atas' => $this->post('taksiran_atas'),
        'taksiran_bawah' => $this->post('taksiran_bawah'),
        'jenisPromo' => $this->post('jenisPromo')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('id_merk_laptop', 'Merek Laptop', 'required');
      $this->form_validation->set_rules('type', 'Type Laptop', 'required');
      $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
      $this->form_validation->set_rules('kapasitas_hdd', 'Kapasitas HDD', 'required|numeric');
      $this->form_validation->set_rules('tahun_pembelian', 'Tahun Pembelian', 'required|numeric');
      $this->form_validation->set_rules('harga_jual', 'Harga Jual Sekarang', 'required|numeric');
      $this->form_validation->set_rules('kode_outlet', 'Kode Outlet', 'required');
      $this->form_validation->set_rules('waktu_kedatangan', 'Waktu Kedatangan', 'required');
      $this->form_validation->set_rules('tanggal_kedatangan', 'Tanggal Kedatangan', 'required');
      $this->form_validation->set_rules('taksiran_atas', 'taksiran_atas', 'required|integer');
      $this->form_validation->set_rules('taksiran_bawah', 'taksiran_bawah', 'required|integer');

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $merk = $this->post('id_merk_laptop');
        $tipe = $this->post('type');
        $deskripsi = $this->post('deskripsi');
        $kapasitasHDD = $this->post('kapasitas_hdd');
        $tahunPembelian = $this->post('tahun_pembelian');
        $hargaJual = $this->post('harga_jual');
        $kodeOutlet = $this->post('kode_outlet');
        $waktuKedatangan = $this->post('waktu_kedatangan');
        $tanggalKedatangan = $this->post('tanggal_kedatangan');

        $taksiranAtas = $this->post('taksiran_atas');
        $taksiranBawah = $this->post('taksiran_bawah');
        $jenisPromo = $this->post('jenis_promo');

        $foto = array('foto' => array());
        if (isset($_FILES['foto'])) {
          $foto = $this->uploadFoto();

          if (count($foto['error']) > 0) {
            $this->response(array(
              'code' => 101,
              'nama' => $this->put('nama'),
              'status' => 'error',
              'message' => 'Internal Server Error',
              'errors' => $foto['error']
            ), 200);
            return;
          }
        }

        $jadwalKedatangan = $this->formatKeUNIXTimestamp($tanggalKedatangan, $waktuKedatangan);

        $this->db->trans_start();

        $kodeBooking = $this->GadaiModel->generateBooking(self::Laptop, self::LaptopStartValue);

        $this->GadaiModel->simpanLaptop(
          $token->id,
          $merk,
          $tipe,
          $deskripsi,
          $kapasitasHDD,
          $tahunPembelian,
          $hargaJual,
          $foto['foto'],
          $kodeOutlet,
          $jadwalKedatangan,
          $kodeBooking,
          $taksiranAtas,
          $taksiranBawah,
          $jenisPromo
        );

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
          $this->dbTransError('TRANSACTION ERROR GADAI LAPTOP');
        }

        $this->sendEmail($token->id, $token->nama, $token->email, $kodeBooking, $kodeOutlet, $jadwalKedatangan, $token->no_hp);

        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => array(
            'kodeBooking' => $kodeBooking
          )
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function logam_mulia_get()
  {
    $token = $this->getToken();
    if ($token) {
      $idGadai = $this->query('id_gadai');

      if ($idGadai) {
        $dataLogam = $this->GadaiModel->getDataLogam($token->id, $idGadai);
      } else {
        $dataLogam = $this->GadaiModel->getDataLogams($token->id);
      }

      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => $dataLogam
      ), 200);
    } else {
      $this->errorUnAuthorized();
    }
  }

  function logam_mulia_god_get()
  {
    $token = $this->getToken();
    if ($token) {
      $kode = $this->query('kode_booking');
      $detailGod = $this->GadaiModel->getDataLogamGod($kode);

      $totalKeping = 0;
      $totalBeratKeping = array();
      $totalAllBeratKeping = 0;
      $arrKeping = json_decode($detailGod[0]->keping_json);
      $arrJumlahKeping = json_decode($detailGod[0]->jumlah_keping);
      foreach ($arrKeping as $key => $value) {
        $beratKeping = $value->berat_keping;
        $jumlahKeping = $arrJumlahKeping[$key]->jumlah_keping;

        $totalBeratKeping[] = $beratKeping * $jumlahKeping;
        $totalAllBeratKeping += $totalBeratKeping[$key];
        $totalKeping = $totalKeping + $jumlahKeping;
      }

      $detailGod[0]->totalBeratKeping = $totalAllBeratKeping;
      $detailGod[0]->totalKeping = $totalKeping;

      if ($detailGod) {
        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => $detailGod
        ), 200);
      } else {
        $this->set_response([
          'status' => 'error',
          'code' => 102,
          'message' => 'Tidak ada data gadai ditemukan'
        ], 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function perhiasan_get()
  {
    $token = $this->getToken();
    if ($token) {
      $idGadai = $this->query('id_gadai');

      if ($idGadai) {
        $dataPerhiasan = $this->GadaiModel->getDataPerhiasan($token->id, $idGadai);
      } else {
        $dataPerhiasan = $this->GadaiModel->getDataPerhiasans($token->id);
      }

      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => $dataPerhiasan
      ), 200);
    } else {
      $this->errorUnAuthorized();
    }
  }

  function perhiasan_god_get()
  {
    $token = $this->getToken();

    // check token validity
    if  (!$token) {
      $this->errorUnAuthorized();
      return;
    }

    $kode = $this->query('kode_booking');
    $totalAllBeratBersih = 0;
    $detailGod = $this->GadaiModel->getDataPerhiasanGod($kode);
    if (empty($detailGod)) {
      $response = [
        'status' => 'error',
        'code' => 102,
        'message' => 'Tidak ada data gadai ditemukan'
      ];
      $this->set_response($response, 200);
      log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
    }

    $arrBeratBersih = json_decode($detailGod[0]->berat_bersih_god);
    foreach ($arrBeratBersih as $value) {
      $beratBersih = $value->beratBersih;
      $totalAllBeratBersih = $totalAllBeratBersih + $beratBersih;
    }

    $detailGod[0]->totalBeratBersih = $totalAllBeratBersih;
    $response = [
      'status' => 'success',
      'message' => '',
      'data' => $detailGod
    ];
    $this->set_response($response, 200);
    log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
  }

  function elektronik_get()
  {
    $token = $this->getToken();
    if ($token) {
      $idGadai = $this->query('id_gadai');

      if ($idGadai) {
        $dataElektronik = $this->GadaiModel->getDataElektronik($token->id, $idGadai);
      } else {
        $dataElektronik = $this->GadaiModel->getDataElektroniks($token->id);
      }


      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => $dataElektronik
      ), 200);
    } else {
      $this->errorUnAuthorized();
    }
  }

  function handphone_get()
  {
    $token = $this->getToken();
    if ($token) {
      $idGadai = $this->query('id_gadai');

      if ($idGadai) {
        $dataSmartphone = $this->GadaiModel->getDataSmartphone($token->id, $idGadai);
      } else {
        $dataSmartphone = $this->GadaiModel->getDataSmartphones($token->id);
      }

      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => $dataSmartphone
      ), 200);
    } else {
      $this->errorUnAuthorized();
    }
  }

  function kendaraan_get()
  {
    $token = $this->getToken();
    if ($token) {
      $idGadai = $this->query('id_gadai');

      if ($idGadai) {
        $dataKendaraan = $this->GadaiModel->getDataKendaraan($token->id, $idGadai);
      } else {
        $dataKendaraan = $this->GadaiModel->getDataKendaraans($token->id);
      }

      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => $dataKendaraan
      ), 200);
    } else {
      $this->errorUnAuthorized();
    }
  }

  function efek_get()
  {
    $token = $this->getToken();
    if ($token) {
      $idGadai = $this->query('id_gadai');
      $gadai = $this->GadaiModel->getUserGadaiEfek($token->id, $idGadai);

      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => $gadai
      ), 200);
    } else {
      $this->errorUnAuthorized();
    }
  }

  function laptop_get()
  {
    $token = $this->getToken();
    if ($token) {
      $idGadai = $this->query('id_gadai');

      $dataLaptop = array();
      if ($idGadai) {
        $dataLaptop = $this->GadaiModel->getDataLaptop($token->id, $idGadai);
      } else {
        $dataLaptop = $this->GadaiModel->getDataLaptops($token->id);
      }

      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => $dataLaptop
      ), 200);
    } else {
      $this->errorUnAuthorized();
    }
  }

  function gadai_get()
  {
    $token = $this->getToken();
    if ($token) {
      $halaman = $this->query('page');
      $startFrom = $halaman ? $halaman - 1 : 0;
      $halaman = $halaman ? $halaman : 1;

      $this->db->trans_start();

      $dataGadai = $this->GadaiModel->getAllGadai($token->id, $startFrom);
      $totalGadai = $this->GadaiModel->getTotalGadai($token->id);

      $this->db->trans_complete();
      if ($this->db->trans_status() === FALSE) {
        $this->dbTransError('TRANSACTION ERROR GET GADAI');
      }

      $maxItem = 3 * 6;
      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => array(
          'data' => $dataGadai->result(),
          'jumlahData' => intval($totalGadai->total),
          'halaman' => intval($halaman),
          'totalHalaman' => (ceil($totalGadai->total / $maxItem))
        )
      ), 200);
    } else {
      $this->errorUnAuthorized();
    }
  }

  function portofolio_get()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'cif' => $this->query('cif')
      );
      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('cif', 'CIF', 'numeric|required');

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $cif = $this->query('cif');
        $iReq = $this->getPortofolioPinjaman($cif, $token->channelId);
        if ($iReq->responseCode == '00') {
          $data = json_decode($iReq->data);
          $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $data
          ), 200);
        } else if ($iReq->responseCode == '14') {
          $this->response(array(
            'code' => 101,
            'status' => 'error',
            'message' => $iReq->responseDesc,
          ), 200);
        } else {
          $this->response(array(
            'code' => 101,
            'status' => 'error',
            'message' => 'Internal Server Error',
          ), 200);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function portofolio_detail_get()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'norek' => $this->query('norek')
      );
      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('norek', 'Nomor Kredit', 'numeric|required');

      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $norek = $this->query('norek');
        $iReq = $this->getPortofolioDetailPinjaman($norek, $token->channelId);
        if ($iReq->responseCode == '00') {
          $data = json_decode($iReq->data);

          // get kode cabang
          $cabang = $this->MasterModel->getSingleCabang($data->kodeCabang);

          $dataResponse = array(
            'noKredit' => $data->noKontrak,
            'namaNasabah' => $data->namaNasabah,
            'kodeCabang' => $data->kodeCabang,
            'namaCabang' => $cabang->nama,
            'tglKredit' => $data->tglKredit,
            'tglJatuhTempo' => $data->tglJatuhTempo,
            'tenor' => $data->tenor,
            'up' => $data->up,
            'sisaPokok' => $data->sisaPokok,
            'angsuran' => $data->angsuran,
            'kodeProduk' => $data->kodeProduk,
            'namaProduk' => $data->namaProduk
          );

          $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $dataResponse
          ), 200);
        } else if ($iReq->responseCode == '14') {
          $this->response(array(
            'code' => 101,
            'status' => 'error',
            'message' => $iReq->responseDesc,
          ), 200);
        } else {
          $this->response(array(
            'code' => 101,
            'status' => 'error',
            'message' => 'Internal Server Error',
            'data' => $iReq
          ), 200);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  /**
   * Endpoint untuk melakukan inquiry payment gadai
   */
  function inquiry_payment_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = array(
        'amount' => $this->post('amount'),
        'no_kredit' => $this->post('no_kredit'),
        'jenis_transaksi' => $this->post('jenis_transaksi')
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('amount', 'amount', 'required|integer');
      $this->form_validation->set_rules('no_kredit', 'amount', 'required|numeric|exact_length[16]');
      $this->form_validation->set_rules('jenis_transaksi', 'jenis_transaksi', 'required|exact_length[2]');

      if ($this->form_validation->run() == FALSE) {
        $this->set_response(array(
          'status' => 'error',
          'message' => 'Invalid Input',
          'code' => 101,
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $amount = $this->post('amount');
        $norek = $this->post('no_kredit');
        $jenisTransaksi = $this->post('jenis_transaksi');


        $inquiry = $this->inquiryGadai($amount, $jenisTransaksi, $norek, $token->channelId);

        if ($inquiry->responseCode == '00') {
          $rawData = json_decode($inquiry->data);

          //Convert object response ke dalam array untuk penyimpanan data
          $saveData = json_decode(json_encode($rawData), True);
          $saveData['user_AIID'] = $token->id;

          $this->GadaiModel->savePayment($saveData);

          $res = $saveData;
          $res['idTransaksi'] = $saveData['reffSwitching'];
          $biayaChannel = array(
            'BNI' => $this->ConfigModel->getBiayaPayment($jenisTransaksi, 'BANK', '01', '009'),
            'MANDIRI' => $this->ConfigModel->getBiayaPayment($jenisTransaksi, 'BANK', '01', '008'),
            'WALLET' => $this->ConfigModel->getBiayaPayment($jenisTransaksi, 'WALLET', '01', ''),
            'VA_BCA' => $this->ConfigModel->getBiayaPayment($jenisTransaksi, 'BANK', '01', '014'),
            'VA_MANDIRI' => $this->ConfigModel->getBiayaPayment($jenisTransaksi, 'BANK', '01', '008'),
            'VA_PERMATA' => $this->ConfigModel->getBiayaPayment($jenisTransaksi, 'BANK', '01', '013'),
            'VA_BRI' => $this->ConfigModel->getBiayaPayment($jenisTransaksi, 'BANK', '01', '002')

          );
          $res['biayaChannel'] = $biayaChannel;
          $res['bookingCode'] = sprintf("%06d", mt_rand(1, 999999));

          //Mendapatkan nama product berdasarkan kode dan jenis transaksi
          $productCode = substr($norek, 7, 2);
          $productName = $this->GadaiModel->getProductName($jenisTransaksi, $productCode);

          $res['namaProduk'] = $productName;



          $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $res
          ), 200);
        } else {
          $this->set_response(array(
            'status' => 'error',
            'message' => 'Gagal melakukan inquiry',
            'code' => 103,
            'reason' => $inquiry
          ), 200);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function payment_post()
  {
    $token = $this->getToken();

    if ($token) {
      $user = $this->User->getUser($token->id);
      $setData = array(
        'id_transaksi' => $this->post('id_transaksi'),
        'payment' => $this->post('payment'),
        'booking_code' => $this->post('booking_code'),
        'card_number' => $this->post('card_number'),
        'token_response' => $this->post('token_response'),
        'pin' => $this->post('pin'),
        'va' => $this->post('va'),
      );

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
      $this->form_validation->set_rules('payment', 'payment', 'required');

      $payment = $this->post('payment');

      if ($payment == 'MANDIRI') {
        $this->form_validation->set_rules('booking_code', 'booking_code', 'required');
        $this->form_validation->set_rules('card_number', 'card_number', 'required');
        $this->form_validation->set_rules('token_response', 'token_response', 'required');
      }

      if ($payment == 'WALLET' || $payment == 'GCASH') {
        $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');
      }

      if ($payment == 'GCASH') {
        $this->form_validation->set_rules('va', 'va', 'required|numeric');
      }

      if ($this->form_validation->run() == FALSE) {
        $this->set_response(array(
          'status' => 'error',
          'message' => 'Invalid input',
          'code' => 101,
          'errors' => $this->form_validation->error_array()
        ), 200);
      } else {
        $idTransaksi = $this->post('id_transaksi');

        //Mendapatkan payment berdasarkan id transaksi
        $checkPayment = $this->GadaiModel->getPaymentByTrxId($idTransaksi);

        if ($checkPayment) {
          $productCode = substr($checkPayment->norek, 7, 2);
          if ($payment == 'BNI') {
            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, '009');
            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, '009');

            log_message("debug", "BIAYA_TRANSAKSI_BAYAR_GADAI_BNI: " . $biayaTransaksi);

            $createBillingBNI = $this->createBillingVABNI(
              $checkPayment->totalKewajiban + $biayaTransaksi,
              $user->email,
              $user->nama,
              $token->no_hp,
              'K',
              $checkPayment->jenisTransaksi,
              "PDS" . $checkPayment->jenisTransaksi . " " . $idTransaksi . " " . $checkPayment->norek,
              $checkPayment->norek,
              $productCode,
              $idTransaksi,
              $token->channelId
            );

            if ($createBillingBNI->responseCode == '00') {

              if (!isset($createBillingBNI->data)) {
                $this->set_response(array(
                  'status' => 'error',
                  'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                  'code' => 103,
                  'reason' => $createBillingBNI
                ), 200);
                return;
              }

              $billingData = json_decode($createBillingBNI->data);


              $virtualAccount = $billingData->virtualAccount;
              $tglExpired = $billingData->tglExpired;

              $updatePaymentData = array(
                'tipe' => $payment,
                'virtual_account' => $virtualAccount,
                'tanggal_expired' => $tglExpired,
                'payment' => $payment,
                'biayaTransaksi' => $biayaTransaksiDisplay
              );

              //Buat payment baru
              //Simpan payment
              $this->GadaiModel->updatePayment($idTransaksi, $updatePaymentData);


              $template = $this->generatePaymentNotif(
                $token->nama,
                $idTransaksi
              );

              $mobileTemplate = $template['mobile'];
              $emailTemplate = $template['email'];
              $minimalTemplate = $template['minimal'];

              //Simpan notifikasi baru
              $notifId = $this->NotificationModel->add(
                $token->id,
                NotificationModel::TYPE_GADAI,
                NotificationModel::CONTENT_TYPE_HTML,
                $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                "Pembayaran Gadai",
                $mobileTemplate,
                $minimalTemplate,
                "GD"
              );

              //Kirim notifikasi pembayaran ke device user
              Message::sendFCMNotif(
                $this->User->getFCMToken($token->id),
                [
                  "id" => $notifId,
                  "tipe" => "GD",
                  "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                  "tagline" => "Pembayaran Gadai",
                  "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                  "token" => $token->no_hp
                ]
              );

              //Kirim Email Notifikasi
              $this->load->helper('message');
              Message::sendEmail($token->email, "Pembayaran Gadai " . $checkPayment->norek, $emailTemplate);


              //Set response
              $this->set_response([
                'status' => 'success',
                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                'data' => array(
                  'idTransaksi' => $idTransaksi,
                  'virtualAccount' => $virtualAccount,
                  'expired' => $tglExpired,
                  'now' => date('Y-m-d H:i:s')
                )
              ], 200);
            } else {
              log_message('debug', 'Create VA error. Reason:' . json_encode($createBillingBNI));
              $this->set_response(array(
                'status' => 'error',
                'message' => $createBillingBNI->responseDesc,
                'code' => 103,
                'reason' => $createBillingBNI
              ), 200);
            }
          } else if ($payment == 'MANDIRI') {

            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, '008');
            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, '008');
            log_message("debug", "BIAYA_TRANSAKSI_BAYAR_GADAI_MANDIRI: " . $biayaTransaksi);

            $bookingCode = $this->post('booking_code');
            $cardNumber = $this->post('card_number');
            $tokenResponse = $this->post('token_response');

            $clickPay = $this->mandiriClickPay(
              $checkPayment->totalKewajiban + $biayaTransaksi,
              $bookingCode,
              $cardNumber,
              $checkPayment->jenisTransaksi,
              'Pembayaran Gadai',
              $user->no_hp,
              $checkPayment->norek,
              $productCode,
              $tokenResponse,
              $idTransaksi,
              $token->channelId
            );

            if ($clickPay->responseCode == '00') {

              $clickpayData = json_decode($clickPay->data);

              $updateData = [];
              $updateData['bookingCode'] = $bookingCode;
              $updateData['cardNumber'] = $cardNumber;
              $updateData['tokenResponse'] = $tokenResponse;
              $updateData['reffBiller'] = $clickpayData->reffBiller;
              $updateData['kodeBankPembayar'] = '008';
              $updateData['payment'] = $payment;
              $updateData['biayaTransaksi'] = $biayaTransaksiDisplay;

              //update data pembayaran
              $this->GadaiModel->updatePayment($checkPayment->reffSwitching, $updateData);

              //Set response
              $this->set_response([
                'status' => 'success',
                'message' => 'Pembayaran Gadai Berhasil ',
                'data' => $updateData
              ], 200);
            } else {
              $this->set_response(array(
                'status' => 'error',
                'code' => 103,
                'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                'reason' => $clickPay
              ));
            }
          } else if ($payment == 'WALLET' || $payment == 'GCASH') {

            $pin = $this->post('pin');
            $va = $this->post('va');

            //Check user pin
            if (!$this->User->isValidPIN2($token->id, $pin)) {
              $this->set_response(array(
                'status' => 'error',
                'message' => 'PIN tidak valid',
                'code' => 102
              ), 200);
              return;
            }

            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi, 'WALLET', $productCode, '');
            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($checkPayment->jenisTransaksi, 'WALLET', $productCode, '');

            $paymentData = array(

              'jenisTransaksi' => $checkPayment->jenisTransaksi,
              'minimalUpCicil' => $checkPayment->minimalUpCicil,
              'norek' => $checkPayment->norek,
              'reffSwitching' => $idTransaksi,
              'flag' => 'K',
              'productCode' => $productCode,
              'channelId' => $token->channelId
            );

            if ($payment == 'WALLET') {
              $paymentData['amount'] = $checkPayment->totalKewajiban + $biayaTransaksi;
              $paymentData['paymentMethod'] = 'WALLET';
              $paymentData['norekWallet'] = $user->norek;
              $paymentData['walletId'] = $user->no_hp;
            } else if ($payment == 'GCASH') {
              $paymentData['amount'] = $checkPayment->totalKewajiban;
              $paymentData['paymentMethod'] = 'GCASH';
              $paymentData['gcashId'] = $va;
            }

            $walletPayment = $this->paymentGadai($paymentData);

            if ($walletPayment->responseCode == '00') {

              if (!isset($walletPayment->data)) {
                $this->set_response(array(
                  'status' => 'error',
                  'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                  'code' => 103
                ), 200);
                return;
              }


              $walletData = json_decode($walletPayment->data, TRUE);

              //update payment data
              $walletData['payment'] = $payment;
              $walletData['reffSwitching'] = $idTransaksi;
              $walletData['paid'] = '1';

              $this->GadaiModel->updatePayment($idTransaksi, $walletData);

              $template = $this->generatePaymentNotif(
                $token->nama,
                $idTransaksi
              );

              $mobileTemplate = $template['mobile'];
              $emailTemplate = $template['email'];
              $minimalTemplate = $template['minimal'];

              //Simpan notifikasi baru
              $notifId = $this->NotificationModel->add(
                $token->id,
                NotificationModel::TYPE_GADAI,
                NotificationModel::CONTENT_TYPE_HTML,
                $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                "Pembayaran KCA",
                $mobileTemplate,
                $minimalTemplate,
                "GD"
              );

              //Kirim notifikasi pembayaran ke device user
              Message::sendFCMNotif(
                $this->User->getFCMToken($token->id),
                [
                  "id" => $notifId,
                  "tipe" => "GD",
                  "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                  "tagline" => "Pembayaran Gadai " . $checkPayment->norek . ' Sukses',
                  "content" => "Pembayaran Gadai " . $checkPayment->norek . ' Sukses',
                  "paymentType" => $payment,
                  "token" => $token->no_hp
                ]
              );

              //Kirim Email Notifikasi
              $this->load->helper('message');
              Message::sendEmail($token->email, "Pembayaran Gadai " . $checkPayment->norek, $emailTemplate);

              //Update Saldo Wallet
              $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
              $walletData['wallet'] = $saldoWallet;

              //Set response
              $this->set_response([
                'status' => 'success',
                'message' => "Pembayaran Gadai " . $checkPayment->norek . ' Sukses',
                'data' => $walletData
              ], 200);
            } else {
              $this->set_response(array(
                'status' => 'error',
                'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                'code' => 103,
                'reason' => $walletPayment
              ), 200);
            }
          }
          if ($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI') {
            $kodeBank = '';
            if ($payment == 'VA_BCA') {
              $kodeBank = '014';
            } else if ($payment == 'VA_MANDIRI') {
              $kodeBank = '008';
            } else if ($payment == 'VA_BRI') {
              $kodeBank = '002';
            }

            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, $kodeBank);
            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, $kodeBank);

            log_message("debug", "BIAYA_TRANSAKSI_BAYAR_GADAI_BNI: " . $biayaTransaksi);

            $createBillingData = [
              'channelId' => $token->channelId,
              'amount' => $checkPayment->totalKewajiban + $biayaTransaksi,
              'customerEmail' => $user->email,
              'customerName' => $user->nama,
              'customerPhone' => $user->no_hp,
              'flag' => 'K',
              'jenisTransaksi' => $checkPayment->jenisTransaksi,
              'kodeProduk' => $productCode,
              'norek' => $checkPayment->norek,
              'keterangan' => "PDS" . $checkPayment->jenisTransaksi . " " . $idTransaksi . " " . $checkPayment->norek,
              'reffSwitching' => $idTransaksi,
              'kodeBank' => $kodeBank
            ];

            $createBilling = $this->createBillingPegadaian($createBillingData);

            if ($createBilling->responseCode == '00') {

              if (!isset($createBilling->data)) {
                $this->set_response(array(
                  'status' => 'error',
                  'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                  'code' => 103,
                  'reason' => $createBilling
                ), 200);
                return;
              }

              $billingData = json_decode($createBilling->data);


              $virtualAccount = $billingData->vaNumber;
              $tglExpired = $billingData->tglExpired;

              $updatePaymentData = array(
                'tipe' => $payment,
                'virtual_account' => $virtualAccount,
                'tanggal_expired' => $tglExpired,
                'payment' => $payment,
                'biayaTransaksi' => $biayaTransaksiDisplay
              );

              //Buat payment baru
              //Simpan payment
              $this->GadaiModel->updatePayment($idTransaksi, $updatePaymentData);


              $template = $this->generatePaymentNotif(
                $token->nama,
                $idTransaksi
              );

              $mobileTemplate = $template['mobile'];
              $emailTemplate = $template['email'];
              $minimalTemplate = $template['minimal'];

              //Simpan notifikasi baru
              $notifId = $this->NotificationModel->add(
                $token->id,
                NotificationModel::TYPE_GADAI,
                NotificationModel::CONTENT_TYPE_HTML,
                $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                "Pembayaran Gadai",
                $mobileTemplate,
                $minimalTemplate,
                "GD"
              );

              //Kirim notifikasi pembayaran ke device user
              Message::sendFCMNotif(
                $this->User->getFCMToken($token->id),
                [
                  "id" => $notifId,
                  "tipe" => "GD",
                  "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                  "tagline" => "Pembayaran Gadai",
                  "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                  "token" => $token->no_hp
                ]
              );

              //Kirim Email Notifikasi
              $this->load->helper('message');
              Message::sendEmail($token->email, "Pembayaran Gadai " . $checkPayment->norek, $emailTemplate);


              //Set response
              $this->set_response([
                'status' => 'success',
                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                'data' => array(
                  'idTransaksi' => $idTransaksi,
                  'virtualAccount' => $virtualAccount,
                  'expired' => $tglExpired,
                  'now' => date('Y-m-d H:i:s')
                )
              ], 200);
            } else {
              log_message('debug', 'Create VA error. Reason:' . json_encode($createBilling));
              $this->set_response(array(
                'status' => 'error',
                'message' => $createBilling->responseDesc,
                'code' => 103,
                'reason' => $createBilling
              ), 200);
            }
          }
          if ($payment == 'VA_PERMATA') {
            $kodeBank = '013';
            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, $kodeBank);
            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, $kodeBank);

            log_message("debug", "BIAYA_TRANSAKSI_BAYAR_GADAI_BNI: " . $biayaTransaksi);

            $createBillingData = [
              'channelId' => $token->channelId,
              'amount' => $checkPayment->totalKewajiban + $biayaTransaksi,
              'customerEmail' => $user->email,
              'customerName' => $user->nama,
              'customerPhone' => $user->no_hp,
              'flag' => 'K',
              'jenisTransaksi' => $checkPayment->jenisTransaksi,
              'productCode' => $productCode,
              'norek' => $checkPayment->norek,
              'keterangan' => "PDS" . $checkPayment->jenisTransaksi . " " . $idTransaksi . " " . $checkPayment->norek,
              'trxId' => $idTransaksi,
            ];

            $createBilling = $this->createBillingPermata($createBillingData);

            if ($createBilling->responseCode == '00') {

              if (!isset($createBilling->data)) {
                $this->set_response(array(
                  'status' => 'error',
                  'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                  'code' => 103,
                  'reason' => $createBilling
                ), 200);
                return;
              }

              $billingData = json_decode($createBilling->data);


              $virtualAccount = $billingData->virtualAccount;
              $tglExpired = $billingData->tglExpired;

              $updatePaymentData = array(
                'tipe' => $payment,
                'virtual_account' => $virtualAccount,
                'tanggal_expired' => $tglExpired,
                'payment' => $payment,
                'biayaTransaksi' => $biayaTransaksiDisplay
              );

              //Buat payment baru
              //Simpan payment
              $this->GadaiModel->updatePayment($idTransaksi, $updatePaymentData);


              $template = $this->generatePaymentNotif(
                $token->nama,
                $idTransaksi
              );

              $mobileTemplate = $template['mobile'];
              $emailTemplate = $template['email'];
              $minimalTemplate = $template['minimal'];

              //Simpan notifikasi baru
              $notifId = $this->NotificationModel->add(
                $token->id,
                NotificationModel::TYPE_GADAI,
                NotificationModel::CONTENT_TYPE_HTML,
                $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                "Pembayaran Gadai",
                $mobileTemplate,
                $minimalTemplate,
                "GD"
              );

              //Kirim notifikasi pembayaran ke device user
              Message::sendFCMNotif(
                $this->User->getFCMToken($token->id),
                [
                  "id" => $notifId,
                  "tipe" => "GD",
                  "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                  "tagline" => "Pembayaran Gadai",
                  "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                  "token" => $token->no_hp
                ]
              );

              //Kirim Email Notifikasi
              $this->load->helper('message');
              Message::sendEmail($token->email, "Pembayaran Gadai " . $checkPayment->norek, $emailTemplate);


              //Set response
              $this->set_response([
                'status' => 'success',
                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                'data' => array(
                  'idTransaksi' => $idTransaksi,
                  'virtualAccount' => $virtualAccount,
                  'expired' => $tglExpired,
                  'now' => date('Y-m-d H:i:s')
                )
              ], 200);
            } else {
              log_message('debug', 'Create VA error. Reason:' . json_encode($createBilling));
              $this->set_response(array(
                'status' => 'error',
                'message' => $createBilling->responseDesc,
                'code' => 103,
                'reason' => $createBilling
              ), 200);
            }
          }
        } else {
          log_message('debug', 'Payment not found' . $this->post('id_transaksi'));
          $this->set_response(array(
            'status' => 'success',
            'message' => 'Payment not found',
            'code' => 102
          ), 200);
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  /**
   * Endpoint untuk mendapatkan data payment gadai user
   */
  function payment_get()
  {
    $token = $this->getToken();
    if ($token) {
      $payment = $this->GadaiModel->getUserPayment($token->id);
      $this->set_response(array(
        'status' => 'success',
        'message' => '',
        'data' => $payment
      ), 200);
    } else {
      $this->errorUnAuthorized();
    }
  }

  function check_payment_gadai_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = [
        'kode_booking' => $this->post('kode_booking'),
      ];
      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('kode_booking', 'kode_booking', 'required');
      if ($this->form_validation->run() == false) {
        $this->set_response([
          'status' => 'error',
          'message' => 'Invalid input',
          'code' => 102,
          'errors' => $this->form_validation->error_array()
        ]);
      } else {
        $kode_booking = $this->post('kode_booking');
        $data = $this->GadaiModel->getPaymentByBookingCode($kode_booking);
        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'data' => $data
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function check_kode_booking_logam_mulia_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = [
        'kode_booking' => $this->post('kode_booking'),
      ];
      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('kode_booking', 'kode Booking', 'required|integer');
      if ($this->form_validation->run() == false) {
        $this->set_response([
          'status' => 'error',
          'message' => 'Invalid input',
          'code' => 102,
          'errors' => $this->form_validation->error_array()
        ]);
      } else {
        $kode_booking = $this->post('kode_booking');
        $data = $this->GadaiModel->getByKodeBookingLogamMulia($kode_booking);
        if ($data->status == 1) {
          $screen_name = 'PickupAndDeliveryServiceLM';
        } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 0) {
          $screen_name = 'LookingDriverLM';
        } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 1) {
          $screen_name = 'SerahTerimaBarangLM';
        } elseif ($data->status == 6) {
          $screen_name = 'DetailTransaksiAfterGopayLM';
        } elseif ($data->status == 7 && $data->respon_nasabah == 1) {
          $screen_name = 'PengajuanDisetujuiLM';
        } elseif ($data->status == 8) {
          $screen_name = 'LookingDriverLM';
        } elseif ($data->status == 9) {
          $screen_name = 'DetailTransaksiAfterGopayLM';
        } elseif ($data->status == 92) {
          $screen_name = 'PengambilanBarangLM';
        } elseif ($data->status == 7 && $data->respon_nasabah == 2 && $data->otp == 0) {
          $screen_name = 'VerifyOTP';
        } elseif ($data->status == 7 && $data->respon_nasabah == 2 && $data->otp == 1) {
          $screen_name = 'PilihNoRekeningPenerima';
        }
        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'kode_booking' => $kode_booking,
          'data' => $data,
          'screen_name' => $screen_name,
          'no_order_gosend' => $data->no_order_gosend
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function check_kode_booking_perhiasan_post()
  {
    $token = $this->getToken();
    if ($token) {
      $setData = [
        'kode_booking' => $this->post('kode_booking'),
      ];
      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('kode_booking', 'kode Booking', 'required|integer');
      if ($this->form_validation->run() == false) {
        $this->set_response([
          'status' => 'error',
          'message' => 'Invalid input',
          'code' => 102,
          'errors' => $this->form_validation->error_array()
        ]);
      } else {
        $kode_booking = $this->post('kode_booking');
        $data = $this->GadaiModel->getByKodeBookingPerhiasan($kode_booking);
        if ($data->status == 1) {
          $screen_name = 'PickupAndDeliveryService';
        } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 0) {
          $screen_name = 'LookingDriver';
        } elseif ($data->row()->status == 5 && $data->row()->status_fcm == 1) {
          $screen_name = 'SerahTerimaBarang';
        } elseif ($data->status == 6) {
          $screen_name = 'DetailTransaksiAfterGopay';
        } elseif ($data->status == 7 && $data->respon_nasabah == 1) {
          $screen_name = 'PengajuanDisetujui';
        } elseif ($data->status == 8) {
          $screen_name = 'LookingDriver';
        } elseif ($data->status == 9) {
          $screen_name = 'DetailTransaksiAfterGopay';
        } elseif ($data->status == 92) {
          $screen_name = 'PengambilanBarang';
        } elseif ($data->status == 7 && $data->respon_nasabah == 2 && $data->otp == 0) {
          $screen_name = 'VerifyOTP';
        } elseif ($data->status == 7 && $data->respon_nasabah == 2 && $data->otp == 1) {
          $screen_name = 'PilihNoRekeningPenerima';
        }
        $this->set_response(array(
          'status' => 'success',
          'message' => '',
          'kode_booking' => $kode_booking,
          'data' => $data,
          'screen_name' => $screen_name,
          'no_order_gosend' => $data->no_order_gosend
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function check_promo_post()
  {
    $token = $this->getToken();

    if ($token) {

      $setData = [
        'promo' => $this->post('promo'),
        'nilai_transaksi' => $this->post('nilai_transaksi'),
        'tenor' => $this->input->post('tenor')
      ];

      $this->form_validation->set_data($setData);

      $this->form_validation->set_rules('promo', 'promo', 'required');
      $this->form_validation->set_rules('nilai_transaksi', 'nilai_transaksi', 'required|numeric');

      if ($this->post('tenor') != null) {
        $this->form_validation->set_rules('tenor', 'tenor', 'required|integer');
      }

      if ($this->form_validation->run() == false) {
        $this->set_response([
          'status' => 'error',
          'message' => 'Invalid input',
          'code' => 102,
          'errors' => $this->form_validation->error_array()
        ]);
      } else {
        $user = $this->User->getUser($token->id);

        if ($user->no_ktp == '') {
          $this->set_response([
            'status' => 'error',
            'message' => 'Lengkapi identitas pada profile terlebih dahulu!',
            'code' => 102
          ]);
          return;
        }

        $promo = $this->post('promo');
        $nilaiTransaksi = $this->post('nilai_transaksi');
        $tenor = $this->post('tenor') == null ? 0 : $this->post('tenor');

        // Check validitas promo
        $data = $this->GadaiModel->checkPromo($promo, $nilaiTransaksi, $tenor);

        if ($data['status'] == 'error') {
          $this->set_response([
            'status' => 'error',
            'code' => 102,
            'message' => $data['message']
          ]);
        } else {
          // Data check promo core
          $checkData = [
            'channelId' => $token->channelId,
            'jenisPromo' => strtoupper($promo),
            'nik' => $user->no_ktp,
            'flag' => 'K',
            'noHp' => $user->no_hp,
            'nilaiTransaksi' => $nilaiTransaksi
          ];

          // Check promo core
          $cekPromo = $this->checkPromo($checkData);

          if ($cekPromo->responseCode == '00') {
            $cekPromoData = json_decode($cekPromo->data);

            if ($cekPromoData->status == "1") {
              $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $data['data']
              ]);
            } else {
              $this->set_response([
                'status' => 'error',
                'message' => 'Kode promo tidak valid',
                'code' => '102'
              ]);
            }
          } else {

            $this->set_response([
              'status' => 'error',
              'message' => 'RC ' . $cekPromo->responseCode . ': ' . $cekPromo->responseDesc,
              'code' => '103'
            ]);
          }
        }
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function promo_get()
  {

    $this->set_response([
      'status' => 'success',
      'message' => '',
      'data' => $this->GadaiModel->getPromo()
    ]);
  }


  /**
   * ========================================================================
   * =========================== GADAI EFEK =================================
   * ========================================================================
   */

  /**
   * Route/endpoint untuk gadai efek
   * @param type $method
   * @param type $option
   */
  function efek_post($method = 'list', $option = 'saham')
  {
    $token = $this->getToken();
    if ($token) {
      if ($method == 'list') {
        $this->_efekList($option);
      } else if ($method == 'simulasi') {
        $this->_efekSimulasi();
      } else if ($method == 'open') {
        $this->_efekOpen($token);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function _efekList($option = 'saham')
  {
    $opt = 'S';
    if ($option == 'obligasi') {
      $opt = 'O';
    }

    $getList = $this->GadaiModel->getEfekList($opt);

    if (is_array($getList)) {
      $this->set_response([
        'status' => 'success',
        'message' => '',
        'data' => $getList
      ]);
    } else {
      $this->set_response([
        'status' => 'error',
        'code' => '103',
        'message' => 'Error: RC ' . $getList,
        'data' => null
      ]);
    }
  }

  function _efekSimulasi()
  {
    $this->form_validation->set_rules('item', 'item', 'required');
    $this->form_validation->set_rules('type', 'type', 'required');
    if ($this->form_validation->run() == false) {
      $this->set_response([
        'status' => 'error',
        'message' => 'Invalid input',
        'code' => 102,
        'errors' => $this->form_validation->error_array()
      ]);
    } else {
      $type = $this->post('type');
      $item = $this->post('item');

      $arrayOrder = json_decode($item, true);

      if (json_last_error() !== JSON_ERROR_NONE) {
        $this->set_response(array(
          'status' => 'error',
          'message' => 'Invalid input item. Not valid JSON: ' . json_last_error_msg(),
          'code' => 101
        ), 200);
        return;
      }

      $collaterals = [];
      foreach ($arrayOrder as $o) {
        if (isset($o['qty']) && isset($o['kode'])) {
          $collaterals[] = [
            'kodeEfek' => $o['kode'],
            'qtyEfek' => $o['qty']
          ];
        }
      }

      log_message('debug', 'Collaterals: ' . json_encode($collaterals));

      $data = [
        'collaterals' => $collaterals,
        'tipeEfek' => $type == 'saham' ? 'S' : 'O',
      ];

      $simulasi = $this->efekSimulasi($data);

      if (is_object($simulasi)) {
        $simulasi->biayaAdmin = $simulasi->biayaAdmin * 100;
        $this->set_response([
          'status' => 'success',
          'message' => '',
          'data' => $simulasi
        ]);
      } else {
        $this->set_response([
          'status' => 'error',
          'code' => '103',
          'message' => 'Error: RC ' . $simulasi,
          'data' => null
        ]);
      }
    }
  }

  function _efekOpen($token)
  {
    // Syarat gadai efek, data user mandatory
    log_message('debug', 'USERID:' . $token->id);
    if ($this->User->isIdentityOk($token->id, true)) {
      $this->form_validation->set_rules('jenis_efek', 'jenis_efek', 'required');
      $this->form_validation->set_rules('item', 'item', 'required');
      $this->form_validation->set_rules('tenor', 'tenor', 'required|integer');
      $this->form_validation->set_rules('total_taksiran', 'total_taksiran', 'required');
      $this->form_validation->set_rules('up', 'up', 'required');
      $this->form_validation->set_rules('minup', 'minup', 'required|integer');
      $this->form_validation->set_rules('maxup', 'maxup', 'required|integer');
      $this->form_validation->set_rules('maxup', 'maxup', 'required|integer');

      if ($this->form_validation->run() == false) {
        $this->set_response([
          'status' => 'error',
          'message' => 'Invalid input',
          'code' => 101,
          'errors' => $this->form_validation->error_array(),
        ]);
      } else {
        $user = $this->User->getUser($token->id);

        $jenisEfek = $this->post('jenis_efek');
        $tenor = $this->post('tenor');
        $totalTaksiran = $this->post('total_taksiran');
        $minup = $this->post('minup');
        $maxup = $this->post('maxup');
        $up = $this->post('up');
        $item = $this->post('item');
        $idBank = $this->post('id_bank');

        $jaminanEfek = [];

        $isValidJson = true;

        $arrayOrder = json_decode($item, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
          return $this->set_response(array(
            'status' => 'error',
            'message' => 'Invalid input item. Not valid JSON: ' . json_last_error_msg(),
            'code' => 101
          ), 200);
        }

        // $item adalah array of object nama, kode, qty, dan satuan
        foreach ($arrayOrder as $o) {
          if (isset($o['qty']) && isset($o['kode']) && isset($o['satuan']) && isset($o['nama'])) {
            $jaminanEfek[] = [
              'kodeEfek' => $o['kode'],
              'jumlah' => $o['qty'],
            ];
          } else {
            $isValidJson = false;
            break;
          }
        }

        if (!$isValidJson) {
          return $this->set_response([
            'status' => 'error',
            'message' => 'Invalid input: item harus teridiri dari kode, nama, qty, dan satuan',
            'code' => 102
          ]);
        }

        $_bookingId = $this->GadaiModel->generateBooking(self::Efek, self::EfekStartValue);
        $openData = [
          'dataEfek' => array([
            // Backend data
            'clientId' => $this->config->item('core_post_username'),
            'channelId' => $token->channelId,
            'flag' => 'K',
            'kodeCabang' => '00050',
            'kodeProduk' => '36',
            'rubrik' => 'EF',
            // Customer data
            'namaNasabah' => $user->nama,
            'noHp' => $user->no_hp,
            'cif' => $user->cif,
            'ibuKandung' => $user->nama_ibu,
            'jalan' => $user->alamat,
            'kewarganegaraan' => $user->kewarganegaraan,
            'tipeIdentitas' => '10',
            'noIdentitas' => $user->no_ktp,
            'tanggalLahir' => $user->tgl_lahir,
            'tempatLahir' => $user->tempat_lahir,
            'jenisKelamin' => $user->jenis_kelamin,
            'statusKawin' => $user->status_kawin,
            'idKelurahan' => $user->id_kelurahan,
            'sid' => $user->no_sid,
            'sidName' => $user->nama,
            // Data Gadai
            'bookingId' => $_bookingId,
            'tanggalPengajuan' => date('Y-m-d'),
            'jenisEfek' => $jenisEfek,
            'tenor' => $tenor,
            'totalTaksiran' => $totalTaksiran,
            'up' => $up,
            'minUp' => $minup,
            'maxUp' => $maxup,
            // User bank
            'kodeBank' => '',
            'namaNasabahBank' => '',
            'norekBank' => '',
            'status' => '1' //default aktif

          ]),
          'jaminanEfek' => $jaminanEfek,
        ];

        // Bank tidak mandatory, jika user mendefinisikan kode bank, ambil by Id
        if ($idBank != null) {
          // Get user bank
          $userBank = $this->BankModel->getById($idBank);
          if ($userBank) {
            // User bank
            $openData['kodeBank'] = $userBank->kode_bank;
            $openData['namaNasabahBank'] = $userBank->nama_pemilik;
            $openData['norekBank'] = $userBank->nomor_rekening;
          }
        }

        $openEfek = $this->efekOpen($openData);

        log_message('debug', 'BOOKING ID' . json_encode($_bookingId));
        log_message('debug', 'OPEN EFEK DATA' . json_encode($openEfek));

        if ($openEfek->responseCode == '00') {
          $responseData = $openEfek->data;
          $saveData = [
            'user_AIID' => $token->id,
            'bookingId' => $_bookingId,
            'kode_booking' => $_bookingId,
            'noPengajuan' => $responseData->noPengajuan,
            'tanggalPengajuan' => date('Y-m-d'),
            'tglTransaksi' => date('Y-m-d'),
            'up' => $up,
            'jaminanEfek' => $item,
            'namaNasabah' => $token->nama,
            'tenor' => $tenor,
            'minUp' => $minup,
            'maxUp' => $maxup,
            'idBank' => $idBank,
          ];

          $this->GadaiModel->saveGadaiEfek($saveData);

          $saveData['ktp'] = $user->no_ktp;
          $saveData['alamat'] = $user->alamat;
          $saveData['noHp'] = $user->no_hp;
          $saveData['listJaminan'] = json_decode($item, true);
          $saveData['nama'] = $user->nama;

          $template = $this->generateEfekNotif($saveData);

          $mobileTemplate = $template['mobile'];
          $emailTemplate = $template['email'];
          $minimalTemplate = $template['minimal'];

          //Simpan notifikasi baru
          $notifId = $this->NotificationModel->add(
            $token->id,
            NotificationModel::TYPE_GADAI,
            NotificationModel::CONTENT_TYPE_HTML,
            "Gadai Efek",
            "Pengajuan Gadai Berhasil (Kode Booking " .  $saveData['bookingId'] . ")",
            $mobileTemplate,
            $minimalTemplate,
            "GD"
          );

          //Kirim notifikasi pembayaran ke device user
          Message::sendFCMNotif(
            $this->User->getFCMToken($token->id),
            [
              "id" => $notifId,
              "tipe" => "GD",
              "title" => "Gadai Efek",
              "tagline" => "Pengajuan Gadai Berhasil (Kode Booking " .  $saveData['bookingId'] . ")",
              "content" => "",
              "token" => $token->no_hp
            ]
          );

          //Kirim Email Notifikasi
          $this->load->helper('message');
          Message::sendEmail($token->email, "Pengajuan Gadai Berhasil (Kode Booking " .  $saveData['bookingId'] . ")", $emailTemplate);


          $this->set_response([
            'status' => 'success',
            'message' => 'Open Gadai Efek Success',
            'data' => $saveData
          ]);
        } else {
          $this->set_response([
            'status' => 'error',
            'code' => 103,
            'message' => $openEfek->responseCode . ' ' . $openEfek->responseDesc,
            'reason' => $openEfek
          ]);
        }
      }
    } else {
      return $this->set_response([
        'status' => 'error',
        'message' => 'Mohon lengkapi identitas diri anda',
        'code' => 104
      ]);
    }
  }

  /**
   * ========================================================================
   * =========================== END GADAI EFEK =============================
   * ========================================================================
   */

  function norek_nasabah_god_post()
  {
    $token = $this->getToken();

    // check token validity
    if  (!$token) {
      $this->errorUnAuthorized();
      return;
    }

    // set request data and rules
    $reqData = array(
      'kode_bank' => $this->post('kode_bank'),
      'norek_bank' => $this->post('norek_bank'),
      'costumer_nama_bank' => $this->post('costumer_nama_bank'),
      'kode_booking' => $this->post('kode_booking'),
      'jenis_gadai' => $this->post('jenis_gadai'),
    );

    $this->form_validation->set_data($reqData);
    $this->form_validation->set_rules('kode_bank', 'kode_bank', 'required|integer');
    $this->form_validation->set_rules('norek_bank', 'norek_bank', 'required');
    $this->form_validation->set_rules('costumer_nama_bank', 'costumer_nama_bank', 'required');
    $this->form_validation->set_rules('kode_booking', 'kode_booking', 'required');
    $this->form_validation->set_rules('jenis_gadai', 'Jenis Gadai', 'required');

    // check validity request data
    if (!$this->form_validation->run()) {
      $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Invalid Input',
        'errors' => $this->form_validation->error_array()
      ];
      $this->response($response, 200);
      log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
      return;
    }

    // get status by jenis gadai
    if ($reqData['jenis_gadai'] == 'perhiasan') {
      $status = $this->GadaiModel->getStatusPerhiasanByKodeBooking($reqData['kode_booking']);
    } else if ($reqData['jenis_gadai'] == 'logam_mulia') {
      $status = $this->GadaiModel->getStatusLogamMuliaByKodeBooking($reqData['kode_booking']);
    }

    // check status
    if ($status->status == 2) {
      $response = [
        'status' => 'error',
        'message' => 'Kode Booking ' . $reqData['kode_booking'] . ' Sudah ada response Sebelumnya',
        'data' => [ 'kode_booking' => $reqData['kode_booking'], 'status' => $status->status ]
      ];
      $this->set_response($response, 400);
      log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
      return;
    }

    // save data gadai
    $this->GadaiModel->simpanNorekNasabah($reqData['kode_booking'], $reqData['norek_bank'],
      $reqData['kode_bank'], $reqData['costumer_nama_bank'], $reqData['jenis_gadai']);

    // get data notif
    $data = $this->db->select('kode_outlet, user_AIID, jenis_layanan, no_order_gosend')
      ->where('kode_booking', $reqData['kode_booking'])
      ->get('gadai_' . $reqData['jenis_gadai'])->row();

    // save admin notification
    $this->NotificationModel->addAdminNotificaion(
      $data->user_AIID,
      NotificationModel::TYPE_GADAI,
      NotificationModel::CONTENT_TYPE_TEXT,
      "Pegadaian Digital Service",
      "Pinjaman Sukses",
      "",
      "",
      $data->jenis_layanan,
      $data->kode_outlet,
      $reqData['kode_booking']
    );

    // send response
    $response = [
      'status' => 'success',
      'message' => 'No Rekening Tersimpan',
      'data' => [ 'norek' => $reqData['norek_bank'], 'kode_booking' => $reqData['kode_booking'] ]
    ];
    $this->set_response($response, 200);
    log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
  }

  function taksir_ulang_logam_mulia_god_post()
  {
    // $token = $this->getToken();
    if(!$this->authCore()){
      $this->errorUnAuthorized();
      return;            
    }
    // if ($token) {
      // get the HTTP POST body of the request
      $request_body = file_get_contents('php://input');

      $request_body = json_decode($request_body);
      $request_body->listJaminan;

      $setData = array(
        'kodeBooking' => $request_body->kodeBooking,
        'totalTaksiran' => $request_body->totalTaksiran,
        'upMaksimal' => $request_body->upMaksimal,
        'up' => $request_body->up,
        'sewaModal' => $request_body->sewaModal,
        'userPenaksir' => $request_body->userPenaksir,
        'cif' => $request_body->cif,
        'diterimaNasabah' => $request_body->diterimaNasabah,
        'tglLelang' => $request_body->tglLelang,
        'tglJatuhTempo' => $request_body->tglJatuhTempo,
        'tglKredit' => $request_body->tglKredit,
        'biayaAdmin' => $request_body->biayaAdmin,
        'biayaAsuransi' => $request_body->biayaAsuransi,
        'totalSewaModal' => $request_body->totalSewaModal,
      );
      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('kodeBooking', 'kodeBooking', 'required|integer');
      $this->form_validation->set_rules('totalTaksiran', 'totalTaksiran', 'required|numeric');
      $this->form_validation->set_rules('upMaksimal', 'upMaksimal', 'required|numeric');
      $this->form_validation->set_rules('up', 'up', 'required|numeric');
      $this->form_validation->set_rules('sewaModal', 'sewaModal', 'required|numeric');
      $this->form_validation->set_rules('userPenaksir', 'userPenaksir', 'required');
      $this->form_validation->set_rules('cif', 'cif', 'required');
      $this->form_validation->set_rules('diterimaNasabah', 'diterimaNasabah', 'required');
      $this->form_validation->set_rules('tglLelang', 'tglLelang', 'required');
      $this->form_validation->set_rules('tglJatuhTempo', 'tglJatuhTempo', 'required');
      $this->form_validation->set_rules('tglKredit', 'tglKredit', 'required');
      $this->form_validation->set_rules('biayaAdmin', 'biayaAdmin', 'required');
      $this->form_validation->set_rules('biayaAsuransi', 'biayaAsuransi', 'required');
      $this->form_validation->set_rules('totalSewaModal', 'totalSewaModal', 'required');
      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
        log_message('debug', 'taksir_ulang_logam_mulia_god ERROR: '.json_encode($request_body));
      } else {
        log_message('debug', 'taksir_ulang_logam_mulia_god SUKSES: '.json_encode($request_body));
        $cek = $this->GadaiModel->cekKodeBookingLogamMulia($request_body->kodeBooking);
        $status =  $this->GadaiModel->getStatusTaksirUlangLogamMuliaByKodeBooking($request_body->kodeBooking);

        if ($cek == FALSE) {
          $this->set_response(array(
            'status' => 'error',
            'message' => 'Data tidak ditemukan'
          ));
        } elseif ($status->status != 9) {
          $this->set_response(array(
            'status' => 'error',
            'message' => 'Barang jaminan belum sampai ke outlet',
            'data' => $status
          ));
        } elseif ($cek == TRUE) {
          // insert or update to table users
          $cif = array('cif' => $request_body->cif);
          if ($cif != Null) {
            $this->db->where('user_AIID', $status->user_AIID)->update('user', $cif);
          } else {
            $this->db->where('user_AIID', $status->user_AIID)->insert('user', $cif);
          }

          // insert or update to table payment_gadai
          $paymentGadai = array(
            'user_AIID' => $status->user_AIID,
            'administrasi' => $request_body->biayaAdmin,
            'asuransi' => $request_body->biayaAsuransi,
            'tglLelang' => date('Y-m-d', strtotime($request_body->tglLelang)),
            'tglJatuhTempo' => date('Y-m-d', strtotime($request_body->tglJatuhTempo)),
            'tglKredit' => date('Y-m-d', strtotime($request_body->tglKredit)),
            'sewaModal' => $request_body->sewaModal,
            'biayaLelang' => $request_body->biayaLelang ?: '2',
            'biayaProsesLelang' => $request_body->biayaProsesLelang ?: '1',
            'biayaAdmBjpdl' => $request_body->biayaAdmBjpdl ?: '1',
            'biayaBjdplMax' => $request_body->biayaBjdplMax ?: '6',
            'up' => $request_body->up,
            'namaProduk' => 'GOD'
          );
          $cekBookingCode = $this->GadaiModel->getPaymentByBookingCode($request_body->kodeBooking);

          if ($cekBookingCode == TRUE) {
            $this->db->where('bookingCode', $request_body->kodeBooking)->update('payment_gadai', $paymentGadai);
          } else {
            // saving to table payment_gadai
            $paymentGadai['bookingCode'] = $request_body->kodeBooking;
            $this->db->insert('payment_gadai', $paymentGadai);
          }

          // update to table gadai
          $gadaiLM = array(
            'taksir_ulang_admin' => $request_body->totalTaksiran,
            'upMaksimal' => $request_body->upMaksimal,
            'up' => $request_body->up,
            'sewa_modal' => $request_body->sewaModal,
            'user_penaksir' => $request_body->userPenaksir,
            'diterimaNasabah' => $request_body->diterimaNasabah,
            'tanggal_lelang' => date('Y-m-d', strtotime($request_body->tglLelang)),
            'tanggal_jatuh_tempo' => date('Y-m-d', strtotime($request_body->tglJatuhTempo)),
            'tanggal_kredit' => date('Y-m-d', strtotime($request_body->tglKredit)),
            'sewa_modal_maksimal' => $request_body->totalSewaModal,
            'list_jaminan' => json_encode($request_body->listJaminan)
          );

          $this->db->where('kode_booking', $request_body->kodeBooking)->update('gadai_logam_mulia', $gadaiLM);

          $this->set_response(array(
            'status' => 'success',
            'message' => 'Data Berhasil di update',
            'data' => $request_body
          ), 200);
        }
      }
    // } else {
    //   $this->errorUnAuthorized();
    // }
  }

  function taksir_ulang_perhiasan_god_post()
  {
    // $token = $this->getToken();
    if(!$this->authCore()){
      $this->errorUnAuthorized();
      return;            
    }
    // if ($token) {
      // get the HTTP POST body of the request
      $request_body = file_get_contents('php://input');

      $request_body = json_decode($request_body);
      $request_body->listJaminan;

      $setData = array(
        'kodeBooking' => $request_body->kodeBooking,
        'totalTaksiran' => $request_body->totalTaksiran,
        'upMaksimal' => $request_body->upMaksimal,
        'up' => $request_body->up,
        'sewaModal' => $request_body->sewaModal,
        'userPenaksir' => $request_body->userPenaksir,
        'cif' => $request_body->cif,
        'diterimaNasabah' => $request_body->diterimaNasabah,
        'tglLelang' => $request_body->tglLelang,
        'tglJatuhTempo' => $request_body->tglJatuhTempo,
        'tglKredit' => $request_body->tglKredit,
        'biayaAdmin' => $request_body->biayaAdmin,
        'biayaAsuransi' => $request_body->biayaAsuransi,
        'totalSewaModal' => $request_body->totalSewaModal,
      );
      $this->form_validation->set_data($setData);
      $this->form_validation->set_rules('kodeBooking', 'kodeBooking', 'required|integer');
      $this->form_validation->set_rules('totalTaksiran', 'totalTaksiran', 'required|numeric');
      $this->form_validation->set_rules('upMaksimal', 'upMaksimal', 'required|numeric');
      $this->form_validation->set_rules('up', 'up', 'required|numeric');
      $this->form_validation->set_rules('sewaModal', 'sewaModal', 'required|numeric');
      $this->form_validation->set_rules('userPenaksir', 'userPenaksir', 'required');
      $this->form_validation->set_rules('cif', 'cif', 'required');
      $this->form_validation->set_rules('diterimaNasabah', 'diterimaNasabah', 'required');
      $this->form_validation->set_rules('tglLelang', 'tglLelang', 'required');
      $this->form_validation->set_rules('tglJatuhTempo', 'tglJatuhTempo', 'required');
      $this->form_validation->set_rules('tglKredit', 'tglKredit', 'required');
      $this->form_validation->set_rules('biayaAdmin', 'biayaAdmin', 'required');
      $this->form_validation->set_rules('biayaAsuransi', 'biayaAsuransi', 'required');
      $this->form_validation->set_rules('totalSewaModal', 'totalSewaModal', 'required');
      if (!$this->form_validation->run()) {
        $this->response(array(
          'code' => 101,
          'status' => 'error',
          'message' => 'Invalid Input',
          'errors' => $this->form_validation->error_array()
        ), 200);
        log_message('debug', 'taksir_ulang_perhiasan_god ERROR: '.json_encode($request_body));
      } else {
        log_message('debug', 'taksir_ulang_perhiasan_god SUKSES: '.json_encode($request_body));
        $cek = $this->GadaiModel->cekKodeBookingPerhiasan($request_body->kodeBooking);
        $status =  $this->GadaiModel->getStatusTaksirUlangPerhiasanByKodeBooking($request_body->kodeBooking);

        if ($cek == FALSE) {
          $this->set_response(array(
            'status' => 'error',
            'message' => 'Data tidak ditemukan'
          ));
        } elseif ($status->status != 9) {
          $this->set_response(array(
            'status' => 'error',
            'message' => 'Barang jaminan belum sampai ke outlet',
            'data' => $status
          ));
        } elseif ($cek == TRUE) {
          // insert or update to table users
          $cif = array('cif' => $request_body->cif);
          if ($cif != Null) {
            $this->db->where('user_AIID', $status->user_AIID)->update('user', $cif);
          } else {
            $this->db->where('user_AIID', $status->user_AIID)->insert('user', $cif);
          }

          // insert or update to table payment_gadai
          $paymentGadai = array(
            'administrasi' => $request_body->biayaAdmin,
            'asuransi' => $request_body->biayaAsuransi,
            'tglLelang' => date('Y-m-d', strtotime($request_body->tglLelang)),
            'tglJatuhTempo' => date('Y-m-d', strtotime($request_body->tglJatuhTempo)),
            'tglKredit' => date('Y-m-d', strtotime($request_body->tglKredit)),
            'sewaModal' => $request_body->sewaModal,
            'up' => $request_body->up,
            'biayaLelang' => $request_body->biayaLelang ?: '2',
            'biayaProsesLelang' => $request_body->biayaProsesLelang ?: '1',
            'biayaAdmBjpdl' => $request_body->biayaAdmBjpdl ?: '1',
            'biayaBjdplMax' => $request_body->biayaBjdplMax ?: '6',
            'namaProduk' => 'GOD'
          );
          $cekBookingCode = $this->GadaiModel->getPaymentByBookingCode($request_body->kodeBooking);

          if ($cekBookingCode == TRUE) {
            $this->db->where('bookingCode', $request_body->kodeBooking)->update('payment_gadai', $paymentGadai);
          } else {
            // saving to table payment_gadai
            $paymentGadai['bookingCode'] = $request_body->kodeBooking;
            $this->db->insert('payment_gadai', $paymentGadai);
          }

          // update to table gadai gadai_perhiasan
          $gadaiPerhiasan = array(
            'taksir_ulang_admin' => $request_body->totalTaksiran,
            'upMaksimal' => $request_body->upMaksimal,
            'up' => $request_body->up,
            'sewa_modal' => $request_body->sewaModal,
            'user_penaksir' => $request_body->userPenaksir,
            'diterimaNasabah' => $request_body->diterimaNasabah,
            'tanggal_lelang' => date('Y-m-d', strtotime($request_body->tglLelang)),
            'tanggal_jatuh_tempo' => date('Y-m-d', strtotime($request_body->tglJatuhTempo)),
            'tanggal_kredit' => date('Y-m-d', strtotime($request_body->tglKredit)),
            'sewa_modal_maksimal' => $request_body->totalSewaModal,
            'list_jaminan' => json_encode($request_body->listJaminan)
          );

          $this->db->where('kode_booking', $request_body->kodeBooking)->update('gadai_perhiasan', $gadaiPerhiasan);

          $this->set_response(array(
            'status' => 'success',
            'message' => 'Data Berhasil di update',
            'data' => $request_body
          ), 200);
        }
      }
    // } else {
    //   $this->errorUnAuthorized();
    // }
  }

  function norek_nasabah_god_logam_mulia_by_kode_booking_get()
  {
    $kode_booking = $this->uri->segment(3);
    $token = $this->getToken();
    if ($token) {
      $user_AIID = $token->id;
      $data = $this->GadaiModel->getDataRekeningLogamMuliaByKodeBooking($kode_booking);

      if ($data->num_rows() > 0) {
        $data->row()->statusKonfirmasi = true;
        $this->set_response(array(
          'status' => 'success',
          'message' => ' ',
          // 'statusKonfirmasi' => true,
          'data' => $data->row()
        ), 200);
      } else {
        $this->set_response(array(
          'status' => 'success',
          'message' => 'There is no data',
          'data' => array('statusKonfirmasi' => false)
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function norek_nasabah_god_perhiasan_by_kode_booking_get()
  {
    $kode_booking = $this->uri->segment(3);
    $token = $this->getToken();
    if ($token) {
      $user_AIID = $token->id;
      $data = $this->GadaiModel->getDataRekeningPerhiasanByKodeBooking($kode_booking);

      if ($data->num_rows() > 0) {
        $data->row()->statusKonfirmasi = true;
        $this->set_response(array(
          'status' => 'success',
          'message' => ' ',
          // 'statusKonfirmasi' => true,
          'data' => $data->row()
        ), 200);
      } else {
        $this->set_response(array(
          'status' => 'success',
          'message' => 'There is no data',
          'data' => array('statusKonfirmasi' => false)
        ), 200);
      }
    } else {
      $this->errorUnAuthorized();
    }
  }

  function generatePaymentNotif($nama, $trxId)
  {

    $payment = $this->GadaiModel->getPaymentByTrxId($trxId);

    $subject = null;
    $viewData = array();

    $jt = null;

    if ($payment->jenisTransaksi == 'CC') {
      $jt = 'Cicil';
    } else if ($payment->jenisTransaksi == 'UG') {
      $jt = 'Ulang Gadai';
    } else if ($payment->jenisTransaksi == 'TB') {
      $jt = 'Tebus Gadai';
    }

    if (
      $payment->payment == 'BNI' ||
      $payment->payment == 'VA_MANDIRI' ||
      $payment->payment == 'VA_BCA' ||
      $payment->payment == 'VA_BRI' ||
      $payment->payment == 'VA_PERMATA'
    ) {
      $subject = "Pembayaran Gadai " . $payment->norek . ' - ' . $jt;
      $fTglExpired = new DateTime($payment->tanggal_expired);

      $viewData = array(
        'totalKewajiban' => $payment->totalKewajiban,
        'nama' => $nama,
        'namaNasabah' => $payment->namaNasabah,
        'jenisTransaksi' => $jt,
        'va' => $payment->virtual_account,
        'tglExpired' => $fTglExpired->format('d/m/Y H:i:s'),
        'norek' => $payment->norek,
        'payment' => $payment->payment,
        'trxId' => $trxId,
        'biayaTransaksi' => $payment->biayaTransaksi
      );
    } else if ($payment->payment == 'WALLET' || $payment->payment == 'GCASH') {
      $subject = "Transaksi " . $payment->norek . ' - ' . $jt . ' sukses';
      $fTglJatuhTempo = new DateTime($payment->tglJatuhTempo);

      $viewData = array(
        'totalKewajiban' => $payment->totalKewajiban,
        'nama' => $nama,
        'namaNasabah' => $payment->namaNasabah,
        'jenisTransaksi' => $jt,
        'tglExpired' => $fTglJatuhTempo->format('d/m/Y H:i:s'),
        'norek' => $payment->norek,
        'payment' => $payment->tipe,
        'trxId' => $trxId,
        'hariTarif' => $payment->jumlahHariTarif,
        'up' => $payment->up,
        'sewaModal' => $payment->sewaModal,
        'administrasi' => $payment->administrasi,
        'tglJatuhTempo' => $fTglJatuhTempo->format('d/m/Y'),
        'upCicil' => $payment->minimalUpCicil,
        'payment' => $payment->payment,
        'biayaTransaksi' => $payment->biayaTransaksi
      );
    }

    $content = $this->load->view('mail/notif_payment_gadai', $viewData, true);

    $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
    $email = $email . $content;
    $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

    $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
    $mobile = $mobile . $content;
    $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

    return array(
      'email' => $email,
      'mobile' => $mobile,
      'minimal' => $content
    );
  }

  function generateEfekNotif($data)
  {
    $subject = "Pengajuan Gadai Berhasil (Kode Booking " .  $data['bookingId'] . ")";

    $content = $this->load->view('mail/efek/open', $data, true);

    $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
    $email = $email . $content;
    $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

    $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
    $mobile = $mobile . $content;
    $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

    return array(
      'email' => $email,
      'mobile' => $mobile,
      'minimal' => $content
    );
  }

  function generateCallbackNotif($data)
  {
    $subject = "Pengajuan Gadai Berhasil (Kode Booking " .  $data['bookingId'] . ")";

    $content = $this->load->view('mail/efek/open', $data, true);

    $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
    $email = $email . $content;
    $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

    $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
    $mobile = $mobile . $content;
    $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

    return array(
      'email' => $email,
      'mobile' => $mobile,
      'minimal' => $content
    );
  }

  /**
   * INQUIRY DARI CORE
   */

  function authCore()
  {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $corePassword = $this->config->item('core_password');
      if ($headers['Authorization'] == $corePassword) {
        return true;
      }

      return false;
    }

    return false;
  }

  function inquiry_get()
  {
    if (!$this->authCore()) {
      $this->errorUnAuthorized();
      return;
    }

    $kode = $this->query('kode');

    $gadai = $this->GadaiModel->getDetailGadaiByKodeBooking($kode);

    if ($gadai) {
      $this->set_response([
        'status' => 'success',
        'message' => '',
        'data' => $gadai
      ], 200);
      log_message('debug', 'inquiry_get: '.json_encode($gadai));
    } else {
      $this->set_response([
        'status' => 'error',
        'code' => 102,
        'message' => 'Tidak ada data gadai ditemukan'
      ], 200);
      log_message('debug', 'inquiry_get Error: '.json_encode($gadai));
    }
  }

  function charge_post()
  {

    // Set url
    $api_url = $this->config->item('midtrans_url');
    // Set your server key (Note: Server key for sandbox and production mode are different)
    $server_key = $this->config->item('midtrans_serverkey');
    log_message('debug', 'Start to request to snap midtrans');

    // get the HTTP POST body of the request
    $request_body = file_get_contents('php://input');

    // set response's content type as JSON
    header('Content-Type: application/json');
    // call charge API using request body passed by mobile SDK
    $charge_result = $this->chargeAPI($api_url, $server_key, $request_body);
    log_message('debug', 'end of request to snap midtrans'.json_encode($charge_result, true));
    // set the response http status code
    http_response_code($charge_result['http_code']);
    // then print out the response body
    echo $charge_result['body'];
  }

  /**
   * call charge API using Curl
   * @param string  $api_url
   * @param string  $server_key
   * @param string  $request_body
   */
  function chargeAPI($api_url, $server_key, $request_body)
  {
    $ch = curl_init();
    $curl_options = array(
      CURLOPT_URL => $api_url,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_POST => 1,
      CURLOPT_HEADER => 0,
      // Add header to the request, including Authorization generated from server key
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Basic ' . base64_encode($server_key . ':')
      ),
      CURLOPT_POSTFIELDS => $request_body
    );
    curl_setopt_array($ch, $curl_options);
    $result = array(
      'body' => curl_exec($ch),
      'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
    );

    return $result;
  }

    // Api check status gosend
    function check_status_gosend_get()
    {
        log_message('debug', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->get()));
        $code_booking = $this->get('kode_booking');
        $gadai = $this->GadaiModel->getDetailGadaiByKodeBookingGod($code_booking);
        log_message('debug', 'GadaiModel->getDetailGadaiByKodeBookingGod ' . json_encode($gadai));
        if (empty($gadai)) {
            return $this->send_response('error', 'Data tidak ditemukan', null);
        }

        $no_order = $gadai->noOrderGosend ?? null;
        $gosend   = $this->restGosend_service->getStatus($no_order);
        if (empty($gosend)) {
            return $this->send_response('error', 'Data tidak ditemukan', null);
        }

        $code_booking = $this->gadai_service->updateGadaiGosend($gadai, $gosend);
        $gadai = $this->GadaiModel->getDetailGadaiByKodeBookingGod($code_booking);

        $gadai->gosendStatus      = $gosend->status;
        $gadai->gosendBookingType = $gosend->bookingType;
        log_message('debug', 'end of ' . __FUNCTION__ . ' => ' . json_encode($gadai));

        return $this->send_response('success', 'Cek status gosend sukses', $gadai);
    }
}