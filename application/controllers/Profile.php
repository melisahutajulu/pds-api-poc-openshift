<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';


class Profile extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('User', 'Otp', 'BankModel', 'PaymentModel', 'NotificationModel', 'EmasModel', 'MasterModel', 'EkycModel', 'AdminModel'));

        $this->load->library('form_validation');
    }

    public function index_get()
    {
        $token = $this->getToken();
        if ($token) {

            // Every user fetch profile, update data from core
            $profile = $this->User->profile($token->id);

            if ($profile->cif != NULL) {
                $inquiryPortofolio = $this->inquiryTabEmas($profile->cif, $token->channelId);
                if ($inquiryPortofolio->responseCode == '00') {
                    $tabunganEmas = json_decode($inquiryPortofolio->data);
                    $profile->tabunganEmas = $tabunganEmas;
                } else {
                    $profile->tabunganEmas = [];
                }

                // Check user data to core
                $checkProfile = $this->check_cif($profile->cif);
                log_message('debug', 'Log response update check profile index_get' . json_encode($checkProfile));

                if ($checkProfile->responseCode == '00') {
                    // Update user info
                    $coreProfileData = json_decode($checkProfile->data);
                    $this->User->updateUser($token->id, [
                        'no_ktp' => $coreProfileData->noIdentitas,
                        'jenis_identitas' => $coreProfileData->tipeIdentitas,
                        'jenis_kelamin' => $coreProfileData->jenisKelamin,
                        'nama' => $coreProfileData->namaNasabah,
                        'nama_ibu' => $coreProfileData->ibuKandung,
                        'tempat_lahir' => $coreProfileData->tempatLahir,
                        'tgl_lahir' => $coreProfileData->tglLahir,
                        'is_dukcapil_verified' => !empty($coreProfileData->isDukcapilVerified) ? $coreProfileData->isDukcapilVerified : 0
                    ]);

                    // Edit profile response

                    $profile->noKTP = $coreProfileData->noIdentitas;
                    $profile->noIdentitas = $coreProfileData->noIdentitas;
                    $profile->jenisKelamin = $coreProfileData->jenisKelamin;
                    $profile->tempatLahir = $coreProfileData->tempatLahir;
                    $profile->tglLahir = $coreProfileData->tglLahir;
                    $profile->nama = $coreProfileData->namaNasabah;
                    $profile->namaIbu = $coreProfileData->ibuKandung;
                    $profile->jenisIdentitas = $coreProfileData->tipeIdentitas;
                }
            } else {
                $profile->tabunganEmas = [];
            }

            $this->response(array(
                'status' => 'success',
                'message' => '',
                'data' => $profile
            ));
        } else {
            $this->errorUnAuthorized();
        }
    }

    function checkAdminToken($token)
    {
        $this->load->model('AdminModel');
        $token = $this->AdminModel->getAdminToken($token);
        return $token;
    }

    public function checkuser_post()
    {
        $admin = $this->checkAdminToken($this->post('token'));
        log_message('info', 'Start checkuser_post ' . $this->post('user_id'));
        if (!$admin) {

            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('cif', 'cif', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'code' => 101,
                'status' => 'error',
                'message' => 'Invalid input',
                'errors' => $this->form_validation->error_array()
            ), 200);
            return;
        }
        // Check user data to core
        $user_id =  $this->post('user_id');
        $profile = $this->User->profile($user_id);

        if (!$profile) {
            $this->response(array(
                'code' => 101,
                'status' => 'error',
                'message' => 'Terjadi kesalahan pada data'
            ), 500);
            return;
        }

        $checkProfile = $this->check_cif($this->post('cif'));
        log_message('debug', 'Log response update check profile index_get' . json_encode($checkProfile));
        if ($checkProfile->responseCode == '00') {
            $coreProfileData = json_decode($checkProfile->data);
            $profile->noKTP = $coreProfileData->noIdentitas;
            $profile->noIdentitas = $coreProfileData->noIdentitas;
            $profile->jenisKelamin = $coreProfileData->jenisKelamin;
            $profile->tempatLahir = $coreProfileData->tempatLahir;
            $profile->tglLahir = $coreProfileData->tglLahir;
            $profile->nama = $coreProfileData->namaNasabah;
            $profile->namaIbu = !empty($coreProfileData->ibuKandung) ? $coreProfileData->ibuKandung : '';
            $profile->jenisIdentitas = !empty($coreProfileData->tipeIdentitas) ? $coreProfileData->tipeIdentitas : '';
            $profile->agama = !empty($coreProfileData->agama) ? $coreProfileData->agama : '';
        } else {
            return $this->response(array(
                'status' => 'error',
                'message' => 'Server sedang maintenance!',
                'data' => null
            ));
        }

        return $this->response(array(
            'status' => 'success',
            'message' => '',
            'data' => $profile
        ));
    }

    function index_put()
    {
        $token = $this->getToken();
        if ($token) {
            $set_data = array(
                'nama'  => $this->put('nama'),
                'alamat'  => $this->put('alamat'),
                'nama_ibu' => $this->put('nama_ibu'),
                'tempat_lahir' => $this->put('tempat_lahir'),
                'tgl_lahir'  => $this->put('tgl_lahir'),
                'no_ktp'  => $this->put('no_ktp'),
                'id_kelurahan' => $this->put('id_kelurahan'),
                'kodepos' => $this->put('kode_pos'),
                'jenis_kelamin' => $this->put('jenis_kelamin'),
                'jenis_identitas' => $this->put('jenis_identitas'),
                'tanggal_expired_identitas' => $this->put('tanggal_expired_identitas'),
                'status_kawin' => $this->put('status_kawin'),
                'kewarganegaraan' => $this->put('kewarganegaraan')
            );

            $this->form_validation->set_data($set_data);

            // Todo Ganti Form Validation
            /*$this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('provinsi_id', 'Provinsi', 'integer');
            $this->form_validation->set_rules('kabupaten_id', 'Kabupaten', 'integer');
            $this->form_validation->set_rules('kecamatan_id', 'Kecamatan', 'integer');
            $this->form_validation->set_rules('no_ktp','Nomor Identitas','required|exact_length[16]');*/
            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('nama_ibu', 'Nama Ibu', 'required');
            $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
            $this->form_validation->set_rules('tgl_lahir', 'Tangggal Lahir', 'required');
            $this->form_validation->set_rules('no_ktp', 'Nomor Identitas', 'required'); // Nomor Identitas
            $this->form_validation->set_rules('id_kelurahan', 'Kode Kelurahan', 'required'); // kode_kelurahan
            $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
            $this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required');
            $this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'required');

            $set_data['last_update_data_nasabah'] = date('Y-m-d H:i:s');

            if (!$this->form_validation->run()) {
                $this->response(array(
                    'code' => 101,
                    'nama' => $this->put('nama'),
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $this->User->updateUser($token->id, $set_data);
                $this->response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $this->User->profile($token->id)
                ), 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    public function password_put()
    {
        $token = $this->getToken();
        if ($token) {
            $set_data = array(
                'current_password'  => $this->put('current_password'),
                'new_password'  => $this->put('new_password'),
            );
            $this->form_validation->set_data($set_data);

            $this->form_validation->set_rules('current_password', 'password', 'required');
            $this->form_validation->set_rules('new_password', 'password', 'required');

            if (!$this->form_validation->run()) {
                $this->response(array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                //Cek if current password match with user password
                if ($this->User->isValidPassword($token->id, $this->put('current_password'))) {
                    $this->User->updateUser($token->id, ['password' => md5($this->put('new_password'))]);

                    $this->response(array(
                        'status' => 'success',
                        'message' => 'Password diperharui',
                    ), 200);
                } else {
                    $this->response(array(
                        'code' => 102,
                        'status' => 'error',
                        'message' => 'Password tidak cocok',
                        'errors' => array(
                            'current_password' => $this->put('current_password'),
                            'new_password' => $this->put('new_password')
                        )
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function pin_post($method = 'check')
    {
        $token = $this->getToken();
        if ($token) {

            $user = $this->User->getUser($token->id);

            //Check apakah pin kosong
            if ($method == 'check') {

                if ($user->pin == '') {
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => FALSE
                    ), 200);
                } else {
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => TRUE
                    ), 200);
                }
            } else if ($method == 'edit') {

                // check data yg masuk untuk edit atau aktifasi
                $currentPin = 0;
                if ($this->post('current_pin') != "") {
                    $this->form_validation->set_rules('current_pin', 'Current Pin', 'required|exact_length[6]|numeric');
                    $this->form_validation->set_rules('new_pin', 'New Pin', 'required|exact_length[6]|numeric');

                    $currentPin = $this->post('current_pin');
                    $newPin = $this->post('new_pin');
                } else {
                    // jika data yg masuk aktifasi
                    $this->form_validation->set_rules('new_pin', 'new pin', 'required|exact_length[6]|numeric');
                    $this->form_validation->set_rules('otp', 'otp', 'required');
                }

                if (!$this->form_validation->run()) {
                    $this->response(array(
                        'code' => 101,
                        'status' => 'error',
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ), 200);
                } else {
                    if ($currentPin != 0) {
                        log_message('debug', 'Start Edit PIN');
                        // proses edit pin
                        if ($this->User->isValidPIN($token->id, $currentPin)) {
                            $this->User->updateUser($token->id, array(
                                'pin' => md5($this->post('new_pin')),
                                'last_update_pin' => date('Y-m-d H:i:s'),
                                // 'aktifasiTransFinansial' => '1'
                            ));

                            $this->set_response(array(
                                'status' => 'success',
                                'message' => 'PIN diperbaharui',
                            ), 200);
                            log_message('debug', 'Edit PIN Sukses');
                        } else {
                            log_message('debug', 'Edit PIN Gagal');
                            $this->response(array(
                                'code' => 102,
                                'status' => 'error',
                                'message' => 'PIN tidak cocok',
                                'errors' => array(
                                    'current_password' => $currentPin,
                                    'new_password' => $newPin
                                )
                            ), 200);
                        }
                    } else {
                        log_message('debug', 'Start Aktifasi PIN');
                        //proses aktifasi pin
                        if ($this->User->isValidPIN($token->id, $this->post('new_pin'))) {
                            $aktifasiData = array(
                                'channelId' => $token->channelId,
                                'cif' => $user->cif,
                                'token' => $this->post('otp'),
                                'username' => $user->no_hp

                            );

                            $checkOTP = $this->aktifasiTransaksi($aktifasiData);

                            if ($checkOTP->responseCode == '00') {
                                // check jika response balikan data = kosong
                                if (!isset($checkOTP->data)) {
                                    $this->response(array(
                                        'code' => 103,
                                        'status' => 'error',
                                        'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                        'reason' => $checkOTP
                                    ), 200);
                                    return;
                                }
                                $resAktifasi = json_decode($checkOTP->data);

                                // check apakah response dari core statusnya sudah aktif
                                if ($resAktifasi->statusAktif == '1') {
                                    $this->User->updateUser($token->id, array(
                                        'pin' => md5($this->post('new_pin')),
                                        'last_update_pin' => date('Y-m-d H:i:s'),
                                        'aktifasiTransFinansial' => '1'
                                    ));
                                    log_message('debug', 'Aktifasi PIN Sukses', $checkOTP);
                                    $this->set_response(array(
                                        'status' => 'success',
                                        'message' => 'PIN diperbaharui',
                                    ), 200);
                                } else {

                                    // response jika balikan dari core gagal
                                    $this->response(array(
                                        'code' => 103,
                                        'status' => 'error',
                                        'message' => 'Aktifasi transaksi gagal. Mohon hubungi cabang terdekat',
                                        'reason' => $checkOTP
                                    ), 200);
                                }
                            } else {
                                log_message('debug', 'Aktifasi PIN Gagal', $checkOTP);
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Terjadi kesalahan. Coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $checkOTP
                                ), 200);
                            }
                        } else {
                            $this->response(array(
                                'code' => 102,
                                'status' => 'error',
                                'message' => 'PIN tidak tersedia',
                                // 'errors' => array(
                                //     'new_password'=>$this->post('new_pin')
                                // )
                            ), 200);
                        }
                    }
                }
            } else if ($method == 'verify') {

                $set_data = array(
                    'otp'  => $this->post('otp')
                );

                $this->form_validation->set_data($set_data);
                $this->form_validation->set_rules('otp', 'otp', 'required|exact_length[6]|numeric');

                if (!$this->form_validation->run()) {
                    $this->response(array(
                        'code' => 101,
                        'status' => 'error',
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ), 200);
                } else {

                    $otp = $this->post('otp');

                    $aktifasiData = array(
                        'channelId' => $token->channelId,
                        'cif' => $user->cif,
                        'token' => $otp,
                        'username' => $user->no_hp
                    );


                    $checkOTP = $this->aktifasiTransaksi($aktifasiData);

                    if ($checkOTP->responseCode == '00') {

                        if (!isset($checkOTP->data)) {
                            $this->response(array(
                                'code' => 103,
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                'reason' => $checkOTP
                            ), 200);
                            return;
                        }

                        $resAktifasi = json_decode($checkOTP->data);

                        if ($resAktifasi->statusAktif == '1') {
                            $this->User->updateUser($token->id, array(
                                'pin' => $user->pin_temp,
                                'last_update_pin' => date('Y-m-d H:i:s')
                            ));

                            $this->set_response(array(
                                'status' => 'success',
                                'message' => 'PIN diperbaharui',
                            ), 200);
                        } else {
                            $this->response(array(
                                'code' => 103,
                                'status' => 'error',
                                'message' => 'Aktifasi transaksi gagal. Mohon hubungi cabang terdekat',
                                'reason' => $checkOTP
                            ), 200);
                        }
                    } else {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Kode OTP tidak cocok atau expired',
                            'code' => 102
                        ), 200);
                    }
                }
            } else if ($method == 'reset') {
                $reffId = mt_rand(1000, 9999);
                $sendOTP = $this->sendOTP($user->no_hp, $reffId, 'pin-reset', $token->channelId);

                if ($sendOTP->responseCode == '00') {

                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Mohon masukan kode OTP yang kami kirimkan ke no telepon anda',
                        'reffId' => $reffId
                    ]);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 103,
                        'message' => 'Mohon coba beberapa saat lagi'
                    ]);
                }
            } else if ($method == 'reset_verify') {
                $set_data = array(
                    'pin'  => $this->post('pin'),
                    'otp' => $this->post('otp')
                );

                $this->form_validation->set_data($set_data);


                $this->form_validation->set_rules('pin', 'new pin', 'required|exact_length[6]|numeric');
                $this->form_validation->set_rules('otp', 'otp', 'required|numeric');

                if (!$this->form_validation->run()) {
                    $this->response(array(
                        'code' => 101,
                        'status' => 'error',
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ), 200);
                } else {

                    //Check apakah user telah melakukan transaksi finansial
                    $whereCheck = array(
                        'user_AIID' => $token->id,
                        'aktifasiTransFinansial' => '1'
                    );
                    $check = $this->db->where($whereCheck)->get('user');

                    if ($check->num_rows() == 0) {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Anda belum melakukan aktifasi transaksi finansial',
                            'code' => 102
                        ));
                        return;
                    }

                    $otp = $this->post('otp');
                    $pin = $this->post('pin');

                    $checkOTP = $this->checkOTP($otp, $user->no_hp, 'pin-reset', $token->channelId);

                    if ($checkOTP->responseCode == '00') {

                        $this->User->updateUser($token->id, array(
                            'pin' => md5($this->post('pin')),
                            'last_update_pin' => date('Y-m-d H:i:s'),
                            'aktifasiTransFinansial' => '1'
                        ));
                        //Kirim Email Notifikasi
                        $this->load->helper('message');
                        Message::sendEmailSuccessResetPin($token->email, $token->nama);

                        $this->set_response(array(
                            'status' => 'success',
                            'message' => 'PIN diperbaharui',
                        ), 200);
                    } else {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan. Coba beberapa saat lagi',
                            'code' => 103,
                            'reason' => $checkOTP
                        ), 200);
                    }
                }
            } else if ($method == 'ekyc') {
                log_message('debug', 'Start Aktifasi PIN');
                //proses aktifasi pin
                if ($this->User->isValidPIN($token->id, $this->post('new_pin'))) {
                    $ekyc = $this->EkycModel->getbyUser($token->id);
                    $data = [
                        'branchCode' => $user->kode_cabang,
                        'cif' => $user->cif,
                        'namaNasabah' => $user->nama,
                        'noHp' => $user->no_hp,
                        'pendidikan' => $ekyc->pendidikan_terakhir,
                        'penghasilan' => $ekyc->penghasilan,
                        'sumberDana' => $ekyc->sumber_dana,
                        'tujuanPengajuanDana' => $ekyc->maksud_dan_tujuan
                    ];
                    $ekycActivation = $this->ekycActivation($data);
                    if ($ekycActivation->responseCode != '00') {
                        $this->response(array(
                            'code' => 102,
                            'status' => 'error',
                            'message' => 'E-KYC Activation ke Core Gagal',
                        ), 200);
                        log_message('debug', 'Aktifasi PIN E-KYC Gagal ' . json_encode($ekycActivation));
                        return;
                    }
                    $this->User->updateUser($token->id, array(
                        'pin' => md5($this->post('new_pin')),
                        'last_update_pin' => date('Y-m-d H:i:s'),
                        // 'aktifasiTransFinansial' => '1'
                    ));
                    $this->EkycModel->update($token->id, array(
                        'status' => 1, //update status E-KYC
                        'update_date' => date('Y-m-d H:i:s'),
                    ));
                    // Ekyc Activation

                    log_message('debug', 'Aktifasi PIN E-KYC Sukses');
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'PIN Berhasil disimpan',
                    ), 200);
                } else {
                    $this->response(array(
                        'code' => 102,
                        'status' => 'error',
                        'message' => 'PIN tidak tersedia',
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    public function pin_put()
    {
        $token = $this->getToken();
        if ($token) {
            $set_data = array(
                'current_pin'  => $this->put('current_pin'),
                'new_pin'  => $this->put('new_pin'),
            );
            $this->form_validation->set_data($set_data);

            $this->form_validation->set_rules('current_pin', 'current pin', 'required');
            $this->form_validation->set_rules('new_pin', 'new pin', 'required');

            if (!$this->form_validation->run()) {
                $this->response(array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {

                $this->load->helper('message');
                if ($this->User->isValidPIN($token->id, $this->put('current_pin'))) {

                    $newPin = $this->put('new_pin');

                    $otpCode = mt_rand(100000, 999999);
                    $reffId = $this->Otp->insert($token->id, $otpCode, Otp::GANTI_PIN, md5($newPin));
                    $this->sendOTP($token->no_hp, $reffId, "pin", $token->channelId);

                    $this->response(array(
                        'status' => 'success',
                        'message' => 'Mohon masukan kode OTP yang kami kirimkan ke no telepon anda',
                        'reffId' => $reffId
                    ), 200);
                } else {
                    $this->response(array(
                        'code' => 102,
                        'status' => 'error',
                        'message' => 'PIN tidak cocok',
                        'errors' => array(
                            'current_password' => $this->put('current_password'),
                            'new_password' => $this->put('new_password')
                        )
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    public function verify_pin_otp_post()
    {
        $token = $this->getToken();
        if ($token) {
            $this->form_validation->set_rules('otp', 'OTP Code', 'required|numeric|exact_length[6]');
            $this->form_validation->set_rules('reff_id', 'reff id', 'required|integer');

            if (!$this->form_validation->run()) {
                $this->response(array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $otp = $this->post('otp');
                $reffId = $this->post('reff_id');

                $otpData = $this->Otp->otpData($reffId);

                if ($otpData->num_rows() > 0) {
                    $this->load->helper('message');
                    $checkOtp = $this->checkOTP($otp, $token->no_hp, "pin", $token->channelId);
                    if ($checkOtp->responseCode == '00') {

                        $this->User->updateUser($token->id, ['pin' => $otpData->row()->data_otp]);
                        $this->response(array(
                            'status' => 'success',
                            'message' => '',
                        ), 200);
                    } else {
                        $response = array(
                            'status' => 'error',
                            'message' => 'OTP tidak ditemukan',
                            'code' => 102
                        );
                        $this->set_response($response, 200);
                    }
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function npwp_post()
    {
        /*
         * TODO: Use png quant to reduce image size
         */
        $uploadDir = $this->config->item('upload_dir');
        $token = $this->getToken();
        if ($token) {
            $config['upload_path']          = $uploadDir . '/user/npwp';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
            $config['max_size']             = 0;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
            $config['encrypt_name']         = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('userfile')) {
                $this->response([
                    'code' => 101,
                    'status' => 'error',
                    'message' => '',
                    'errors' => $this->upload->display_errors()
                ], 200);
            } else {

                $this->form_validation->set_rules('no_npwp', 'No NPWP', 'required|exact_length[15]');

                if ($this->form_validation->run() == FALSE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input',
                        'code' => 101,
                        'errors' => $this->form_validation->error_array()
                    ), 200);
                } else {
                    //Delete previous user file
                    $prevFile = $this->User->profile($token->id)->fotoNPWP;

                    if ($prevFile != null && file_exists($uploadDir . '/user/npwp/' . $prevFile)) {
                        unlink($uploadDir . '/user/npwp/' . $prevFile);
                    }

                    $data = $this->upload->data();
                    $this->User->updateUser($token->id, array(
                        'foto_npwp' => $data['file_name'],
                        'no_npwp' => $this->post('no_npwp'),
                        'last_update_data_npwp' => date('Y-m-d H:i:s')
                    ));
                    $this->response([
                        'status' => 'success',
                        'message' => '',
                        'data' => array(
                            'filename' => $data['file_name']
                        )
                    ], 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function sid_post()
    {
        /*
         * TODO: Use png quant to reduce image size
         */
        $uploadDir = $this->config->item('upload_dir');
        $token = $this->getToken();
        if ($token) {
            $this->form_validation->set_rules('no_sid', 'No SID', 'required|exact_length[15]');

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $data = null;

                $prevFile = $this->User->profile($token->id)->fotoSid;

                if (isset($_FILES['userfile']['name'])) {

                    $config['upload_path']          = $uploadDir . '/user/npwp';
                    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
                    $config['max_size']             = 0;
                    $config['max_width']            = 0;
                    $config['max_height']           = 0;
                    $config['encrypt_name']         = true;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('userfile')) {
                        $this->response([
                            'code' => 101,
                            'status' => 'error',
                            'message' => '',
                            'errors' => $this->upload->display_errors()
                        ], 200);
                    } else {
                        //Delete previous user file
                        if ($prevFile != null && file_exists($uploadDir . '/user/npwp/' . $prevFile)) {
                            unlink($uploadDir . '/user/npwp/' . $prevFile);
                        }

                        $data = $this->upload->data();
                    }
                }


                $this->User->updateUser($token->id, array(
                    'foto_sid' => $data['file_name'],
                    'no_sid' => $this->post('no_sid'),
                    'last_update_data_npwp' => date('Y-m-d H:i:s')
                ));
                $this->response([
                    'status' => 'success',
                    'message' => '',
                    'data' => array(
                        'filename' => $data ? $data['file_name'] : $prevFile
                    )
                ], 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function avatar_post()
    {
        /*
         * TODO: Use png quant to reduce image size
         */
        $token = $this->getToken();
        $uploadDir = $this->config->item('upload_dir');
        if ($token) {
            $config['upload_path']          = $uploadDir . '/user/avatar';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
            $config['max_size']             = 0;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
            $config['encrypt_name']         = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('userfile')) {
                $this->response([
                    'code' => 101,
                    'status' => 'error',
                    'message' => '',
                    'errors' => $this->upload->display_errors()
                ], 200);
            } else {

                //Delete previous user file
                $prevFile = $this->User->profile($token->id)->fotoKTP;

                if ($prevFile != null && file_exists($uploadDir . '/user/avatar/' . $prevFile)) {
                    unlink($uploadDir . '/user/avatar/' . $prevFile);
                }

                $data = $this->upload->data();
                $this->User->updateUser($token->id, array(
                    'foto_url' => $data['file_name'],
                    'last_update_data_nasabah' => date('Y-m-d H:i:s')
                ));
                $this->response([
                    'status' => 'success',
                    'message' => '',
                    'data' => $data
                ], 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function ktp_post()
    {
        /*
         * TODO: Use png quant to reduce image size
         */
        $token = $this->getToken();
        $uploadDir = $this->config->item('upload_dir');
        if ($token) {
            $config['upload_path']          = $uploadDir . '/user/ktp';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
            $config['max_size']             = 0;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
            $config['encrypt_name']         = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('userfile')) {
                $this->response([
                    'code' => 101,
                    'status' => 'error',
                    'message' => '',
                    'errors' => $this->upload->display_errors()
                ], 200);
            } else {

                //Delete previous user file
                $prevFile = $this->User->profile($token->id)->fotoKTP;

                if ($prevFile != null && file_exists($uploadDir . '/user/ktp/' . $prevFile)) {
                    unlink($uploadDir . '/user/ktp/' . $prevFile);
                }

                $data = $this->upload->data();
                $this->User->updateUser($token->id, array(
                    'foto_ktp_url' => $data['file_name']
                ));
                $this->response([
                    'status' => 'success',
                    'message' => '',
                    'data' => $data
                ], 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function cif_check_post()
    {
        $token = $this->getToken();
        if ($token) {
            $this->form_validation->set_rules('cif', 'cif', 'required|exact_length[10]|numeric');
            $this->form_validation->set_rules('ibu_kandung', 'Nama ibu kandung', 'required');
            $this->form_validation->set_rules('tgl_lahir', 'tgl_lahir', 'required');
            $this->form_validation->set_rules('nama', 'nama', 'required');
            $this->form_validation->set_rules('no_hp', 'no_hp', 'required');


            if (!$this->form_validation->run()) {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => '',
                    'code' => 102,
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $cif = $this->post('cif');
                $ibuKandung = $this->post('ibu_kandung');
                $tglLahir = $this->post('tgl_lahir');
                $date = new DateTime($tglLahir);
                $nama = $this->post('nama');
                $no_hp = $this->post('no_hp');


                $user = $this->User->profile($token->id);

                $cek = $this->customerLink($cif, $ibuKandung, $nama, $date->format('Y-m-d'), $no_hp, $token->channelId);

                if ($cek->responseCode == '00') {
                    $data = json_decode($cek->data);

                    //Langsung lakukan aktifasi wallet
                    $paramsWallet = array(
                        'channelId' => $token->channelId,
                        'cif' => $cif,
                        'noHp' => $no_hp,
                        'tipe' => 1
                    );
                    $response = $this->aktivasiWallet($paramsWallet);
                    $norekWallet = null;
                    $saldoWallet = null;

                    if ($response->responseCode == '00') {
                        if (isset($response->data)) {
                            $responseWallet = json_decode($response->data);
                            $saldoWallet = $responseWallet->saldo;
                            $norekWallet = $responseWallet->norek;
                        }
                    }
                    // End aktifasi wallet

                    $tanggalCif = date('Y-m-d H:i:s');

                    //Update user data berdasarkan data dari core
                    $update = array(
                        'cif' => $data->cif,
                        'nama_ibu' => $data->ibuKandung,
                        'jenis_kelamin' => $data->jenisKelamin,
                        'kode_cabang' => $data->kodeCabang,
                        'tempat_lahir' => $data->tempatLahir,
                        'tgl_lahir' => $data->tglLahir,
                        'nama' => $data->namaNasabah,
                        'no_ktp' => $data->noIdentitas,
                        'norek' => $norekWallet,
                        'saldo' => $saldoWallet,
                        'last_update_link_cif' => $tanggalCif,
                        'id_kelurahan' => $data->idKelurahan,
                        'alamat' => $data->jalan,
                        'kewarganegaraan' => $data->kewarganegaraan,
                        'status_kawin' => $data->statusKawin,
                        'kyc_verified' => $data->statusKyc,
                        'jenis_identitas' => $data->tipeIdentitas
                    );

                    $this->User->updateUser($token->id, $update);
                    $resData = $this->User->profile($token->id);
                    $resData->tabunganEmas = [];
                    $resData->rekeningGadai = [];
                    $resData->rekeningMikro = [];

                    //Check daftar tabungan yang dimiliki nasabah berdasarkan CIF
                    $tabemas = $this->daftarTabunganEmas($cif, $token->channelId);
                    if ($tabemas->responseCode == '00') {
                        $tabemasData = json_decode($tabemas->data);

                        $daftarRekening = array();

                        foreach ($tabemasData as $td) {
                            $daftarRekening[] = array(
                                'noRekening' => $td->norek,
                                'namaNasabah' => $data->namaNasabah,
                                'kodeCabang' => $td->kodeCabang,
                                'cif' => $td->cif,
                                'tanggalBuka' => $td->tglBuka,
                                'saldo' => $td->saldoEmas
                            );
                            //var_dump($td);
                            //Untuk setiap tabungan emas user, simpan di tabungan emas
                            $this->EmasModel->add($token->id, $ibuKandung, $tglLahir, $td);
                        }

                        //Tambahkan tabungan emas ke response
                        $resData->tabunganEmas = $daftarRekening;
                    }


                    //Lakukan aktivasi wallet
                    $paramsWallet = array(
                        'channelId' => $token->channelId,
                        'cif' => $data->cif,
                        'noHp' => $no_hp,
                        'tipe' => 1
                    );

                    $responseWallet = $this->aktivasiWallet($paramsWallet);

                    if ($responseWallet->responseCode == '00') {
                        $this->load->model('user');
                        $response_data = json_decode($responseWallet->data);
                        $data_update = array(
                            'saldo' => $response_data->saldo,
                            'norek' => $response_data->norek
                        );
                        $this->user->updateUser($token->id, $data_update);

                        $resData->norek = $response_data->norek;
                        $resData->saldo = $response_data->saldo;
                    }


                    //Send Notifikasi Link CIF success
                    $notifData = array(
                        'namaNasabah' => $user->nama,
                        'cif' => $cif,
                        'tanggalCif' => $tanggalCif
                    );

                    $template = $this->generateLinkCifNotif($notifData);

                    $mobileTemplate = $template['mobile'];
                    $emailTemplate = $template['email'];
                    $minimalTemplate = $template['minimal'];

                    //Simpan notifikasi baru
                    $notifId = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_PROFILE,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Profile Info",
                        "Link CIF Berhasil",
                        $mobileTemplate,
                        $minimalTemplate,
                        "LK"
                    );

                    $this->load->helper('message');
                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id),
                        [
                            "id" => $notifId,
                            "tipe" => "LK",
                            "title" => "Profile Info",
                            "tagline" => "Link CIF Berhasil",
                            "content" => "Link CIF Berhasil",
                            "token" => $no_hp
                        ]
                    );

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmail(
                        $user->email,
                        'Selamat LINK CIF Anda Berhasil',
                        $emailTemplate
                    );


                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => $resData
                    ), 200);
                } else {
                    $this->set_response(array(
                        'code' => 103,
                        'status' => 'error',
                        'message' => 'Data tidak cocok',
                        'reason' => $cek
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function generateLinkCifNotif($viewData)
    {

        $subject = "Selamat Link CIF Berhasil";

        $content = $this->load->view('mail/notif_link_cif', $viewData, true);

        $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $email = $email . $content;
        $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);


        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    function kcy_check_post()
    {
        $token = $this->getToken();
        if ($token) {
            $this->form_validation->set_rules('tanggal_kyc', 'Tanggal KYC', 'required');
            $this->form_validation->set_rules('cif', 'CIF', 'required|numeric|exact_length[10]');

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $tanggal_kyc = $this->post('tanggal_kyc');
                $cif = $this->post('cif');

                $cekKYC = $this->checkKYC($cif, $tanggal_kyc, $token->channelId);

                if ($cekKYC->responseCode == '00') {
                    $data = json_decode($cekKYC->data);
                    //Update profile set KYC Verified
                    $this->User->updateUser($token->id, array('kyc_verified' => '1'));
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => array(
                            'verified' => $data->status,
                            'tglKYC' => $data->tanggalKYC,
                            'cif' => $data->cif
                        )
                    ), 200);
                } else {
                    $this->set_response(array(
                        'code' => 103,
                        'status' => 'error',
                        'message' => 'Data tidak cocok',
                        'reason' => $cekKYC
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function bank_get()
    {
        $token = $this->getToken();
        if ($token) {
            $bankList = $this->BankModel->getBank($token->id);
            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $bankList
            ), 200);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function bank_post()
    {
        $token = $this->getToken();

        // check valid token
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        // set request rules
        $this->form_validation->set_rules('no_rekening', 'no rekening', 'required|numeric');
        $this->form_validation->set_rules('nama_pemilik', 'nama pemilik', 'required');
        $this->form_validation->set_rules('kode_bank', 'kode_bank', 'required|numeric');

        // check valid request
        if (!$this->form_validation->run()) {
            $this->set_response(array(
                'code' => 201,
                'status' => 'error',
                'message' => '',
                'errors' => $this->form_validation->error_array()
            ), 200);

            return;
        }

        $kode = $this->post('kode_bank');
        $nama = $this->post('nama_pemilik');
        $no = $this->post('no_rekening');

        // check is rekening exist
        if ($this->BankModel->isBankExist($token->id, $kode, $no)) {
            $this->set_response(array(
                'code' => 202,
                'status' => 'errror',
                'message' => 'Nomor rekening dan bank ini sudah anda gunakan'
            ));

            return;
        }

        // Lakukan pengecekan validitas rekening bank
        $dataCheck = array(
            'kodeBank' => $kode,
            'namaRekeningBank' => $nama,
            'noRekeningBank' => $no,
            'channelId' => $token->channelId
        );

        $checkValid = $this->checkRekBank($dataCheck);

        if ($checkValid->responseCode == '00') {
            $newId = $this->db->insert('rekening_bank', array(
                'kode_bank' => $kode,
                'nama_pemilik' => $nama,
                'nomor_rekening' => $no,
                'user_AAID' => $token->id
            ));
            $this->set_response(array(
                'status' => 'success',
                'message' => 'Bank berhasil ditambahkan',
                'id' => $newId,
            ), 200);
        } else if ($checkValid->responseCode == '15') {
            $this->set_response(array(
                'status' => 'error',
                'message' => 'Nama pemilik rekening tidak sesuai',
                'code' => 102
            ), 200);
        } else if ($checkValid->responseCode == '12') {
            $this->set_response(array(
                'status' => 'error',
                'message' => 'Kode bank/no rekening tidak cocok',
                'code' => 102
            ), 200);
        } else {
            $this->set_response(array(
                'status' => 'error',
                'code' => 103,
                'message' => 'Terjadi kesalahan pada sistem mohon coba beberapa saat lagi',
                'reason' => $checkValid
            ), 200);
        }
    }

    function bank_put()
    {
        $token = $this->getToken();
        if ($token) {
            $setData = array(
                'id' => $this->put('id'),
                'kode_bank' => $this->put('kode_bank'),
                'no_rekening' => $this->put('no_rekening'),
                'nama_pemilik' => $this->put('nama_pemilik'),
                'id_bank' => $this->put('id_bank')
            );

            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('no_rekening', 'no rekening', 'required|numeric');
            $this->form_validation->set_rules('nama_pemilik', 'nama pemilik', 'required');
            $this->form_validation->set_rules('kode_bank', 'id_bank', 'required|integer');


            if (!$this->form_validation->run()) {
                $this->set_response(array(
                    'code' => 201,
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {

                $kode = $this->put('kode_bank');
                $nama = $this->put('nama_pemilik');
                $no = $this->put('no_rekening');
                $id = $this->put('id');
                $idBank = $this->put('id_bank');

                if ($this->BankModel->isBankExist($token->id, $idBank, $no)) {
                    $this->BankModel->update($id, $no, $nama, $idBank);
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'Bank berhasil diperbaharui',
                        'data' => ''
                    ), 200);
                } else {
                    $this->errorForbbiden();
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function bank_delete()
    {
        log_message('info', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->query()));
        $token = $this->getToken();
        $id = $this->query('id');

        // check token
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('info', 'end of ' . __FUNCTION__ . ' => errorUnAuthorized');

            return;
        }

        // check if rek is belong to current user
        if (!$this->BankModel->isUserBank($token->id, $id)) {
            $this->errorForbbiden();
            log_message('info', 'end of ' . __FUNCTION__ . ' => errorForbbiden');

            return;
        }

        // delete rekening
        if (!$this->BankModel->delete($id)) {
            $response = $this->send_response('error', 'Gagal menghapus data', '');
            log_message('info', 'end of ' . __FUNCTION__ . ' => ' . json_encode($response));

            return;
        }

        $response = $this->send_response('success', 'Rekening bank telah dihapus', '');
        log_message('info', 'end of ' . __FUNCTION__ . ' => ' . json_encode($response));
    }

    function buka_tabungan_emas_post()
    {
        $token = $this->getToken();
        if ($token) {

            $user = $this->User->getUser($token->id);

            $setData = array(
                'amount' => $this->post('amount'),
                'domisili' => $this->post('domisili'),
                'nama' => $this->post('nama'),
                'jenis_identitas' => $this->post('jenis_identitas'),
                'no_identitas' => $this->post('no_identitas'),
                'tanggal_expired_identitas' => $this->post('tanggal_expired_identitas'),
                'tempat_lahir' => $this->post('tempat_lahir'),
                'tanggal_lahir' => $this->post('tanggal_lahir'),
                'no_hp' => $this->post('no_hp'),
                'jenis_kelamin' => $this->post('jenis_kelamin'),
                'status_kawin' => $this->post("status_kawin"),
                'kode_kelurahan' => $this->post("kode_kelurahan"),
                'jalan' => $this->post("jalan"),
                'ibu_kandung' => $this->post("ibu_kandung"),
                'kewarganegaraan' => $this->post("kewarganegaraan"),
                'kode_cabang' => $this->post("kode_cabang"),
                'flag' => $this->post("flag"),
                'payment' => $this->post("payment")
            );

            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('amount', 'Amount', 'required');
            $this->form_validation->set_rules('domisili', 'Domisili', 'required');
            $this->form_validation->set_rules('nama', 'Nama Nasabah', 'required');

            $this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required');
            $this->form_validation->set_rules('no_identitas', 'No Identitas', 'required');
            $this->form_validation->set_rules('tanggal_expired_identitas', 'Status Kawin', 'required');

            $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
            $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');

            $this->form_validation->set_rules('no_hp', 'No HP', 'required');

            $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
            $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required');

            if ($this->post('domisili') == '1') {
                $this->form_validation->set_rules('kode_kelurahan', 'Kode kelurahan', 'required');
            } else if ($this->post('domisili') == '2') {
                $this->form_validation->set_rules('jalan', 'Jalan', 'required');
            }

            $this->form_validation->set_rules('ibu_kandung', 'Ibu Kandung', 'required');
            $this->form_validation->set_rules('kewarganegaraan', 'Jenis Kelamin', 'required');

            $this->form_validation->set_rules('kode_cabang', 'Kode Cabang', 'required');
            $this->form_validation->set_rules('flag', 'Flag', 'required');
            $this->form_validation->set_rules('payment', 'Payment', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => '',
                    'errros' => $this->form_validation->error_array()
                ), 200);
            } else {
                $amount = $this->post('amount');
                $domisili = $this->post('domisili');
                $nama = $this->post('nama');
                $jenis_identitas = $this->post('jenis_identitas');
                $no_identitas = $this->post('no_identitas');
                $tanggal_expired_identitas = $this->post('tanggal_expired_identitas');
                $tempat_lahir = $this->post('tempat_lahir');
                $tanggal_lahir = $this->post('tanggal_lahir');
                $no_hp = $this->post('no_hp');
                $jenis_kelamin = $this->post('jenis_kelamin');
                $status_kawin = $this->post("status_kawin");

                $kode_kelurahan = $this->post("kode_kelurahan");
                $jalan = $this->post("jalan");

                $ibu_kandung = $this->post("ibu_kandung");
                $kewarganegaraan = $this->post("kewarganegaraan");
                $kode_cabang = $this->post("kode_cabang");
                $flag = $this->post("flag");
                $payment = $this->post('payment');

                $uploadDir = $this->config->item('upload_dir');
                // Update foto KTP User
                $config['upload_path']          = $uploadDir . '/user/ktp';
                $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
                $config['max_size']             = 0;
                $config['max_width']            = 0;
                $config['max_height']           = 0;
                $config['encrypt_name']         = true;

                $this->load->library('upload', $config);


                if (!$this->upload->do_upload('userfile')) {
                    $this->response([
                        'code' => 101,
                        'status' => 'error',
                        'message' => '',
                        'errors' => $this->upload->display_errors()
                    ], 200);
                } else {

                    //Delete previous user file
                    $prevFile = $this->User->profile($token->id)->fotoKTP;

                    if ($prevFile != null && file_exists($uploadDir . '/user/ktp/' . $prevFile)) {
                        unlink($uploadDir . '/user/ktp/' . $prevFile);
                    }

                    $uploadData = $this->upload->data();
                    $this->User->updateUser($token->id, array(
                        'foto_ktp_url' => $uploadData['file_name']
                    ));


                    /*
                     * Lakukan inquiry tabungan.
                     * Response data:
                     * administrasi
                     */

                    $inquiry = $this->inquiryTabunganEmas($amount, 'OP', $flag, NULL, NULL, $token->channelId, $user->cif);
                    if ($inquiry->responseCode == '00') {
                        $dataInquiry = json_decode($inquiry->data);

                        //Dapatkan id transaksi sesuai dengan reffSwitching
                        $trxId = $dataInquiry->reffSwitching;
                        $gram = $dataInquiry->gram;

                        $jenisTransaksi = 'OP';
                        $kodeProduct = '62';
                        $keterangan = 'Pembayaran Tabungan Emas';

                        //Buat billing melalui VA, Bisa BNI atau Mandiri
                        if ($payment == 'BNI') {
                            $billingBNI = $this->createBillingVABNI(
                                $amount,
                                $token->email,
                                $nama,
                                $no_hp,
                                $flag,
                                $jenisTransaksi,
                                $keterangan, //keterangan pembayaran emas
                                '',
                                $kodeProduct,
                                $trxId,
                                $token->channelId
                            );

                            if ($billingBNI->responseCode == '00') {

                                $billingData = json_decode($billingBNI->data);
                                $virtualAccount = $billingData->virtualAccount;
                                $tglExpired = $billingData->tglExpired;

                                //Save tabungan emas dan update data user
                                $this->User->saveTabunganEmas(
                                    $token->id,
                                    $domisili,
                                    $nama,
                                    $jenis_identitas,
                                    $no_identitas,
                                    $tanggal_expired_identitas,
                                    $tempat_lahir,
                                    $tanggal_lahir,
                                    $no_hp,
                                    $jenis_kelamin,
                                    $status_kawin,
                                    $kode_kelurahan,
                                    $jalan,
                                    $ibu_kandung,
                                    $kewarganegaraan,
                                    $kode_cabang,
                                    $flag,
                                    $uploadData['file_name'],
                                    $amount,
                                    $trxId
                                );

                                //Simpan payment
                                $this->PaymentModel->add(
                                    $trxId,
                                    $jenisTransaksi,
                                    $kodeProduct,
                                    $amount,
                                    $gram,
                                    '',
                                    $token->id,
                                    $keterangan,
                                    $payment,
                                    $virtualAccount,
                                    $tglExpired
                                );


                                //Simpan notifikasi baru
                                $this->NotificationModel->add(
                                    $token->id,
                                    NotificationModel::TYPE_EMAS,
                                    NotificationModel::CONTENT_TYPE_TEXT,
                                    "Tabungan Emas",
                                    "Pembayaran Pembukaan Tabungan Emas",
                                    "Mohon lakukan pembayaran ke nomor rekening BNI " . $virtualAccount . " sebelum " . $tglExpired . " untuk melanjutkan pembukaan rekening emas.\n\nTerima kasih."
                                );

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmailOpenTabunganPayment($token->email, $nama, $virtualAccount, $tglExpired);


                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $trxId,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired
                                    )
                                ], 200);
                            } else {
                                $this->set_response([
                                    'status' => 'error',
                                    'message' => 'Error Create Billing BNI',
                                    'code' => 103,
                                    'reason' => $billingBNI
                                ], 200);
                            }
                        }
                    } else {
                        $this->set_response([
                            'status' => 'error',
                            'message' => 'Error Inquiry',
                            'code' => 103,
                            'reason' => $inquiry
                        ], 200);
                    }
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    /**
     * Endpoint untuk mendapatkan transaksi mpo favorite user
     *
     * @param string $method
     * @return void
     */
    function favorite_mpo_get($method = "")
    {
        log_message('info', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        $token = $this->getToken();
        if (!$this->getToken()) {
            return $this->errorUnAuthorized();
        }

        // Set data validation
        $this->form_validation->set_data([
            'tipe'  => $this->query('tipe'),
            'group' => $this->query('group')
        ]);

        $this->form_validation->set_rules('tipe', 'tipe', 'required');
        $this->form_validation->set_rules('group', 'group', 'required');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            return $this->send_response('error', $error ?? null, '', '', 101);
        }

        $query = [
            'user_AIID' => $token->id,
            'tipe'      => $this->query('tipe'),
            'mpo_group' => $this->query('group')
        ];

        switch ($method) {
            case 'link_aja':
                $code = '070801';
                break;
            case 'dana':
                $code = '070601';
                break;
        }

        $code_mpo = $this->getCodeMpo($code ?? null);
        if (!empty($code_mpo)) {
            $query['kode_layanan_mpo'] = $code_mpo;
        }

        log_message('info', 'query where data favorite' . json_encode($query));

        $favorite = $this->User->getMpoFavorite($query);

        log_message('info', 'end of ' . __FUNCTION__ . ' => ' . json_encode($favorite));

        return $this->send_response('success', '', $favorite);
    }

    private function getCodeMpo($code_mpo = null)
    {
        $mpo_code = null;
        if (!empty($code_mpo)) {
            $mpo = $this->db->select('kodeLayananMpo')->where('kodeLayananMpo', $code_mpo)->get('ref_mpo_seluler');
            if ($mpo->num_rows() > 0) {
                $mpo_code = $mpo->row()->kodeLayananMpo;
            }
        }

        return $mpo_code;
    }

    /**
     * Endpoint untuk mendapatkan transaksi favorite user
     */
    function favorite_get()
    {
        log_message('info', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->query()));
        $token = $this->getToken();
        if (!$token){
            $this->errorUnAuthorized();
        }

        $tipe = $this->query('tipe');
        $favorite = $this->User->getFavorite($token->id, $tipe);
        $res = $this->send_response('success', '', $favorite);
        log_message('info', 'end of ' . __FUNCTION__ . ' => ' . json_encode($res));
    }

    /**
     * Endpoint untuk user favorite
     */
    function favorite_post()
    {
        log_message('debug', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->post()));

        $token = $this->getToken();
        // Token Validation
        if (!$token) {
            return $this->errorUnAuthorized();
        }

        // set Validation
        $this->form_validation->set_rules('tipe', 'Tipe', 'required|callback_checkTipeFavorite');
        $this->form_validation->set_rules('no_rekening', 'no_rekening', 'required|numeric');
        $this->form_validation->set_rules('jenis_transaksi', 'jenis_transaksi', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            return $this->send_response('error', $error ?? null, '', 101);
        }

        $data = [
            'no_rekening'      => $this->post('no_rekening'),
            'nama_nasabah'     => $this->post('nama'),
            'tipe'             => $this->post('tipe'),
            'jenis_transaksi'  => $this->post('jenis_transaksi'),
            'user_AIID'        => $token->id,
            'kodeBank'         => $this->post('kode_bank'),
            'namaBank'         => $this->post('nama_bank'),
            'mpo_group'        => $this->post('group'),
            'kode_layanan_mpo' => $this->post('kodeLayananMpo')
        ];

        log_message('debug', 'start of insert favorite ' . __FUNCTION__ . ' => ' . json_encode($data));

        $dataResponse = [
            'id'             => $this->User->addFavorite($data),
            'tipe'           => $data['tipe'],
            'jenisTransaksi' => $data['jenis_transaksi'],
            'noRekening'     => $data['no_rekening']
        ];

        log_message('debug', 'end of ' . __FUNCTION__ . ' => ' . json_encode($dataResponse));

        return $this->send_response('success', '', $dataResponse);
    }

    function favorite_delete()
    {
        $token = $this->getToken();
        if ($token) {
            $id = $this->query('id');
            $this->User->deleteFavorite($id, $token->id);
            $this->set_response(array(
                'status' => 'success',
                'message' => 'Favorite berhasil dihapus'
            ));
        } else {
            $this->errorUnAuthorized();
        }
    }

    function checkNoRekening($noRek, $tipe)
    {
        if ($noRek == NULL) {
            $this->form_validation->set_message('checkNoRekening', 'No rekening tidak boleh kosong');
            return FALSE;
        }

        //Check no rekening berdasarkan tipe favorite
        if ($tipe == 'emas') {
            //Check apakah rekening emas ada
            if ($this->EmasModel->isRekeningExist($noRek)) {
                return TRUE;
            } else {
                $this->form_validation->set_message('checkNoRekening', 'No rekening tidak valid/ditemukan');
                return FALSE;
            }
        }
    }


    /**
     * Callback function untuk vaidasi tipe favorite
     * @param type $str
     * @return boolean
     */
    function checkTipeFavorite($str)
    {
        if ($str == NULL) {
            $this->form_validation->set_message('checkTipeFavorite', 'tipe favorite tidak valid');
            return FALSE;
        }

        $allowedStr = array('emas', 'payment', 'gadai', 'mikro', 'gcash', 'mpo');
        if (in_array($str, $allowedStr)) {
            $this->form_validation->set_message('checkTipeFavorite', 'tipe favorite tidak valid');
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Endpoint untuk mendapatkan beberapa status user
     */
    function status_get()
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);

            //Check customer untuk mendapatkan KYC
            $kycVerified = false;

            if ($user->cif) {

                $checkCustomer = $this->check_cif($user->cif);
                if ($checkCustomer->responseCode == '00') {

                    if (isset($checkCustomer->data)) {
                        $customerData = json_decode($checkCustomer->data);

                        $kycVerified = $customerData->statusKyc == '1' ? TRUE : FALSE;

                        //Update status KYC user
                        $this->User->updateUser($token->id, array(
                            'kyc_verified' => $customerData->statusKyc
                        ));
                    }
                }
            }

            $status = array(
                'cif' => $user->cif,
                'emailVerified' => $user->email_verified == '1' ? TRUE : FALSE,
                'kycVerified' => $kycVerified,
                'pinAvailable' => $user->pin != "" ? TRUE : FALSE,
                'aktifasiTransFinansial' => $user->aktifasiTransFinansial
            );

            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $status
            ));
        } else {
            $this->errorUnAuthorized();
        }
    }

    // function reset_pin_post(){
    //     $token = $this->getToken();
    //     if($token)
    //     {
    //         $user = $this->User->getUser($token->id);

    //         $reffId = rand ( 10000 , 99999 );
    //         $sendOTP = $this->sendOTP($user->no_hp, $reffId, 'pin-reset', $token->channelId);



    //     }
    //     else
    //     {
    //         $this->errorUnAuthorized();
    //     }
    // }

    function check_profile_post()
    {
        $token = $this->getToken();

        if ($token) {

            $data = $this->db->select('jenis_identitas, no_hp, nama, jenis_kelamin, tempat_lahir, tgl_lahir, alamat, id_kelurahan, status_kawin,nama_ibu, foto_ktp_url')
                ->where('user_AIID', (int) $token->id)
                ->get('user')->row();

            $data = (array) $data;

            if (in_array('', $data, true)) {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Data is not complete',
                    'data' => $data
                ));
            } else {
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Data has completed',
                    'data' => $data
                ), 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    public function check_password_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
        }
        $set_data = array(
            'current_password'  => $this->put('current_password')
        );
        $this->form_validation->set_data($set_data);

        $this->form_validation->set_rules('current_password', 'password', 'required');
        if (!$this->form_validation->run()) {
            $this->response(array(
                'code' => 101,
                'status' => 'error',
                'message' => 'Invalid input',
                'errors' => $this->form_validation->error_array()
            ), 200);
        }
        if (!$this->User->isValidPassword($token->id, $this->post('current_password'))) {
            $this->response(array(
                'code' => 102,
                'status' => 'error',
                'message' => 'Password tidak cocok',
                'errors' => array(
                    'current_password' => $this->post('current_password'),
                )
            ), 200);
            return;
        }

        $this->response(array(
            'status' => 'success',
            'message' => 'Password Sesuai',
        ), 200);
    }


    // ***********
    // ini fungsi untuk register pin yang di hit dari switching, dimana fungsi ini untuk aktifasi finansial
    function registrasi_pin_post()
    {
        log_message('debug',  __FUNCTION__  . ' MPOS Registrasi Pin ' . ' Start');
        if (!$this->authCore()) {
            $this->errorUnAuthorized();
            log_message('debug',  __FUNCTION__  . ' MPOS Registrasi Pin ' . ' UnAuthorized');
            return;
        }

        $this->form_validation->set_rules('cif', 'Cif', 'required');
        $this->form_validation->set_rules('pin', 'Pin', 'required');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101,
            ]);
            log_message('debug',  __FUNCTION__  . ' MPOS Registrasi Pin ' . $error);
        } else {
            $val = [
                'cif' => $this->post('cif'),
                'pin' => md5($this->post('pin'))
            ];

            log_message('debug',  __FUNCTION__  . ' MPOS Registrasi Pin ' . json_encode($val));

            $register = $this->User->registerPin($val);
            if ($register == true) {
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Registrasi PIN Sukses'
                ), 200);
            } else {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Pin tidak di temukan atau belum dilakukan aktifasi finansial'
                ), 200);
                log_message('debug',  __FUNCTION__  . ' MPOS Registrasi Pin Pin tidak di temukan atau belum dilakukan aktifasi finansial ');
            }
        }
        log_message('debug',  __FUNCTION__  . 'MPOS Registrasi Pin ' . ' End');
    }

    // ***********
    // ini fungsi untuk aktifasi finansial yang di hit dari MPOS, dimana fungsi ini untuk flag aktifasiTransFinansial yang ada di db pds
    function aktivasi_finansial_post()
    {
        log_message('debug',  __FUNCTION__  . ' MPOS Aktifasi Finansial ' . ' Start');
        if (!$this->authCore()) {
            $this->errorUnAuthorized();
            log_message('debug',  __FUNCTION__  . ' MPOS Aktifasi Finansial ' . ' UnAuthorized');
            return;
        }
        $this->form_validation->set_rules('cif', 'Cif', 'required');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101,
            ]);
            log_message('debug',  __FUNCTION__  . ' MPOS Aktifasi Finansial ' . $error);
            return;
        } else {
            $val = [
                'cif' => $this->post('cif'),
            ];
            $register = $this->User->aktifasiTransFinansial($val);
            if ($register == true) {
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Akfitasi finansial sukses'
                ), 200);
            } else {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'CIF Tidak Tersedia'
                ), 200);
                log_message('debug',  __FUNCTION__  . ' MPOS Aktifasi Finansial CIF Tidak Tersedia');
            }
        }
        log_message('debug',  __FUNCTION__  . 'MPOS Registrasi Pin ' . ' End');
    }
    // ***********
    // ini fungsi untuk cek pin yang di hit dari switching, dimana fungsi ini untuk cek pin yang ada di pds
    function cek_pin_post()
    {
        log_message('debug',  __FUNCTION__  . 'MPOS Cek Pin ' . ' Start');
        if (!$this->authCore()) {
            $this->errorUnAuthorized();
            return;
            log_message('debug',  __FUNCTION__  . 'MPOS Cek Pin ' . ' UnAuthorized');
        }

        $this->form_validation->set_rules('cif', 'Cif', 'required');
        $this->form_validation->set_rules('pin', 'Pin', 'required');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101,
            ]);
            log_message('debug',  __FUNCTION__  . 'MPOS Cek Pin ' . $error);
            return;
        } else {
            $val = [
                'cif' => $this->post('cif'),
                'pin' => md5($this->post('pin'))
            ];
            $register = $this->User->cekPin($val);
            if ($register == true) {
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'PIN Tersedia dan sudah di aktifasi finansial'
                ), 200);
            } else {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'PIN Tidak Tersedia dan belum di aktifasi finansial'
                ), 200);
            }
        }
        log_message('debug',  __FUNCTION__  . 'MPOS Cek Pin ' . ' End');
    }
    // ***********
    // ini fungsi untuk update cif (Link cif) yang di hit dari switching
    function update_cif_post()
    {
        log_message('debug',  __FUNCTION__  . ' MPOS Link CIF ' . ' Start');
        if (!$this->authCore()) {
            $this->errorUnAuthorized();
            log_message('debug',  __FUNCTION__  . ' MPOS Link CIF ' . ' Authorized');
            return;
        }
        $this->form_validation->set_rules('cif', 'Cif', 'required');
        $this->form_validation->set_rules('no_tlp', 'No Tlp', 'required');
        if ($this->form_validation->run() == FALSE) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101,
            ]);
            log_message('debug', __FUNCTION__ . ' MPOS Link CIF Error ' . $error);
            return;
        }

        $cek = $this->User->getUser($this->post('no_tlp'));

        if (!$cek) {
            $this->set_response(array(
                'status' => 'error',
                'message' => 'CIF Tidak Tersedia'
            ), 200);
            return;
        }

        //Send Notifikasi Link CIF success
        $notifData = array(
            'namaNasabah' => $cek->nama,
            'cif' => $this->post('cif'),
            'tanggalCif' => date('Y-m-d H:i:s')
        );
        log_message('debug', __FUNCTION__ . ' MPOS Link CIF Notif ' . json_encode($notifData));

        $template = $this->generateLinkCifNotif($notifData);

        $mobileTemplate = $template['mobile'];
        $emailTemplate = $template['email'];
        $minimalTemplate = $template['minimal'];

        //Simpan notifikasi baru
        $notifId = $this->NotificationModel->add(
            $cek->user_AIID,
            NotificationModel::TYPE_PROFILE,
            NotificationModel::CONTENT_TYPE_HTML,
            "Profile Info",
            "Link CIF Berhasil",
            $mobileTemplate,
            $minimalTemplate,
            "LK"
        );

        $this->load->helper('message');
        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $cek->fcm_token,
            [
                "id" => $notifId,
                "tipe" => "LK",
                "title" => "Profile Info",
                "tagline" => "Link CIF Berhasil",
                "content" => "Link CIF Berhasil",
                "token" => $cek->no_hp
            ]
        );


        // Update CIF
        $val = [
            'cif' => $this->post('cif'),
        ];
        $this->User->updateUser($this->post('no_tlp'), $val);
        $this->set_response(array(
            'status' => 'success',
            'message' => 'CIF Sukses di update'
        ), 200);

        log_message('debug', __FUNCTION__  . ' MPOS Link CIF ' . ' End');
    }

    function check_rekening_user_get()
    {
        log_message('info', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->query()));
        $token = $this->getToken();

        // check token
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('info', 'end of ' . __FUNCTION__ . ' => errorUnAuthorized');

            return;
        }

        // check if user has bank account
        if (empty($this->BankModel->getBank($token->id))) {
            $response = $this->send_response('error', 'Belum ada rekening terdaftar.', '');
            log_message('info', 'end of ' . __FUNCTION__ . ' => ' . json_encode($response));

            return;
        }

        $response = $this->send_response('success', 'Ada rekening terdaftar', '');
        log_message('info', 'end of ' . __FUNCTION__ . ' => ' . json_encode($response));
    }
}
