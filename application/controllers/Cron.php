<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \Curl\Curl;

class Cron extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $models = [
            'User',
            'NotificationModel',
            'PaymentModel',
            'EmasModel',
            'GadaiModel',
            'MikroModel',
            'MpoModel',
            'WalletModel',
            'ConfigModel',
            'MuliaModel',
            'MasterModel',
            'GpoinModel'
        ];
        $this->load->model($models);
        $this->load->helper(array('Message'));
    }

    /**
     * =========================================================================================================
     * CORE RELATED FUNCTION
     * =========================================================================================================
     */

    /**
     * Mendapatkan access token core pegadaian
     * Token digunakan untuk inquiry data ke pegadaian yang membutuhkan authorization
     * @return string
     */
    public function getCoreToken($refresh = '0')
    {

        if ($refresh == "1") {

            $process = curl_init($this->config->item("core_API_URL") . "/oauth/token");
            curl_setopt($process, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: ' . $this->config->item("core_auth_header")
            ));
            $post = [
                'username' => $this->config->item('core_post_username'),
                'password' => $this->config->item('core_post_password'),
                'grant_type' => 'password',
            ];
            curl_setopt($process, CURLOPT_HEADER, 0);
            curl_setopt($process, CURLOPT_TIMEOUT, 30);
            curl_setopt($process, CURLOPT_POST, 1);
            curl_setopt($process, CURLOPT_PROXY, '');
            curl_setopt($process, CURLOPT_POSTFIELDS, http_build_query($post));
            curl_setopt($process, CURLOPT_HTTPPROXYTUNNEL, false);
            curl_setopt($process, CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($process, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($process, CURLOPT_SSL_VERIFYPEER, 0);

            curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
            $return = curl_exec($process);
            curl_close($process);


            if ($return == false) {
                log_message('error', 'cronjob - ' . __FUNCTION__ . ' => CRON Get Core Token Error');
                log_message('error', 'cronjob - ' . __FUNCTION__ . ' => CRON Curl error: ' . curl_error($process));
                return null;
            } else {
                $data = json_decode($return);
                $access_token = $data->access_token;

                $this->db->where('variable', 'core_access_token')->update('config', array('value' => $access_token));
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON Refresh Core Token Success');
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => ' . $access_token);

                return $access_token;
            }
        }

        $cek = $this->db->where('variable', 'core_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        }
    }

    /**
     * =========================================================================================================
     * GADAI ONLINE RELATED FUNCTION
     * =========================================================================================================
     */

    public function sendNotifFiveMinutesLogamMulia()
    {
        // get configuration production is true/false
        $gosend_url = $this->config->item('gosend_url');
        $gosend_client_id = $this->config->item('gosend_clientid');
        $gosend_passkey = $this->config->item('gosend_passkey');

        $dataUnCompleted = $this->GadaiModel->getUnCompleteGodLogamMulia();
        foreach ($dataUnCompleted as $key => $value) {
            $un_completed = $dataUnCompleted[$key];
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach kode_booking : ' . $un_completed->kode_booking);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach no_oder_gosend: ' . $un_completed->no_order_gosend);
            $client = new GuzzleHttp\Client(); //GuzzleHttp\Client
            $result = $client->request('GET', $gosend_url . '/' . $un_completed->no_order_gosend, [
                'headers' => [
                    'Client_id' => $gosend_client_id,
                    'pass_key'     => $gosend_passkey
                ]
            ]);
            $response = $result->getBody()->getContents();
            $data = json_decode($response);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => response status gosend : ' . $data->status);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => data un completed : ' . json_encode($dataUnCompleted));
            if ($data->status == 'Enroute Pickup' || $data->status == 'Out For Pickup' || $data->status == 'Out For Delivery') {
                $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => '5'));
                $idNotif = $this->NotificationModel->add(
                    $un_completed->user_AIID,
                    NotificationModel::TYPE_GADAI,
                    NotificationModel::CONTENT_TYPE_HTML,
                    "Status Pengiriman",
                    "Barang Sedang dijemput Kurir",
                    "mobile",
                    "content",
                    "GOD"
                );
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: SUCCESS');

                Message::sendFCMNotif(
                    $this->User->getFCMToken($un_completed->user_AIID),
                    [
                        "id" => $idNotif,
                        "tipe" => "SerahTerimaBarangLM",
                        "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#1",
                        "tagline" => $un_completed->kode_booking . " Kurir Sudah Ditemukan",
                        "action_url" => "SerahTerimaBarangLM",
                        "kode_booking" => $un_completed->kode_booking
                    ]
                );
            } else {
                log_message('error', 'cronjob - ' . __FUNCTION__ . ' => ' . $response);
            }
        }
    }

    public function sendNotifFiveMinutesPerhiasan()
    {
        // get configuration production is true/false
        $gosend_url = $this->config->item('gosend_url');
        $gosend_client_id = $this->config->item('gosend_clientid');
        $gosend_passkey = $this->config->item('gosend_passkey');

        $dataUnCompleted = $this->GadaiModel->getUnCompleteGodPerhiasan();
        foreach ($dataUnCompleted as $key => $value) {
            $un_completed = $dataUnCompleted[$key];
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach kode_booking : ' . $un_completed->kode_booking);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach no_oder_gosend: ' . $un_completed->no_order_gosend);
            $client = new GuzzleHttp\Client(); //GuzzleHttp\Client
            $result = $client->request('GET', $gosend_url . '/' . $un_completed->no_order_gosend, [
                'headers' => [
                    'Client_id' => $gosend_client_id,
                    'pass_key' => $gosend_passkey
                ]
            ]);
            $response = $result->getBody()->getContents();
            $data = json_decode($response);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => response status gosend : ' . $data->status);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => data un completed : ' . json_encode($dataUnCompleted));
            if ($data->status == 'Enroute Pickup' || $data->status == 'Out For Pickup' ||  $data->status == 'Out For Delivery') {
                $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => '5'));
                $idNotif = $this->NotificationModel->add(
                    $un_completed->user_AIID,
                    NotificationModel::TYPE_GADAI,
                    NotificationModel::CONTENT_TYPE_HTML,
                    "Status Pengiriman",
                    "Barang Sedang dijemput Kurir",
                    "mobile",
                    "content",
                    "GOD"
                );
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: SUCCESS');

                Message::sendFCMNotif(
                    $this->User->getFCMToken($un_completed->user_AIID),
                    [
                        "id" => $idNotif,
                        "tipe" => "SerahTerimaBarang",
                        "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#2",
                        "tagline" => $un_completed->kode_booking . " Kurir Sudah Ditemukan",
                        "action_url" => "SerahTerimaBarang",
                        "kode_booking" => $un_completed->kode_booking
                    ]
                );
            } else {
                log_message('error', 'cronjob - ' . __FUNCTION__ . ' => ' . $response);
            }
        }
    }

    public function getStatusGosendLogamMulia()
    {
        // get configuration production is true/false
        $gosend_url = $this->config->item('gosend_url');
        $gosend_client_id = $this->config->item('gosend_clientid');
        $gosend_passkey = $this->config->item('gosend_passkey');

        $dataUnCompleted = $this->GadaiModel->getUnCompleteGodLogamMulia();
        foreach ($dataUnCompleted as $key => $value) {

            $un_completed = $dataUnCompleted[$key];
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach kode_booking : ' . $un_completed->kode_booking);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach no_oder_gosend: ' . $un_completed->no_order_gosend);

            try {
                $client = new GuzzleHttp\Client(); //GuzzleHttp\Client
                $result = $client->request('GET', $gosend_url . '/' . $un_completed->no_order_gosend, [
                    'headers' => [
                        'Client_id' => $gosend_client_id,
                        'pass_key' => $gosend_passkey
                    ]
                ]);
                $response = $result->getBody()->getContents();
                $data = json_decode($response);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => response GOJEK Logam Mulia dan kode booking:' . json_encode($data) . 'kode_booking : ' . $un_completed->kode_booking);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => response status gosend : ' . $data->status);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => data un completed : ' . json_encode($dataUnCompleted));


                if (($data->status == 'Enroute Pickup' || $data->status == 'Out For Pickup' || $data->status == 'Out For Delivery') && ($un_completed->status_fcm == 0 || $un_completed->status_fcm == 99 || $un_completed->status_fcm == 98)) {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => '5'));

                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Barang Sedang dijemput Kurir",
                        "mobile",
                        "content",
                        "GOD"
                    );


                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: ' . $user->email . ' SUCCESS');

                    //cek status fcm belum kirim (status_fcm == 0)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 5)
                        ->get('gadai_logam_mulia')->row();
                    if ($data->status_fcm == 0 || $data->status_fcm == 99 || $un_completed->status_fcm == 98) {

                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $idNotif,
                                "tipe" => "SerahTerimaBarangLM",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#1",
                                "tagline" => $un_completed->kode_booking . " Kurir Sudah Ditemukan",
                                "action_url" => "SerahTerimaBarangLM",
                                "kode_booking" => $un_completed->kode_booking,
                                "message_gosend" => "Driver sedang menuju lokasi Anda"
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => 5, 'status_fcm' => 1));
                        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => kode booking : ' . $un_completed->kode_booking . ' gadai logam mulia status Enroute Pickup (status_fcm=1)');
                    }
                } elseif ($data->status == 'Driver not found' && $un_completed->status_fcm == 0) {
                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Driver not found",
                        "mobile",
                        "content",
                        "GOD"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Driver not found');

                    //cek status fcm belum kirim (status_fcm == 0)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 8)
                        ->get('gadai_logam_mulia')->row();
                    if ($data->status_fcm == 0) {

                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $idNotif,
                                "tipe" => "LookingDriverLM",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#1",
                                "tagline" => $un_completed->kode_booking . " Maaf gagal mencari Kurir, Silahkan Pesan Kurir kembali",
                                "action_url" => "LookingDriverLM",
                                "kode_booking" => $un_completed->kode_booking,
                                "message_gosend" => "Gagal menemukan driver"
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => 8, 'status_fcm' => 99));
                        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => kode booking :' . $un_completed->kode_booking . ' gadai logam mulia status Driver not found (status_fcm=1)');
                    }
                } elseif ($data->status == 'Completed') {
                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => barang sudah di outlet : ' . $data->status);
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => 9));
                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Barang sudah sampai outlet",
                        "mobile",
                        "content",
                        "GOD"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: SUCCESS');

                    //cek status fcm belum kirim (status_fcm == 1)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 9)
                        ->get('gadai_logam_mulia')->row();
                    if ($data->status_fcm == 1) {
                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $idNotif,
                                "tipe" => "DetailTransaksiAfterGopayLM",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#1",
                                "tagline" => $un_completed->kode_booking . " Barang sudah sampai outlet",
                                "action_url" => "DetailTransaksiAfterGopayLM",
                                "kode_booking" => $un_completed->kode_booking,
                                "message_gosend" => "Driver Sampai di lokasi penyerahan barang"
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status_fcm' => 2));
                    }
                } elseif ($data->status == 'Cancelled') {
                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Driver Cancelled",
                        "mobile",
                        "content",
                        "GOD"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Driver Cancelled');

                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => 8));
                    //cek status fcm belum kirim (status_fcm == 0 atau 1)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 8)
                        ->get('gadai_logam_mulia')->row();

                    if ($data->status_fcm == 1 || $data->status_fcm == 0) {
                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $idNotif,
                                "tipe" => "LookingDriverLM",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#1",
                                "tagline" => $un_completed->kode_booking . " Kurir telah membatalkan gosend, Silahkan Pesan Kurir kembali",
                                "action_url" => "LookingDriverLM",
                                "kode_booking" => $un_completed->kode_booking,
                                "message_gosend" => "Kurir membatalkan pesanan"
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => 8, 'status_fcm' => 98));

                        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => kode booking :' . $un_completed->kode_booking . ' gadai logam mulia status Driver Cancelled (status_fcm=98)');
                    }
                } else {
                    log_message('error', 'cronjob - ' .__FUNCTION__ . ' => ' . $response);
                }
            } catch (Exception $e) {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => order gosend tidak ditemukan' . $e->getMessage());
                log_message('error', 'cronjob - ' . __FUNCTION__ . ' => order gosend tidak ditemukan' . $e->getMessage());
            }
        }
    }

    public function getStatusGosendPerhiasan()
    {
        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' start get status gosend perhiasan');
        // get configuration production is true/false
        $gosend_url = $this->config->item('gosend_url');
        $gosend_client_id = $this->config->item('gosend_clientid');
        $gosend_passkey = $this->config->item('gosend_passkey');

        $dataUnCompleted = $this->GadaiModel->getUnCompleteGodPerhiasan();
        foreach ($dataUnCompleted as $key => $value) {
            $un_completed = $dataUnCompleted[$key];
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach kode_booking : ' . $un_completed->kode_booking);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach no_oder_gosend: ' . $un_completed->no_order_gosend);

            try {
                $client = new GuzzleHttp\Client(); //GuzzleHttp\Client
                $result = $client->request('GET', $gosend_url . '/' . $un_completed->no_order_gosend, [
                    'headers' => [
                        'Client_id' => $gosend_client_id,
                        'pass_key' => $gosend_passkey
                    ]
                ]);
                $response = $result->getBody()->getContents();
                $data = json_decode($response);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => response GOJEK Perhiasan dan kode booking:' . json_encode($data) . 'kode_booking : ' . $un_completed->kode_booking);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => response status gosend : ' . $data->status);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => data un completed : ' . json_encode($dataUnCompleted));

                if (($data->status == 'Enroute Pickup' || $data->status == 'Out For Pickup' || $data->status == 'Out For Delivery') && ($un_completed->status_fcm == 0 || $un_completed->status_fcm == 99 || $un_completed->status_fcm == 98)) {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => '5'));
                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Barang Sedang dijemput Kurir",
                        "mobile",
                        "content",
                        "GOD"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: SUCCESS');

                    //cek status fcm belum kirim (status_fcm == 0)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 5)
                        ->get('gadai_perhiasan')->row();
                    if ($data->status_fcm == 0 || $data->status_fcm == 99 || $un_completed->status_fcm == 98) {
                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $idNotif,
                                "tipe" => "SerahTerimaBarang",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#2",
                                "tagline" => $un_completed->kode_booking . " Kurir Sudah Ditemukan",
                                "action_url" => "SerahTerimaBarang",
                                "kode_booking" => $un_completed->kode_booking,
                                "message_gosend" => "Driver sedang menuju lokasi Anda"
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => 5, 'status_fcm' => 1));
                        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => kode booking :' . $un_completed->kode_booking . ' gadai perhiasan status Enroute Pickup (status_fcm=1)');
                    }
                } elseif ($data->status == 'Driver not found' && $un_completed->status_fcm == 0) {
                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Driver not found",
                        "mobile",
                        "content",
                        "GOD"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Driver not found');
                    //cek status fcm belum kirim (status_fcm == 0)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 8)
                        ->get('gadai_perhiasan')->row();
                    if ($data->status_fcm == 0) {
                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $idNotif,
                                "tipe" => "LookingDriver",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#2",
                                "tagline" => $un_completed->kode_booking . " Maaf gagal mencari Kurir, Silahkan Pesan Kurir kembali",
                                "action_url" => "LookingDriver",
                                "kode_booking" => $un_completed->kode_booking,
                                "message_gosend" => "Gagal menemukan driver"
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => 8, 'status_fcm' => 99));
                        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => kode booking :' . $un_completed->kode_booking . ' gadai perhiasan status Driver not found (status_fcm=1)');
                    }
                } elseif ($data->status == 'Completed') {

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => barang sudah di outlet : ' . $data->status);
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => 9));
                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Barang sudah sampai outlet",
                        "mobile",
                        "content",
                        "GOD"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: SUCCESS');

                    //cek status fcm belum kirim (status_fcm == 1)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 9)
                        ->get('gadai_perhiasan')->row();
                    if ($data->status_fcm == 1) {

                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $idNotif,
                                "tipe" => "DetailTransaksiAfterGopay",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#2",
                                "tagline" => $un_completed->kode_booking . " Barang sudah sampai outlet",
                                "action_url" => "DetailTransaksiAfterGopay",
                                "kode_booking" => $un_completed->kode_booking,
                                "message_gosend" => "Driver Sampai di lokasi penyerahan barang"
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status_fcm' => '2'));
                    }
                } elseif ($data->status == 'Cancelled') {
                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Driver Cancelled",
                        "mobile",
                        "content",
                        "GOD"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Driver Cancelled');
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => 8));
                    //cek status fcm belum kirim (status_fcm == 0)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 8)
                        ->get('gadai_perhiasan')->row();
                    if ($data->status_fcm == 1 || $data->status_fcm == 0) {
                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $idNotif,
                                "tipe" => "LookingDriver",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#2",
                                "tagline" => $un_completed->kode_booking . " Kurir telah membatalkan gosend, Silahkan Pesan Kurir kembali",
                                "action_url" => "LookingDriver",
                                "kode_booking" => $un_completed->kode_booking,
                                "message_gosend" => "Kurir membatalkan pesanan"
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => 8, 'status_fcm' => 98));

                        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => kode booking :' . $un_completed->kode_booking . ' gadai  perhiasan status Driver Cancelled (status_fcm=98)');
                    }
                } else {
                    log_message('error', 'cronjob - ' .__FUNCTION__ . ' => ' . $response);
                }
            } catch (Exception $e) {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => order gosend tidak ditemukan' . $e->getMessage());
                log_message('error', 'cronjob - ' . __FUNCTION__ . ' => order gosend tidak ditemukan' . $e->getMessage());
            }
        }
    }

    public function getStatusTolakPerhiasan()
    {
        // get configuration production is true/false
        $gosend_url = $this->config->item('gosend_url');
        $gosend_client_id = $this->config->item('gosend_clientid');
        $gosend_passkey = $this->config->item('gosend_passkey');

        $dataUnCompleted = $this->GadaiModel->getStatusDitolakPerhiasan();
        foreach ($dataUnCompleted as $key => $value) {

            $un_completed = $dataUnCompleted[$key];
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach kode_booking : ' . $un_completed->kode_booking);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => foreach no_oder_gosend: ' . $un_completed->no_order_gosend);

            try {
                $client = new GuzzleHttp\Client(); //GuzzleHttp\Client
                $result = $client->request('GET', $gosend_url . '/' . $un_completed->no_order_gosend, [
                    'headers' => [
                        'Client_id' => $gosend_client_id,
                        'pass_key' => $gosend_passkey
                    ]
                ]);
                $response = $result->getBody()->getContents();
                $data = json_decode($response);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => response status gosend : ' . $data->status);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => data tolak : ' . json_encode($dataUnCompleted));

                if ($data->status == 'Enroute Pickup' || $data->status == 'Out For Pickup' || $data->status == 'Out For Delivery') {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => '92'));
                    //cek status fcm belum kirim (status_fcm == 2)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 92)
                        ->get('gadai_perhiasan')->row();
                    if ($data->status_fcm == 2) {
                        $idNotif = $this->NotificationModel->add(
                            $un_completed->user_AIID,
                            NotificationModel::TYPE_GADAI,
                            NotificationModel::CONTENT_TYPE_HTML,
                            "Status Pengiriman",
                            "Barang Sedang dikembalikan",
                            "mobile",
                            "content",
                            "GOD"
                        );

                        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: SUCCESS');

                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $data->liveTrackingUrl,
                                "tipe" => "Pengembalian",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#2",
                                "tagline" => $un_completed->kode_booking . " Barang Sedang dikembalikan",
                                "track_url" => "",
                                "token" => $user->no_hp
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status_fcm' => 97));
                    }
                } elseif ($data->status == 'Driver not found') {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => 94, 'status_fcm' => 2));
                    $idNotif = $this->NotificationModel->addAdminNotificaion(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Pegadaian Digital Service",
                        "Gagal Memesan Kurir",
                        "",
                        "",
                        "",
                        "",
                        $un_completed->kode_booking
                    );
                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => kode booking :' . $un_completed->kode_booking . ' gadai perhiasan status Driver not found (status_fcm=1)');
                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Driver not found');
                } elseif ($data->status == 'Completed' && $un_completed->status == 92) {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => '4'));
                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Barang Sudah dikembalikan",
                        "mobile",
                        "content",
                        "GOD"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: ' . $user->email . ' SUCCESS');
                    //cek status fcm belum kirim (status_fcm == 97)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 4)
                        ->get('gadai_perhiasan')->row();
                    if ($data->status_fcm == 97) {
                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $data->liveTrackingUrl,
                                "tipe" => "Pengembalian",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#1",
                                "tagline" => $un_completed->kode_booking . " Barang Sudah dikembalikan",
                                "track_url" => "",
                                "token" => $user->no_hp
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status_fcm' => 96));
                    }
                } elseif ($data->status == 'Cancelled') {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_perhiasan', array('status' => 94, 'status_fcm' => 2));
                    $idNotif = $this->NotificationModel->addAdminNotificaion(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Driver Cancelled",
                        "",
                        "",
                        "",
                        "",
                        $un_completed->kode_booking
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Driver Cancelled');
                } else {
                    log_message('error', 'cronjob - ' .__FUNCTION__ . ' => ' . $response);
                }
            } catch (Exception $e) {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => order gosend tidak ditemukan: ' . $e->getMessage());
                log_message('error', 'cronjob - ' . __FUNCTION__ . ' => order gosend tidak ditemukan: ' . $e->getMessage());
            }
        }
    }

    public function getStatusTolakLogamMulia()
    {
        // get configuration production is true/false
        $gosend_url = $this->config->item('gosend_url');
        $gosend_client_id = $this->config->item('gosend_clientid');
        $gosend_passkey = $this->config->item('gosend_passkey');

        $dataUnCompleted = $this->GadaiModel->getStatusDitolakLogamMulia();
        foreach ($dataUnCompleted as $key => $value) {

            $un_completed = $dataUnCompleted[$key];
            try {
                $client = new GuzzleHttp\Client(); //GuzzleHttp\Client
                $result = $client->request('GET', $gosend_url . '/' . $un_completed->no_order_gosend, [
                    'headers' => [
                        'Client_id' => $gosend_client_id,
                        'pass_key' => $gosend_passkey
                    ]
                ]);
                $response = $result->getBody()->getContents();
                $data = json_decode($response);

                if ($data->status == 'Enroute Pickup' || $data->status == 'Out For Pickup' || $data->status == 'Out For Delivery') {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => '92'));
                    //cek status fcm belum kirim (status_fcm == 2)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 92)
                        ->get('gadai_logam_mulia')->row();
                    if ($data->status_fcm == 2) {
                        $user = $this->User->getUser($un_completed->user_AIID);
                        $idNotif = $this->NotificationModel->add(
                            $user->user_AIID,
                            NotificationModel::TYPE_GADAI,
                            NotificationModel::CONTENT_TYPE_HTML,
                            "Status Pengiriman",
                            "Barang Sedang dikembalikan",
                            "mobile",
                            "minimal",
                            "GO-SEND"
                        );
                        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: ' . $user->email . ' SUCCESS');

                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $data->liveTrackingUrl,
                                "tipe" => "PengembalianLM",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#1",
                                "tagline" => $un_completed->kode_booking . " Barang Sedang dikembalikan",
                                "track_url" => "",
                                "token" => $user->no_hp
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status_fcm' => 97));
                    }
                } elseif ($data->status == 'Driver not found') {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => 94, 'status_fcm' => 2));
                    $idNotif = $this->NotificationModel->addAdminNotificaion(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Pegadaian Digital Service",
                        "Gagal Memesan Kurir",
                        "",
                        "",
                        "",
                        "",
                        $un_completed->kode_booking
                    );
                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => kode booking :' . $un_completed->kode_booking . ' gadai perhiasan status Driver not found (status_fcm=1)');
                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Driver not found');
                } elseif ($data->status == 'Completed' && $un_completed->status == 92) {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => '4'));
                    $idNotif = $this->NotificationModel->add(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Barang Sudah dikembalikan",
                        "mobile",
                        "content",
                        "GOD"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message: ' . $user->email . ' SUCCESS');
                    //cek status fcm belum kirim (status_fcm == 97)
                    $data = $this->db->select('status_fcm')
                        ->where('kode_booking', $un_completed->kode_booking)
                        ->where('status', 4)
                        ->get('gadai_logam_mulia')->row();
                    if ($data->status_fcm == 97) {
                        Message::sendFCMNotif(
                            $this->User->getFCMToken($un_completed->user_AIID),
                            [
                                "id" => $data->liveTrackingUrl,
                                "tipe" => "PengembalianLM",
                                "title" => "Pegadaian Digital Service kode booking #" . $un_completed->kode_booking . "#1",
                                "tagline" => $un_completed->kode_booking . " Barang Sudah dikembalikan",
                                "track_url" => "",
                                "token" => $user->no_hp
                            ]
                        );
                        $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status_fcm' => 96));
                    }
                } elseif ($data->status == 'Cancelled') {
                    $this->db->where('kode_booking', $un_completed->kode_booking)->update('gadai_logam_mulia', array('status' => 94, 'status_fcm' => 2));
                    $idNotif = $this->NotificationModel->addAdminNotificaion(
                        $un_completed->user_AIID,
                        NotificationModel::TYPE_GADAI,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Status Pengiriman",
                        "Driver Cancelled",
                        "",
                        "",
                        "",
                        "",
                        $un_completed->kode_booking
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Driver Cancelled');
                } else {
                    log_message('error', 'cronjob - ' . __FUNCTION__ . ' => CRON error ');
                }
            } catch (Exception $e) {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => order gosend tidak ditemukan' . $e->getMessage());
                log_message('error', 'cronjob - ' . __FUNCTION__ . ' => order gosend tidak ditemukan' . $e->getMessage());
            }
        }
    }


    /**
     * Mendapatkan access token core pegadaian
     * Token digunakan untuk inquiry data ke pegadaian yang membutuhkan authorization
     * @return string
     */
    public function getGadaiEfekToken($refresh = '0')
    {

        if ($refresh == "1") {
            $url = $this->config->item("gadaiefek_API_URL") . "authenticate";
            $username = $this->config->item('gadaiefek_username');
            $password = $this->config->item('gadaiefek_password');

            $data = [
                'username' => $username,
                'password' => $password,
                'rememberMe' => 'true'
            ];

            $curl = new Curl();
            $curl->setHeader('Content-Type', 'application/json');
            $curl->post($url, $data);

            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Get GadaiEfekToken: ' . json_encode($curl->response));

            if ($curl->error) {
                return null;
            } else {
                //Cek jika responseCode == 00 dan tidak mempunyai data
                $resp = $curl->response;
                $this->db->where('variable', 'gadaiefek_access_token')->update('config', [
                    'value' => $resp->id_token,
                ]);
            }
        }

        $cek = $this->db->where('variable', 'gadaiefek_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        }
    }

    public function getGPointToken($refresh = '0')
    {

        if ($refresh == "1") {
            $username = $this->config->item('gpoint_username');
            $password = $this->config->item('gpoint_password');
            $url = $this->config->item("gpoint_API_URL") . "token/get?username=" . $username . "&password=" . $password;
            // $url = $this->config->item("gpoint_API_URL")."token";

            $authUsername = $this->config->item('gpoint_auth_username');
            $authPassword = $this->config->item('gpoint_auth_password');

            $curl = new Curl();
            $curl->setBasicAuthentication($authUsername, $authPassword);
            $curl->get($url);

            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => GPOINT URL:' . $url);


            if ($curl->error) {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => ' . $curl->errorCode . ' : ' . $curl->errorMessage);
                return null;
            } else {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Get GadaiEfekToken: ' . json_encode($curl->response));

                //Cek jika responseCode == 00 dan tidak mempunyai data
                $resp = $curl->response;

                $this->db->where('variable', 'gpoint_access_token')->update('config', [
                    'value' => $resp->data->token,
                ]);
            }
        }

        $cek = $this->db->where('variable', 'gpoint_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        }
    }

    public function getMsGcashToken($refresh = '0')
    {

        if ($refresh == "1") {
            $clientId = $this->config->item('msgcash_client_id');
            $clientSecret = $this->config->item('msgcash_client_secret');
            $grantType = $this->config->item('msgcash_grant_type');
            $refreshToken = $this->ConfigModel->getMsGcashRefreshToken();

            $url = $this->config->item("msgcash_API_URL") . "oauth2/token";

            $data  = [
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'grant_type' => $grantType,
                'refresh_token' => $refreshToken
            ];

            $curl = new Curl();
            $curl->post($url, $data);


            if ($curl->error) {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => ' . $curl->errorCode . ' : ' . $curl->errorMessage);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => ' . json_encode($data));
                return null;
            } else {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Get Gcash Microservice Token: ' . json_encode($curl->response));

                //Cek jika responseCode == 00 dan tidak mempunyai data
                $resp = $curl->response;

                $this->db->where('variable', 'msgcash_access_token')->update('config', [
                    'value' => $resp->access_token,
                ]);

                $this->db->where('variable', 'msgcash_refresh_token')->update('config', [
                    'value' => $resp->refresh_token,
                ]);
            }

            $curl->close();
        }

        $cek = $this->db->where('variable', 'msgcash_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        }
    }

    public function getLosToken($refresh = '0')
    {

        if ($refresh == "1") {
            $url = $this->config->item("los_API_URL") . "userlogin";
            $username = $this->config->item('los_username');
            $password = $this->config->item('los_password');

            $data = [
                'userid' => $username,
                'password' => $password,
            ];

            $curl = new Curl();
            $curl->setHeader('Content-Type', 'application/json');
            $curl->post($url, $data);

            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Get LOSToken: ' . json_encode($curl->response));

            if ($curl->error) {
                return null;
            } else {
                //Cek jika responseCode == 00 dan tidak mempunyai data
                $resp = $curl->response;
                if (isset($resp->access_token)) {
                    $this->db->where('variable', 'los_access_token')->update('config', [
                        'value' => $resp->access_token,
                    ]);
                }
            }
        }

        $cek = $this->db->where('variable', 'los_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Get LosEfekToken: ' . $cek->row()->value);
            return $cek->row()->value;
        }
    }

    public function coreRequest($path, $data, $operationId)
    {

        //Log message
        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => ' . $operationId . ' : ' . json_encode($data));

        $tokenChar = $this->getCoreToken('0');
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer ' . $tokenChar);
        $curl->post($this->config->item('core_API_URL') . $path, $data);

        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Response: ' . $operationId . ': ' . json_encode($curl->response));

        if ($curl->error) {
            log_message('error', 'cronjob - ' . __FUNCTION__ . ' => CRON CORE CONNECT ' . $operationId . ': ' . $curl->errorMessage);
            return (object) array(
                'responseCode' =>  $curl->errorCode,
                'responseDesc' => $curl->errorMessage
            );
        } else {
            //Cek jika responseCode == 00 dan tidak mempunyai data
            $resp = $curl->response;
            if ($resp->responseCode == '00' && !isset($resp->data)) {
                return (object) array(
                    'responseCode' =>  $curl->errorCode,
                    'responseDesc' => $curl->errorMessage
                );
            }
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' =>  Response: ' . $operationId . ' : ' . json_encode($curl->response));
            return $curl->response;
        }
    }

    /**
     * Cron untuk mendapatkan daftar gadai efek, saham atau obligasi
     */

    public function getEfekList($option)
    {
        $efekToken = $this->getEfekToken();

        $url = $this->config->item("gadaiefek_API_URL") . "gemefeks/" . $option;

        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer ' . $efekToken);
        $curl->get($url);

        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Get GadaiEfekToken: ' . json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => ' . $curl->errorCode . ' : ' . $curl->errorMessage);
        } else {
            // Clear dan insert  ulang data list efek
            $this->db->where('tipeEfek', $option)->delete('ref_efek');

            $list = $curl->response;
            foreach ($list as $l) {
                $l->tipeEfek = $option;
                $saveData = (array) $l;
                $this->db->insert('ref_efek', $saveData);
            }
        }
    }


    /**
     * Buka tabungan emas baru khusus untuk bank
     * @param type $amount
     * @param type $flag
     * @param type $ibuKandung
     * @param type $idKelurahan
     * @param type $jalan
     * @param type $jenisKelamin
     * @param type $kewarganegaraan
     * @param type $kodeCabang
     * @param type $namaNasabah
     * @param type $noHP
     * @param type $noIdentitas
     * @param type $reffSwitching
     * @param type $statusKawin
     * @param type $tanggalExpiredId
     * @param type $tanggalLahir
     * @param type $tempatLahir
     * @param type $tipeIdentitas
     */
    public function bukaTabunganEmas(
        $amount,
        $flag,
        $ibuKandung,
        $idKelurahan,
        $jalan,
        $jenisKelamin,
        $kewarganegaraan,
        $kodeCabang,
        $namaNasabah,
        $noHP,
        $noIdentitas,
        $reffSwitching,
        $statusKawin,
        $tanggalExpiredId,
        $tanggalLahir,
        $tempatLahir,
        $tipeIdentitas,
        $channelId = '6017',
        $cif = null,
        $kodeBankPembayar,
        $paymentMethod = 'BANK',
        $gcashId = null
    ) {
        $url = '/tabunganemas/open';

        $data = array(
            'channelId' => $channelId,
            'amount' => $amount,
            'clientId' => $this->config->item('core_post_username'),
            'cif' => $cif,
            'flag' => $flag,
            'ibuKandung' => $ibuKandung,
            'idKelurahan' => $idKelurahan,
            'jalan' => $jalan,
            'jenisKelamin' => $jenisKelamin,
            'kewarganegaraan' => $kewarganegaraan,
            'kodeCabang' => $kodeCabang,
            'namaNasabah' => $namaNasabah,
            'noHp' => $noHP,
            'noIdentitas' => $noIdentitas,
            'reffSwitching' => $reffSwitching,
            'statusKawin' => $statusKawin,
            'tanggalExpiredId' => $tanggalExpiredId,
            'tanggalLahir' => $tanggalLahir,
            'tempatLahir' => $tempatLahir,
            'tipeIdentitas' => $tipeIdentitas,
            'paymentMethod' => $paymentMethod,
            'kodeBankPembayar' => $kodeBankPembayar
        );

        if ($paymentMethod == 'GCASH') {
            $data['gcashId'] = $gcashId;
        }

        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Data tabungan open emas ' . json_encode($data));
        return $this->coreRequest($url, $data, "OpenTabunganEmas");
    }

    public function bukaTabunganEmas2($data)
    {
        $url = '/tabunganemas/open';
        if ($data['paymentMethod'] == 'GCASH') {
            $url = '/gcash/payment';
        }
        $data['clientId'] = $this->config->item('core_post_username');
        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Data tabungan open emas2 ' . json_encode($data));
        return $this->coreRequest($url, $data, "OpenTabunganEmas");
    }

    function detailTabunganEmas($norek, $channelId = '6017')
    {
        $path = '/portofolio/dettabemas';
        $data = array(
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'norek' => $norek
        );

        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Data detail tabungan emas ' . json_encode($data));
        return $this->coreRequest($path, $data, 'DetailTabunganEmas');
    }

    function muliaOpen($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/mulia/open';
        return $this->coreRequest($url, $data, 'Mulia Open');
    }

    function checkCustomer($nama, $ibuKandung, $tanggalLahir, $flag, $channelId = '6017')
    {
        $path = '/customer/check';
        $data = array(
            'ibuKandung' => $ibuKandung,
            'namaNasabah' => $nama,
            'tanggalLahir' => $tanggalLahir,
            'clientId' => $this->config->item('core_post_username'),
            'channelId' => $channelId,
            'flag' => $flag
        );
        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Data check customer ' . json_encode($data));
        return $this->coreRequest($path, $data, 'CheckCustomer');
    }

    function customerLink($cif, $ibuKandung, $namaNasabah, $tglLahir, $username, $channelId = '6017')
    {
        $path = '/customer/link';
        $data = array(
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'cif' => $cif,
            'ibuKandung' => $ibuKandung,
            'namaNasabah' => $namaNasabah,
            'tanggalLahir' => $tglLahir,
            'noHp' => $username,
            'username' => $username
        );

        return $this->coreRequest($path, $data, "LINK CUSTOMER");
    }

    function aktivasiWallet($data)
    {
        $url = '/deposit/activation';
        $data['clientId'] = $this->config->item('core_post_username');

        return $this->coreRequest($url, $data, 'AktivasiWallet');
    }

    function getEfekToken()
    {
        $cek = $this->db->where('variable', 'gadaiefek_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        }
        return null;
    }

    public function gPointValue($data)
    {
        $gPointToken = $this->getGPointToken();

        $url = $this->config->item("gpoint_API_URL") . "api/campaigns/point?userId=" . $data['cif'];

        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer ' . $gPointToken);

        $curl->post($url, $data);

        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => GPOINT VALUE: ' . json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => ' . $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }


    /**
     * =========================================================================================================
     * END CORE RELATED FUNCTION
     * =========================================================================================================
     */



    /**
     * =========================================================================================================
     * CRON FUNCTION
     * =========================================================================================================
     */

    /**
     * Method cron untuk melakukan pengecekan apakah terdapat data open tabungan emas yang belum berhasil tembus core sedangkan sudah bayar
     */
    public function openTabunganEmas()
    {

        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN TABUNGAN BEGIN');

        $where = array(
            'jenis_transaksi' => 'OP',
            'is_paid' => '1',
            'openTabSuccess' => '0'
        );
        $unfinished = $this->db->where($where)->get('payment');

        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN TABUNGAN EMAS DATA' . json_encode($unfinished->result()));

        foreach ($unfinished->result() as $checkPayment) {

            $trxId = $checkPayment->id_transaksi;
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN TABUNGAN BEGIN TRANSACTION: ' . json_encode($checkPayment));

            //Mendapatkan data tabungan user berdasarkan id transaksi
            $dataTabunganEmas = $this->User->getTabunganEmas($trxId);

            if (!$dataTabunganEmas) {
                $this->db->where('id_transaksi', $trxId)->delete('payment');
                continue;
            }

            //Mendapatkan biaya channel sesuai dengan pembayaran
            $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '62', $checkPayment->kodeBankPembayar);
            $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP', 'BANK', '62', $checkPayment->kodeBankPembayar);

            $kodeBankPembayar = $checkPayment->kodeBankPembayar;

            $paymentMethod = 'BANK';

            if (in_array($checkPayment->payment, ['VA_MANDIRI', 'VA_BCA'])) {
                $paymentMethod = 'VA';
                //$kodeBankPembayar = null;
            } else if ($checkPayment->payment == 'GCASH') {
                $paymentMethod = 'GCASH';
            } else {
                $paymentMethod = 'BANK';
            }

            // Get promo (if Any)
            $gpoin = $this->GpoinModel->get_promo($checkPayment->reffSwitching);
            $idPromosi = '';
            $discountAmount = 0;
            $promoAmount = 0;
            if ($gpoin != '') {
                if ($gpoin->type == 'discount') {
                    $discountAmount = $gpoin->value;
                }
                $promoAmount = $gpoin->value;
                $idPromosi = $gpoin->idPromosi;
            };


            $openDataTab = array(
                'channelId' => '6017',
                'amount' => $checkPayment->amount + $biayaTransaksi - $discountAmount,
                'cif' => $this->User->getCif($checkPayment->user_AIID),
                'flag' => $dataTabunganEmas->flag,
                'ibuKandung' => $dataTabunganEmas->ibu_kandung,
                'idKelurahan' => $dataTabunganEmas->kode_kelurahan,
                'jalan' => $dataTabunganEmas->jalan,
                'jenisKelamin' => $dataTabunganEmas->jenis_kelamin,
                'kewarganegaraan' => $dataTabunganEmas->kewarganegaraan,
                'kodeCabang' => sprintf("%05s", $dataTabunganEmas->kode_cabang),
                'namaNasabah' => $dataTabunganEmas->nama,
                'noHp' => $dataTabunganEmas->no_hp,
                'noIdentitas' => $dataTabunganEmas->no_identitas,
                'paymentMethod' => $paymentMethod,
                'reffSwitching' => $trxId,
                'statusKawin' => $dataTabunganEmas->status_kawin,
                'tanggalExpiredId' => $dataTabunganEmas->tanggal_expired_identitas,
                'tanggalLahir' => $dataTabunganEmas->tanggal_lahir,
                'tempatLahir' => $dataTabunganEmas->tempat_lahir,
                'tipeIdentitas' => $dataTabunganEmas->jenis_identitas,
                'kodeBankPembayar' => $kodeBankPembayar,
                'idPromosi' => $idPromosi,
                'discountAmount' => $promoAmount
            );

            $openTabungan = $this->bukaTabunganEmas2($openDataTab);
            // $checkPayment->amount + $biayaTransaksi - $checkPayment->discountAmount,
            // $dataTabunganEmas->flag,
            // $dataTabunganEmas->ibu_kandung,
            // $dataTabunganEmas->kode_kelurahan,
            // $dataTabunganEmas->jalan,
            // $dataTabunganEmas->jenis_kelamin,
            // $dataTabunganEmas->kewarganegaraan,
            // sprintf("%05s", $dataTabunganEmas->kode_cabang),
            // $dataTabunganEmas->nama,
            // $dataTabunganEmas->no_hp,
            // $dataTabunganEmas->no_identitas,
            // $trxId,
            // $dataTabunganEmas->status_kawin,
            // $dataTabunganEmas->tanggal_expired_identitas,
            // $dataTabunganEmas->tanggal_lahir,
            // $dataTabunganEmas->tempat_lahir,
            // $dataTabunganEmas->jenis_identitas,
            // '6017',
            // $this->User->getCif($checkPayment->user_AIID),
            // $kodeBankPembayar,
            // $paymentMethod,
            // $checkPayment->gcashId,
            // $idPromosi,
            // $discountAmount


            if ($openTabungan->responseCode == '00') {
                $resData = json_decode($openTabungan->data);

                $noRekening = $resData->norek;
                $saldoEmas = $resData->saldoEmas;
                $saldoNominal = $resData->saldoNominal;
                $tglBuka = $resData->tglBuka;
                $administrasi = $resData->administrasi;
                $gram = $resData->gram;
                $harga = $resData->harga;
                $nilaiTransaksi = $resData->nilaiTransaksi;
                $surcharge = $resData->surcharge;
                $totalKewajiban = $resData->totalKewajiban;

                //Lakukan pengecekan CIF customer dan update data rekening tabungan
                $checkCustomer = $this->detailTabunganEmas($noRekening);
                if ($checkCustomer->responseCode == '00') {
                    $checkCustomerData = json_decode($checkCustomer->data);

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CORON Check Customer DATA: ' . json_encode($checkCustomerData));

                    $tanggalBuka = $checkCustomerData->tglBuka;
                    $namaNasabah = $checkCustomerData->namaNasabah;
                    $noRek = $checkCustomerData->norek;
                    $saldo = $checkCustomerData->saldo;
                    $cif = $checkCustomerData->cif;

                    $updatePaymentData = array(
                        'norek' => $noRek,
                        'saldoEmas' => $saldoEmas,
                        'saldoNominal' => $saldoNominal,
                        'tglBuka' => $tglBuka,
                        'administrasi' => $administrasi,
                        'gram' => $gram,
                        'harga' => $harga,
                        'nilaiTransaksi' => $nilaiTransaksi,
                        'surcharge' => $surcharge,
                        'totalKewajiban' => $totalKewajiban,
                        'openTabSuccess' => '1'
                    );

                    $this->PaymentModel->update($trxId, $updatePaymentData);

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CheckCustomer : ' . json_encode($checkCustomerData));
                    $this->EmasModel->updateRekeningByTrxId($trxId, array(
                        'no_rekening' => $noRek,
                        'saldo_nominal' => $saldoNominal,
                        'saldo_emas' => $saldoEmas,
                        'tanggal_buka' => $tanggalBuka,
                        'cif' => $cif
                    ));


                    //Get detail user berdasarkan nomor rekening user tabungan
                    $user = $this->User->getUser($checkPayment->user_AIID);

                    //Update CIF user
                    $this->User->updateUser($user->user_AIID, array('cif' => $cif));

                    //Masukan saldo emas ke dalam transaksi (saldo dan saldo akhir jumlahnya sama)
                    $this->EmasModel->addHistoryIn($noRek, $saldo, $saldo, '1', 'OP');

                    $namaOutlet = $dataTabunganEmas->namaOutlet;
                    $alamatOutlet = $dataTabunganEmas->alamatOutlet . ', ' .
                        $dataTabunganEmas->kelurahanOutlet . ', ' .
                        $dataTabunganEmas->kecamatanOutlet . ', ' .
                        $dataTabunganEmas->kabupatenOutlet . ', ' .
                        $dataTabunganEmas->provinsiOutlet;

                    $teleponOutlet = $dataTabunganEmas->teleponOutlet;

                    $body = $this->generateOpenTabemasNotif(
                        $namaNasabah,
                        $noRek,
                        $cif,
                        $tanggalBuka,
                        $saldo,
                        $namaOutlet,
                        $alamatOutlet,
                        $teleponOutlet,
                        $trxId
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => BODY HTML : ' . $body['mobile']);

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Generate Body Message OP: ' . $user->email . ' SUCCESS');


                    //Tambahkan ke notifikasi
                    $idNotif = $this->NotificationModel->add(
                        $user->user_AIID,
                        NotificationModel::TYPE_EMAS,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "Buka Tabungan Emas",
                        "Tabungan Emas Anda Sudah Aktif",
                        $body['mobile'],
                        $body['minimal'],
                        "OP"
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Save Notif Message OP: ' . $user->email . ' SUCCESS');

                    //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                    Message::sendFCMNotif(
                        $user->fcm_token,
                        [
                            "id" => $idNotif,
                            "title" => "Buka Tabungan Emas",
                            "tagline" => "Tabungan emas anda sudah aktif",
                            "tipe" => "OP_SUCCESS",
                            "namaNasabah" => $namaNasabah,
                            "noRekening" => $noRek,
                            "idTransaksi" => $trxId,
                            "saldoEmas" => $saldo,
                            "tanggal_buka" => $tanggalBuka,
                            "cif" => $cif,
                            "paymentType" => $checkPayment->tipe ? $checkPayment->tipe : $checkPayment->payment,
                            "token" => $user->no_hp
                        ]
                    );

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => SEND FCM Message OP: ' . $user->email . ' SUCCESS');

                    //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                    $this->load->helper('message');
                    Message::sendEmailOpenTabungaEmasSuccess($user->email, $body['email']);

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Send Email Message OP: ' . $user->email . ' SUCCESS');

                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN TABUNGAN SUCCESS: ' . json_encode($checkPayment));

                    // Update point gpoint
                    // $this->gPointValue([
                    //     'userId' => $cif,
                    //     "channel" => $this->config->item("core_post_username"),
                    //     "product" => "62",
                    //     "transactionType" => 'OP',
                    //     "unit" => "gram",
                    //     "transactionAmount" => (double) $gram,
                    //     "reffCore" => $checkPayment->reffCore
                    // ]);

                    // Redeem voucher if exist
                    // Redeem vouchers
                    // $promoCode = substr($checkPayment->idPromosi, strpos($checkPayment->idPromosi, ";") + 1);
                    // $redeemData = [
                    //     "promoCode" => $promoCode,
                    //     "voucherId" => null,
                    //     "userId" => $user->cif,
                    //     "transactionAmount" => $totalKewajiban
                    // ];

                    // log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => GPOINT REDEEM VOUCHER');
                    // log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => GPOINT REDEEM VOUCHER DATA: '.json_encode($redeemData));

                    // $redeem = $this->gPointRedeem($data);
                    // log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => GPOINT REFRESH POINT RESPONSE: '.json_encode($redeem));


                } else {
                    log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN TABUNGAN CHECK NASABAH ERROR: ' . json_encode($checkPayment));
                }
            } else {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN TABUNGAN ERROR: ' . json_encode($openTabungan) . 'Req Body : ' . json_encode($openDataTab));
            }
        }
    }

    function openMulia()
    {
        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN MULIA BEGIN');

        // Get unfinished open mulia
        $where = [
            'isPaid' => '1',
            'isOpenSuccess' => '0'
        ];


        $payment = $this->db
            ->select('ref_vendor_cetak.nama as jenisLogamMulia')
            ->select('payment_mulia.*')
            ->join('ref_vendor_cetak', 'ref_vendor_cetak.id=payment_mulia.idVendor', 'left')
            ->where($where)
            ->get('payment_mulia');

        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Count Unfinished: ' . count($payment));

        foreach ($payment->result() as $payment) {
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => Process Open Mulia ' . $payment->reffSwitching);
            log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN MULIA BEGIN TRANSACTION: ' . json_encode($payment));

            $user = $this->User->getUser($payment->user_AIID);

            // Get biaya transaksi
            $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '37', $payment->kodeBankPembayar);
            $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', $payment->kodeBankPembayar);

            $kodeBankPembayar = $payment->kodeBankPembayar;

            $paymentMethod = 'BANK';

            // Kode Bank pembayar harus isi kodeBankPembayar: Angga 2018-12-10
            if (in_array($payment->payment, ['VA_MANDIRI', 'VA_BCA'])) {
                $paymentMethod = 'VA';
                //$kodeBankPembayar = null;
            } else if ($payment->payment == 'GCASH') {
                $paymentMethod = 'GCASH';
            } else {
                $paymentMethod = 'BANK';
            }

            // Flaging untuk menentukan apakah perlu link cif dan activation lagi
            $isNeedLinkActivation = false;


            /**
             * Pastikan user harus punya cif dan sudah link
             * Jika belum, harus link dan aktifasi wallet
             */
            if ($user->cif == '') {

                // Pertama check customer
                $checkCustomer = $this->checkCustomer(
                    $user->nama,
                    $user->nama_ibu,
                    $user->tgl_lahir,
                    'K',
                    '6017'
                );

                if ($checkCustomer->responseCode == '00') {
                    $checkCustomerData = json_decode($checkCustomer->data);

                    // Response check customer array
                    $_cif = $checkCustomerData[0]->cif;

                    // Link customer dan activation deposit (wallet)
                    $customerLink = $this->customerLink(
                        $_cif,
                        $user->nama_ibu,
                        $user->nama,
                        $user->tgl_lahir,
                        $user->no_hp,
                        '6017'
                    );

                    if ($customerLink->responseCode == '00') {
                        // Lakukan activasi deposit (wallet)

                        $depositActivation = $this->aktivasiWallet([
                            'channelId' => '6017',
                            'cif' => $_cif,
                            'noHp' => $user->no_hp,
                            'tipe' => '1'
                        ]);
                    }

                    // Update user CIF
                    $this->User->updateUser($payment->user_AIID, [
                        'cif' => $_cif
                    ]);
                } else if ($checkCustomer->responseCode == '14') {
                    // Jika data nasabah belum ada di core
                    $isNeedLinkActivation = true;
                }
            }

            // Get promo (if Any)
            $gpoin = $this->GpoinModel->get_promo($payment->reffSwitching);
            $idPromosi = '';
            $promoCode = '';
            $discountAmount = 0;
            $promoAmount = 0;
            if ($gpoin != '') {
                if ($gpoin->type == 'discount') {
                    $discountAmount = $gpoin->value;
                }
                $promoCode = $gpoin->promoCode;
                $promoAmount = $gpoin->value;
                $idPromosi = $gpoin->idPromosi;
            };

            $totalKewajiban = $payment->totalKewajiban + $payment->biayaTransaksi - $discountAmount;
            // Set opend data
            $openData = [
                'uangMuka' => $payment->uangMuka,
                'tenor' => $payment->tenor,
                'idVendor' => $payment->idVendor,
                'jenisTransaksi' => 'OP',
                'kepingMulia' => $payment->kepingMulia,
                'channelId' => $payment->channelId,
                'flag' => 'K',
                'productCode' => '37',
                'namaNasabah' => $user->nama,
                'ibuKandung' => $user->nama_ibu,
                'tempatLahir' => $user->tempat_lahir,
                'tanggalLahir' => $user->tgl_lahir,
                'kodeCabang' => $payment->kodeOutlet,
                'jenisKelamin' => $user->jenis_kelamin,
                'kewarganegaraan' => $user->kewarganegaraan,
                'statusKawin' => $user->status_kawin,
                'jalan' => $user->alamat,
                'idKelurahan' => $user->id_kelurahan,
                'tipeIdentitas' => $user->jenis_identitas,
                'noIdentitas' => $user->no_ktp,
                'noHp' => $user->no_hp,
                'tanggalExpiredId' => $user->tanggal_expired_identitas,
                'administrasi' => $payment->administrasi,
                'reffSwitching' => $payment->reffSwitching,
                'surcharge' => $payment->biayaTransaksi,
                'nominalUangMuka' => $payment->nominalUangMuka,
                'totalKewajiban' => (string) $totalKewajiban,
                'totalHarga' => $payment->totalHarga,
                'paymentMethod' => $paymentMethod,
                'kodeBankPembayar' => $kodeBankPembayar,
                'gcashId' => $payment->gcashId,
                'idPromosi' => $idPromosi,
                'discountAmount' => $promoAmount

            ];

            $openMulia = $this->muliaOpen($openData);

            if ($openMulia->responseCode == '00') {
                $openData = json_decode($openMulia->data, true);

                // Lakukan link cif dan deposit activation gunakan response CIF open mulia
                // Jika customer belum link cif
                if ($isNeedLinkActivation) {
                    $customerLink = $this->customerLink(
                        $openData['cif'],
                        $user->nama_ibu,
                        $user->nama,
                        $user->tgl_lahir,
                        $user->no_hp,
                        '6017'
                    );

                    if ($customerLink->responseCode == '00') {

                        // Lakukan activasi deposit (wallet)
                        $depositActivation = $this->aktivasiWallet([
                            'channelId' => '6017',
                            'cif' => $openData['cif'],
                            'noHp' => $user->no_hp,
                            'tipe' => '1'
                        ]);
                    }
                }

                // Update user CIF
                $this->User->updateUser($payment->user_AIID, [
                    'cif' => $openData['cif']
                ]);

                // Update open data
                $openData['isOpenSuccess'] = '1';

                $this->MuliaModel->updatePayment($payment->reffSwitching, $openData);

                // Get outlet details
                $outlet = $this->MasterModel->getSingleCabang($payment->kodeOutlet);

                $tglTransaksi =  new DateTime($payment->realTglTransaksi);
                $tglPembayaran = new DateTime($payment->tglPembayaran);

                $dateExpired = new DateTime($payment->tglExpired);

                // Set kontrak token untuk notifikasi
                $kontrakTokenData = [
                    'reffSwitching' => $payment->reffSwitching,
                    'email' => $user->email,
                ];

                // Tokenize kontrak data
                $kontrakToken = Authorization::generateToken($kontrakTokenData);

                // Set kontrak URL
                $frontUrlKontrak = $this->config->item('kontrak_mulia_url');
                $kontrakUrl = $frontUrlKontrak . '?token=' . $kontrakToken;

                // Send success notification
                $templateData = [
                    'nama'           => $user->nama,
                    'namaNasabah'    => $user->nama,
                    'payment'        => $payment->payment,
                    'va'             => $payment->virtualAccount,
                    'totalKewajiban' => $payment->totalKewajiban,
                    'reffSwitching'  => $payment->reffSwitching,
                    'trxId'          => $payment->reffSwitching,
                    'namaCabang'     => $outlet->nama,
                    'alamatCabang'   => $outlet->alamat . ', ' . $outlet->kelurahan . ', ' . $outlet->kecamatan . ', ' . $outlet->kabupaten . ', ' . $outlet->provinsi,
                    'telpCabang'     => $outlet->telepon,
                    'item'           => json_decode($payment->kepingMuliaJson),
                    'tglTransaksi'   => $tglTransaksi->format('d/m/Y H:i:s'),
                    'tglExpired'     => $dateExpired->format('d/M/Y H:i:s'),
                    'noKredit'       => $openData['noKredit'],
                    'tglPembayaran'  => $tglPembayaran->format('d/m/Y H:i:s'),
                    'pokokPembiayaan' => $payment->pokokPembiayaan,
                    'totalHarga'     => $payment->totalHarga,
                    'administrasi'   => $payment->administrasi,
                    'uangMuka'       => $payment->payment == 'GCASH' ? $payment->uangMuka : $payment->nominalUangMuka,
                    'biayaTransaksi' => $payment->biayaTransaksi,
                    'jenisLogamMulia' => $payment->jenisLogamMulia,
                    'kontrakUrl'     => $kontrakUrl,
                    'realTglTransaksi' => $payment->realTglTransaksi,
                    'pokokPembiayaan' => $payment->pokokPembiayaan,
                    'tenor'           => $payment->tenor,
                    'angsuran'        => $payment->angsuran,
                    // Promo
                    'promoCode'       => $promoCode,
                    'discountAmount'  => $discountAmount,
                    'promoAmount'     => $promoAmount
                ];


                $template = $this->generateOpenMuliaNotif($templateData);

                $emailTemplate   = $template['email'];
                $mobileTemplate  = $template['mobile'];
                $minimalTemplate = $template['minimal'];

                $totalBayar = $templateData['totalKewajiban'] + $templateData['biayaTransaksi'];
                $fTotalBayar = number_format($totalBayar, 0, ",", ".");

                $subtitle = "Pembelian Mulia " . $openData['noKredit'] . " sukses";

                // Simpan notifikasi baru
                $idNotif = $this->NotificationModel->add(
                    $payment->user_AIID,
                    NotificationModel::TYPE_MPO,
                    NotificationModel::CONTENT_TYPE_HTML,
                    $this->ConfigModel->getNamaProduk('OP', '37'),
                    $subtitle,
                    $mobileTemplate,
                    $minimalTemplate,
                    "ML"
                );

                //Kirim notifikasi pembayaran ke device user
                Message::sendFCMNotif(
                    $this->User->getFCMToken($payment->user_AIID),
                    [
                        "id"      => $idNotif,
                        "tipe"    => "ML",
                        "title"   => $this->ConfigModel->getNamaProduk('OP', '37'),
                        "tagline" => $subtitle,
                        "content" => "Pembelian Mulia " . $openData['noKredit'] . " sukses",
                        "token"   => $user->no_hp,
                        "cif"     => $openData['cif']
                    ]
                );

                //Kirim Email Notifikasi
                $this->load->helper('message');
                Message::sendEmail($user->email, $subtitle, $emailTemplate);

                // Update point gpoint
                // $this->gPointValue([
                //     'userId' => $openData['cif'],
                //     "channel" => $this->config->item("core_post_username"),
                //     "product" => "37",
                //     "transactionType" => 'OP',
                //     "reffCore" => $checkPayment->reffCore
                // ]);

                // // Redeem voucher if exist
                // // Redeem vouchers
                // $promoCode = substr($checkPayment->idPromosi, strpos($checkPayment->idPromosi, ";") + 1);
                // $redeemData = [
                //     "promoCode" => $promoCode,
                //     "voucherId" => null,
                //     "userId" => $openData['cif'],
                // ];

                // log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => GPOINT REDEEM VOUCHER');
                // log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => GPOINT REDEEM VOUCHER DATA: ' . json_encode($redeemData));

                // $redeem = $this->gPointRedeem($data);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN MULIA RESPONSE: ' . json_encode($templateData));
            } else {
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON OPEN MULIA ERROR: ' . json_encode($openData) . 'RESPONSE:' . $openMulia->responseDesc);
            }
        }
    }

    function _toWord($x)
    {
        $x = abs($x);
        $angka = array(
            "", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } elseif ($x < 20) {
            $temp = $this->_toWord($x - 10) . " belas";
        } elseif ($x < 100) {
            $temp = $this->_toWord($x / 10) . " puluh" . $this->_toWord($x % 10);
        } elseif ($x < 200) {
            $temp = " seratus" . _toWord($x - 100);
        } elseif ($x < 1000) {
            $temp = $this->_toWord($x / 100) . " ratus" . $this->_toWord($x % 100);
        } elseif ($x < 2000) {
            $temp = " seribu" . $this->_toWord($x - 1000);
        } elseif ($x < 1000000) {
            $temp = $this->_toWord($x / 1000) . " ribu" . $this->_toWord($x % 1000);
        } elseif ($x < 1000000000) {
            $temp = $this->_toWord($x / 1000000) . " juta" . $this->_toWord($x % 1000000);
        } elseif ($x < 1000000000000) {
            $temp = $this->_toWord($x / 1000000000) . " milyar" . $this->_toWord(fmod($x, 1000000000));
        } elseif ($x < 1000000000000000) {
            $temp = $this->_toWord($x / 1000000000000) . " trilyun" . $this->_toWord(fmod($x, 1000000000000));
        }
        return $temp;
    }


    function _toBilangan($x, $style = 4)
    {
        if ($x < 0) {
            $hasil = "minus " . trim(_toWord($x));
        } else {
            $hasil = trim($this->_toWord($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    function _toDay($strDate)
    {
        $day = date('D', strtotime($strDate));
        $dayList = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );
        return $dayList[$day];
    }

    function _toBulan($bulan)
    {
        switch ($bulan) {
            case 1:
                $bulan = "Januari";
                break;
            case 2:
                $bulan = "Februari";
                break;
            case 3:
                $bulan = "Maret";
                break;
            case 4:
                $bulan = "April";
                break;
            case 5:
                $bulan = "Mei";
                break;
            case 6:
                $bulan = "Juni";
                break;
            case 7:
                $bulan = "Juli";
                break;
            case 8:
                $bulan = "Agustus";
                break;
            case 9:
                $bulan = "September";
                break;
            case 10:
                $bulan = "Oktober";
                break;
            case 11:
                $bulan = "November";
                break;
            case 12:
                $bulan = "Desember";
                break;
        }
        return $bulan;
    }

    /**
     * Mendapatkan informasi harga emas dari Pegadaian Core
     */
    public function hargaEmas()
    {

        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON: Get Harga Emas');
        $url = '/param/hargaemas/';

        $data = array(
            "channelId" => '6017',
            "clientId" => $this->config->item('core_post_username'),
            "flag" => "K"
        );
        $reqres =  $this->coreRequest($url, $data, 'HargaEmas');

        if (isset($reqres->data) && $reqres->data != null) {
            $reqData = json_decode($reqres->data);

            //Get last harga emas
            $hargaEmas = $this->db->order_by('harga_emas_AIID', 'desc')->get('harga_emas')->row();
            $lastHargaEmas = $hargaEmas->waktu_update;
            $lastDate = new DateTime($lastHargaEmas);
            $newHarga = new DateTime($reqData->tglBerlaku);


            //Jika masih di hari yang sama, update saja
            if ($newHarga->format('Y-m-d') == $lastDate->format('Y-m-d')) {

                echo 'Tanggal tetap \n';

                $dataSave = array(
                    'harga_jual' => $reqData->hargaJual,
                    'harga_beli' => $reqData->hargaBeli,
                    'waktu_update' => $reqData->tglBerlaku
                );
                $this->db->where('harga_emas_AIID', $hargaEmas->harga_emas_AIID)->update('harga_emas', $dataSave);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON: Info Harga Emas [Update] Success');
            } else if ($newHarga->format('Y-m-d') !=  $lastDate->format('Y-m-d')) {
                $dataSave = array(
                    'harga_jual' => $reqData->hargaJual,
                    'harga_beli' => $reqData->hargaBeli,
                    'waktu_update' => $reqData->tglBerlaku
                );
                $this->db->insert('harga_emas', $dataSave);
                log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON: Info Harga Emas [Insert] Success');
            }
        }
    }


    /*
     * Cron untuk mendapatkan harga mulia
     */
    public function hargaMulia()
    {
        log_message('debug', 'cronjob - ' . __FUNCTION__ . ' => CRON: Get Harga Mulia');
        $url = '/param/hargamulia/';

        $data = array(
            "channelId" => '6017',
            "clientId" => $this->config->item('core_post_username'),
            "flag" => "K"
        );
        $mulia =  $this->coreRequest($url, $data, 'HargaMulia');

        if ($mulia->responseCode == '00') {
            if (isset($mulia->data) && $mulia->data != null) {
                $productList = json_decode($mulia->data);

                if ($productList) {
                    foreach ($productList as $p) {
                        $vendorId = $p->jenis;
                        $berat = $p->berat;
                        $harga = $p->harga;
                        $tglBerlaku = $p->tglBerlaku;
                        $this->updateProdukMulia($vendorId, $berat, $harga, $tglBerlaku);
                    }
                }

                log_message('info', 'cronjob - ' . __FUNCTION__ . ' => CRON: Get harga mulia sukses');
            }
        }
    }

    function updateProdukMulia($vendorId, $berat, $harga, $tglBerlaku)
    {
        $where = [
            'vendorId' => $vendorId,
            'berat' => $berat,
        ];

        $cek = $this->db->where($where)->get('mulia_produk');

        if ($cek->num_rows() > 0) {
            $this->db->where($where)->update('mulia_produk', [
                'harga' => $harga,
                'tglBerlaku' => $tglBerlaku
            ]);
            log_message('info', 'cronjob - ' . __FUNCTION__ . ' => CRON: Harga Mulia Updated');
        } else {
            $this->db->insert('mulia_produk', [
                'vendorId' => $vendorId,
                'harga' => $harga,
                'berat' => $berat,
                'tglBerlaku' => $tglBerlaku
            ]);
            log_message('info', 'cronjob - ' . __FUNCTION__ . ' => CRON: Harga Mulia Inserted');
        }
    }

    /**
     * =========================================================================================================
     * END CRON FUNCTION
     * =========================================================================================================
     */



    /**
     * =========================================================================================================
     * NOTIFIKASI GENERATOR
     * =========================================================================================================
     */

    /**
     * Method untuk menggenerate HTML Konten notifikasi sukses buka tabungan emas
     * @param type $nama
     * @param type $noRekening
     * @param type $cif
     * @param type $tanggalBuka
     * @param type $saldo
     * @param type $namaOutlet
     * @param type $alamatOutlet
     * @param type $teleponOutlet
     * @return String $message HTML Content yang sudah tergenerate
     */
    function generateOpenTabemasNotif($nama, $noRekening, $cif, $tanggalBuka, $saldo, $namaOutlet, $alamatOutlet, $teleponOutlet, $trxId)
    {
        $subject = "Selamat Rekening Tabungan Emas Anda Sudah Aktif";

        //Batas waktu KYC = tanggalBuka + 6 bulan
        $batasWaktu = new DateTime($tanggalBuka);
        $interval = new DateInterval('P6M');
        $batasWaktu->add($interval);

        //Format tanggal buka
        $fTanggalBuka = new DateTime($tanggalBuka);


        $viewData = array(
            'nama' => $nama,
            'noRekening' => $noRekening,
            'cif' => $cif,
            'tanggalBuka' => $fTanggalBuka->format('d/m/Y'),
            'saldo' => $saldo,
            'namaOutlet' => $namaOutlet,
            'alamatOutlet' => $alamatOutlet,
            'teleponOutlet' => $teleponOutlet,
            'batasWaktu' => $batasWaktu->format('d/m/Y'),
            'trxId' => $trxId
        );

        $content = $this->load->view('mail/notif_opentabemas_success', $viewData, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    function generateOpenMuliaNotif($data)
    {
        $subject = "Pembelian Logam Mulia";

        $content = $this->load->view('mail/mulia/payment_success', $data, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }
}
