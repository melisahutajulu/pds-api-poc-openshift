<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Bank extends CorePegadaian
{

    public function __construct()
    {
        parent::__construct();
        $models = [
            'User',
            'BankModel',
            'MasterModel',
            'NotificationModel'
        ];
        $this->load->model($models);
        $this->load->library('form_validation');
        $this->load->helper('message');
    }

    /**
     * Digunakan untuk inquiry data rekening ke Bank
     */
    function inquiry_post()
    {
        log_message('debug', __FUNCTION__ . ' Inquiry Rekening Bank ' . 'Start');
        // get token
        $token = $this->getToken();
        // get data user
        $dataUser = $this->User->getUser($token->id);

        // token validation
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'bank/inquiry = Unauthorized');
            return;
        }

        log_message('debug', 'E-KYC bank/inquiry Body request' . json_encode($this->post()));

        $setData = array(
            'kode_bank'       => $this->post('kode_bank'),
            'no_rekening'     => $this->post('no_rekening'),
            'nama_rekening'   => $this->post('nama_rekening'),
        );

        $this->form_validation->set_data($setData);
        $this->form_validation->set_rules('kode_bank', 'kode_bank', 'required');
        $this->form_validation->set_rules('no_rekening', 'no_rekening', 'required');
        $this->form_validation->set_rules('nama_rekening', 'nama_rekening', 'required');

        // request validation
        if ($this->form_validation->run() == false) {
            $this->set_response(array(
                'status'  => 'error',
                'message' => 'Invalid Input',
                'code'    => 101,
                'errors'  => $this->form_validation->error_array(),
            ), 200);
            return;
        }

        $kode_bank       = $this->post('kode_bank');
        $no_rekening     = $this->post('no_rekening');
        $nama_rekening   = $this->post('nama_rekening');

        $req_bank = array(
            'kodeBank'           => $kode_bank,
            'noRekeningBank'     => $no_rekening,
            'namaRekeningBank'   => $nama_rekening,
        );
        log_message('debug', 'E-KYC bank/inquiry request Bank' . json_encode($req_bank));

        // send data inquiry bank
        $res_bank = $this->checkRekBank($req_bank);
        log_message('debug', 'E-KYC bank/inquiry response Bank' . json_encode($res_bank));

        switch ($res_bank->responseCode) {
            case '00':
                $data = json_decode($res_bank->data);

                $response                    = (array) $data;
                $response['user_AIID']       = $token->id;
                $response['kodeBank']        = $kode_bank;
                $response['noRekening']      = $data->noRekeningBank;
                $response['namaRekening']    = $data->namaRekeningBank;
                $response['ccy']             = $data->ccy;

                log_message('debug', 'E-KYC bank/inquiry Body response' . json_encode($response));

                $this->send_response('success', 'Inquiry rekening berhasil', $response);
                break;
            case '68':
                $this->send_response('error', 'Koneksi ke Bank bermasalah', $res_bank);
                break;
            case '12' :
                $this->send_response('error', 'Nomor Rekening Tidak Sesuai', $res_bank);
                break;
            case '15' :
                $this->send_response('error', 'Nama Pemilik Rekening Tidak Sesuai', $res_bank);
                break;
            default:
                $this->send_response('error', 'Internal Server Error', $res_bank);
        }

        log_message('debug', __FUNCTION__ . ' Inquiry Rekening Bank ' . 'End');
        return;
    }

    /**
     * Digunakan untuk simpan data rekening Bank
     * Sebelumnya dilakukan validasi PIN
     */
    function save_post()
    {
        log_message('debug', __FUNCTION__ . ' Save Rekening Bank ' . 'Start => ' . json_encode($this->post()));

        // get token
        $token = $this->getToken();

        // token validation
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'bank/save = Unauthorized');
            return;
        }

        log_message('debug', 'bank/save Body request' . json_encode($this->post()));

        $setData = array(
            'pin'             => $this->post('pin'),
            'kode_bank'       => $this->post('kode_bank'),
            'no_rekening'     => $this->post('no_rekening'),
            'nama_rekening'   => $this->post('nama_rekening'),
        );

        $this->form_validation->set_data($setData);
        $this->form_validation->set_rules('pin', 'pin', 'required');
        $this->form_validation->set_rules('kode_bank', 'kode_bank', 'required');
        $this->form_validation->set_rules('no_rekening', 'no_rekening', 'required');
        $this->form_validation->set_rules('nama_rekening', 'nama_rekening', 'required');

        // request validation
        if ($this->form_validation->run() == false) {
            $this->send_response('error', 'Invalid Input', $this->form_validation->error_array());
            log_message('debug', 'bank/save = Invalid input');
            return;
        }

        $pin             = $this->post('pin');
        $kode_bank       = $this->post('kode_bank');
        $no_rekening     = $this->post('no_rekening');
        $nama_rekening   = $this->post('nama_rekening');

        // pin validation
        if ($this->User->isValidPIN($token->id, $pin) == false) {
            $this->send_response('error', 'Invalid Pin', '');
            log_message('debug', 'bank/save = Invalid Pin');
            return;
        }

        // rekening validation
        if ($this->BankModel->isBankExist($token->id, $kode_bank, $no_rekening)) {
            $this->send_response('error', 'Nomor Rekening Sudah Terdaftar', '');
            log_message('debug', 'bank/save = Invalid Rekening');
            return;
        }

        $data = array(
            'user_AAID'      => $token->id,
            'id_bank'        => 0,
            'nomor_rekening' => $no_rekening,
            'kode_bank'      => $kode_bank,
            'nama_pemilik'   => $nama_rekening
        );

        // save rekening bank
        $id = $this->BankModel->addRekeningBank($data);

        $response = $this->BankModel->getRekeningBank($id);

        log_message('debug', 'bank/save Body response' . json_encode($response));

        // kirim notifikasi berahasil menyimpan rekening bank
        $this->_send_notification($data, $token);
        $this->send_response('success', 'Rekening berhasil disimpan', $response);

        log_message('debug', __FUNCTION__ . ' Save Rekening Bank ' . 'End');
        return;
    }

    function _send_notification($data, $token)
    {
        // mapping data notif
        $nama_bank = $this->BankModel->getNamaBank($data['kode_bank']);
        $dataNotif = [
            'email' => $token->email,
            'user_id' => $data['user_AAID'],
            'namaNasabah' => $token->nama,
            'rekening' => [[
                'namaBank' => $nama_bank,
                'norekBank' => $data['nomor_rekening'],
                'namaNasabahBank' => $data['nama_pemilik']
            ]]
        ];

        // get template data for profile bank
        $template = Message::generateBankNotif($dataNotif);
        log_message('debug', 'start sending notification with => ' . json_encode($dataNotif));

        // store to notification model
        $idNotif = $this->NotificationModel->add(
            $data['user_AAID'],
            NotificationModel::TYPE_PROFILE,
            NotificationModel::CONTENT_TYPE_HTML,
            "Profile Info",
            "Rekening Bank",
            $template['email'],
            $template['mobile'],
            "profile_bank"
        );

        // send notif to user apps
        Message::sendFCMNotif(
            $this->User->getFCMToken($data['user_AAID']),
            [
                "id" => $idNotif,
                "tipe" => "addRekeningBank",
                "title" => "Rekening Berhasil Ditambah",
                "tagline" => "Rekening Bank " . $nama_bank . " " . $data['nomor_rekening'] . " berhasil ditambah",
                "action_url" => "addRekeningBank"
            ]
        );

        // send email notif to user
        Message::sendEmail($token->email, "Penambahan Rekening Bank", $template['email']);
        log_message('debug', 'end sending notification');
    }
}
