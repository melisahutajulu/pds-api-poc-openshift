<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Mikro extends CorePegadaian {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('User', 'NotificationModel','MikroModel','ConfigModel'));
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->helper('message');

        //Log request and post
        $CI =& get_instance();
        $uri = $CI->uri->uri_string();
        
        $postParam = $CI->input->post();
        $getParam = $CI->input->get();
        
        log_message('debug', 'REQUEST INFO');
        log_message('debug', 'REQUEST INFO URI: ' . $uri);
        log_message('debug', 'REQUEST INFO POST PARAM:'. json_encode($postParam));
        log_message('debug', 'REQUEST INFO GET PARAM:'. json_encode($getParam));
    }
    
    
    function index_get()
    {
        $token = $this->getToken();
        if($token){
            $page = $this->query('page');
            $page == null ? $page=1 : $page;
            $page < 1 ? $page = 1 : $page;
            
            $offset = 20 * ($page-1);
                
            
            $mikro = $this->MikroModel->getMikroList($token->id, $offset);
            
            $config['base_url'] = base_url().'mikro/list';
            $config['total_rows'] = $this->MikroModel->countMikro($token->id);
            $config['per_page'] = 20;
            $config["use_page_numbers"] = TRUE;
            $config["page_query_string"] = TRUE;
            $config['query_string_segment'] = 'page';                        
            $this->pagination->initialize($config); 
            
            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $mikro,
                'jumlahData' => $config['total_rows'],
                'halaman' => $this->pagination->cur_page+1,
                'totalHalaman' => ceil($config['total_rows']/20)
            ), 200);
            
        }else{
            $this->errorUnAuthorized();
        }
    }

    function pengajuan_post() {
        $token = $this->getToken();
        if ($token) {
            $setData = array(
                'kebutuhan_modal' => $this->post('kebutuhan_modal'),
                'jenis_usaha' => $this->post('jenis_usaha'),
                'omset' => $this->post('omset'),
                'laba' => $this->post('laba'),
//                'deskripsi' => $this->post('deskripsi'),
                'nama_usaha' => $this->post('nama_usaha'),
//                'bidang_usaha' => $this->post('bidang_usaha'),
                'lama_usaha' => $this->post('lama_usaha'),
                'kode_kelurahan' => $this->post('kode_kelurahan'),
                'alamat' => $this->post('alamat'),
                'latitude' => $this->post('latitude'),
                'longitude' => $this->post('longitude'),
                'tenor' => $this->post('tenor'),
                'kode_outlet' => $this->post('kode_outlet'),                
                'jenis_kendaraan' => $this->post('jenis_kendaraan'),
                'merk' => $this->post('merk'),
                'tipe' => $this->post('tipe'),
                'tahun_pembuatan' => $this->post('tahun_pembuatan'),
                'kepemilikan_kendaraan' => $this->post('kepemilikan_kendaraan'),
                'harga_taksiran_kendaraan' => $this->post('harga_taksiran_kendaraan'),
                'deskripsi_kendaraan' => $this->post('deskripsi_kendaraan'),
                'nobpkb_kendaraan' => $this->post('nobpkb_kendaraan'),
                'namabpkb_kendaraan' => $this->post('namabpkb_kendaraan')                
            );

            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('kebutuhan_modal', 'kebutuhan_modal', 'required|integer');
            $this->form_validation->set_rules('jenis_usaha', 'jenis_usaha', 'required');
            $this->form_validation->set_rules('omset', 'omset', 'required|integer');
            $this->form_validation->set_rules('laba', 'laba', 'required|integer');
//            $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');
            $this->form_validation->set_rules('nama_usaha', 'nama_usaha', 'required');
//            $this->form_validation->set_rules('bidang_usaha', 'bidang_usaha', 'required');
            $this->form_validation->set_rules('lama_usaha', 'lama_usaha', 'required|integer');
            $this->form_validation->set_rules('kode_kelurahan', 'kode_kelurahan', 'required|numeric');
            $this->form_validation->set_rules('alamat', 'alamat', 'required');
            $this->form_validation->set_rules('tenor', 'Tenor', 'required|integer');
            $this->form_validation->set_rules('kode_outlet', 'Kode Outelet', 'required|numeric');

            $this->form_validation->set_rules('jenis_kendaraan', 'jenis_kendaraan', 'required');
            $this->form_validation->set_rules('merk', 'merk', 'required|integer');
            $this->form_validation->set_rules('tipe', 'tipe', 'required');
            $this->form_validation->set_rules('tahun_pembuatan', 'tahun_pembuaan', 'required|exact_length[4]');
            $this->form_validation->set_rules('kepemilikan_kendaraan', 'kepemilikan_kendaraan', 'required');
            $this->form_validation->set_rules('deskripsi_kendaraan', 'deskripsi_kendaraan', 'required');
            $this->form_validation->set_rules('harga_taksiran_kendaraan', 'harga_taksiran_kendaraan', 'required');
            $this->form_validation->set_rules('nobpkb_kendaraan', 'nobpkb_kendaraan', 'required');
            $this->form_validation->set_rules('namabpkb_kendaraan', 'namabpkb_kendaraan', 'required');

            if (!$this->form_validation->run()) {
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Invalid input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                        ), 200);
            } else {
                
                $fotoKendaraan = array('foto' => array());
                if (isset($_FILES['foto_kendaraan'])) {
                    $fotoKendaraan = $this->uploadFotoKendaraan();

                    if (count($fotoKendaraan['error']) > 0) {
                        $this->response(array(
                            'code' => 101,
                            'status' => 'error',
                            'message' => 'Foto kendaraan',                            
                            'errors' => $fotoKendaraan['error']
                        ), 200);
                        return;
                    }
                }
                
                $fotoUsaha = array('foto' => array());
                if (isset($_FILES['foto_usaha'])) {
                    $fotoUsaha = $this->uploadFotoUsaha();

                    if (count($fotoUsaha['error']) > 0) {
                        $this->response(array(
                            'code' => 101,
                            'status' => 'error',
                            'message' => 'Foto usaha',
                            'errors' => $fotoUsaha['error']
                        ), 200);
                        return;
                    }
                }
                                
                $setData['foto_kendaraan'] = json_encode($fotoKendaraan['foto']);
                $setData['foto_usaha'] = json_encode($fotoUsaha['foto']);
                $setData['user_AIID'] = $token->id;      
                $setData['status'] =  '1';
                
                $user = $this->User->profile($token->id);
                $id = $this->MikroModel->savePengajuan($setData);
                
                $noPengajuan = sprintf('%06d', $id);
                
                $template = $this->generateMikroNotif(
                        $user->nama, 
                        $noPengajuan, 
                        $user->jenisIdentitas, 
                        $user->noKTP, 
                        $user->alamat,
                        $user->noHP,
                        $this->post('kebutuhan_modal'),
                        $this->post('jenis_kendaraan'),
                        $this->post('merk'),
                        $this->post('tahun_pembuatan'),
                        $this->post('nama_usaha'),
                        $this->post('jenis_usaha'),
                        $this->post('alamat'),
                        $this->post('kode_outlet')                    
                        );
                
                $mobile = $template['mobile'];
                $emailTemplate = $template['email'];
                $minimalTemplate = $template['minimal'];
                
                //Simpan notifikasi baru
                $idNotif = $this->NotificationModel->add(
                            $token->id, 
                            NotificationModel::TYPE_MICRO, 
                            NotificationModel::CONTENT_TYPE_HTML, 
                            "Pengajuan Pembiayaan Usaha", 
                            "Kode Booking ".$noPengajuan,
                            $mobile,
                            $minimalTemplate,
                            "MC"
                            );

                //Kirim notifikasi pembayaran ke device user
                Message::sendFCMNotif(
                    $this->User->getFCMToken($token->id)
                        , [
                    "id" => $idNotif,
                    "tipe" => "MC",
                    "title" => "Pengajuan Pembiayaan Usaha",
                    "tagline" => "Kode Booking ".$noPengajuan,
                    "content" => "Pengajuan pembiayaan usaha anda dengan nomor ".$noPengajuan." telah kami terima. Mohon menunggu proses verifikasi dari kami. Agen kami akan segera menghubungi Anda. \n\nTerima kasih.",
                    "token" => $token->no_hp        
                ]);                                

                //Kirim Email Notifikasi
                $this->load->helper('message');
                Message::sendEmail($user->email, 'Pengajuan Pembiayaan Usaha Anda Berhasil ('.$noPengajuan.')', $emailTemplate);
                
                $emitter = new Emitter();
                $emitter->in('pds-admin')
                ->emit('message', array(
                    'type' => 'mikro',
                    'kodeBooking' => $noPengajuan,
                    'kodeOutlet'=>  $this->post('kode_outlet') 
                    )); 
                
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => array(
                        'id' => $id,
                        'noPengajuan' => $noPengajuan,
                        'tanggalPengajuan' => date('Y-m-d H:i:s')                                
                    ),
                ), 200);
            }
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function inquiry_payment_post()
    {
        $token = $this->getToken();
        if($token){
            $setData = array(
                'no_kredit' => $this->post('no_kredit'),
                'jenis_transaksi' => $this->post('jenis_transaksi')                
            );
            
            $this->form_validation->set_data($setData);
            
            $this->form_validation->set_rules('no_kredit','no_kredit','required|numeric|exact_length[16]');
            $this->form_validation->set_rules('jenis_transaksi','jenis_transaksi','required|exact_length[2]');

           
            if($this->form_validation->run() == FALSE)
            {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ),200);
            }else{
                $norek = $this->post('no_kredit');
                $jenisTransaksi = $this->post('jenis_transaksi');
                
                $inquiry = $this->inquiryMikro($jenisTransaksi, $norek, $token->channelId);
                
                if($inquiry->responseCode == '00'){
                    $rawData = json_decode($inquiry->data);
                    
                    //Convert object response ke dalam array untuk penyimpanan data
                    $saveData = json_decode(json_encode($rawData), True);
                    $saveData['user_AIID'] = $token->id;
                    
                    $this->MikroModel->savePayment($saveData);
                    
                    $res = $saveData;
                    $res['idTransaksi'] = $saveData['reffSwitching'];
                    $biayaChannel = array(
                        'BNI' => $this->ConfigModel->getBiayaPayment($jenisTransaksi,'BANK','03','009'),
                        'MANDIRI' => $this->ConfigModel->getBiayaPayment($jenisTransaksi,'BANK','03','008'),
                        'WALLET' => $this->ConfigModel->getBiayaPayment($jenisTransaksi,'WALLET','03',''),
                        'VA_BCA' => $this->ConfigModel->getBiayaPayment($jenisTransaksi,'BANK','03','014'),
                        'VA_MANDIRI' => $this->ConfigModel->getBiayaPayment($jenisTransaksi,'BANK','03','008'),
                        'VA_PERMATA' => $this->ConfigModel->getBiayaPayment($jenisTransaksi,'BANK','03','013'),
                        'VA_BRI' => $this->ConfigModel->getBiayaPayment($jenisTransaksi,'BANK','03','002')
                    );
                    $res['biayaChannel'] = $biayaChannel;
                    $res['bookingCode'] = sprintf("%06d", mt_rand(1, 999999));
                    
                    $this->set_response(array(
                        'status' =>'success',
                        'message' => '',
                        'data' => $res
                    ),200);                    
                    
                }else{
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Gagal membuat inquiry',
                        'code' => 103,
                        'reason' => $inquiry
                    ),200);
                }
            }
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    /**
     * Endpoint untuk melakukan payment mikro
     */
    function payment_post()
    {
        $token = $this->getToken();
        if($token){
           
           $user = $this->User->getUser($token->id);
           $setData = array(
               'id_transaksi' => $this->post('id_transaksi'),
               'payment' => $this->post('payment'),
               'booking_code' => $this->post('booking_code'),
               'card_number' => $this->post('card_number'),
               'token_response' => $this->post('token_response'),
               'pin' => $this->post('pin'),
               'va' => $this->post('va') 
           ); 
           
           $this->form_validation->set_data($setData);
           
           $this->form_validation->set_rules('id_transaksi','id_transaksi','required');
           $this->form_validation->set_rules('payment','payment','required');
           
           $payment = $this->post('payment');
           
            if ($payment == 'MANDIRI') {
                $this->form_validation->set_rules('booking_code', 'booking_code', 'required');
                $this->form_validation->set_rules('card_number', 'card_number', 'required');
                $this->form_validation->set_rules('token_response', 'token_response', 'required');
            }
            
            if($payment == 'WALLET' || $payment == 'GCASH'){
                $this->form_validation->set_rules('pin','pin','required|exact_length[6]|numeric');
            }

            if($payment == 'GCASH'){
                $this->form_validation->set_rules('va','va','required|numeric');
            }
           
           if($this->form_validation->run() == FALSE){
               $this->set_response(array(
                   'status' => 'error',
                   'message' => 'Invalid input',
                   'code' => 101,
                   'errors' => $this->form_validation->error_array()
               ),200);               
           }else{
               $idTransaksi = $this->post('id_transaksi');
               
               
               //Mendapatkan payment berdasarkan id transaksi
               $checkPayment = $this->MikroModel->getPaymentByTrxId($idTransaksi);
               
               $user = $this->User->getUser($token->id);
               
               if($checkPayment){
                   $productCode = substr($checkPayment->norek, 7, 2);
                   $typeKantor = $checkPayment->norek[0] == '1' ? 'K' : 'S'; 
                           
                    if($payment == 'BNI')
                    {
                       $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi,'BANK',$productCode,'009');
                       $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($checkPayment->jenisTransaksi,'BANK',$productCode,'009');

                       $createBillingBNI = $this->createBillingVABNI(
                                $checkPayment->totalKewajiban + $biayaTransaksi,
                                $user->email,
                                $user->nama,
                                $token->no_hp,
                                $typeKantor,
                                $checkPayment->jenisTransaksi,
                                "PDS".$checkPayment->jenisTransaksi." ".$idTransaksi." ".$checkPayment->norek,
                                $checkPayment->norek,
                                $productCode,
                                $idTransaksi,
                                $token->channelId
                                );
                       
                            if ($createBillingBNI->responseCode == '00') {
                                
                                if(!isset($createBillingBNI->data)){
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message'=> 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBillingBNI
                                    ),200);
                                    return;
                                }

                                $billingData = json_decode($createBillingBNI->data);


                                $virtualAccount = $billingData->virtualAccount;
                                $tglExpired = $billingData->tglExpired;
                                
                                $updatePaymentData  = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'kodeBankPembayar' => '009',
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay
                                );

                                //Buat payment baru
                                //Simpan payment                                
                                $this->MikroModel->updatePayment($idTransaksi, $updatePaymentData);
                                
                                
                                $template = $this->generatePaymentNotif(
                                        $token->nama,                                        
                                        $idTransaksi);
                                
                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];
                                
                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                    $token->id,
                                    NotificationModel::TYPE_GADAI,
                                    NotificationModel::CONTENT_TYPE_HTML,
                                    $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                                    "No Kredit ".$checkPayment->norek,
                                    $mobileTemplate,
                                    $minimalTemplate,
                                    "MC"    
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                    $this->User->getFCMToken($token->id)
                                    , [
                                    "id" => $notifId,
                                    "tipe" => "MC",
                                    "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                                    "tagline" => "No Kredit ".$checkPayment->norek,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,                                    
                                    "token" => $token->no_hp    
                                ]);

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmail($token->email, "Konfirmasi Pembayaran Angsuran Nomor ".$checkPayment->norek, $emailTemplate);
                                
                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s')                                        
                                    )
                                ], 200);

                            } else {
                                log_message('debug', 'Create VA error. Reason:'.json_encode($createBillingBNI));
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => $createBillingBNI->responseDesc,
                                    'code' => 103,
                                    'reason' => $createBillingBNI
                                ), 200);
                            }
                    }
                    else if($payment == 'MANDIRI')
                    {
                       
                        $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi,'BANK',$productCode,'008');
                        $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment($checkPayment->jenisTransaksi,'BANK',$productCode,'008');

                        $bookingCode = $this->post('booking_code');
                        $cardNumber = $this->post('card_number');
                        $tokenResponse = $this->post('token_response');
                        
                        $clickPay = $this->mandiriClickPay(
                                $checkPayment->totalKewajiban + $biayaTransaksi, 
                                $bookingCode, 
                                $cardNumber, 
                                $checkPayment->jenisTransaksi, 
                                "PDS".$checkPayment->jenisTransaksi." ".$idTransaksi." ".$checkPayment->norek,
                                $user->no_hp, 
                                $checkPayment->norek, 
                                '03', 
                                $tokenResponse, 
                                $idTransaksi, 
                                $token->channelId);
                        
                        if($clickPay->responseCode == '00'){
                            
                            $clickpayData = json_decode($clickPay->data);
                            
                            $updateData['bookingCode'] = $bookingCode;
                            $updateData['cardNumber'] = $cardNumber;
                            $updateData['tokenResponse'] = $tokenResponse;
                            $updateData['reffBiller'] = $clickpayData->reffBiller;
                            $updateData['kodeBankPembayar'] ='008';
                            $updateData['payment'] = $payment;
                            $updateData['biayaTransaksi'] = $biayaTransaksiDisplay;
                            
                            //update data pembayaran
                            $this->MikroModel->updatePayment($checkPayment->reffSwitching, $updateData);
                                                        
                            //Set response
                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Pembayaran Angsuran Mikro Berhasil ',
                                'data' => $updateData
                            ], 200);                                

                           
                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'code' => 103,
                                'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                'reason' => $clickPay
                            ));
                        }                   
                    }
                    else if($payment == 'WALLET' || $payment == 'GCASH')
                    {
                        
                        $pin = $this->post('pin');    
                        $va = $this->post('va');

                        //Check user pin                                 
                        if(!$this->User->isValidPIN2($token->id, $pin)){
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'PIN tidak valid',
                                'code' => 102
                            ),200);
                            return;
                        }    
                       
                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi,'WALLET',$productCode,'');
                        $biayaTransaksiDisplay = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi,'WALLET',$productCode,'');

                        $paymentData = array(
                            'jenisTransaksi' => $checkPayment->jenisTransaksi,
                            //'minimalUpCicil' => $checkPayment->minimalUpCicil,
                            'norek' => $checkPayment->norek,
                            'reffSwitching' => $idTransaksi,  
                            'flag' => 'K',                          
                            'channelId' => $token->channelId,
                            'productCode' => $productCode
                        );

                        if($payment == 'WALLET'){
                            $paymentData['norekWallet'] = $user->norek;
                            $paymentData['paymentMethod'] = 'WALLET';
                            $paymentData['walletId'] = $user->no_hp;
                            $paymentData['amount'] = $checkPayment->totalKewajiban + $biayaTransaksi;

                        } else if($payment == 'GCASH'){
                            $paymentData['gcashId'] = $va;
                            $paymentData['paymentMethod'] = 'GCASH';
                            $paymentData['amount'] = $checkPayment->totalKewajiban;
                        }
                        
                        $walletPayment = $this->paymentMikro($paymentData);
                        if($walletPayment->responseCode == '00'){
                            
                            if(!isset($walletPayment->data)){
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $walletPayment
                                ),200);
                                return;
                            }
                            
                            
                            $walletData = json_decode($walletPayment->data, TRUE);

                            //update payment data
                            $walletData['payment'] = $payment;
                            $walletData['reffSwitching'] = $idTransaksi;
                            $walletData['paid'] = '1';
                            $walletData['biayaTransaksi'] = $biayaTransaksiDisplay;
                            
                            $this->MikroModel->updatePayment($idTransaksi, $walletData);

                            $template = $this->generatePaymentNotif(
                                    $token->nama, $idTransaksi);

                            $mobileTemplate = $template['mobile'];
                            $emailTemplate = $template['email'];
                            $minimalTemplate = $template['minimal'];

                            //Simpan notifikasi baru
                            $notifId = $this->NotificationModel->add(
                                    $token->id, NotificationModel::TYPE_GADAI, NotificationModel::CONTENT_TYPE_HTML, "Pembayaran Angusran", "Transaksi Sukses", $mobileTemplate, $minimalTemplate, "MC"
                            );

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                    $this->User->getFCMToken($token->id)
                                    , [
                                "id" => $notifId,
                                "tipe" => "MC",
                                "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                                "tagline" => "Pembayaran Angsuran Mikro Sukses",
                                "content" => "Pembayaran Angsuran Mikro Sukses",
                                "paymentType" => $payment,
                                "token" => $token->no_hp        
                            ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmail($token->email, "Pembayaran Angsuran " . $checkPayment->norek, $emailTemplate);

                            //Update Saldo Wallet
                            $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                            $walletData['wallet'] = $saldoWallet;

                            //Set response
                            $this->set_response([
                                'status' => 'success',
                                'message' => "Pembayaran Angsuran Mikro ".$checkPayment->norek. ' Sukses',
                                'data' => $walletData
                                    ], 200);
                                                     
                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                                'code' => 103,
                                'reason' => $walletPayment
                            ),200);
                        }                        
                    }
                    else if($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI')
                    {
                        $kodeBank = '';
                        if($payment == 'VA_BCA'){
                            $kodeBank = '014';
                        }
                        else if($payment == 'VA_MANDIRI') {
                            $kodeBank = '008';
                        }
                        else if($payment == 'VA_BRI') {
                            $kodeBank = '002';
                        }
                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi,'BANK',$productCode, $kodeBank);
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($checkPayment->jenisTransaksi,'BANK',$productCode, $kodeBank);

                        $createBillingData = [
                            'channelId' => $token->channelId,
                            'amount' => $checkPayment->totalKewajiban + $biayaTransaksi,
                            'customerEmail' => $user->email,
                            'customerName' => $user->nama,
                            'customerPhone' => $token->no_hp,
                            'flag' => 'K',
                            'jenisTransaksi' => $checkPayment->jenisTransaksi,
                            'kodeProduk' => $productCode,
                            'norek' => $checkPayment->norek,
                            'keterangan' => "PDS".$checkPayment->jenisTransaksi." ".$idTransaksi." ".$checkPayment->norek,
                            'reffSwitching' => $idTransaksi,
                            'kodeBank' => $kodeBank
                        ];
                        
                        $createBilling = $this->createBillingPegadaian($createBillingData);
                       
                        if ($createBilling->responseCode == '00') {

                            if(!isset($createBilling->data)){
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message'=> 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBilling
                                ),200);
                                return;
                            }

                            $billingData = json_decode($createBilling->data);


                            $virtualAccount = $billingData->vaNumber;
                            $tglExpired = $billingData->tglExpired;

                            $updatePaymentData  = array(
                                'tipe' => $payment,
                                'virtual_account' => $virtualAccount,
                                'tanggal_expired' => $tglExpired,
                                'kodeBankPembayar' => $kodeBank,
                                'payment' => $payment,
                                'biayaTransaksi' => $biayaTransaksiDisplay
                            );

                            //Buat payment baru
                            //Simpan payment                                
                            $this->MikroModel->updatePayment($idTransaksi, $updatePaymentData);


                            $template = $this->generatePaymentNotif(
                                    $token->nama,                                        
                                    $idTransaksi);

                            $mobileTemplate = $template['mobile'];
                            $emailTemplate = $template['email'];
                            $minimalTemplate = $template['minimal'];

                            //Simpan notifikasi baru
                            $notifId = $this->NotificationModel->add(
                                $token->id,
                                NotificationModel::TYPE_GADAI,
                                NotificationModel::CONTENT_TYPE_HTML,
                                $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                                "No Kredit ".$checkPayment->norek,
                                $mobileTemplate,
                                $minimalTemplate,
                                "MC"    
                            );

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                "id" => $notifId,
                                "tipe" => "MC",
                                "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                                "tagline" => "No Kredit ".$checkPayment->norek,
                                "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,                                    
                                "token" => $token->no_hp    
                            ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmail($token->email, "Konfirmasi Pembayaran Angsuran Nomor ".$checkPayment->norek, $emailTemplate);

                            //Set response
                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                'data' => array(
                                    'idTransaksi' => $idTransaksi,
                                    'virtualAccount' => $virtualAccount,
                                    'expired' => $tglExpired,
                                    'now' => date('Y-m-d H:i:s')                                        
                                )
                            ], 200);

                        } else {
                            log_message('debug', 'Create VA error. Reason:'.json_encode($createBilling));
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => $createBilling->responseDesc,
                                'code' => 103,
                                'reason' => $createBilling
                            ), 200);
                        }
                    }
                    else if($payment == 'VA_PERMATA')
                    {
                        $kodeBank = '013';
                        
                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($checkPayment->jenisTransaksi,'BANK',$productCode, $kodeBank);
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($checkPayment->jenisTransaksi,'BANK',$productCode, $kodeBank);

                        $createBillingData = [
                            'channelId' => $token->channelId,
                            'amount' => $checkPayment->totalKewajiban + $biayaTransaksi,
                            'customerEmail' => $user->email,
                            'customerName' => $user->nama,
                            'customerPhone' => $token->no_hp,
                            'flag' => 'K',
                            'jenisTransaksi' => $checkPayment->jenisTransaksi,
                            'productCode' => $productCode,
                            'norek' => $checkPayment->norek,
                            'keterangan' => "PDS".$checkPayment->jenisTransaksi." ".$idTransaksi." ".$checkPayment->norek,
                            'trxId' => $idTransaksi,
                        ];
                        
                        $createBilling = $this->createBillingPegadaian($createBillingData);
                       
                        if ($createBilling->responseCode == '00') {

                            if(!isset($createBilling->data)){
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message'=> 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBilling
                                ),200);
                                return;
                            }

                            $billingData = json_decode($createBilling->data);


                            $virtualAccount = $billingData->virtualAccount;
                            $tglExpired = $billingData->tglExpired;

                            $updatePaymentData  = array(
                                'tipe' => $payment,
                                'virtual_account' => $virtualAccount,
                                'tanggal_expired' => $tglExpired,
                                'kodeBankPembayar' => $kodeBank,
                                'payment' => $payment,
                                'biayaTransaksi' => $biayaTransaksiDisplay
                            );

                            //Buat payment baru
                            //Simpan payment                                
                            $this->MikroModel->updatePayment($idTransaksi, $updatePaymentData);


                            $template = $this->generatePaymentNotif(
                                    $token->nama,                                        
                                    $idTransaksi);

                            $mobileTemplate = $template['mobile'];
                            $emailTemplate = $template['email'];
                            $minimalTemplate = $template['minimal'];

                            //Simpan notifikasi baru
                            $notifId = $this->NotificationModel->add(
                                $token->id,
                                NotificationModel::TYPE_GADAI,
                                NotificationModel::CONTENT_TYPE_HTML,
                                $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                                "No Kredit ".$checkPayment->norek,
                                $mobileTemplate,
                                $minimalTemplate,
                                "MC"    
                            );

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                "id" => $notifId,
                                "tipe" => "MC",
                                "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                                "tagline" => "No Kredit ".$checkPayment->norek,
                                "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,                                    
                                "token" => $token->no_hp    
                            ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmail($token->email, "Konfirmasi Pembayaran Angsuran Nomor ".$checkPayment->norek, $emailTemplate);

                            //Set response
                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                'data' => array(
                                    'idTransaksi' => $idTransaksi,
                                    'virtualAccount' => $virtualAccount,
                                    'expired' => $tglExpired,
                                    'now' => date('Y-m-d H:i:s')                                        
                                )
                            ], 200);

                        } else {
                            log_message('debug', 'Create VA error. Reason:'.json_encode($createBilling));
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => $createBilling->responseDesc,
                                'code' => 103,
                                'reason' => $createBilling
                            ), 200);
                        }
                    }
                   
               }else{
                   log_message('debug', 'Payment not found'.$this->post('id_transaksi'));
                   $this->set_response(array(
                       'status' => 'success',
                       'message' => 'Payment not found',
                       'code' => 102
                   ),200);
               }
           }
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    /**
     * Generate payment notif mikro
     * @param String $nama nama user
     * @param String $trxId id transaksi 
     * @return Array template notifikasi
     */
    function generatePaymentNotif($nama, $trxId)
    {
        $payment = $this->MikroModel->getPaymentByTrxId($trxId);
        $subject = null;
        $viewData = array();
       
        if(
            $payment->payment  == 'BNI' ||
            $payment->payment  == 'VA_BCA' ||
            $payment->payment  == 'VA_MANDIRI' ||
            $payment->payment  == 'VA_PERMATA' ||
            $payment->payment  == 'VA_BRI'
        ) {
            $subject = "Pembayaran Angsuran No ".$payment->norek;        
            $fTglExpired = new DateTime($payment->tanggal_expired);
            $viewData = array(
                'amount' => $payment->totalKewajiban,
                'nama' => $nama,
                'namaNasabah' => $payment->namaNasabah,
                'va' => $payment->virtual_account,
                'tglExpired' => $fTglExpired->format('d/m/Y H:i:s'),
                'norek' => $payment->norek,
                'payment' => $payment->tipe,
                'trxId' => $payment->reffSwitching,
                'namaProduk' => $payment->namaProduk,
                'payment' => $payment->payment,
                'biayaTransaksi' => $payment->biayaTransaksi    
            );
        }else if($payment->payment == 'WALLET' || $payment->payment == 'GCASH'){
            $subject = "Transaksi Pembayaran Angsuran ".$payment->norek.' Sukses';        
            $viewData = array(
                'nama' => $nama,
                'namaNasabah' => $payment->namaNasabah,            
                'norek' => $payment->norek,
                'trxId' => $trxId,
                'produk' => $payment->namaProduk,
                'angsuranKe' => $payment->angsuranKe,
                'administrasi' => $payment->administrasi,
                'angsuran' => $payment->angsuran,
                'denda' => $payment->denda,
                'total' => $payment->totalKewajiban,
                'payment' => $payment->payment,
                'biayaTransaksi' => $payment->biayaTransaksi   
            );
        }
        
        $content = $this->load->view('mail/notif_payment_mikro', $viewData, true);
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    /**
     * Generate template notifikasi pengajuan mikro
     * @param type $nama
     * @param type $noPengajuan
     * @param type $jenisId
     * @param type $noId
     * @param type $alamat
     * @param type $noHp
     * @param type $kebutuhanModal
     * @param type $agunan
     * @param type $namaUsaha
     * @param type $jenisUsaha
     * @param type $alamatUsaha
     * @return type
     */
    function generateMikroNotif(
            $nama,
            $noPengajuan,
            $jenisId,
            $noId,
            $alamat,
            $noHp,
            $kebutuhanModal,
            $agunan,
            $merk,
            $tahunPembuatan,
            $namaUsaha,
            $jenisUsaha,
            $alamatUsaha,
            $kodeOutlet
            )
    {
        $outlet = $this->MikroModel->getOutlet($kodeOutlet);
        $ji = $jenisId == '10' ? 'KTP' : 'Paspor';
        $templateData = array(
            'nama' => $nama,
            'noPengajuan' => $noPengajuan,
            'tglPengajuan' => date('d/m/Y H:i:s'),
            'jenisId' => $ji,
            'noId' => $noId,
            'alamat' => $alamat,
            'noHp' => $noHp,
            'kebutuhanModal' => $kebutuhanModal,
            'agunan' => $agunan,
            'namaUsaha' => $namaUsaha,
            'jenisUsaha' => $jenisUsaha,
            'alamatUsaha' => $alamatUsaha,
            'merk'=>$this->MikroModel->getNamaKendaraan($merk),
            'tahunPembuatan'=> $tahunPembuatan,
            'namaOutlet' => $outlet->namaOutlet,
            'alamatOutlet' => $outlet->alamat,
            'telpOutlet' => $outlet->telepon
        );
        
        $subjekEmail = 'Pengajuan Pembiayaan Usaha Anda Berhasil ('.$noPengajuan.')';
        
        $content = $this->load->view('mail/notif_micro', $templateData, TRUE);
        
        $template = $this->load->view('mail/email_template_top', array('title'=>$subjekEmail), true); 
        $template = $template.$content;
        $template = $template.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subjekEmail), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'mobile' => $mobile,
            'email' => $template,
            'minimal' => $content
        );
        
    }
    
    function uploadFotoUsaha()
    {
        $uploadDir = $this->config->item('upload_dir');
        $config['upload_path'] = $uploadDir . '/user/mikro/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['encrypt_name'] = true;

        $this->load->library('upload');

        $foto = [];
        $errors = [];
        $jmlFoto = count($_FILES['foto_usaha']['name']);
        for ($i = 0; $i < $jmlFoto; $i++) :
            $_FILES['userfile']['name'] = $_FILES['foto_usaha']['name'][$i];
            $_FILES['userfile']['type'] = $_FILES['foto_usaha']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['foto_usaha']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $_FILES['foto_usaha']['error'][$i];
            $_FILES['userfile']['size'] = $_FILES['foto_usaha']['size'][$i];

            $this->upload->initialize($config);
            if (!$this->upload->do_upload()) :
                $errors[] = $this->upload->display_errors();
            else :
                $foto[]['nama_foto'] = $this->upload->data()['file_name'];
            endif;
        endfor;

        return array(
            'foto' => $foto,
            'error' => $errors
        );
    }
    
    function detail_get()
    {
        $token = $this->getToken();
        if($token)
        {
            $noPengjuan = $this->query('no_pengajuan');
            
            $detail = $this->MikroModel->getDetail($token->id, $noPengjuan);
            
            if($detail){
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $detail
                ));
            }else{
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 102,
                    'message' => 'Pengajuan tidak ditemukan'                    
                ),200);
            }            
        }else{
            $this->errorUnAuthorized();
        }
        
    }
    
    function uploadFotoKendaraan()
    {
        $uploadDir = $this->config->item('upload_dir');
        $config['upload_path'] = $uploadDir . '/user/mikro/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['encrypt_name'] = true;
        

        $this->load->library('upload');

        $foto = [];
        $errors = [];
        $jmlFoto = count($_FILES['foto_kendaraan']['name']);
        for ($i = 0; $i < $jmlFoto; $i++) :
            $_FILES['userfile']['name'] = $_FILES['foto_kendaraan']['name'][$i];
            $_FILES['userfile']['type'] = $_FILES['foto_kendaraan']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['foto_kendaraan']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $_FILES['foto_kendaraan']['error'][$i];
            $_FILES['userfile']['size'] = $_FILES['foto_kendaraan']['size'][$i];

            $this->upload->initialize($config);
            if (!$this->upload->do_upload()) :
                $errors[] = $this->upload->display_errors();
            else :
                $foto[]['nama_foto'] = $this->upload->data()['file_name'];
            endif;
        endfor;

        return array(
            'foto' => $foto,
            'error' => $errors
        );
    } 
    
    function save_rekening_post()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        //Cek if user already CIF
        $isUserCIF = $this->User->isUserCif($token->id);

        if ($isUserCIF) {
            $this->errorForbbiden();
            return;
        }

        $setData = array(
            'nama' => $this->post('nama'),
            'jenis_identitas' => $this->post('jenis_identitas'),
            'no_identitas' => $this->post('no_identitas'),
            'tanggal_expired_identitas' => $this->post('tanggal_expired_identitas'),
            'tempat_lahir' => $this->post('tempat_lahir'),
            'tanggal_lahir' => $this->post('tanggal_lahir'),
            'no_hp' => $this->post('no_hp'),
            'jenis_kelamin' => $this->post('jenis_kelamin'),
            'status_kawin' => $this->post("status_kawin"),
            'kode_kelurahan' => $this->post("kode_kelurahan"),
            'jalan' => $this->post("jalan"),
            'ibu_kandung' => $this->post("ibu_kandung")
        );

        $this->form_validation->set_data($setData);
        $this->form_validation->set_rules('nama', 'Nama Nasabah', 'required');

        $this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required');
        $this->form_validation->set_rules('no_identitas', 'No Identitas', 'required');
        
        if($this->post('jenis_identitas')=='12'){
            $this->form_validation->set_rules('tanggal_expired_identitas', 'tanggal_expired_identitas', 'required');
        }

        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');

        $this->form_validation->set_rules('no_hp', 'No HP', 'required');

        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required');

        $this->form_validation->set_rules('kode_kelurahan', 'Kode kelurahan', 'required');

        $this->form_validation->set_rules('jalan', 'Jalan', 'required');
        $this->form_validation->set_rules('ibu_kandung', 'Ibu Kandung', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->set_response(array(
                'status' => 'error',
                'code' => 201,
                'message' => '',
                'errros' => $this->form_validation->error_array()
            ), 200);
        } else {
            $nama = $this->post('nama');
            $jenis_identitas = $this->post('jenis_identitas');
            $no_identitas = $this->post('no_identitas');
            $tanggal_expired_identitas = $this->post('tanggal_expired_identitas');
            $tempat_lahir = $this->post('tempat_lahir');
            $tanggal_lahir = $this->post('tanggal_lahir');
            $no_hp = $this->post('no_hp');
            $jenis_kelamin = $this->post('jenis_kelamin');
            $status_kawin = $this->post("status_kawin");

            $kode_kelurahan = $this->post("kode_kelurahan");
            $jalan = $this->post("jalan");

            $ibu_kandung = $this->post("ibu_kandung");

            $namaFoto = '';
            // Cek jika ada data foto dari user
            if (!isset($_FILES['userfile']['name'])) {
                // get foto name from UserModel
                $namaFoto = $this->User->profile($token->id)->fotoKTP;

                if (!$namaFoto) {
                    $this->response([
                        'code' => 101,
                        'status' => 'error',
                        'message' => '',
                        'errors' => 'Foto KTP Tidak boleh kosong'
                    ], 200);
                    return;
                }
            } else {
                // Upload file
                // Update foto KTP User
                
                $uploadDir = $this->config->item('upload_dir');
                
                $config['upload_path'] = $uploadDir . '/user/ktp';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                $config['max_size'] = 0;
                $config['max_width'] = 0;
                $config['max_height'] = 0;
                $config['encrypt_name'] = true;

                $this->load->library('upload', $config);

                $uploadData = null;
                if (!$this->upload->do_upload('userfile')) {
                    $this->response([
                        'code' => 101,
                        'status' => 'error',
                        'message' => '',
                        'errors' => $this->upload->display_errors()
                    ], 200);
                    return;
                } else {
                    //Delete previous user file
                    $prevFile = $this->User->profile($token->id)->fotoKTP;

                    if ($prevFile != null && file_exists($uploadDir . '/user/ktp/' . $prevFile)) {
                        unlink($uploadDir . '/user/ktp/' . $prevFile);
                    }

                    $uploadData = $this->upload->data();
                    $namaFoto = $uploadData['file_name'];
                }
            }

            //Save tabungan emas dan update data user
            $idRekening = $this->MikroModel->saveRekening(
                $token->id,
                $nama,
                $jenis_identitas,
                $no_identitas,
                $tanggal_expired_identitas,
                $tempat_lahir,
                $tanggal_lahir,
                $no_hp,
                $jenis_kelamin,
                $status_kawin,
                $kode_kelurahan,
                $jalan,
                $ibu_kandung,
                $namaFoto
            );

            $this->set_response(array(
                'status' => 'success',
                'message' => 'Rekening Berhasil Dibuat',
                'data' => array(
                    'id' => $idRekening
                )
            ), 200);
        }
    }

    
        /**
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     * INQUIRY DARI CORE
     */
    
    function authCore()
    {
        $headers = $this->input->request_headers();
        if(Authorization::tokenIsExist($headers)){
            $corePassword = $this->config->item('core_password');
            if($headers['Authorization'] == $corePassword){
                return true;
            }
            
            return false;
        }
        
        return false;
    }
    
    function inquiry_get()
    {
        if (!$this->authCore()) {
            $this->errorUnAuthorized();
            return;
        }
        
        $kode = $this->query('kode');
        
        $mikro = $this->MikroModel->getMikroByKodeBooking2($kode);  
        
        if($mikro){
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $mikro
            ], 200);
        }else{
            $this->set_response([
                'status' => 'error',
                'code' => 102,
                'message' => 'Tidak ada data pengajuan yang ditemukan'
            ],200);
        }
    }
    
    /**
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     * END INQUIRY DARI CORE
     */

}
