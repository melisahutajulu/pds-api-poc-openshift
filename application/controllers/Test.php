<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'CorePegadaian.php';
use \Curl\Curl;
class Test extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('User','NotificationModel','PaymentModel','GadaiModel','BankModel'));
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->load->helper(array('Message'));
    }
    
    function test_email_get()
    {
        $subject = 'Ini test email lainnya';
        $message = '<h1>Hello Preketew Jajaja</h1><p>Apa kabar anda sekalian?</p>';
        Message::sendEmail('budasuyasa@gmail.com', $subject, $message);
        Message::sendEmailMandrill('komangirawadi@gmail.com', $subject, $message);
        Message::sendEmailMandrill('komangirawadi9@gmail.com', $subject, $message);
    }
    
    function emiter_get(){
        $emitter = new Emitter();
        $emitter->in('pds-web')
                ->emit('message', array(
                    'title' => 'sapimandi',
                    'username'=>'test', 
                    'message'=>'mandi dulu sapi')
                        ); 
                $emitter->in('pds-admin')
                ->emit('message', array(
                    'title' => 'sapimandi',
                    'username'=>'test', 
                    'message'=>'mandi dulu sapi')
                        ); 
    }

    function emiter2_get()
    {
        $emitter = new Emitter();
        $emitter
                ->emit('message', array(
                    'title' => 'sapimandi',
                    'username'=>'test', 
                    'message'=>'mandi dulu sapi')
                        ); 
        
    }
    
    
    function receiver_get(){
        $this->load->view('test/receiver');
    }
    
    function biaya_get(){
        $this->load->model('ConfigModel');
        echo $this->ConfigModel->getBiayaPayment('UG','BANK','01','009');
    }
    
    function minimum_get(){
        $this->load->model('ConfigModel');
        echo $this->ConfigModel->getMinimumBuybackEmas();
    }

   function info_get(){
    phpinfo();
   }
    
    function curl_get()
    {
        
        $curl = new Curl();
        $curl->get('https://www.example.com/');

        if ($curl->error) {
            echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
        } else {
            echo 'Response:' . "\n";
            var_dump($curl->response);
        }
    }
    
    function date_get(){
        echo date('Y-m-d H:i:s');
    }

    function saldo_get(){
        $saldo = $this->getSaldo('1232317620000360');
        $reqPaymentData['saldo'] = $saldo['saldo'];
        $reqPaymentData['saldoAkhir'] = $saldo['saldoAkhir'];
        $reqPaymentData['saldoBlokir'] = $saldo['saldoBlokir'];

        $this->set_response($reqPaymentData);
    }

    function redaksi_get()
    {
        $this->load->model('ConfigModel');
        $data =  $this->ConfigModel->getRedaksiPayment('014','0877782828','50000');
        $this->set_response($data);
    }
    
    function testpln_get()
    {
        $str = '["201802"," 201803"," 201804"]';
        $arrPeriode = json_decode($str);
        $strFormat = '';
        if($arrPeriode){
            $c = 1;
            foreach ($arrPeriode as $a){
                $withoutSpace = str_replace(" ", "", $a);                
                if($withoutSpace !== ""){
                    echo $withoutSpace;
                    $d = DateTime::createFromFormat('Ym', $withoutSpace);
                    var_dump($d);
                    $strFormat = $strFormat. $d->format('M Y');
                    if($c < count($arrPeriode)){
                        $strFormat = $strFormat.', ';
                    }
                }
                $c++;
            }
        }
        
        echo $strFormat;
        
    }
    
    function tabungan_get()
    {
        $id = '99970000371524551026372';
        $tabuganEmas = $this->User->getTabunganEmas($id);
        $this->set_response($tabuganEmas);
    }
    
    function permate_get(){
        $billing = $this->createBillingPermata('087863165600');
        echo json_encode($billing);
    }
    
    function hargamulia_get(){
        $hargamulia = $this->harga_mulia('6017');
        $this->set_response([
            'status' => 'success',
            'message' => '',
            'data' => json_decode($hargamulia->data)
        ]);
    }
    

    function co_get(){
        $data="[{\"cif\":\"1012678073\",\"hp\":\"087863165600\",\"ibuKandung\":\"IBU KANDUNG\",\"jenisKelamin\":\"L\",\"kodeCabang\":\"11892\",\"namaNasabah\":\"BUDA\",\"noIdentitas\":\"5103020404920009\",\"telp\":\"\",\"tglLahir\":\"1992-08-07\",\"tglKyc\":\"2018-12-08\",\"statusKyc\":\"0\",\"statusKawin\":\"1\",\"kewarganegaraan\":\"1\",\"tipeIdentitas\":\"10\",\"idKelurahan\":\"21020310\",\"jalan\":\"DENPASAR\"}]";
        $resData = json_decode($data); 
        echo ($resData[0]->cif);
    }
    
    function efektoken_get()
    {
        echo $this->getEfekToken();
    }

    function identitas_get(){
        var_dump($this->User->isIdentityOk(1310, true));
    }

    function noefek_get(){
        $_bookingId = $this->GadaiModel->generateBooking('gadai_efek', '700001');
        echo $_bookingId;
    }
    
    function gcashget_post(){
        $path = $this->post('path');
        $data = $this->post('data');

        $url = "http://api-ms.pegadaian.co.id".$path;
            
        $curl = new Curl();        
        $curl->setHeader('Content-Type','application/json');
        //$curl->setHeader('Authorization','Bearer '.$gPointToken);
        
        log_message('debug', 'TEST GCASH MICROSERVICE URL :'. $url);
        log_message('debug', 'TEST GCASH MICROSERVICE DATA :'. $data);
        $curl->get($url);

        if ($curl->error)
        {
            log_message('debug', 'TEST GCASH MICROSERVICE RESPONSE :'. $curl->errorCode . ': ' . $curl->errorMessage);
            $this->set_response([
                'status' => 'error',
                'message' => $curl->errorCode . ': ' . $curl->errorMessage,
                'data' => []

            ]);            
        }
        else
        {
            log_message('debug', 'TEST GCASH MICROSERVICE RESPONSE HEADER:'. json_encode($curl->responseHeaders));
            log_message('debug', 'TEST GCASH MICROSERVICE RESPONSE STATUSCODE:'. $curl->getHttpStatusCode);
            log_message('debug', 'TEST GCASH MICROSERVICE RESPONSE:'. json_encode($curl->response));
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $curl->response 
            ]);
        }
    }

    function bankname_get($kodeBank){
        $bankDetails = $this->BankModel->getBankList($kodeBank);
        var_dump($bankDetails);

    }

    function gpayment_get(){
        $this->load->model('GcashModel');
        $payment = $this->GcashModel->getPayment('15544571499604939');
        var_dump($payment);
    }

    function point_get(){
        $data = json_decode('{"userId":"1012678073","channel":"9997","product":"62","transactionType":"SL","unit":"rupiah","transactionAmount":"152500","reffCore":"1556073737601118921"}', true);

        $data['transactionAmount'] = (int) $data['transactionAmount'];

        var_dump($data['transactionAmount']);

        log_message('debug', 'GPOINT REFRESH POINT');
        log_message('debug', 'GPOINT REFRESH POINT DATA: '.json_encode($data));

        $refrehPoint = $this->gPointValue($data);

        log_message('debug', 'GPOINT REFRESH POINT RESPONSE: '.json_encode($refrehPoint));
    }
    
}
