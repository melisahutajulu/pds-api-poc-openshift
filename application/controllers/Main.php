<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'CorePegadaian.php';
class Main extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('User','MainModel'));
        $this->load->library('form_validation');
        
    }
    
    function harga_emas_get()
    {
        $token = $this->getToken();
        if($token)
        {            
            $hargaEmas = $this->MainModel->getLastHargaEmas();
            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $hargaEmas
            ), 200);
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function fluktuasi_emas_get()
    {
        $token = $this->getToken();
        if($token){
            $harga = $this->MainModel->getFluktuasiEmas();
            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $harga
            ),200);
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function banner_get()
    {
        $adminURL = $this->config->item('admin_base_url');
        $banner = $this->MainModel->getFrontBanner();
        $resBanner = array();

        foreach($banner as $b){
            $b->imgURL = $adminURL.'upload/banner/'.$b->banner;
            $resBanner[] = $b;            
        }

        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $resBanner
        ), 200);        
        
    }

    function shortcut_banner_get(){
        $token = $this->getToken();
        if($token)
        {
            $adminURL = $this->config->item('admin_base_url');
            $banner = $this->MainModel->getShortCutBanner();
            $res = array();
            foreach($banner as $b){
                $b->gambar =  $adminURL.'upload/images/'.$b->gambar;
                $res[] = $b;
            }

            $this->set_response(array(
                'status' => 'success',
                'message' => 'success',
                'data' => $res
            ),200);
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function check_otp_post()
    {
        $token = $this->getToken();
        if($token)
        {
            $this->form_validation->set_rules('no_telepon','no_telepon','required|numeric');
            $this->form_validation->set_rules('reff_id', 'reff_id', 'required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->set_response(array(
                    'status'=>'error',
                    'message'=>'invalid input',
                    'code'=>101,
                    'errors'=>$this->form_validation->error_array()
                ), 401);
            }else{
                $this->load->helper('message');
                $phone = $this->post('no_telepon');
                $otp = $this->post('otp');
                if($otp == null) $otp = "123456";                
                $reffId = $this->post('reff_id');                
                $checkOtp = $this->checkOTP($otp, $phone, NULL, $token->channelId);
                
                if($checkOtp->responseCode == '00'){
                    $this->response(array(
                        'status'=>'success',
                        'message'=>'OTP Valid',
                    ), 200);
                }else{
                    $response = array(
                        'status' => 'error',
                        'message' => 'OTP tidak ditemukan',
                        'code' => 102                    
                    );
                    $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                }            
            }
            
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function info_product_get(){
        $productInfo = $this->MainModel->getProductInfo();
        echo $productInfo;
    }
            
    
}
