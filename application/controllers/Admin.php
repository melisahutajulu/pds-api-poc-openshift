<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'CorePegadaian.php';

class Admin extends CorePegadaian{
    public function __construct()
    {
        parent::__construct();        
        $this->load->model(array('AdminModel','MuliaModel','User','NotificationModel'));  
        $this->load->library('form_validation');
        $this->load->helper('message');

    }
    
    function portofolio_gadai_mikro_get()
    {
        $token = $this->getToken();
        if($token)
        {
            $setData = array(
                'cif' => $this->query('cif')
            );
            $this->form_validation->set_data($setData);
            $this->form_validation->set_rules('cif', 'CIF', 'numeric|required');

            if (!$this->form_validation->run()) {
                $this->response(array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $cif = $this->query('cif');
                $iReq = $this->getPortofolioPinjaman($cif, $token->channelId);
                if ($iReq->responseCode == '00') {
                    $data = json_decode($iReq->data);
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => $data
                    ), 200);
                } else if ($iReq->responseCode == '14') {
                    $this->response(array(                        
                        'status' => 'success',
                        'message' => '',
                        'data' => []
                    ), 200);
                } else {
                    $this->response(array(
                        'code' => 101,
                        'status' => 'error',
                        'message' => 'Internal Server Error',
                    ), 200);
                }
            }
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function portofolio_emas_get()
    {
        $token = $this->getToken();
        if($token){
            $setData = array(
                'cif' => $this->query('cif')
            );
            $this->form_validation->set_data($setData);
            $this->form_validation->set_rules('cif', 'CIF', 'numeric|required');

            if (!$this->form_validation->run()) {
                $this->response(array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $cif = $this->query('cif');
                $iReq = $this->inquiryTabEmas($cif, $token->channelId);

                if ($iReq->responseCode == '00') {
                    $data = json_decode($iReq->data);
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => $data
                    ), 200);
                } else if ($iReq->responseCode == '14') {
                    $this->response(array(
                        'code' => 101,
                        'status' => 'error',
                        'message' => $iReq->responseDesc,
                    ), 200);
                }else {
                    $this->response(array(
                        'code' => 101,
                        'status' => 'error',
                        'message' => 'Internal Server Error',
                        'reason' => $iReq
                    ), 200);
                }
            }
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function check_otp_post()
    {
        $token = $this->getToken();
        if($token){
            
            $setData = array(
                'otp' => $this->post('otp'),
                'no_hp' => $this->post('no_hp'),
                'tipe' => $this->post('tipe')
            );
            $this->form_validation->set_data($setData);
            $this->form_validation->set_rules('otp', 'otp', 'numeric|required|exact_length[6]');
            $this->form_validation->set_rules('no_hp', 'no_hp', 'numeric|required');
            $this->form_validation->set_rules('tipe', 'tipe', 'required');
            
            if($this->form_validation->run() == false){
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ),200);
                return;
            }
            
            $otp = $this->post('otp');
            $no_hp = $this->post('no_hp');
            $tipe = $this->post('tipe');
            
            $checkOtp = $this->checkOTP($otp, $no_hp, $tipe);
            
            if($checkOtp->responseCode == '00'){
                
                $otpData = json_decode($checkOtp->data);
                
                $this->response(array(
                    'status'=>'success',
                    'message'=>'OTP Valid',
                    'data' => $otpData
                ), 200);
                
            }else{
                $response = array(
                    'status' => 'error',
                    'message' => 'OTP tidak ditemukan/expired',
                    'data' => $checkOtp
                );
                $this->set_response($response, 200);
            }
        }else{
            $this->errorUnAuthorized();
        }
    }    


    /**
     * Check admin token
     * @param  $token string admin token
     * @return
     */
    public function checkAdminToken($token)
    {
        $this->load->model('AdminModel');
        $token = $this->AdminModel->getAdminToken($token);
       
        return $token;        
    }

    /**
     * Routing Endpoint API untuk mulia
     * @param  $method
     * @return [type]
     */
    function mulia_post($method = 'thumbnail', $action = 'add')
    {
        $admin = $this->checkAdminToken($this->post('token'));
        if($admin)
        {
            if($method == 'thumbnail')
            {
                if($action == 'add')
                {
                    $this->muliaAddThumbnail();
                }
                else if($action == 'update')
                {
                    $this->muliaUpdateThumbnail();
                }
                else if($action == 'delete')
                {
                    $this->muliaDeleteThumbnail();
                }    
            }
            else if($method == 'images')
            {
                if($action == 'add')
                {
                    $this->muliaAddImages();
                }
                else if($action == 'delete')
                {
                    $this->muliaDeleteImages();
                }
            }
            else{
                $this->errorForbbiden();
            }            
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    /**
     * Method untuk konfigurasi file upload thumbnail dan images mulia
     * @return 
     */
    function imageUploadRules()
    {
        $uploadDir = $this->config->item('upload_dir');

        $config['upload_path']          = $uploadDir.'/mulia/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;
        $config['encrypt_name']         = true;

        return $config;        
    }

    /**
     * Tambah thumbnail produk mulia 
     */
    function muliaAddThumbnail()
    {
        $config = $this->imageUploadRules();
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {                
            $this->response([
                'code' => 101,
                'status' => 'error',
                'message' => '',
                'errors' => $this->upload->display_errors()
            ], 200);
        }
        else
        {           
            $data = $this->upload->data();
            
            $this->response([
                'status'=>'success',
                'message'=>'',
                'data'=> array(
                    'filename' => $data['file_name']
                )
            ], 200);            
        }
    }

    /**
     * Update thumbnail produk mulia 
     */
    function muliaUpdateThumbnail()
    {
        $this->form_validation->set_rules('id','id','required|integer');

        if($this->form_validation->run() == FALSE)
        {   
            $this->set_response([
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 201,
                'error' => $this->form_validation->error_array()
            ]);
        }
        else
        {
            $config = $this->imageUploadRules();
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('userfile'))
            {                
                $this->response([
                    'code' => 101,
                    'status' => 'error',
                    'message' => '',
                    'errors' => $this->upload->display_errors()
                ], 200);
            }
            else
            {
                $id = $this->post('id');

                //Get previous product by id
                $produk = $this->MuliaModel->getProdukById($id);

                //Delete previous user file
                $prevFile = $produk->thumbnail;

                $uploadDir = $this->config->item('upload_dir');
                if($prevFile != null && file_exists($uploadDir.'/mulia/'.$prevFile)){                    
                    unlink($uploadDir.'/mulia/'.$prevFile);                                        
                }

                $data = $this->upload->data();
                
                $this->response([
                    'status'=>'success',
                    'message'=>'',
                    'data'=> array(
                        'filename' => $data['file_name']
                    )
                ], 200);            
            }    
        }
    }

    /**
     * Delete thumbnail produk mulia
     */
    function muliaDeleteThumbnail()
    {
        $this->form_validation->set_rules('id','id','required|integer');

        if($this->form_validation->run() == FALSE)
        {   
            $this->set_response([
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 201,
                'error' => $this->form_validation->error_array()
            ]);
        }
        else
        {
            $id = $this->post('id');

            //Get previous product by id
            $produk = $this->MuliaModel->getProdukById($id);

            //Delete previous user file
            $prevFile = $produk->thumbnail;

            $uploadDir = $this->config->item('upload_dir');
            if($prevFile != null && file_exists($uploadDir.'/mulia/'.$prevFile)){                    
                unlink($uploadDir.'/mulia/'.$prevFile);                                        
            }

            $this->set_response([
                'status' => 'success',
                'message' => 'Thumbnail deleted',
                'data' => [
                    'filename' => $prevFile
                ]
            ]);
        }
    }

    /**
     * Add images produk mulia
     */
    function muliaAddImages()
    {
        $uploadDir = $this->config->item('upload_dir');

        $config['upload_path'] = $uploadDir . '/mulia/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['encrypt_name'] = true;
        $this->load->library('upload');
        
        $images = [];
        $errors = [];
        $jmlFoto = count($_FILES['images']['name']);
        
        for ($i = 0; $i < $jmlFoto; $i++)
        {
            $_FILES['userfile']['name'] = $_FILES['images']['name'][$i];
            $_FILES['userfile']['type'] = $_FILES['images']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $_FILES['images']['error'][$i];
            $_FILES['userfile']['size'] = $_FILES['images']['size'][$i];
         
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload())
            {
                $errors[] = $this->upload->display_errors();
            }
            else 
            {
                $data = $this->upload->data();
                $images[] = $data['file_name'];
            }
        }

        $this->set_response([
            'status' => 'success',
            'message' => 'Images added',
            'data' => [
                'images' => $images,
                'error' => $errors
            ]
        ], 200);
    }

    /**
     * Delete images produk mulia
     */
    function muliaDeleteImages()
    {   
        $images = $this->input->post('images');
        $uploadDir = $this->config->item('upload_dir');

        $deleted = [];
        foreach ($images as $prevFile) {
            if($prevFile != null && file_exists($uploadDir.'/mulia/'.$prevFile)){                    
                unlink($uploadDir.'/mulia/'.$prevFile);
                $deleted[] = $prevFile;
            }    
        }
        
        $this->set_response([
            'status' => 'success',
            'message' => '',
            'data' => [
                'deleted' => $deleted
            ]
        ]);
    }

    public function vendor_post($type='mulia', $params='add', $params2=null){

        $admin = $this->checkAdminToken($this->post('token'));

        if($admin)
        {
            $uploadDir = $this->config->item('upload_dir');
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
            $config['encrypt_name'] = true;

            if($type == 'mulia')
            {
                $config['upload_path'] = $uploadDir . '/mulia/';
            }
            else if($type == 'emas')
            {
                $config['upload_path'] = $uploadDir . '/emas/';
            }

            if($params == 'add')
            {
                $this->uploadVendorThumbnail($config);
            }
            else if($params !== 'add' && $params2=='delete')
            {
                $this->deleteVendorThumbnail($type, $params);
            }

        }
        else
        {
            $this->errorUnAuthorized();
        }
    }


    function uploadVendorThumbnail($config)
    {
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
            $this->response([
                'code' => 101,
                'status' => 'error',
                'message' => '',
                'errors' => $this->upload->display_errors()
            ], 200);
        }
        else
        {
            $data = $this->upload->data();

            $this->response([
                'status'=>'success',
                'message'=>'',
                'data'=> array(
                    'filename' => $data['file_name']
                )
            ], 200);
        }
    }

    function deleteVendorThumbnail($type, $fileName)
    {
        $uploadDir = $this->config->item('upload_dir');
        if(file_exists($uploadDir.'/'.$type.'/'.$fileName)){
            unlink($uploadDir.'/'.$type.'/'.$fileName);
        }

        $this->set_response([
            'status' => 'success',
            'message' => 'Thumbnail deleted',
            'data' => [
                'filename' => $fileName
            ]
        ]);
    }

    function change_user_email_post()
    {
        $admin = $this->checkAdminToken($this->post('token'));
        if(!$admin){
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_rules('email','email|valid_email','required');
        $this->form_validation->set_rules('user_id','user_id','required');

        if($this->form_validation->run() == FALSE)
        {
            $this->set_response([
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 101,
                'errors' => $this->form_validation->error_array()
            ]);
        }
        else
        {
            $email = $this->post('email');
            $userId = $this->post('user_id');

            if($this->User->isEmailChangeable($email, $userId))
            {
                $user = $this->User->getUser($userId);

                $emailVerifyToken = bin2hex(random_bytes(78));
                $this->User->updateUser($userId, [
                    'email' => $email,
                    'email_verification_token' => $emailVerifyToken,
                ]);

                // Kirim ulang notifikasi
                $templateData = array(
                    'nama' => $user->nama,
                    'email' => $email
                );
                
                $template = $this->load->view('notification/email_notification', $templateData, TRUE);
                //Simpan notifikasi baru
                
                $notifType = NotificationModel::TYPE_ACCOUNT;
                $notifTitle = "Selamat Datang di Pegadaian Digital Service";
                $notifTagline = "Hai ".$user->nama.', lakukan verifikasi email untuk melanjutkan registrasi';                

                $newNotif = $this->NotificationModel->add($userId, $notifType, 'html', $notifTitle, $notifTagline, $template);

                //Kirim notifikasi FCM
                Message::sendNotifWelcome($user->fcm_token, $newNotif, $user->nama);
                
                //Send welcome message to email
                Message::sendEmailVerificationEmail($email, $user->nama, $emailVerifyToken);
                
                $this->set_response([
                    'status' => 'success',
                    'message' => 'Email user berhasil diperbaharui.',
                    'data' => null
                ]); 
            }
            else
            {
                $this->set_response([
                    'status' => 'error',
                    'message' => 'Email sudah digunakan',
                    'code' => 101,
                ]);
            }

        }

    }

    function unlink_cif_post(){
        //die("test");
        //log_message('debug', 'unlink_cif: '.json_encode($this->post()));

        error_log("\n DEBUG : ".json_encode($this->post())."\n", 3, "/data/log/unlink_cif.log");

        //print_r($this->post());die;
        
        $admin = $this->checkAdminToken($this->post('token'));
        
        if(!$admin){
            
            return $this->errorUnAuthorized();
        }
        $this->form_validation->set_rules('channelId','cif','ibuKandung','namaNasabah','noHp','username','required');
        // print_r($this->form_validation->error_array()); die();

        
        if($this->form_validation->run() == FALSE)
        {
            
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }
            
            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101,
            ]);
        } else {
           
            $data = [
                'cif' => $this->post('cif'),
                'clientId' => $this->post('clientId'),
                'ibuKandung' => $this->post('ibuKandung'),
                'namaNasabah' => $this->post('namaNasabah'),
                'noHp' => $this->post('noHp'),
                'username' => $this->post('username')
            ];
            
            $unlinkcif = $this->unlinkCif($data);

            error_log("DEBUG Response Core : ".json_encode($unlinkcif)."\n", 3, "/data/log/unlink_cif.log");
               
            if ($unlinkcif->responseCode == '00' && $unlinkcif->responseDesc == 'Approved') {
                $data = json_decode($unlinkcif->data);
                $dataArr                = $data;

                if ($dataArr->cif != '') {
                    //die("oke");
                    try {
                        $this->User->updateCif($dataArr->cif);
                        //if(){
                            $this->set_response([
                                'response_code' => $unlinkcif->responseCode,
                                'status' => 'success',
                                'message' => $unlinkcif->responseDesc,
                                'debug' => "fajar",
                                'data' => $dataArr
                            ], 200);
                        //} else {
                        //    $this->set_response([
                        //        'response_code' => $unlinkcif->responseCode,
                        //        'status' => 'success',
                        //        'message' => 'Delete CIF gagal di Database PDS',
                        //        'data' => $dataArr
                        //    ], 200);
                        //}
                    } catch (Exception $e) {
                        $this->set_response([
                            "response_code" => 99,
                            "status" => "error",
                            "message" => $e->getMessage()
                        ], 201);
                        //echo($e->getMessage());
                    }
                }
            } else {
                $this->set_response([
                    'response_code' => $unlinkcif->responseCode,
                    'status' => 'error',
                    'message' => $unlinkcif->responseDesc,
                    'data' => $unlinkcif->data
                ], 201);
            }
        }
    }


    function change_phone_number_post(){

        /* Example PARAM
         * {
              "channelId": "6017",
              "cif": "9005351602",
              "clientId": "9997",
              "ibuKandung": "IBU",
              "namaNasabah": "YUSNIAR IMAM SYAH",
              "noHp": "082302200022",
              "noHpNew": "081289304584",
              "tanggalLahir": "1981-11-22"
            }
         */

        $admin = $this->checkAdminToken($this->post('token'));
        if(!$admin){
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_rules('user_AIID', 'user_AIID', 'required');
        $this->form_validation->set_rules('ibuKandung', 'Ibu Kandung', 'required');
        $this->form_validation->set_rules('namaNasabah', 'Nama Nasabah', 'required');
        $this->form_validation->set_rules('tanggalLahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('no_hp_current','No HP Sekarang', 'required');
        $this->form_validation->set_rules('no_hp_new','No HP baru', 'required|valid_phone_number|min_length[10]|max_length[15]');
        $this->form_validation->set_message('valid_phone_number', 'Invalid prefix number');


        if ($this->form_validation->run()){

            $data = $this->post();

            $data['clientId'] = $this->config->item('core_post_username');
            $data['channelId'] = '6017';
            $data['noHp'] = $data['no_hp_current'];

            if (substr($data['no_hp_new'], 0, 2) == '62')
                $data['no_hp_new'] = '0'.substr($data['no_hp_new'], 2);

            $data['noHpNew'] = $data['no_hp_new'];


            //check phone number exist
            $is_phone_exist = $this->User->isPhoneExist($data['noHpNew']);

//            print_r($is_phone_exist);die;

            if($is_phone_exist != ""){
                $this->set_response([
                    'response_code' => "94",
                    'status' => 'error',
                    'message' => "Phone number exist, please try with another phone number",
                    'data' => array("noHp" => $data['noHpNew'])
                ], 201);
            }else{

                unset($data['no_hp_new']);
                unset($data['token']);
                unset($data['no_hp_current']);

                if ($data['cif'] != '' || $data['cif'] != null) {

                    $path = '/customer/phonenumber';
                    $req = $this->coreRequest($path, $data, "Change Phone Number");

                    if ($req->responseCode == "00") {

                        // update db
                        $this->User->updatePhone($data);

                        $this->set_response([
                            'response_code' => $req->responseCode,
                            'status' => 'success',
                            'message' => $req->responseDesc,
                            'data' => $req->data
                        ], 200);
                    } else {
                        $this->set_response([
                            'response_code' => $req->responseCode,
                            'status' => 'error',
                            'message' => $req->responseDesc,
                            'data' => $req->data
                        ], 201);
                    }
                }else{
                    // update db
                    $this->User->updatePhone($data);

                    $this->set_response([
                        'response_code' => "00",
                        'status' => 'success',
                        'message' => "success, but we don't hit core, because cif doesnt exist",
                        'data' => array("noHp" => $data['noHpNew'])
                    ], 200);
                }
            }
        }else{
            $error = "";
            foreach ($this->form_validation->error_array() as $value){
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101
            ]);
        }

    }

}
