<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Psr\Http\Message\ResponseInterface;

class RestGosend_service extends MY_Service
{
    /**
     * @var
     */
    private $client;

    /**
     * RestGosend_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = new Client([
            'base_uri' => $this->config->item('gosend_url'),
            'headers' => [
                'Client_id' => $this->config->item('gosend_clientid'),
                'pass_key' => $this->config->item('gosend_passkey'),
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]
        ]);
    }

    /**
     * @param $url
     * @return ResponseInterface
     */
    public function getData($url)
    {
        try {
            $client = $this->client->get($url);
            log_message('debug', 'RestGosend_service url => ' . $url);
            return $client;
        } catch (BadResponseException $e) {
            log_message('debug', 'RestGosend_service  => ' . json_encode($e->getMessage()));
            return null;
        }
    }

    /**
     * @param $no_order
     * @return mixed|null
     */
    public function getStatus($no_order)
    {
        $result = null;
        try {
            $url = "orderno/$no_order";
            $response = $this->getData($url);
            if ($response) {
                $response = $response->getBody()->getContents();
                $result = json_decode($response);
                log_message('debug', 'RestGosend_service getStatus => ' . json_encode($result));
            }

            return $result;
        } catch (BadResponseException $exception) {
            return null;
        }
    }
}