<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once(APPPATH."third_party/phpmailer/src/PHPMailer.php");
require_once(APPPATH."third_party/phpmailer/src/Exception.php");
require_once(APPPATH."third_party/phpmailer/src/SMTP.php");
        
class PHPMailer_library
{
    public function __construct()
    {
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function load()
    {
        $objMail = new PHPMailer(TRUE);
        return $objMail;
    }
}