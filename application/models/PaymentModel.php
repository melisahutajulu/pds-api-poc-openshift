<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaymentModel extends CI_Model
{  
    function add($data)
    {
         $this->db->insert('payment', $data);
    }
    
     
    
    function check($trxId, $tglPembayaran)
    {
        $where = array(
            'id_transaksi'=>$trxId,
            'status' => 0
        );
        $cek = $this->db->where($where)->get('payment');
        if($cek->num_rows() > 0)        {
            $transaksi = $cek->row();
            return $transaksi;
        }else{
            return FALSE;
        }
    }
    
    function paid($trxId, $tglPembayaran) {
        $updatePayment = array(
            'status' => '1',
            'tanggal_pembayaran' => $tglPembayaran,
        );
        $where = array(
            'id_transaksi' => $trxId,
            'status' => 0
        );

        $this->db->where($where)->update('payment', $updatePayment);
    }
    
    /**
     * Method untuk mendapatkan detail payment berdasarkan id transaksi
     * Pengecekan dilakukan di dua table yaitu payment dan payment_gadai
     * 
     * @param type $trxId
     * @return object record data payment
     */
    function getPaymentByTrxId($trxId, $rowArray = false)
    {
        $cek = $this->db->select('payment.*, tabungan_emas.nama as namaNasabah')
                        ->join('tabungan_emas','tabungan_emas.no_rekening=payment.no_rekening','left')
                        ->where('payment.id_transaksi', $trxId)->get('payment');
        if($cek->num_rows() > 0){
            return $rowArray ? $cek->row_array() : $cek->row();
        }        
        
    }
    
    function getPaymentByTrxId2($trxId, $rowArray = false)
    {
        $cek = $this->db->select('payment.*, user.email, user.fcm_token, user.nama, user.no_hp')
                        ->join('user','user.user_AIID=payment.user_AIID')
                        ->where('reffSwitching', $trxId)->get('payment');
        if($cek->num_rows() > 0)
        {
            return $rowArray ? $cek->row_array() : $cek->row();
        }
        
        return FALSE;
    }
    
    
    
    /**
     * Method untuk melakukan update payment berdasarkan id transaksi
     * @param String $trxId id transaksi
     * @param array $data field dan data yang diupdate
     */
    function update($trxId, $data)
    {
        $this->db->where('id_transaksi', $trxId)->update('payment', $data);

    }

}

