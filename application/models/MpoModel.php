<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MpoModel extends CI_Model
{
    function add($data)
    {
        $this->db->insert('mpo', $data);
    }

    function update($data)
    {
        $this->db->where('reffSwitching', $data['reffSwitching'])->update('mpo', $data);
    }

    function getMpo($reffSwitching)
    {
        $cek = $this->db
            ->select('user.nama, user.email, user.fcm_token, user.no_hp, mpo.*')
            ->join('user', 'user.user_AIID=mpo.user_AIID')
            ->where('reffSwitching', $reffSwitching)
            ->get('mpo');

        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }

    /**
     * Function get mpo ref by kodeLayanan, kodeBiller, group
     *
     * @param $kodeLayananMpo
     * @param $kodeBiller
     * @param null $group
     * @return bool|mixed
     */
    function getMpoProduct($kodeLayananMpo, $kodeBiller, $group = null)
    {
        $cek = $this->db
            ->where('kodeLayananMpo', $kodeLayananMpo)
            ->where('kodeBiller', $kodeBiller);

        if (!empty($group)) {
            $cek->where('groups', $group);
        }

        $cek = $cek->get('ref_mpo_seluler');

        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }

    function getHistory($user_AIID)
    {
        $res = [];
        $history = $this->db->select('id, administrasi, kodeBiller, kodeLayananMpo')
            ->select('paymentStatus, totalKewajiban, biayaTransaksi')
            ->select('(totalKewajiban + biayaTransaksi) as totalBayar')
            ->where('user_AIID', $user_AIID)
            ->where('payment !=', '')
            ->order_by('id', 'desc')
            ->limit(30)
            ->get('mpo');

        foreach ($history->result() as $h) {
            $product = $this->getMpoProduct($h->kodeLayananMpo, $h->kodeBiller);
            if ($product) {
                $h->product = $product->namaLayanan;
                $res[] = $h;
            }
        }

        return $res;
    }

    /**
     * Function store data inquiry
     *
     * @param $code_mpo
     * @param $data
     * @return bool
     */
    function storeInquiryByMpo($code_mpo, $data)
    {
        $status = false;
        if (!empty($code_mpo)) {
            switch ($code_mpo) {
                case '070801' || '070601'; // Link aja || Dana
                    $data = $this->setDataMpo($data);
                    break;
            }

            self::add($data);

            $status = true;
        }

        return $status;
    }

    /**
     * Function setDataMpo
     *
     * @param $data
     * @return array
     */
    private function setDataMpo($data)
    {
        return [
            'billReff'       => $data['billReff'] ?? '',
            'keterangan1'    => $data['keterangan'] ?? '',
            'sid'            => $data['sid'] ?? '',
            'administrasi'   => $data['administrasi'] ?? '',
            'group'          => $data['group'] ?? '',
            'hargaDasar'     => $data['hargaDasar'] ?? '',
            'hargaJual'      => $data['hargaJual'] ?? '',
            'kodeBiller'     => $data['kodeBiller'] ?? '',
            'kodeLayananMpo' => $data['kodeLayananMpo'] ?? '',
            'respCodeMpo'    => $data['respCodeMpo'] ?? '',
            'respDescMpo'    => $data['respDescMpo'] ?? '',
            'reffSwitching'  => $data['reffSwitching'] ?? '',
            'surcharge'      => $data['surcharge'] ?? '',
            'totalKewajiban' => $data['totalKewajiban'] ?? '',
            'user_AIID'      => $data['user_AIID'] ?? '',
            'bookingCode'    => $data['bookingCode'] ?? '',
            'idTambahan'     => $data['idTambahan'] ??  '',
            'norek'          => $data['norek'] ?? '',
            'jenisTransaksi' => $data['jenisTransaksi'] ??  ''
        ];
    }
}