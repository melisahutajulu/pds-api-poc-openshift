<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MuliaModel extends CI_Model
{
    /**
     * Mendapatkan data notifikasi user
     * @param integer $offset
     * @param string $order_by
     * @param string $order
     * @return array
     */
    function getProduk($offset, $search = null, $category = null, $vendor)
    {
        $produksi = array();

        if($vendor == null){
             $produksi = ['ANTAM','UBS'];
        }else{
            array_push($produksi, $vendor);
        }

        $select = "*";
        $return = [];

        if($search != null && $category != null)
        {
            $this->db->select($select);
            $this->db->where('status','1');
		    $this->db->where('kategori', $category);
		    $this->db->where_in('produksi', $produksi);
            $this->db->like('nama', $search);
            $this->db->order_by('sold','desc');
		    $this->db->limit(20, $offset);
		    $return['data'] =$this->db->get('mulia_produk')->result();

            $this->db->select('id');
            $this->db->where('status','1');
            $this->db->where('kategori', $category);
            $this->db->where_in('produksi', $produksi);
            $this->db->like('nama', $search);	                            
            $return['totalRow'] = $this->db->get('mulia_produk')->num_rows();
        }
        else if($search != null && $category == null)
        {
        	$this->db->select($select);
            $this->db->where('status','1');
            $this->db->where_in('produksi', $produksi);
            $this->db->like('nama', $search);
            $this->db->order_by('sold','desc');
            $this->db->limit(20, $offset);
		    $return['data'] = $this->db->get('mulia_produk')->result();

            $this->db->select('id');
            $this->db->where('status','1');
            $this->db->where_in('produksi', $produksi);
            $this->db->like('nama', $search);		                            
		    $return['totalRow'] = $this->db->get('mulia_produk')->num_rows();                        
        	
        }
        else if($search == null && $category != null)
        {
        	$this->db->select($select);
            $this->db->where('status','1');
            $this->db->where('kategori', $category);
            $this->db->where_in('produksi', $produksi);                            
            $this->db->order_by('sold','desc');
            $this->db->limit(20, $offset);
		    $return['data'] = $this->db->get('mulia_produk')->result();

            $this->db->select('id');
            $this->db->where('status','1');
            $this->db->where('kategori', $category);
            $this->db->where_in('produksi', $produksi);
            $return['totalRow'] = $this->db->get('mulia_produk')->num_rows();
        }
        else if($search == null && $category == null)
        {
            //Get the data
        	$this->db->select($select);
            $this->db->where_in('produksi', $produksi);
			$this->db->limit(20, $offset);
			$this->db->order_by('sold','desc');			
            $return['data'] = $this->db->get('mulia_produk')->result();
             
			//Count all data
            $this->db->select('id')->where_in('produksi', $produksi);            
			$return['totalRow'] = $this->db->get('mulia_produk')->num_rows();                        
        }

        return $return;
    }  
    
    /**
     * Get data produk berdasarkan id produk
     * @param integer $id
     * @return array produk mulia
     */
    function getProdukById($id)
    {
    	$check = $this->db->where('id', $id)->get('mulia_produk');
    	if($check->num_rows() > 0)
    	{
    		return $check->result();
    	}
    	else
    	{
    		return [];
    	}
    }

    /** USER CART **/
    function getCart($userId)
    {
    	$this->db->select('s.id, p.id as produkId, p.nama, p.harga, p.thumbnail');
        $this->db->select('p.produksi, p.berat');
        $this->db->select('SUBSTRING(`deskripsi`, 1, 100) as deskripsi');
        $this->db->select('s.qty, s.status');
        $this->db->from('mulia_produk p');
        $this->db->join('mulia_shop_cart s','p.id=s.produkId');
        $this->db->where('s.user_AIID', $userId);
		return $this->db->get();
    }

    function addCart($userId, $produkId, $qty)
    {
        //Pastikan produknya ada
        $getProduct = $this->getProdukById($produkId);
        if(!$getProduct[0])
        {
            return [
                'id' => null,
                'produkId' => null,
                'nama' => null,
                'harga' => null,
                'thumbnail' => null,
                'deskripsi' => null,
                'qty' => null,
                'status' => null
            ];
        }

        $product = $getProduct[0];

    	//Check if produkId exist, jika ya, update qty saja
        $where = array(
            'user_AIID' => $userId,
            'produkId' => $produkId
        );
        $cek = $this->db->select('id')->where($where)->get('mulia_shop_cart');
        
        if($cek->num_rows() > 0)
    	{
    		$row = $cek->row();

    		//Update qty dan last update
    		$this->db->where('id', $row->id)->update('mulia_shop_cart', array(
    			'qty' => $qty,
    			'last_update' => date('Y-m-d H:i:s')
    		));

    		return (object) [
                'id' => $row->id,
                'produkId' => $produkId,
                'nama' => $product->nama,
                'harga' => $product->harga,
                'thumbnail' => $product->thumbnail,
                'produksi' => $product->produksi,
                'berat' => $product->berat,
                'deskripsi' => word_limiter($product->deskripsi, 30),
                'qty' => $qty,
                'status' => 0
            ];
    	}
    	else
    	{
    		$lastUpdate = date('Y-m-d H:i:s');

	    	$this->db->insert('mulia_shop_cart', array(
	    		'user_AIID' => $userId,
	    		'produkId' => $produkId,
	    		'qty' => $qty,
	    		'status' => "0",
	    		'last_update' => date('Y-m-d H:i:s')
	    	));

            return (object) [
                'id' => $this->db->insert_id(),
                'produkId' => $produkId,
                'nama' => $product->nama,
                'harga' => $product->harga,
                'thumbnail' => $product->thumbnail,
                'produksi' => $product->produksi,
                'berat' => $product->berat,
                'deskripsi' => word_limiter($product->deskripsi, 30),
                'qty' => $qty,
                'status' => 0
            ];	    	 
    	}
    }


    function deleteCart($id)
    {
    	return $this->db->where('id', $id)->delete('mulia_shop_cart');
    }

    function getProdukOverview()
    {
        $this->db->where('isOverview','1');
        $this->db->where('produksi','UBS');
        $this->db->limit(6);    					 
    	$ubs = $this->db->get('mulia_produk')->result();

 		$this->db->where('isOverview','1');
        $this->db->where('produksi','ANTAM');
        $this->db->limit(6);
    	$antam = $this->db->get('mulia_produk')->result();  
    	
        return [
    		'ubs' => $ubs,
    		'antam' => $antam
    	]; 					 
    }

    /**
     * MULIA V2
     */

    /**
     * @param $vendorId string id cetak ubs atau vendor
     * @return array result database
     */
    function getProduct($vendorId)
    {
        return $this->db->select('id, nama, harga, berat, vendorId, thumbnail, dimensi, sertifikat, deskripsi, images')
                        ->where([
                            'vendorId' => $vendorId,
                            'harga !=' => '0',
                            'status' => '1'
                        ])
                        ->get('mulia_produk')
                        ->result();
        
    }

    function getProductById($id)
    {
        $cek =  $this->db->select('id, nama, harga, berat, vendorId, thumbnail, dimensi, sertifikat, deskripsi, images')
                         ->where('id', $id)
                         ->get('mulia_produk');
        if($cek->num_rows() > 0)
        {
            return $cek->row();
        }

        return false;
                         
    }

    /**
     * Save payment mulia baru (inquiry)
     * @param $data
     * @return mixed
     */
    function addPayment($data)
    {
        $this->db->insert('payment_mulia', $data);
        return $this->db->insert_id();
    }

    /**
     * Update payment mulia (payment, open)
     * @param $reffSwitching string id transaksi
     * @param $data array
     */
    function updatePayment($reffSwitching, $data)
    {
        $this->db->where('reffSwitching', $reffSwitching)->update('payment_mulia', $data);
    }

    function getPaymentByTrxId($trxId)
    {
        $cek =  $this->db->select('ref_vendor_mulia.nama as jenisLogamMulia')
                         ->select('user.nama as namaNasabah, user.alamat as alamatNasabah')
                         ->select('user.no_hp as noHpNasabah, user.no_ktp as noKtpNasabah')
                         ->select('user.email as emailNasabah')
                         ->select('payment_mulia.*')
                         ->join('ref_vendor_mulia','ref_vendor_mulia.vendorId=payment_mulia.idVendor','left')
                         ->join('user','payment_mulia.user_AIID=user.user_AIID')
                         ->where('reffSwitching', $trxId)
                         ->get('payment_mulia');
                         
        if($cek->num_rows() > 0){
            return $cek->row();
        }

        return false;
    }

    function getDenomDetail($denom)
    {
        $cek = $this->db->where('berat', $denom)->get('mulia_produk');
        if($cek->num_rows() > 0)
        {
            return $cek->row();
        }
        else
        {
            return false;
        }
    }

    function getProductThumbnail($berat, $vendorId){
        $cek = $this->db->where([
            'berat' => $berat,
            'vendorId' => $vendorId
        ])->get('mulia_produk');
        if($cek->num_rows() > 0){
            return $cek->row()->thumbnail;
        }

        return  '';
    }

    function getHistory($user_AIID, $id)
    {
        $imagePath = $this->config->item('mulia_image_path');
        date_default_timezone_set("Asia/Jakarta");

        if($id == null){
            $cek =  $this->db->select('payment_mulia.id, namaProduk, tglTransaksi, realTglTransaksi, isPaid, noKredit')
                ->select('idVendor, ref_vendor_mulia.nama as namaVendor, ref_vendor_mulia.thumbnail as thumbnail, uangMuka, tenor, kepingMuliaJson as items')
                ->select('kodeOutlet, totalHarga, diskonMargin, nominalUangMuka')
                ->select('pokokPembiayaan, angsuran, reffSwitching, payment')
                ->select('isPaid as statusBayar, isOpenSuccess as statusTransaksi, tglExpired')
                ->select('nama_outlet as namaOutlet, alamat as alamatOutlet, telepon as teleponOutlet')
                ->select('totalHarga as hargaDasar, nominalUangMuka as uangMuka, pokokPembiayaan as sisaPembayaran, tenor as jangkaWaktu, angsuran, administrasi, (totalKewajiban + biayaTransaksi) as totalPembayaran')
                ->join('ref_vendor_mulia','payment_mulia.idVendor=ref_vendor_mulia.vendorId')
                ->join('ref_cabang','payment_mulia.kodeOutlet=ref_cabang.kode_outlet','left')                
                ->where('user_AIID', $user_AIID)
                ->order_by('payment_mulia.id','desc')
                ->limit(10)
                ->get('payment_mulia')->result();

            $history = [];
            foreach($cek as $h)
            {
                $items = json_decode($h->items);
                $rItem = [];

                $_tglExpired = new DateTime($h->tglExpired);
                $_now = new DateTime();

                if($_tglExpired < $_now && $h->isPaid == '0'){
                    $h->isPaid = '2';
                }

                foreach($items as $i){
                    $i->totalHarga = $i->qty * $i->harga;
                    $i->thumbnail = $imagePath . $this->getProductThumbnail($i->denom, $h->idVendor); 
                    $rItem[] = $i;
                }
                
                $h->items = $rItem; 
                $h->thumbnail = $imagePath.$h->thumbnail;

                $history[] = $h;
            }
            return $history;
        }
        else
        {
            $cek =  $this->db->select('payment_mulia.id, namaProduk, tglTransaksi, realTglTransaksi, isPaid, noKredit')
                             ->select('idVendor, ref_vendor_mulia.nama as namaVendor, ref_vendor_mulia.thumbnail as thumbnail, uangMuka, tenor, kepingMuliaJson as items')
                             ->select('kodeOutlet, totalHarga, diskonMargin, nominalUangMuka')
                             ->select('pokokPembiayaan, angsuran, reffSwitching, payment')
                             ->select('isPaid as statusBayar, isOpenSuccess as statusTransaksi, tglExpired')
                             ->select('nama_outlet as namaOutlet, alamat as alamatOutlet, telepon as teleponOutlet')
                             ->select('kel.nama_kelurahan as kelurahanOutlet, kec.nama_kecamatan  as kecamatanOutlet')
                             ->select('kab.nama_kabupaten as kabupatenOutlet, prov.nama_provinsi as provinsiOutlet')
                             ->select('totalHarga as hargaDasar, nominalUangMuka as uangMuka, pokokPembiayaan as sisaPembayaran, tenor as jangkaWaktu, angsuran, administrasi, (totalKewajiban + biayaTransaksi) as totalPembayaran')
                             ->join('ref_vendor_mulia','payment_mulia.idVendor=ref_vendor_mulia.vendorId')
                             ->join('ref_cabang','payment_mulia.kodeOutlet=ref_cabang.kode_outlet','left')
                             ->join('ref_kelurahan kel','ref_cabang.kode_kelurahan=kel.kode_kelurahan')
                             ->join('ref_kecamatan kec','kel.kode_kecamatan=kec.kode_kecamatan')
                             ->join('ref_kabupaten kab','kec.kode_kabupaten=kab.kode_kabupaten')
                             ->join('ref_provinsi prov','kab.kode_provinsi=prov.kode_provinsi')
                             ->where('user_AIID', $user_AIID)
                             ->where('payment_mulia.id', $id)
                             ->get('payment_mulia');
            if($cek->num_rows() > 0){
                $row = $cek->row();
                $items = json_decode($row->items);

                $rItem = [];
                foreach($items as $i){
                    $i->totalHarga = $i->qty * $i->harga; 
                    $i->thumbnail = $imagePath . $this->getProductThumbnail($i->denom, $row->idVendor); 
                    $rItem[] = $i;
                }

                $row->items = $rItem;

                $_tglExpired = new DateTime($row->tglExpired);
                $_now = new DateTime();

                if($_tglExpired < $_now && $row->isPaid == '0'){
                    $row->isPaid = '2';
                }

                $row->thumbnail = $imagePath.$row->thumbnail;

                return $row;
            }

            return false;
        }
    }

    function getVendor()
    {
        $imagePath = $this->config->item('mulia_image_path');
        $cek = $this->db->where('status','1')->get('ref_vendor_mulia')->result();
        $return = [];
        foreach($cek as $c){
            $c->thumbnail = $imagePath.$c->thumbnail;
            $return[] = $c;
        }

        return $return;
    }
}
