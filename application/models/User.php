<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Model
{
    /**
     * 
     * @param type $email
     * @param type $password
     * @return type
     * @todo Jika user menggunakan email, harus verified
     */
    public function login($email, $password)
    {
        $user = NULL;

        //Jika yang digunakan login email, email harus verified sebelumnya
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // $checkLock = $this->db->select('is_locked, login_fail_count')->where('email',$email)->get('user');
            // if($checkLock->num_rows() > 0){
            //     if($checkLock->row()->is_locked == '1'){
            //         return array(
            //             'status' => 'error',
            //             'message' => 'Akun terblokir. Mohon segera menghubungi Customer Service Pegadaian Digital.',
            //             'data' => null
            //         );
            //     }
            // }
            $user = $this->db
                ->select('user_AIID, nama, no_hp, email')
                ->where('email', $email)
                ->where('password', md5($password))
                ->where('email_verified', '1')
                ->where('status', '1')
                ->get('user')
                ->row();
            if (!$user) {
                // if($checkLock->num_rows() > 0){
                //     if($checkLock->row()->login_fail_count >= 4){
                //         $this->db->where('email', $email)->update('user',[
                //             'is_locked' => '1',
                //             'login_fail_count' => '0'
                //         ]);
                //     } else {
                //         $this->db->where('email', $email)->update('user', [
                //             'login_fail_count' => $checkLock->row()->login_fail_count + 1
                //         ]);
                //     }
                // }
                return array(
                    'status' => 'error',
                    'message' => 'Email dan password tidak cocok. Pastikan email telah terverifikasi!',
                    'data' => null
                );
            } else {
                // Reset blockir count
                // $this->db->where('email', $email)->update('user', [
                //     'login_fail_count' => '0',
                //     'is_locked' => '0'
                // ]);
                return array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $user
                );
            }
        } else {
            // $checkLock = $this->db->select('is_locked, login_fail_count')->where('no_hp',$email)->get('user');
            // if($checkLock->num_rows() > 0){
            //     if($checkLock->row()->is_locked == '1'){
            //         return array(
            //             'status' => 'error',
            //             'message' => 'Akun terblokir. Mohon segera menghubungi Customer Service Pegadaian Digital.',
            //             'data' => null
            //         );
            //     }
            // }

            $user = $this->db
                ->select('user_AIID, nama, no_hp, email')
                ->where('no_hp', $email)
                ->where('status', '1')
                ->where('password', md5($password))
                ->get('user')
                ->row();
            if (!$user) {
                // if($checkLock->num_rows() > 0){
                //     if($checkLock->row()->login_fail_count >= 4){
                //         $this->db->where('no_hp', $email)->update('user',[
                //             'is_locked' => '1',
                //             'login_fail_count' => '0'
                //         ]);
                //     } else {
                //         $this->db->where('no_hp', $email)->update('user', [
                //             'login_fail_count' => $checkLock->row()->login_fail_count + 1
                //         ]);
                //     }
                // }

                return array(
                    'status' => 'error',
                    'message' => 'No HP dan password tidak cocok.',
                    'data' => null
                );
            } else {
                return array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $user
                );
            }
        }
    }

    /**
     * Update data user berdasarkan id user
     * @param integer $id
     * @param array $field
     */
    public function updateUser($id, $field = array())
    {
        return $this->db
            ->group_start()
            ->where('user_AIID', (int) $id)
            ->or_where('no_hp', (string) $id)
            ->or_where('email', (string) $id)
            ->group_end()
            ->update('user', $field);
    }

    public function updatePhone($data)
    {
        if (is_array($data)) {
            $this->db->where('user_AIID', $data['user_AIID'])->update('user', ['no_hp' => $data['noHpNew']]);
            //            error_log("DEBUG Change Number : ".$this->db->last_query()."\n", 3, "/data/log/change_number.log");
        }
    }

    public function register($nama, $hp)
    {
        $r = array();

        $userId = NULL;

        $user = $this->isPhoneExist($hp);

        //Jika no telepon ada sebelumnya, update namanya
        if ($user) {
            $this->updateUser($hp, array('nama' => $nama));
            $userId = $user->user_AIID;
        } else {
            //Buat user baru
            $this->db->insert('user', array(
                'nama' => $nama,
                'no_hp' => $hp,
                'status' => 0,
            ));
            $userId = $this->db->insert_id();
        }

        $r['userId'] = $userId;
        //Simpan code OTP
        $this->db->insert('otp', array(
            'user_AIID' => $userId,
            'tipe_otp' => 'registrasi_user',
        ));

        $r['reffId'] = $this->db->insert_id();

        return $r;
    }

    /**
     * Check email apakah terverifikasi
     * @param type $email
     * @return type
     */
    public function isEmailExist($email)
    {
        $where = array(
            'email' => $email,
            'email_verified' => 1
        );

        $cek = $this->db->where($where)->get('user')->num_rows();
        return $cek > 0 ? true : false;
    }

    /**
     * Method untuk melakukan pengecekan apakah email sudah ada dan status user
     * dari email tersebut aktif
     * @param type $email
     * @return type
     */
    public function isEmailExist2($email)
    {
        $where = array(
            'email' => $email,
            'status' => 1
        );

        $cek = $this->db->where($where)->get('user')->num_rows();
        return $cek > 0 ? true : false;
    }

    public function isOtpExist($otp, $userId)
    {
        $cek = $this->db->where(array(
            'konten_otp' => $otp,
            'tipe_otp' => 'registrasi_user',
            'user_AIID' => $userId
        ))->get('otp')->num_rows();
        return $cek > 0 ? true : false;
    }

    public function isActive($id)
    {
        $cek = $this->db
            ->group_start()
            ->where('user_AIID', (int) $id)
            ->or_where('email', (string) $id)
            ->or_where('no_hp', (string) $id)
            ->group_end()
            ->where('status', 1)
            ->get('user');

        if ($cek->num_rows() > 0) {
            return true;
        }

        return false;
    }

    public function isValidPassword($userId, $password)
    {
        $where = array(
            'user_AIID' => $userId,
            'password' => md5($password)
        );

        $cek = $this->db->where($where)->get('user')->num_rows();
        return $cek > 0 ? true : false;
    }

    public function isValidPIN($userId, $pin)
    {
        $cek = $this->db->where('user_AIID', $userId)->get('user')->row();

        if ($cek->pin == '') {
            //PIN belum terset, iizinjan tanpa pencocokan
            return true;
        } else {
            // Pengecekan Jika PIN sudah ada dan sama, maka di izinkan untuk lanjut.
            // Jika sudah ada tetapi berbeda dengan PIN yang sudah ada, maka tidak di izinkan lanjut, 
            // di haruskan menggunakan reset PIN.
            $where = array(
                'user_AIID' => $userId,
                'pin' => md5($pin)
            );

            $cek = $this->db->where($where)->get('user')->num_rows();
            return $cek > 0 ? true : false;
        }
    }

    public function isValidPIN2($userId, $pin)
    {
        $where = array(
            'user_AIID' => $userId,
            'pin' => md5($pin)
        );

        $cek = $this->db->where($where)->get('user')->num_rows();
        return $cek > 0 ? true : false;
    }

    /**
     * Get data user berdasarkan id, atau email atau no_hp. User harus
     * pasti sudah ada.
     * @param $id
     * @return mixed
     */
    public function profile($id)
    {

        $cek = $this->db
            ->select('user_AIID as id, cif, kyc_verified as isKYC, nama, nama_ibu as namaIbu, no_ktp as noKTP, email, jenis_kelamin as jenisKelamin, tempat_lahir as tempatLahir, tgl_lahir as tglLahir')
            ->select('alamat, ref_provinsi.kode_provinsi as idProvinsi, ref_kabupaten.kode_kabupaten as idKabupaten, ref_kecamatan.kode_kecamatan as idKecamatan, ref_kelurahan.kode_kelurahan as idKelurahan,  nama_provinsi as provinsi, nama_kabupaten as kabupaten, nama_kecamatan as kecamatan')
            ->select('kodepos as kodePos, no_hp as noHP, foto_url as avatar, foto_ktp_url as fotoKTP, email_verified as isEmailVerified')
            ->select('kewarganegaraan')
            ->select('jenis_identitas as jenisIdentitas')
            ->select('no_ktp as noIdentitas')
            ->select('tanggal_expired_identitas as tglExpiredIdentitas')
            ->select('no_npwp as noNPWP, foto_npwp as fotoNPWP')
            ->select('no_sid as noSid, foto_sid as fotoSid')
            ->select('status_kawin as statusKawin')
            ->select('norek, saldo')
            ->select('aktifasiTransFinansial')
            ->select('is_dukcapil_verified as isDukcapilVerified')
            ->group_start()
            ->where('user_AIID', (int) $id)
            ->or_where('email', (string) $id)
            ->or_where('no_hp', (string) $id)
            ->or_where('cif', (string) $id)
            ->group_end()
            ->join('ref_kelurahan', 'user.id_kelurahan=ref_kelurahan.kode_kelurahan', 'left')
            ->join('ref_kecamatan', 'ref_kelurahan.kode_kecamatan=ref_kecamatan.kode_kecamatan', 'left')
            ->join('ref_kabupaten', 'ref_kecamatan.kode_kabupaten=ref_kabupaten.kode_kabupaten', 'left')
            ->join('ref_provinsi', 'ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi', 'left')
            ->get('user');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }

    public function isValidEmailVerificationToken($token)
    {
        if ($token == null) return false;

        $cek = $this->db->select('user_AIID')->where('email_verification_token', $token)->get('user')->num_rows();
        return $cek > 0 ? true : false;
    }

    public function verifyEmail($token)
    {
        $where = array(
            'email_verification_token' => $token
        );

        //Get detail user berdasarkan token verifikasi        
        $user = $this->db->where($where)->get('user')->row();

        $this->db->where($where)->update('user', array(
            'email_verified' => 1,
            'email_verification_token' => ''
        ));

        return $user;
    }

    public function saveTabunganEmas(
        $userId,
        $domisili,
        $nama,
        $jenis_identitas,
        $no_identitas,
        $tanggal_expired_identitas,
        $tempat_lahir,
        $tanggal_lahir,
        $no_hp,
        $jenis_kelamin,
        $status_kawin,
        $kode_kelurahan,
        $jalan,
        $ibu_kandung,
        $kewarganegaraan,
        $kode_cabang,
        $flag,
        $foto_ktp,
        $amount,
        $trxId
    ) {

        //Update user profile berdasarkan data yang baru dikirimkan
        $updateProfile = array(
            'no_ktp' => $no_identitas,
            'nama' => $nama,
            'jenis_kelamin' => $jenis_kelamin,
            'tempat_lahir' => $tempat_lahir,
            'tgl_lahir' => $tanggal_lahir,
            'id_kelurahan' => $kode_kelurahan,
            'alamat' => $jalan,
            'kode_cabang' => $kode_cabang,
            'nama_ibu' => $ibu_kandung,
            'foto_ktp_url' => $foto_ktp,
            'status_kawin' => $status_kawin,
            'kewarganegaraan' => $kewarganegaraan,
            'jenis_identitas' => $jenis_identitas,
            'tanggal_expired_identitas' => $tanggal_expired_identitas,
        );
        $this->updateUser($userId, $updateProfile);

        $saveTabunganEmas = array(
            'domisili' => $domisili,
            'nama' => $nama,
            'jenis_identitas' => $jenis_identitas,
            'no_identitas' => $no_identitas,
            'tanggal_expired_identitas' => $tanggal_expired_identitas,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir,
            'no_hp' => $no_hp,
            'jenis_kelamin' => $jenis_kelamin,
            'status_kawin' => $status_kawin,
            'kode_kelurahan' => $kode_kelurahan,
            'jalan' => $jalan,
            'ibu_kandung' => $ibu_kandung,
            'kewarganegaraan' => $kewarganegaraan,
            'kode_cabang' => $kode_cabang,
            'foto_ktp' => $foto_ktp,
            'flag' => $flag,
            'user_AIID' => $userId,
            'status' => 0,
            'amount' => $amount,
            'id_transaksi' => $trxId
        );
        //Save tabungan emas
        $this->db->insert('tabungan_emas', $saveTabunganEmas);
    }

    function getTabunganEmas($trxId)
    {
        $cek =  $this->db->select('tabungan_emas.*, c.nama_outlet as namaOutlet')
            ->select('c.alamat as alamatOutlet')
            ->select('c.telepon as teleponOutlet')
            ->select('c.nama_kelurahan as kelurahanOutlet, c.nama_kecamatan  as kecamatanOutlet')
            ->select('c.nama_kabupaten as kabupatenOutlet, c.nama_provinsi as provinsiOutlet')
            ->join('ref_cabang_flat c', 'tabungan_emas.kode_cabang=c.kode_outlet')
            ->where('id_transaksi', $trxId)
            ->get('tabungan_emas');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }

    function activeTabunganEmas($idTabungan, $noRekening, $saldoEmas, $saldoNominal, $tanggalBuka, $cif)
    {
        $where = array(
            'id' => $idTabungan,
            'status' => 0
        );

        $update = array(
            'no_rekening' => $noRekening,
            'tanggal_buka' => $tanggalBuka,
            'saldo_emas' => $saldoEmas,
            'saldo_nominal' => $saldoNominal,
            'cif' => $cif
        );

        $this->db
            ->group_start()
            ->where('id', $idTabungan)
            ->or_where('id_transaksi', $idTabungan)
            ->group_end()
            ->where('status', '0')
            ->update('tabungan_emas', $update);
    }

    public function isPhoneExist($phone)
    {
        $select = "user_AIID`, 
                      `norek`,
                      `saldo`,
                      `jenis_identitas`,
                      `no_ktp`,
                      `tanggal_expired_identitas`,
                      `cif`,
                      `email`,
                      `no_hp`,
                      `nama`,
                      `nama_ibu`,
                      `jenis_kelamin`,
                      `tempat_lahir`,
                      `tgl_lahir`,
                      `alamat`,
                      `kewarganegaraan`,
                      `status_kawin`,
                      `kodepos`,
                      `id_kelurahan`,
                      `no_npwp`,
                      `foto_npwp`,
                      `no_sid`,
                      `foto_sid`,
                      `kode_cabang`,
                      `foto_url`,
                      `foto_ktp_url`,
                      `status`,
                      `pin`,
                      `email_verification_token`,
                      `email_verified`,
                      `kyc_verified`,
                      `token`,
                      `token_web`,
                      `fcm_token`,
                      `aktifasiTransFinansial`";
        $cek = $this->db->select($select)->where('no_hp', $phone)->get('user');

        //        error_log("DEBUG Change Number : ".$this->db->last_query()."\n", 3, "/data/log/change_number.log");

        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }


    function getFCMToken($idUser)
    {
        $cek = $this->db->select('fcm_token')->where('user_AIID', $idUser)->get('user');
        if ($cek->num_rows() > 0) {
            return $cek->row()->fcm_token;
        }
        return false;
    }

    function getUserByNoRek($noRekening)
    {

        $cek =   $this->db
            ->select('tabungan_emas.*')
            ->select('user.user_AIID, user.email, user.cif, user.fcm_token')
            ->where('no_rekening', $noRekening)
            ->join('user', 'user.user_AIID=tabungan_emas.user_AIID')
            ->get('tabungan_emas');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }

    /**
     * Get user data
     * @param type $userId
     */
    function getUser($userId)
    {
        $select = "user_AIID`, 
                      `norek`,
                      `saldo`,
                      `jenis_identitas`,
                      `no_ktp`,
                      `tanggal_expired_identitas`,
                      `cif`,
                      `email`,
                      `no_hp`,
                      `nama`,
                      `nama_ibu`,
                      `jenis_kelamin`,
                      `tempat_lahir`,
                      `tgl_lahir`,
                      `alamat`,
                      `domisili`,
                      `kewarganegaraan`,
                      `status_kawin`,
                      `kodepos`,
                      `id_kelurahan`,
                      `no_npwp`,
                      `foto_npwp`,
                      `no_sid`,
                      `foto_sid`,
                      `kode_cabang`,
                      `foto_url`,
                      `foto_ktp_url`,
                      `status`,
                      `pin`,
                      `email_verification_token`,
                      `email_verified`,
                      `kyc_verified`,
                      `token`,
                      `token_web`,
                      `fcm_token`,
                      `aktifasiTransFinansial`";
        if (is_array($userId)) {
            $cek = $this->db->select($select)->where($userId)->get('user');
            if ($cek->num_rows() > 0) {
                return $cek->row();
            }
            return false;
        } else {
            $cek = $this->db->select($select)
                ->group_start()
                ->where('user_AIID', (int) $userId)
                ->or_where('no_hp', (string) $userId)
                ->or_where('cif', (string) $userId)
                ->group_end()
                ->get('user');

            if ($cek->num_rows() > 0) {
                return $cek->row();
            }
            return FALSE;
        }
    }

    function isUserCif($idUser)
    {
        $select = "user_AIID`, 
                      `norek`,
                      `saldo`,
                      `jenis_identitas`,
                      `no_ktp`,
                      `tanggal_expired_identitas`,
                      `cif`,
                      `email`,
                      `no_hp`,
                      `nama`,
                      `nama_ibu`,
                      `jenis_kelamin`,
                      `tempat_lahir`,
                      `tgl_lahir`,
                      `alamat`,
                      `domisili`,
                      `kewarganegaraan`,
                      `status_kawin`,
                      `kodepos`,
                      `id_kelurahan`,
                      `no_npwp`,
                      `foto_npwp`,
                      `no_sid`,
                      `foto_sid`,
                      `kode_cabang`,
                      `foto_url`,
                      `foto_ktp_url`,
                      `status`,
                      `pin`,
                      `email_verification_token`,
                      `email_verified`,
                      `kyc_verified`,
                      `token`,
                      `token_web`,
                      `fcm_token`,
                      `aktifasiTransFinansial`";

        $cek = $this->db->select($select)
            ->where('cif !=', '')
            ->where('user_AIID', $idUser)->get('user');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }

    /**
     * Method untuk menambahkan transaksi favorite baru
     * @param String $tipe Tipe favorite emas|payment 
     * @param String $noRekening No rekening emas atau bank
     * @param String $namaPemilik Nama Pemilik Rekening
     * @param String $jenisTransaksi Jenis transaksi sesuai dengan tipe
     * @param String $userId Id user
     * @return String $id id favorite
     * 
     * Perubahan Parameter jadi array
     */
    function addFavorite($data)
    {
        //Jika favorite sudah ada abaikan
        $whereCheck = array(
            'no_rekening' => $data['no_rekening'],
            'user_AIID' => $data['user_AIID']
        );
        $cek = $this->db->where($whereCheck)->get('transaksi_favorite');
        if ($cek->num_rows() == 0) {
            //Add new favorite
            $this->db->insert('transaksi_favorite', $data);
            return $this->db->insert_id();
        } else {
            return $cek->row()->id;
        }
    }

    function getMpoFavorite($where){
        $res = $this->db->select('id, user_AIID, no_rekening, nama_nasabah, mpo_group, kode_layanan_mpo')->where($where)->order_by('last_update', 'desc')->get('transaksi_favorite');
        return $res->result();
    }

    /**
     * Method untuk mendapatkan daftar transaksi favorite user
     * @param type $idUser id user PDS
     * @return array array of object transaksi favorite
     */
    function getFavorite($idUser, $tipe)
    {
        if ($tipe != null) {
            return $this->db->select('id')
                ->select('nama_nasabah as namaNasabah')
                ->select('tipe')
                ->select('concat(no_rekening, \' - \', nama_nasabah) as item')
                ->select('no_rekening as noRekening')
                ->select('jenis_transaksi as jenisTransaksi')
                ->select('kodeBank, namaBank')
                ->select('last_update as lastUpdate')
                ->where('user_AIID', $idUser)
                ->where('tipe', $tipe)
                ->order_by('last_update', 'desc')
                ->get('transaksi_favorite')->result();
        }
        return $this->db->select('id')
            ->select('nama_nasabah as namaNasabah')
            ->select('tipe')
            ->select('concat(no_rekening, \' - \', nama_nasabah) as item')
            ->select('no_rekening as noRekening')
            ->select('jenis_transaksi as jenisTransaksi')
            ->select('kodeBank, namaBank')
            ->select('last_update as lastUpdate')
            ->where('user_AIID', $idUser)->order_by('last_update', 'desc')
            ->get('transaksi_favorite')->result();
    }

    function deleteFavorite($id, $idUser)
    {
        $where = array(
            'user_AIID' => $idUser,
            'id' => $id
        );

        $this->db->where($where)->delete('transaksi_favorite');
    }

    function getUserCif($userId)
    {
        $cek = $this->db->select('cif')->where('user_AIID', $userId)->get('user');
        if ($cek->num_rows() > 0) {
            return $cek->row()->cif;
        }
        return "";
    }

    /**
     * Check mobile token
     * @param String $accessToken
     * @return boolean
     */
    function isValidAccessToken($accessToken)
    {
        $cek = $this->db->select('user_AIID')->where('token', $accessToken)->get('user');
        if ($cek->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Check access token web
     * @param $accessToken
     * @return bool
     */
    function isValidWebAccessToken($accessToken)
    {
        $cek = $this->db->select('user_AIID')->where('token_web', $accessToken)->get('user');
        if ($cek->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Update saldo wallet user
     * @param $user_AIID
     * @param $saldo
     */
    function updateSaldo($user_AIID, $saldo)
    {
        $this->db->where('user_AIID', $user_AIID)->update('user', array('saldo' => $saldo));
    }

    function isValidPinMD5($pin)
    {
        $cek = $this->db->select('user_AIID')->where('pin', $pin)->get('user');
        if ($cek->num_rows() > 0) {
            return true;
        }
        return false;
    }

    /**
     * Update CIF user
     * @param $phoneno
     * @return bool
     */
    function updateCif($cif)
    {
        $data = array(
            'cif' => '',
            'pin' => '',
            'aktifasiTransFinansial' => '0',
            'last_update_unlink_cif' => date('Y-m-d H:i:s'),
            'last_update_pin' => date('Y-m-d H:i:s')
        );

        try {
            $this->db->where('cif', $cif)->update('user', $data);
            error_log("DEBUG Update DB sukses -> " . $this->db->last_query() . "\n", 3, "/data/log/unlink_cif.log");
            //die("STOP");
        } catch (Exception $e) {
            error_log("DEBUG Update DB : " . $e->getMessage() . "\n", 3, "/data/log/unlink_cif.log");
        }

        //$this->db->where('no_hp', $phoneno)->update($data);
        //if ($this->db->update('user', $data)) {
        //    return true;
        //} else {
        //    return false;
        //}
        //$this->db->update($data);
    }

    /**
     * Mendapatkan CIF user
     * @param $id
     * @return null
     */
    function getCif($id)
    {
        $cek = $this->db->select('cif')->where('user_AIID', $id)->get('user');
        if ($cek->num_rows() > 0) {
            return $cek->row()->cif;
        }
        return null;
    }


    /**
     * Mendapatkan no hp berdarkan user id
     * @param type $id
     */
    function getNoHp($id)
    {
        $cek = $this->db->select('no_hp')->where('user_AIID', $id)->get('user');
        if ($cek->num_rows() > 0) {
            return $cek->row()->no_hp;
        }
        return '';
    }

    /**
     * Mendapatkan alamat lengkap user
     * @param $id
     * @return null|string
     */
    function getFullAddress($id)
    {
        $cek = $this->db->select('user.alamat as alamat')
            ->select('kel.nama_kelurahan as kelurahan, kec.nama_kecamatan  as kecamatan')
            ->select('kab.nama_kabupaten as kabupaten, prov.nama_provinsi as provinsi')
            ->from('user')
            ->join('ref_kelurahan kel', 'user.id_kelurahan=kel.kode_kelurahan', 'left')
            ->join('ref_kecamatan kec', 'kel.kode_kecamatan=kec.kode_kecamatan', 'left')
            ->join('ref_kabupaten kab', 'kec.kode_kabupaten=kab.kode_kabupaten', 'left')
            ->join('ref_provinsi prov', 'kab.kode_provinsi=prov.kode_provinsi', 'left')
            ->where('user_AIID', $id)
            ->get();
        if ($cek->num_rows() > 0) {
            $r = $cek->row();
            return $r->alamat . ', ' . $r->kelurahan . ', ' . $r->kecamatan . ', ' . $r->kabupaten . ', ' . $r->provinsi;;
        }
        return null;
    }

    /**
     * Check kelengkapan data identitas user
     * @param $id user id
     * @param bool $withNpwp optional jika ingin mengecek npwp
     * @param bool $withSid optional jika ingin mengecek sid
     * @return bool
     */
    function isIdentityOK($id, $withNpwp = false, $withSid = false)
    {
        $cek = $this->db->where('user_AIID', $id)->get('user');
        if ($cek->num_rows() > 0) {

            $row = $cek->row();
            $base = false;
            $npwp = false;
            $sid = false;

            if (
                $row->nama != null &&
                $row->email != null &&
                $row->no_ktp != null &&
                $row->no_hp != null &&
                $row->nama_ibu != null &&
                $row->jenis_kelamin != null &&
                $row->tempat_lahir != null &&
                $row->tgl_lahir != null &&
                $row->alamat != null &&
                $row->kewarganegaraan != null &&
                $row->status_kawin != null &&
                $row->id_kelurahan != null
            ) {
                $base = true;
            }

            if ($row->no_npwp != null && $row->foto_npwp != null) {
                $npwp = true;
            }

            if ($row->no_sid != null && $row->foto_sid != null) {
                $sid = true;
            }

            if ($withNpwp && $withSid) {
                return $base && $npwp && $sid;
            } else if ($withNpwp) {
                return $base && $npwp;
            } else {
                return $base;
            }
        }
        return false;
    }

    function isEmailChangeable($email, $userId)
    {
        $check = $this->db->where([
            'email' => $email,
            'user_AIID !=' => $userId
        ])->get('user');

        if ($check->num_rows() > 0) {
            return false;
        }

        return true;
    }


    function isResetPasswordAvailable($email)
    {
        $check = $this->db->where([
            'email' => $email,
            'next_password_reset >' => date('Y-m-d H:i:s')
        ])->get('user');

        if ($check->num_rows() > 0) {
            return true;
        }

        return false;
    }

    function registerPin($data)
    {
        $where = array(
            'cif' => $data['cif'],
            'aktifasiTransFinansial' => '1'
        );
        $cek = $this->db->where($where)->get('user');
        if ($cek->row()) {
            $this->db->where($where)->update('user', array('pin' => $data['pin']));
            return true;
        } else {
            return false;
        }
    }

    function cekPin($data)
    {
        $where = array(
            'cif' => $data['cif'],
            'pin' => $data['pin'],
            'aktifasiTransFinansial' => '1'
        );
        $cek = $this->db->where($where)->get('user');
        if ($cek->row()) {
            return true;
        } else {
            return false;
        }
    }

    function aktifasiTransFinansial($data)
    {
        $where = array(
            'cif' => $data['cif']
        );
        $cek = $this->db->where($where)->get('user');
        if ($cek->row()) {
            $this->db->where($where)->update('user', array('aktifasiTransFinansial' => '1', 'tanggal_aktifasi_finansial' => date('Y-m-d H:i:s')));
            return true;
        } else {
            return false;
        }
    }
}
