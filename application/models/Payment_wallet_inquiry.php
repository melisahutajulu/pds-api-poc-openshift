<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_wallet_inquiry extends CI_Model
{
    private $table;
    private $date;
    
    public function __construct() {
      parent::__construct();
      
      $this->table = 'payment_wallet_inquiry';
      $this->date = date('Y-m-d H:i:s');
    }
    
    public function insert($data) {
      $data['date_insert'] = $this->date;
      $this->db->insert($this->table, $data);
      return $this->db->insert_id();
    }
    
    public function row($where) {
      return $this->db->where($where)->get($this->table)->row();
    }
    
    public function checkInquiryId($id, $user_AIID) {
      $where = array('id'=>$id, 'user_AIID'=>$user_AIID, 'status'=>'pending');
      $check = $this->db->where($where)->get($this->table)->num_rows();
      if($check == 0) {
        return FALSE;
      } else {
        return TRUE;
      }
    }
    
    public function payment_success($id) {
      $data = array(
        'status'=>'success'
      );
      $this->db->where('id', $id)->update($this->table, $data);
    }
        
}
