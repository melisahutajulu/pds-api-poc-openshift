<?php

class BankModel extends CI_Model
{
    /*
     * Global
     */

    function getBankList($kodeBank = NULL)
    {
        $imagePath = $this->config->item('bank_image_path');

        if ($kodeBank != NULL) {
            $cek = $this->db->where('kode', $kodeBank)->get('ref_bank');
            if ($cek->num_rows() > 0) {
                $row = $cek->row();
                $row->thumbnail = $imagePath . $row->thumbnail;
                log_message('debug', 'ROW' . json_encode($row));
                return $row;
            }
        } else {
            $cek = $this->db->get('ref_bank')->result();
            $return = [];
            foreach ($cek as $c) {
                $c->thumbnail = $imagePath . $c->thumbnail;
                $return[] = $c;
            }
            return $return;
        }
    }

    /*
     * User Bank
     */

    function add($idUser, $no, $nama, $idBank)
    {
        $data = array(
            'nomor_rekening' => $no,
            'nama_pemilik' => $nama,
            'user_AAID' => $idUser,
            'id_bank' => $idBank
        );

        $this->db->insert('rekening_bank', $data);
        return $this->db->insert_id();
    }

    function update($id, $no, $nama, $idBank)
    {
        $updateData = array(
            'nomor_rekening' => $no,
            'nama_pemilik' => $nama,
            'id_bank' => $idBank
        );

        $this->db->where('rekening_bank_AIID', $id)->update('rekening_bank', $updateData);
    }

    function getBank($idUser = null)
    {
        $imagePath = $this->config->item('bank_image_path');
        $cek = $this->db->select('rekening_bank_AIID as id')
            ->select('nomor_rekening as noRekening')
            ->select('ref_bank.nama as namaBank')
            ->select('nama_pemilik as namaPemilik')
            ->select('kode_bank as kodeBank')
            ->select('thumbnail')
            ->where('user_AAID', $idUser)
            ->join('ref_bank', 'rekening_bank.kode_bank=ref_bank.kode')
            ->get('rekening_bank')
            ->result();
        $return = [];
        foreach ($cek as $c) {
            $c->thumbnail = $imagePath . $c->thumbnail;
            $return[] = $c;
        }
        return $return;
    }

    function isBankExist($idUser, $kode, $no_rekening)
    {
        $where = array(
            'user_AAID' => $idUser,
            'kode_bank' => $kode,
            'nomor_rekening' => $no_rekening
        );

        $cek = $this->db->where($where)
            ->get('rekening_bank')
            ->num_rows();
        if ($cek > 0) {
            return true;
        }

        return false;
    }

    function isUserBank($idUser, $id)
    {
        if ($id == null) {
            return false;
        }

        $where = array(
            'rekening_bank_AIID' => $id,
            'user_AAID' => $idUser
        );

        $cek = $this->db->where($where)->get('rekening_bank')->num_rows();

        if ($cek > 0) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        return $this->db->where('rekening_bank_AIID', $id)->delete('rekening_bank');
    }

    function getNamaBank($kode)
    {
        $cek = $this->db->where('kode', $kode)->get('ref_bank');
        if ($cek->num_rows() > 0) {
            return $cek->row()->nama;
        }

        return "";
    }

    function getById($id)
    {
        $get = $this->db->where('rekening_bank_AIID', $id)->get('rekening_bank');
        if ($get->num_rows() > 0) {
            return $get->row();
        }

        return false;
    }

    function addRekeningBank($data) {
        $this->db->insert('rekening_bank', $data);
        return $this->db->insert_id();
    }

    function getRekeningBank($id) {
        $imagePath = $this->config->item('bank_image_path');
                
        return $this->db->select('rekening_bank_AIID as id')
            ->select('nomor_rekening as noRekening')
            ->select('ref_bank.nama as namaBank')
            ->select('nama_pemilik as namaPemilik')
            ->select('kode_bank as kodeBank')
            ->select('CONCAT("'.$imagePath.'", thumbnail) as thumbnail')
            ->where('rekening_bank_AIID', $id)
            ->join('ref_bank', 'rekening_bank.kode_bank=ref_bank.kode')
            ->get('rekening_bank')
            ->row_array();
    }
}
