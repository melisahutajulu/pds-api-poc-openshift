<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmasModel extends CI_Model{
    
    function updateRekening($noRekening, $data)            
    {
        $this->db->where('no_rekening', $noRekening)->update('tabungan_emas', $data);
    }
    
    function addHistoryIn($noRekening, $jumlah, $saldoAkhir, $status, $jenisTransaksi)
    {
        $data = array(
            'no_rekening' => $noRekening,
            'jumlah_emas_masuk' =>$jumlah,
            'saldo_akhir' => $saldoAkhir,
            'status' => $status,
            'jenis_transaksi' => $jenisTransaksi
        );
        
        $this->db->insert('history_transaksi_emas', $data);
    }
    
    
    /**
     * Method untuk melakukan pengecekan apakah rekening emas ada
     * @param type $noRekening
     * @return boolean
     */
    function isRekeningExist($noRekening)
    {
        $cek = $this->db->select('no_rekening')->where('no_rekening', $noRekening)->get('tabungan_emas');
        if($cek->num_rows() > 0){
            return TRUE;
        }
        
        return FALSE;
    }
    
    function getRekeningEmas($idUser)
    {
        return $this->db->select('nama, no_rekening as noRekening, kode_cabang as kodeCabang')
                        ->select('cif, saldo_emas as saldo, tanggal_buka as tanggalBuka, is_kyc as kycStatus')
                        ->where('user_AIID', $idUser)
                        ->where('no_rekening !=','')
                        ->get('tabungan_emas')->result();
    }
    
    function getRekeningEmasByPayment($trxId){
        $cek = $this->db->where('payment.id_transaksi', $trxId)
                        ->join('tabungan_emas','payment.no_rekening=tabungan_emas.no_rekening')
                        ->get('payment');
        
        if($cek->num_rows() > 0){
            return $cek->row();
        }
        
        return false;
    }
    
    function getUserByTrxId($trxId){
        $cek = $this->db->select('user.*, payment.no_rekening')
                        ->join('user','user.user_AIID=payment.user_AIID')
                        ->where('id_transaksi',$trxId)
                        ->get('payment');
        if($cek->num_rows() > 0){
            return $cek->row();
        }
        
        return FALSE;
    }
    
    function addHistoryOut($noRekening, $jumlah, $saldoAkhir )
    {
        
    }
    
    function getHistory($noRekening)
    {
        $sql = "SELECT 
                history_transaksi_emas_AIID,
                no_rekening as noRekening,
                jenis_transaksi as jenisTransaksi,
                jumlah_emas_masuk as emasMasuk,
                jumlah_emas_keluar as emasKeluar,
                saldo_akhir as saldoAkhir,
                waktu_update as tanggal
                FROM history_transaksi_emas
                WHERE MONTH(waktu_update) = MONTH(CURRENT_DATE())
                AND YEAR(waktu_update) = YEAR(CURRENT_DATE()) 
                AND no_rekening= '".$noRekening."' ORDER BY waktu_update DESC";
        
        return $this->db->query($sql)->result();
        
    }
    
    
    function add($idUser,  $ibuKandung, $tglLahir, $data)
    {
        $cek = $this->db->where('no_rekening', $data->norek)->get('tabungan_emas');
        if($cek->num_rows() > 0){
            $save = array(
                'cif' => $data->cif,
                'kode_cabang' => $data->kodeCabang,
                'nama' => $data->namaNasabah,
                'user_AIID' => $idUser,
                'saldo_emas' => $data->saldoEmas,
                'no_rekening' => $data->norek,
                'tanggal_buka' => $data->tglBuka,
                'ibu_kandung' => $ibuKandung 
            );
            $this->db->where('no_rekening', $data->norek)->update('tabungan_emas', $save);
        }else{
            $save = array(
                'cif' => $data->cif,
                'kode_cabang' => $data->kodeCabang,
                'nama' => $data->namaNasabah,
                'user_AIID' => $idUser,
                'saldo_emas' => $data->saldoEmas,
                'no_rekening' => $data->norek,
                'tanggal_buka' => $data->tglBuka,
                'ibu_kandung' => $ibuKandung 
            );
            $this->db->insert('tabungan_emas', $save);
        }        
    }
    
    function getUnpaidPayment($idUser)
    {
        $sql = "SELECT * FROM payment WHERE user_AIID = ".$idUser." "
                . "AND NOW() < tanggal_expired AND status = 0 ORDER BY id DESC";
        $cek = $this->db->query($sql);
        return $cek->row();
    }
    
    function updateRekeningByTrxId($trxId, $data){
        
        $this->db->where('id_transaksi', $trxId)->update('tabungan_emas', $data);
    }

    function saveGTE($data){
        $this->db->insert('gte', $data);
    }

    function getGTE($trxId){
        $cek = $this->db->select('user.user_AIID, user.email, user.no_hp, user.fcm_token, gte.*')
                        ->from('gte')
                        ->join('user','gte.user_AIID=user.user_AIID')    
                        ->where('reffSwitching', $trxId)
                        ->get();
        if($cek->num_rows() > 0){
            return $cek->row();
        }

        return false;
    }

    function updateGTE($trxId, $data){
        $this->db->where('reffSwitching', $trxId)->update('gte', $data);
    }

    function getRasioTaksiran($hari){
        $cek = $this->db->where('hari', $hari)->get('ref_rasiotakaran');
        if($cek->num_rows() > 0){
            return $cek->row()->rasio;
        }
        return false;
    }

    function getGolonganKCA($up){
        //Mendapatkan pembulatan ke persepulu
        $golongan = $this->db->query('SELECT * FROM ref_golonganupkca WHERE '.$up.' BETWEEN min AND max');


        if($golongan->num_rows() > 0){
            return $golongan->row();
        }                        

        return false;

    }

    function isUserRekening($cif, $noRekening)
    {
        $check = $this->db->where([
            'cif' => $cif,
            'norek' => $noRekening
        ])->get('cache_tabemas');

        if($check->num_rows() > 0){
            return true;
        }

        return false;
    }

    
}


