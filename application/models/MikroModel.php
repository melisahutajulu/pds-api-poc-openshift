<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MikroModel extends CI_Model
{
    
    function getMikroList($userId, $offset)
    {
        $cek = $this->db->select('mikro.id')
                        ->select('no_pengajuan as noPengajuan')                        
                        ->select('kebutuhan_modal as kebutuhanModal')
                        ->select('nama_usaha as namaUsaha')                        
                        ->select('tenor')
                        ->select('status, tanggal_booking as waktu_update')
                        ->where('mikro.user_AIID', $userId)
                        ->join('ref_kelurahan', 'mikro.kode_kelurahan=ref_kelurahan.kode_kelurahan','left')
                        ->join('ref_kecamatan', 'ref_kelurahan.kode_kecamatan=ref_kecamatan.kode_kecamatan','left')
                        ->join('ref_kabupaten','ref_kecamatan.kode_kabupaten=ref_kabupaten.kode_kabupaten','left')
                        ->join('ref_provinsi','ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi','left')
                        ->order_by('id','desc')
                        ->limit(20,$offset)
                        
                        ->get('mikro');
        return $cek->result();
    }
    
   
    function countMikro($userId){
        return $this->db->where('user_AIID', $userId)->get('mikro')->num_rows();
    }
    
    function savePengajuan($data)
    {
        $this->db->insert('mikro', $data);        
        $id =  $this->db->insert_id();
        
        $noPengajuan = sprintf('%06d', $id);
        
        //Update no pengajuan
        $this->db->where('id', $id)->update('mikro', array('no_pengajuan'=>$noPengajuan));
        
        return $id;
    }
    
    function getDetail($idUser, $noPengajuan){
        if($noPengajuan == null){
            return false; 
        };
        
        $cek = $this->db
                    ->select('no_pengajuan as noPengajuan')
                    ->select('kebutuhan_modal as kebutuhanModal')
                    ->select('jenis_usaha as jenisUsaha')
                    ->select('omset, laba, deskripsi as deskripsiUsaha')
                    ->select('nama_usaha as namaUsaha')
                    ->select('bidang_usaha as bidangUsaha')
                    ->select('lama_usaha as lamaUsaha')
                    ->select('nama_provinsi as provinsi')
                    ->select('nama_kabupaten as kabupaten')
                    ->select('nama_kecamatan as kecamatan')
                    ->select('nama_kelurahan as kelurahan')
                    ->select('mikro.alamat as alamat')
                    ->select('mikro.latitude, mikro.longitude, tenor, jenis_kendaraan as jenisJaminanKendaraan, ref_kendaraan.merek as merkJaminanKendaraan, tipe as tipeJaminanKendaraan')
                    ->select('tahun_pembuatan as tahunJaminanKendaraan ')
                    ->select('kepemilikan_kendaraan as kepemilikanJaminanKendaraan')                    
                    ->select('foto_kendaraan as fotoKendaraanJaminan')                    
                    ->select('foto_usaha as fotoUsaha')
                    ->select('mikro.status as status')
                    ->select('nama_outlet as namaOutlet, ref_cabang.alamat as alamatOutlet, telepon as teleponOutlet,')
                    ->where('no_pengajuan', $noPengajuan)
                    ->where('user_AIID', $idUser)
                    ->join('ref_kelurahan', 'mikro.kode_kelurahan=ref_kelurahan.kode_kelurahan','left')
                    ->join('ref_kecamatan', 'ref_kelurahan.kode_kecamatan=ref_kecamatan.kode_kecamatan','left')
                    ->join('ref_kabupaten','ref_kecamatan.kode_kabupaten=ref_kabupaten.kode_kabupaten','left')
                    ->join('ref_provinsi','ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi','left')
                    ->join('ref_cabang', 'ref_cabang.kode_outlet=mikro.kode_outlet', 'left')
                    ->join('ref_kendaraan', 'ref_kendaraan.id=mikro.merk')
                    ->get('mikro');
        if($cek->num_rows() > 0){
            return $cek->row();
        }
        
        return false;
    }
    
    function saveRekening(
                    $userId,
                    $nama,
                    $jenis_identitas,
                    $no_identitas,
                    $tanggal_expired_identitas,
                    $tempat_lahir,
                    $tanggal_lahir,
                    $no_hp,
                    $jenis_kelamin,
                    $status_kawin,
                    $kode_kelurahan,
                    $jalan,
                    $ibu_kandung,
                    $foto_ktp                    
            )
    {
        $flipDateLahir = date('Y-m-d', strtotime($tanggal_lahir));

        $updateProfile = array(
            'no_ktp' => $no_identitas,
            'nama' => $nama,
            'jenis_kelamin' => $jenis_kelamin,
            'tempat_lahir' => $tempat_lahir,
            'tgl_lahir' => $flipDateLahir,
            'id_kelurahan' => $kode_kelurahan,
            'alamat' => $jalan,
            'nama_ibu' => $ibu_kandung,
            'foto_ktp_url' => $foto_ktp,
            'status_kawin' => $status_kawin,
            'jenis_identitas' => $jenis_identitas,
            'tanggal_expired_identitas' => $tanggal_expired_identitas,            
        );        
        $this->updateUser($userId, $updateProfile);
        
        $saveMikro = array(
            'nama' => $nama,
            'jenis_identitas' => $jenis_identitas,
            'no_identitas' => $no_identitas,
            'tanggal_expired_identitas' => $tanggal_expired_identitas,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir,
            'no_hp' => $no_hp,
            'jenis_kelamin' => $jenis_kelamin,
            'status_kawin' => $status_kawin,
            'kode_kelurahan' => $kode_kelurahan,
            'jalan' => $jalan,
            'ibu_kandung' => $ibu_kandung,
            'foto_ktp' => $foto_ktp,
            'user_AIID' => $userId            
        );
        //Save tabungan emas
        $this->db->insert('rekening_mikro', $saveMikro);
        
        return $this->db->insert_id();
    }
    
    /**
     * Method untuk mendapatkan data Mikro berdasarkan no_pengajuan (kode booking)
     * @param String $kodeBooking
     * @return boolea return false jika data tidak ada dan return object jika data ada
     */
    function getMikroByKodeBooking($kodeBooking)
    {
        $cek = $this->db->select('user.user_AIID, user.nama, user.email, user.fcm_token, mikro.*')
                        ->join('user','user.user_AIID=mikro.user_AIID')
                        ->where('no_pengajuan', $kodeBooking)
                        ->get('mikro');
        if($cek->num_rows() > 0){
            return $cek->row();
        }
        
        return FALSE;
    }
    
    /**
     * Method untuk mengupdate pengajuan mikro setelah mendapatkan notifikasi dari Core
     * @param String $kodeBooking
     * @param String $status 0 atau 1
     * @param object $mikroData mikro data
     */
    function mikroDiproses($kodeBooking, $status, $data)
    {
        $updateData = NULL;
        
        if($status == '1'){
            $updateData = array(
                'no_kredit' => $data->noKredit,
                'tanggal_kredit' => $data->tglKredit,
                'tanggal_jatuh_tempo' => $data->tglJatuhTempo,
                'tanggal_lelang' => $data->tglLelang,
                'up' => $data->up,
                'bunga' => $data->bunga,
                'sewa_modal' => $data->sewaModal,
                'sewa_modal_maksimal' => $data->sewaModalMaksimal,
                'taksiran' => $data->taksiran,
                'status' => '2'            
            );
        }else{
            $updateData = array(
                 'status' => '0'
            );
        }
        
        $this->db->where('no_pengajuan', $kodeBooking)->update('mikro', $updateData);
    }   
    
    function getMikroByKodeBooking2($noPengajuan){
        if($noPengajuan == null){
            return false; 
        };
        
        $selectId = "nama as namaNasabah, jenis_identitas as tipeIdentitas, no_ktp as noIdentitas, tanggal_expired_identitas as tglExpiredId, "
                . "cif, nama_ibu as ibuKandung, tempat_lahir as tempatLahir, tgl_lahir as tglLahir, "
                . "jenis_kelamin as jenisKelamin, no_hp as noHp, kewarganegaraan, status_kawin as statusKawin,"
                . "user.alamat as alamatNasabah, user.id_kelurahan as kodeKelurahanNasabah";
        
        $cek = $this->db
                    ->select($selectId)
                    ->select('no_pengajuan as kodeBooking')
                    ->select('tanggal_booking as tglBooking')
                    ->select('kebutuhan_modal as kebutuhanModal')
                    ->select('jenis_usaha as jenisUsaha')
                    ->select('omset, laba, deskripsi as deskripsiUsaha')
                    ->select('nama_usaha as namaUsaha')
                    ->select('bidang_usaha as bidangUsaha')
                    ->select('lama_usaha as lamaUsaha')
                    ->select('nama_provinsi as provinsi')
                    ->select('nama_kabupaten as kabupaten')
                    ->select('nama_kecamatan as kecamatan')
                    ->select('nama_kelurahan as kelurahan')
                    ->select('mikro.kode_kelurahan as kodeKelurahanUsaha')
                    ->select('mikro.alamat as  alamatUsaha')
                    ->select('latitude, longitude, tenor, "KN" as rubrik, "jenisJaminan" as jenisJaminan, jenis_kendaraan as jenisJaminanKendaraan, merek as merkJaminanKendaraan, tipe as tipeJaminanKendaraan')
                    ->select('tahun_pembuatan as tahunJaminanKendaraan ')
                    ->select('kepemilikan_kendaraan as kepemilikanJaminanKendaraan')                    
                    ->select('nobpkb_kendaraan as noBpkb')                    
                    ->select('namabpkb_kendaraan as namaBpkb')                    
                    ->select('foto_kendaraan as fotoKendaraanJaminan')
                    ->select('deskripsi_kendaraan as deskripsiKendaraan')
                    ->select('harga_taksiran_kendaraan as taksiranKendaraan')
                    ->select('foto_usaha as fotoUsaha')
                    ->select('kode_outlet as kodeOutlet')
                    ->where('no_pengajuan', $noPengajuan)
                    ->join('ref_kendaraan', 'mikro.merk=ref_kendaraan.id','left')
                    ->join('ref_kelurahan', 'mikro.kode_kelurahan=ref_kelurahan.kode_kelurahan','left')
                    ->join('ref_kecamatan', 'ref_kelurahan.kode_kecamatan=ref_kecamatan.kode_kecamatan','left')
                    ->join('ref_kabupaten','ref_kecamatan.kode_kabupaten=ref_kabupaten.kode_kabupaten','left')
                    ->join('ref_provinsi','ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi','left')
                    ->join('user','user.user_AIID=mikro.user_AIID')
                    ->get('mikro');
        if($cek->num_rows() > 0){
            $r = $cek->row();
            $baseUrl = $this->config->item('base_url');
            
            $resFotoKendaraan = array();
            $resFotoUsaha = array();
            
            $arrKendaraan = json_decode($r->fotoKendaraanJaminan);
            $arrUsaha = json_decode($r->fotoUsaha);
            
            foreach($arrKendaraan as $k){
                $resFotoKendaraan[] = $baseUrl.'uploaded/user/mikro/'.$k->nama_foto;
            }
            
            foreach($arrUsaha as $u){
                $resFotoUsaha[] = $baseUrl.'uploaded/user/mikro/'.$u->nama_foto;
            }
            
            $r->fotoKendaraanJaminan = $resFotoKendaraan;
            $r->fotoUsaha = $resFotoUsaha;
            
            if($r->jenisJaminanKendaraan == 'Motor'){
                $r->jenisJaminan = 'KNSM';
            }else if($r->jenisJaminanKendaraan=='Mobil'){
                $r->jenisJaminan = 'KNML';
            }
           
            return $r;
        }
        
        return false;
    }
    
    /**
     * Get data kendaraan berdasarkan id kendaraan
     * @param String $id
     * @return String nama kendaraan
     */
    function getNamaKendaraan($id)
    {
        $cek = $this->db->where('id',$id)->get('ref_kendaraan');
        if($cek->num_rows() > 0){
            return $cek->row()->merek;
        }
        return FALSE;
    }
    
    function getOutlet($kodeOutlet)
    {
        $cek = $this->db->select('nama_outlet as namaOutlet')
                        ->select('alamat')
                        ->select('telepon')
                        ->where('kode_outlet', $kodeOutlet)
                        ->get('ref_cabang');
        if($cek->num_rows() > 0){
            return $cek->row();
        }
        
        return FALSE;
    }
    
    function savePayment($data){
        $this->db->insert('payment_mikro', $data);
    }
    
    function getPaymentByTrxId($trxId)
    {
        $cek = $this->db->select('payment_mikro.*, user.email, user.fcm_token, user.nama, user.no_hp')
                        ->join('user','user.user_AIID=payment_mikro.user_AIID')
                        ->where('reffSwitching', $trxId)->get('payment_mikro');
        if($cek->num_rows() > 0)
        {
            return $cek->row();
        }
        
        return FALSE;
    }
    
    /**
     * Update mikro paymet
     * @param String $trxId reffSwitching/id transaksi
     * @param Array $data 
     */
    function updatePayment($trxId, $data)
    {
        $this->db->where('reffSwitching',$trxId)->update('payment_mikro', $data);
    }
        
    
}