<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotificationModel extends CI_Model
{
    //Set const sesuai dengan type enum pada database
    const TYPE_PROMO = "promo";
    const TYPE_ACCOUNT = "account";
    const TYPE_PROFILE = "profile";
    const TYPE_EMAS = "emas";
    const TYPE_GADAI = "gadai";
    const TYPE_MICRO = "micro";
    const TYPE_MPO = "mpo";
    const TYPE_GCASH = "gcash";
    const TYPE_EKYC = "e-kyc";

    const CONTENT_TYPE_HTML = "html";
    const CONTENT_TYPE_TEXT = "text";

    /**
     * Mendapatkan data notifikasi user
     * @param integer $userId
     * @param integer $offset
     * @param string $order_by
     * @param string $order
     * @return array
     */
    function getNotification($userId, $offset, $search = null, $filter = null)
    {
        $select = "notifikasi_AIID as id, type, jenisTransaksi, content_type as contentType, judul, tagline, readed, last_update as lastUpdate";

        $where = array(
            'user_AIID' => $userId,
            'status' => '1'
        );

        $allowedFilter = ['promo','account','macro','gadai','emas','mpo','admin'];
        if($filter != null && in_array($filter, $allowedFilter)){
            $where['type'] = $filter;
        }

        if($search != null){
            return $this->db->select($select)
                            ->where($where)
                            ->group_start()
                                ->like('judul', $search)
                                ->or_like('isi', $search)
                            ->group_end()
                            ->order_by('id','desc')
                            ->limit(20, $offset)
                            ->get('notifikasi')->result();
        }

        return $this->db->select($select)
                        ->where($where)->limit(20, $offset)
                        ->order_by('id','desc')
                        ->get('notifikasi')->result();
    }

    /**
     * 
     * @param type $idUser
     * @param type $type
     * @param type $content_type
     * @param type $title
     * @param type $tagline
     * @param type $body
     * @param type $bodymin
     * @param type $jenisTransaksi
     * @return type
     */
    function add($idUser, $type, $content_type, $title, $tagline, $body, $bodymin = NULL, $jenisTransaksi = 'IN')
    {
        $this->db->insert('notifikasi',array(
            'user_AIID' => $idUser,
            'file_jatuh_tempo_id' => '',
            'type' => $type,
            'content_type' => $content_type,
            'jenisTransaksi' => $jenisTransaksi,
            'judul' => $title,
            'tagline' => $tagline,
            'isi' => $body,
            'isi_minimal' => $bodymin,
            'payload' => '',
            'readed' => '',
        ));
        
        return $this->db->insert_id();
    }

    function addAdminNotificaion( 
            $idUser, 
            $type, 
            $content_type, 
            $title, 
            $tagline, 
            $body, 
            $bodymin = NULL, 
            $jenisTransaksi = 'IN', 
            $kodeOutlet, 
            $kodeBooking)
    {
        $this->db->insert('admin_notifikasi',array(
            'user_AIID' => $idUser,
            'type' => $type,
            'content_type' => $content_type,
            'judul' => $title,
            'tagline' => $tagline ." Kode Booking ". $kodeBooking,
            'isi' => $body,
            'isi_minimal' => $bodymin,
            'jenisTransaksi' => $jenisTransaksi,
            'kode_outlet' => $kodeOutlet,
            'kode_booking' => $kodeBooking
        ));

        return $this->db->insert_id();
    }
    
    function readed($id, $idUser)
    {
        $where = array(
            'notifikasi_AIID' => $id,
            'user_AIID' => $idUser
        );
        
        $this->db->where($where)->update('notifikasi', array('readed'=>'1'));
    }
    
    function getAttachment($id)
    {
        return $this->db->where('notifikasi_AIID', $id)->get('notif_attachment')->result();
    }
    
    function countNotif($userId)
    {
        $where = array(
            'user_AIID' => $userId,
            'status' => '1'
        );
        return $this->db->where($where)->get('notifikasi')->num_rows();
    }
    
    function countUnreadNotif($userId)
    {
        $where = array(
            'user_AIID' => $userId,
            'status' => '1',
            'readed' => '0'
        );
        return $this->db->where($where)->get('notifikasi')->num_rows();
    }
    
    function delete($id, $userId)
    {
        $where = array(
            'notifikasi_AIID' => $id,
            'user_AIID' => $userId
        );
        return $this->db->where($where)->update('notifikasi', array(
           'status' => 0 
        ));
    }
    
    function isAdmin($token)
    {
        if($token == null){
            return false;
        }else{
            $cek = $this->db->where('access_token', $token)->get('admin')->num_rows();
            if($cek > 0){
                return true;
            }
            return false;
        }
    }
    
    function getDetail($userId, $notifId){
        
        $select = "notifikasi_AIID as id, type, content_type as contentType, judul, tagline, isi, isi_minimal as isiMinimal, last_update as lastUpdate";

        $cek = $this->db
                ->select($select)
                ->where(array(
                    'user_AIID' => $userId,
                    'notifikasi_AIID' => $notifId
                ))->get('notifikasi');
        
        if($cek->num_rows() > 0){
            return $cek->row();
        }else{
            return false;            
        }
    }
    
    function updateKYC($cif, $status){
        
        $where = array(
            'cif' => $cif            
        );
        
        //tabungan emas
        $this->db->where($where)->update('tabungan_emas', array('is_kyc'=>$status));
        
        //gadai
        $this->db->where($where)->update('rekening_gadai', array('is_kyc'=>$status));
        
        //mikro
        $this->db->where($where)->update('rekening_mikro', array('is_kyc'=>$status));
        
        //update user
        $this->db->where($where)->update('user', array('kyc_verified'=>$status));
    }
}