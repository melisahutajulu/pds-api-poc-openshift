<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GteModel extends CI_Model {

    function checkPromo($jenisPromo) {
        $where = 'expired >= NOW() AND jenisPromo = "'.$jenisPromo.'"';
        return $this->db->where($where)
                        ->get('ref_gte_promo')
                        ->result();
    }

}
