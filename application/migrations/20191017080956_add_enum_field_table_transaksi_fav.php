<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Enum_Field_Table_Transaksi_Fav extends CI_Migration
{
    public function up()
    {
        # Change the ENUM contstraint for EmployeeRole to include a contractor role
        $field = array(
            'tipe' => array(
                'type' => 'ENUM',
                'constraint' => array('emas','gadai','mikro','payment','gcash','mpo')
            )
        );
        
        # Modify the field
        $this->dbforge->modify_column('transaksi_favorite', $field);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}