<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_New_Fields_Ekyc extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs

        // define colomn for modify table
        $column = array(
            'user_id' => array(
                'type' => 'INT',
                'after' => 'id'
            ),
            'account_type' => array(
                'type' => 'VARCHAR',
                'constraint' => '200',
                'after' => 'status',
                'null' => true
            ),
            'error_count' => array(
                'type' => 'INT',
                'after' => 'syarat_dan_ketentuan',
                'null' => true
            )

        );
        // add column
        $this->dbforge->add_column('ekyc',$column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}