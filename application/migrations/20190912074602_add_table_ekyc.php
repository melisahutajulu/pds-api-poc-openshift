<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Ekyc extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('ekyc', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ),
            'image_ktp' => array(
                'type' => 'TEXT',
                'null' => true
            ),
            'status' => array(
                'type' => 'INT',
                'constraint' => '8',
                'null' => true
            ),
            'screen' => array(
                'type' => 'VARCHAR',
                'constraint' => '150',
                'null' => true
            ),
            'pendidikan_terakhir' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true
            ),
            'sumber_dana' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true
            ),
            'penghasilan' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true
            ),
            'maksud_dan_tujuan' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true
            ),
            'syarat_dan_ketentuan' => array(
                'type' => 'BOOLEAN',
                'null' => true
            ),
            'created_date' => array(
                'type' => 'DATETIME',
                'null' => false,
            ),
            'update_date' => array(
                'type' => 'DATETIME',
                'null' => false,
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('ekyc');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('ekyc', true);
    }
}