<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Column_On_Ekyc extends CI_Migration
{
    public function up()
    {
        $column = array(
            'nik' => array(
                'type' => 'VARCHAR',
                'after' => 'status',
                'constraint' => '250'
            )
        );
        $this->dbforge->add_column('ekyc', $column);
    }

    public function down()
    { }
}
