<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Image_File_Selfie extends CI_Migration
{
    public function up()
    {
         // define colomn for modify table
         $column = array(
            'image_file_selfie' => array(
                'type' => 'TEXT',
                'after' => 'image_selfie'
            )
        );
        // add column
        $this->dbforge->add_column('ekyc',$column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}