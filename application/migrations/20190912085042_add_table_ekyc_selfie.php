<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Ekyc_Selfie extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'ekyc_selfie' if it exists
        $this->dbforge->drop_table('ekyc_selfie', true);

        // Table structure for table 'ekyc_selfie'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ),
            'id_ekyc' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'null' => true
            ),
            'image' => array(
                'type' => 'TEXT',
                'null' => true
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'null' => false,
            ),
            'update_at' => array(
                'type' => 'DATETIME',
                'null' => false,
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('ekyc_selfie');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('ekyc_selfie', true);
    }
}