<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_On_Table_Transaksi_Favorite extends CI_Migration
{
    public function up()
    {
        $column = array(
            'mpo_group' => array(
                'type' => 'VARCHAR',
                'after' => 'namaBank',
                'constraint' => '30'
            ),
            'kode_layanan_mpo' => array(
                'type' => 'VARCHAR',
                'after' => 'mpo_group',
                'constraint' => '100'
            )
        );
        $this->dbforge->add_column('transaksi_favorite', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}