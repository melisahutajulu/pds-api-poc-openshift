<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Is_Dukcapil_Verified extends CI_Migration
{
    public function up()
    {
        // define colomn for modify table
        $column = array(
            'is_dukcapil_verified' => array(
            'type' => 'BOOLEAN',
            'after' => 'tanggal_aktifasi_finansial'
            )
        );
        // add column
        $this->dbforge->add_column('user',$column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}