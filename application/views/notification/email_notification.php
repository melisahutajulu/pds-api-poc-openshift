<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pegadaian Digital Service</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .banner {
            border: 1px solid #0F941A;
            background-color: #5EBD3B;
            padding: 5px;
            text-decoration: none;
            color: white;
        }

        .btn-primary-pegadaian {
            border: 1px solid #0F941A;
            background-color: #5EBD3B;
            color: white;
        }

        .btn-primary-pegadaian:hover{
            background-color: #0F941A;
            color: white !important;
        }

    </style>
</head>
<body>
<div class="banner">
    <div class="container">
        <h3>Pegadaian Digital Service </h3>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default" style="margin-top: 60px">
                <div class="panel-body">
                    <div class="center-block">
                        <img class="center-block" src="<?php echo base_url()?>assets/logo.png">
                        <br>
                        <h3 class="center-block text-center">Selamat datang di Pegadaian Digital Service</h3>
                    </div>
                    
                    <br>
                    <div>
                        Anda telah berhasil melakukan registrasi pada Pegadaian Digital Service. Anda mendaftakan email anda <?php echo $email; ?> saat registrasi.
                        <br><br>
                        Untuk melengkapi registrasi anda, mohon lakukan verifikasi email. Silahkan cek kotak masuk anda dan lakukan verifikasi.
                        <br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>