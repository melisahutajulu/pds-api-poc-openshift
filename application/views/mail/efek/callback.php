<?php if($status == '1'):?>

Hi, <?php echo $nama ?>
<br /></br /><br />
Selamat Pengajuan Gadai Efek Kode Booking (<?php echo $bookingId ?>) telah cair. <br>

Rincian gadai:

<br /><br />
<table class="table table-responsive">
    <tr>
        <td style="width: 30%">Nama Nasabah</td>
        <td style="width: 1%">:</td>
        <td> <strong><?php echo $nama ?></strong></td>
    </tr>
    <tr>
        <td>No Pengajuan</td>
        <td>:</td>
        <td> <strong><?php echo $bookingId; ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Pengajuan</td>
        <td>:</td>
        <td> <strong><?php $_tglPengajuan = new DateTime($tanggalPengajuan);  echo $_tglPengajuan->format('d-m-Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Kredit</td>
        <td>:</td>
        <td> <strong><?php $_tglKredit = new DateTime($tglKredit);  echo $_tglKredit->format('d-m-Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Cair</td>
        <td>:</td>
        <td> <strong><?php $_tglCair = new DateTime($tglCair);  echo $_tglCair->format('d-m-Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Jatuh Tempo</td>
        <td>:</td>
        <td> <strong><?php $_tglJatuhTempo = new DateTime($tglJatuhTempo);  echo $_tglJatuhTempo->format('d-m-Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Lelang</td>
        <td>:</td>
        <td> <strong><?php $_tglLelang = new DateTime($tglLelang);  echo $_tglLelang->format('d-m-Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Uang Pinjaman</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($up, 0, ',', '.') ?></strong></td>
    </tr>    
    <tr>
        <td>Taksiran</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($taksiran, 0, ',', '.') ?></strong></td>
    </tr>
    <tr>
        <td>Bunga</td>
        <td>:</td>
        <td> <strong><?php echo $bunga ?></strong></td>
    </tr>
    <tr>
        <td>Sewa Modal</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($sewaModal, 0, ',', '.') ?></strong></td>
    </tr>
    <tr>
        <td>Sewa Modal Maksimal</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($sewaModalMaksimal, 0, ',', '.') ?></strong></td>
    </tr>
</table>

<?php endif; ?>

<?php if($status == '0'):?>

Hi, <?php echo $nama ?>
<br /></br /><br />
Maaf, Pengajuan Gadai Efek Kode Booking (<?php echo $bookingId ?>) ditolak. <br>

Rincian gadai:

<br /><br />
<table class="table table-responsive">
    <tr>
        <td style="width: 30%">Nama Nasabah</td>
        <td style="width: 1%">:</td>
        <td> <strong><?php echo $nama ?></strong></td>
    </tr>
    <tr>
        <td>No Pengajuan</td>
        <td>:</td>
        <td> <strong><?php echo $bookingId; ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Pengajuan</td>
        <td>:</td>
        <td> <strong><?php $_tglPengajuan = new DateTime($tanggalPengajuan);  echo $_tglPengajuan->format('d-m-Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Batal</td>
        <td>:</td>
        <td> <strong><?php $_tglCair = new DateTime($tglCair);  echo $_tglCair->format('d-m-Y'); ?></strong></td>
    </tr>    
</table>

<?php endif; ?>


<br>

<strong>Unit Gadai Efek</strong> <br> 
Kantor Pusat PT Pegadaian <br>
Jalan Kramat Raya 162 Jakarta Pusat <br>
10430 <br>

<br><br>
Apabila Anda memiliki pertanyaan dapat menghubungi Petugas Kami pada :<br>

<strong>021 -31555550 ext 100</strong>
<br /><br />


