
<?php if($payment == 'BNI'):?>
Hi, <?php echo $namaNasabah ?> 
<br /></br /><br />
Terima kasih telah melakukan Transfer Tabungan Emas Pegadaian.
<br /><br /><br />
Berikut merupakan rincian transaksi Anda
<br /><br />

<table class="table table-responsive">
    <tr>
        <td>Jenis Transaksi</td>
        <td>:</td>
        <td> <strong>Transfer Tabungan Emas</strong></td>
    </tr>
    <tr>
        <td>No Rekening Tabungan Emas</td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</td>
        <td>:</td>
        <td> <strong><?php echo $id_transaksi; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td> <strong><?php echo $namaNasabah; ?></strong></td>
    </tr>
    <tr>
        <td>CIF</td>
        <td>:</td>
        <td> <strong><?php echo $cif != "" ? $cif : "-"; ?></strong></td>
    </tr>
    <tr>
        <td>Batas Waktu Pembayaran </td>
        <td>:</td>
        <td> <strong><?php
        $date = new DateTime($tanggal_expired);
        echo $date->format('d/M/Y');
        ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Pembayaran</td>
        <td>:</td>    
        <td> <strong>Rp <?php echo number_format($totalKewajiban, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong>Virtual Account BNI</strong></td>
    </tr>
    <tr>
        <td>Rekening Tujuan</td>
        <td>:</td>
        <td> <strong><?php echo $virtual_account ?></strong></td>
    </tr>
</table>

<br /><br />
<strong>Tata Cara Pembayaran:</strong>
<br /><br>

ATM BNI
<br />
<ol>
<li>Masukkan Kartu Anda.</li>
<li>Pilih Bahasa.</li>
<li>Masukkan PIN ATM Anda.</li>
<li>Pilih "Menu Lainnya".</li>
<li>Pilih "Transfer".</li>
<li>Pilih "Rekening Tabungan".</li>
<li>Pilih "Ke Rekening BNI".</li>
<li>Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: <?php echo $virtual_account ?>).</li>
<li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
<li>Konfirmasi, apabila telah sesuai, lanjutkan transaksi.</li>
<li>Transaksi telah selesai.</li>
</ol>
<br />

iBank Personal
<br />
<ol>
<li>Ketik alamat https://ibank.bni.co.id kemudian klik "Enter".</li>
<li>Masukkan User ID dan Password.</li>
<li>Klik menu "TRANSFER" kemudian pilih "TAMBAH REKENING FAVORIT". Jika menggunakan desktop untuk menambah rekening, pada menu "Transaksi" lalu pilih "Info & Administrasi Transfer" kemudian "Atur Rekening Tujuan" lalu "Tambah Rekening Tujuan".</li>
<li>Masukkan nomor Virtual Account sebagai nomor rekening tujuan (contoh: <?php echo $virtual_account ?>).</li>
<li>Masukkan Kode Otentikasi Token. Nomor rekening tujuan berhasil ditambahkan.</li>
<li>Kembali ke menu "TRANSFER". Pilih "VIRTUAL ACCOUNT BILLING", kemudian pilih rekening tujuan. Sistem akan mengkonfirmasi nama pemilik Virtual Account dan nominal transfer.</li>
<li>Pilih Rekening Debit dan ketik nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
<li>Lalu masukkan kode otentikasi token.</li>
<li>Transfer Anda Telah Berhasil.</li>
</ol>
<br />

Mobile Banking
<br />
<ol>
<li>Akses BNI Mobile Banking dari handphone kemudian masukkan user ID dan password.</li>
<li>Pilih menu Transfer.</li>
<li>Pilih "Antar Rekening BNI" kemudian "Input Rekening Baru".</li>
<li>Masukkan nomor Rekening Debit dan nomor Virtual Account Tujuan (contoh: <?php echo $virtual_account ?>).</li>
<li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
<li>Konfirmasi transaksi dan masukkan Password Transaksi.</li>
<li>Transfer Anda Telah Berhasil.</li>
</ol>
<br />

SMS Banking
<br />
<ol>
<li>Buka aplikasi SMS Banking BNI</li>
<li>Pilih menu Transfer</li>
<li>Pilih menu Trf rekening BNI</li>
<li>Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: <?php echo $virtual_account ?>).</li>
<li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
<li>Pilih &ldquo;Proses&rdquo; kemudian &ldquo;Setuju&rdquo;</li>
<li>Reply sms dengan ketik pin sesuai perintah</li>
<li>Transaksi Berhasil</li>
</ol>
Atau Dapat juga langsung mengetik sms dengan format:&nbsp;<br> <strong><span>TRF[SPASI]NomorVA[SPASI]NOMINAL</strong>&nbsp;<br> dan kemudian kirim ke 3346&nbsp;<br> Contoh : TRF <?php echo $virtual_account. ' '. $totalKewajiban ?></span>
<br /><br />    

ATM Bersama
<br />
<ol>
<li>Masukkan kartu ke mesin ATM Bersama.</li>
<li>Pilih "Transaksi Lainnya".</li>
<li>Pilih menu "Transfer".</li>
<li>Pilih "Transfer ke Bank Lain".</li>
<li>Masukkan kode bank BNI (009) dan 16 Digit Nomor Virtual Account (contoh: <?php echo $virtual_account ?>).</li>
<li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
<li>Konfirmasi rincian Anda akan tampil di layar, cek dan tekan 'Ya' untuk melanjutkan.</li>
<li>Transaksi Berhasil.</li>
</ol>
<br />

Transfer Dari Bank Lain
<br />
<ol>
<li>Pilih menu "Transfer antar bank" atau "Transfer online antarbank".</li>
<li>Masukkan kode bank BNI (009) atau pilih bank yang dituju yaitu BNI.</li>
<li>Masukan 16 Digit Nomor Virtual Account pada kolom rekening tujuan, (contoh: <?php echo $virtual_account ?>).</li>
<li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
<li>Masukkan jumlah pembayaran : <?php echo $totalKewajiban ?>.</li>
<li>Konfirmasi rincian Anda akan tampil di layar, cek dan apabila sudah sesuai silahkan lanjutkan transaksi sampai dengan selesai.</li>
<li>Transaksi Berhasil.</li>
</ol>
<br /><br />

Terima Kasih
<?php endif;?>

<?php if($payment == 'WALLET'):?>
Hi, <?php echo $namaNasabah ?> 
<br></br><br>
Terima kasih telah melakukan Transfer Tabungan Emas Pegadaian.
<br /><br /><br />
Berikut merupakan rincian transaksi Anda

<br /><br />
<table border="0">                                                       
    <tr>
        <td>Tanggal</td>
        <td>: <strong><?php $tglTrx = new DateTime($tglTransaksi); echo $tglTrx->format('d/M/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</th>
        <td>: <strong><?php echo $id_transaksi ?></strong></td>
    </tr>
    <tr>
        <td>Jumlah Transfer</td>
        <td>: <strong><?php echo $gramTransaksi ?> gram</strong></td>
    </tr>    
    <tr>
        <td>No Rekening</td>
        <td>: <strong><?php echo $norek ?></strong></td>
    </tr> 
    <tr>
        <td>Nama Nasabah</td>
        <td>: <strong><?php echo $namaNasabah ?></strong></td>
    </tr>
    <tr>
        <td>No Rekening Tujuan</td>
        <td>: <strong><?php echo $norekTujuan ?></strong></td>
    </tr> 
    <tr>
        <td>Nama Nasabah Tujuan</td>
        <td>: <strong><?php echo $namaNasabahTujuan ?></strong></td>
    </tr>
</table>

<br /><br />
Terima kasih

<?php endif;?>
