<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>

	<meta charset="utf-8" http-equiv="Content-Type" content="text/html" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<meta name="format-detection" content="telephone=no" />
  	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Pegadaian Digital Service</title>
	<style type="text/css">
		
		/* ==> Importing Fonts <== */
		@import url(https://fonts.googleapis.com/css?family=Fredoka+One);
		@import url(https://fonts.googleapis.com/css?family=Quicksand);
		@import url(https://fonts.googleapis.com/css?family=Open+Sans);

		/* ==> Global CSS <== */
		.ReadMsgBody{width:100%;background-color:#ffffff;}
		.ExternalClass{width:100%;background-color:#ffffff;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
		html{width: 100%;}
		body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0;}
		table{border-spacing:0;border-collapse:collapse;}
		table td{border-collapse:collapse;}
		img{display:block !important;}
		a{text-decoration:none;color:#00A74E;}
		

		/* ==> Responsive CSS For Tablets <== */
		@media only screen and (max-width:640px) {
			body{width:auto !important;}
			table[class="tab-1"] {width:450px !important;}
			table[class="tab-2"] {width:47% !important;text-align:left !important;}
			table[class="tab-3"] {width:100% !important;text-align:center !important;}
			img[class="img-1"] {width:100% !important;height:auto !important;}
		}

		/* ==> Responsive CSS For Phones <== */
		@media only screen and (max-width:480px) {
			body { width: auto !important; }
			table[class="tab-1"] {width:290px !important;}
			table[class="tab-2"] {width:100% !important;text-align:left !important;}
			table[class="tab-3"] {width:100% !important;text-align:center !important;}
			img[class="img-1"] {width:100% !important;}
		}

	</style>
</head>
<body>
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">

	<!-- =======> START ======== -->
	<!--    CUSTOMAR THINKING    -->
	<!-- ======================= -->
	<tr>
		<td align="center">
			<table class="tab-1" align="center" cellspacing="0" cellpadding="0" width="600">

				<tr><td height="60"></td></tr>

				<tr>
					<td align="">
						<img src="<?php echo base_url()?>assets/pegadaian_logo.png" alt="Customer" width="200px">
					</td>
				</tr>


				<tr><td height="25"></td></tr>

				<tr>
					<td style="font-family: 'Quicksand', sans-serif; font-weight: 700; letter-spacing: 1px; font-size: 23px; color: #02A84F; border-bottom: 3px solid #DADADA; padding: 8px 5px;">
						Verifikasi KYC
					</td>
				</tr>
				<tr><td height="25"></td></tr>

				<tr>
					<td  style="font-family: 'open sans', sans-serif; font-size: 15px; color: #7c7c7c;">
                                            Hi, <?php echo $nama ?> <br></br><br>
                                            <?php 
                                                if($status == '1'){
                                                    echo 'Selamat, status KCY anda telah terverifikasi. Terima kasih telah melakukan proses verifikasi KYC. '.$deskripsi;
                                                }else if(status== '0'){
                                                    $message = "Mohon segera lakukan verifikasi KYC ke outlet Pegadaian terdekat untuk dapat menggunakan berbagai fitur tabungan emas. "
                                                            . "Status KYC anda saat ini belum terverifikasi, sehingga anda hanya bisa melakukan transaksi beli tabungan emas dan inquiry saldo. "
                                                            . "Apabila sampai batas akhir waktu KYC, nasabah belum melakukan KYC, "
                                                            . "maka pegadaian akan Melakukan pembekuan rekening tabungan emas nasabah sampai dengan 18 (delapan belas) "
                                                            . "bulan sejak berakhirnya masa KYC atau akumulasi transaksi pembelian tabungan emas telah mencapai 40 gram atau setara Rp 20.000.000 "
                                                            . $deskripsi;
                                                    echo $message;
                                                }
                                            ?>
                                            
					</td>
				</tr>


				<tr><td height="60"></td></tr>

				<tr>
					<td style="background: #EAEAEA; font-family: 'open sans', sans-serif; font-size: 0.7em; color: #7c7c7c;">
						<table>
							<tr>
								<td>
									<img src="<?php echo base_url()?>assets/sign-warning-icon.png" style="width: 30px; padding-left: 5px; padding-right: 5px;">
								</td>
								<td style="padding: 5px;">
									Segala bentuk informasi seperti nomor kontak, alamat e-mail, atau password kamu bersifat rahasia. Jangan menginformasikan data-data tersebut kepada siapa pun, termasuk kepada pihak yang mengatasnamakan Pegadaian Digital Service. 
								</td>
							</tr>
						</table>
					</td>
				</tr>


				<tr>
					<td style="background: #F3F3F3; font-family: 'open sans', sans-serif; font-size: 0.7em; color: #7c7c7c;">

						<table>
							<tr>
								<td style="padding: 5px; width: 300px;">
									PT Pegadaian (Persero)<br>
									Jl.Kramat Raya No.162 - Jakarta Pusat - 10430<br>
									Phone : +628125623032<br>
									Telp.(+6221) 3155550 ext.147, Fax.(+6221) 3914221<br>
									Website : <a href="http://www.pegadaian.co.id">http://www.pegadaian.co.id</a>
								</td>
								<td style="text-align: right !important; padding-left: 10px;">									<img src="<?php echo base_url()?>assets/googleplay-download-grey.png" style="width: 100px;">
								</td>
								<td style="text-align: right !important; padding-left: 10px;">									<img src="<?php echo base_url()?>assets/App-Store-Icon.png" style="width: 100px;">
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="font-family: 'open sans', sans-serif; font-size: 0.7em; color: #7c7c7c; text-align: center;padding : 5px; border-bottom: 1px solid #F3F3F3">
						Harap jangan membalas e-mail ini, karena e-mail ini dikirimkan secara otomatis oleh sistem.
					</td>
				</tr>




			</table>
		</td>
	</tr>

</table>
</body>
</html>