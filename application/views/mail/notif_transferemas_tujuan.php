Hi, <?php echo $nasabahTujuan ?> 
<br></br><br>
Anda menerima transfer emas dari <?php echo $namaNasabah.' ('.$norek.')' ?>.
<br /><br /><br />
Berikut merupakan rincian transaksi Anda

<br /><br />
<table border="0">                                                      

    <tr>
        <td>Tanggal</td>
        <td>: <strong><?php $tglTrx = new DateTime($tglTransaksi); echo $tglTrx->format('d/M/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</th>
        <td>: <strong><?php echo $id_transaksi ?></strong></td>
    </tr>
    <tr>
        <td>Jumlah Transfer</td>
        <td>: <strong><?php echo $gramTransaksi ?> gram</strong></td>
    </tr>    
    <tr>
        <td>No Rekening</td>
        <td>: <strong><?php echo $norek ?></strong></td>
    </tr> 
    <tr>
        <td>Nama Nasabah</td>
        <td>: <strong><?php echo $namaNasabah ?></strong></td>
    </tr>
    <tr>
        <td>No Rekening Tujuan</td>
        <td>: <strong><?php echo $norekTujuan ?></strong></td>
    </tr> 
    <tr>
        <td>Nama Nasabah Tujuan</td>
        <td>: <strong><?php echo $namaNasabahTujuan ?></strong></td>
    </tr>
</table>

<br /><br />
Terima kasih