Hi, <?php echo $nama ?>
<br /></br /><br />
Anda telah melakukan Tarik Tunai

<br /><br />
<table class="table table-responsive">
    <tr>
        <td>Nomor G-Cash</td>
        <td>:</td>
        <td> <strong><?php echo $va ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Waktu</td>
        <td>:</td>
        <td> <strong><?php echo $tglRequest ?></strong></td>
    </tr>
    <tr>
        <td>Nominal</td>
        <td>:</td>
        <td> <strong>Rp <?php echo $amount ?></strong></td>
    </tr>
    <tr>
        <td>Kode OTP</td>
        <td>:</td>
        <td> <strong><?php echo $otp ?></strong></td>
    </tr>
    <tr>
        <td>Batas masa berlaku OTP</td>
        <td>:</td>
        <td> <strong><?php echo $tglExpired ?></strong></td>
    </tr>
</table>

<br />
Kode OTP berlaku selama <strong>5 Menit</strong> dari waktu pengajuan.

<br><br>

<strong>Tata Cara Penarikan Saldo</strong>

<?php if($requestType == '1'):?>
    <ol>
        <li>Datang ke ATM BRI terdekat</li>
        <li>Input nomor G-Cash Anda</li>
        <li>Input kode OTP</li>
        <li>Silahkan ambil uang dan resi Anda</li>
    </ol>
<?php endif; ?> 
<?php if($requestType == '2'):?>
    <ol>
        <li>Datang ke Cabang BRI terdekat</li>
        <li>Informasikan nomor G-Cash Anda</li>
        <li>Informasikan kode OTP</li>
        <li>Silahkan tunggu transaksi Anda diproses oleh Teller</li>
    </ol>
<?php endif; ?>     
