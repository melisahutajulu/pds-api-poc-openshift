<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Reset Password</title>
    <style type="text/css">
        body {margin: 0; padding: 0; min-width: 100%!important;}
        .content {width: 100%; max-width: 600px;}
        .btn{
            border: 1px solid #0F941A;
            background-color: #5EBD3B;
            padding: 5px;
            border-radius: 4px;
            text-decoration: none;
            color: white;
            margin-top: 5px;
        }
    </style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <h3 style="color: #0F941A ">Reset Password</h3>
                        <hr><br>

                        Halo <?php echo $nama ?>,
                        <br><br>
                        Beberapa waktu yang lalu kami menerima permintaan reset password anda pada akun <?php echo $email; ?>.
                        <br><br>
                        Untuk mengubah password anda, klik tombol di bawah ini.
                        <br><br><br>
                        <a class="btn" href="<?php echo base_url()?>auth/reset_password/?t=<?php echo $token ?>">Reset Password</a>
                        <br><br><br>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>