Hi, <?php echo $nama ?> 
<br></br><br>
Selamat Top Up Tabungan Emas Berhasil.

<br /><br />
<table border="0">                                                       
    <tr>
        <td>Metode Pembayaran</td>
        <td>: <strong><?php echo $payment ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>: <strong><?php echo $tglPembayaran ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</th>
        <td>: <strong><?php echo $referensi ?></strong></td>
    </tr>    
    <tr>
        <td>No Rekening</td>
        <td>: <strong><?php echo $noRekening ?></strong></td>
    </tr> 
    <tr>
        <td>Nama Nasabah</td>
        <td>: <strong><?php echo $namaNasabah ?></strong></td>
    </tr>
    <tr>
        <td>CIF</td>
        <td>: <strong><?php echo $cif != "" ? $cif : '-'; ?></strong></td>
    </tr>
    <tr>
        <td>Harga/<?php echo $satuan?>gr</td>
        <td>: <strong>Rp. <?php echo number_format($harga, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Top Up</td>
        <td>: <strong><?php echo number_format($gram, 4, ",", ".") ?> gram</strong></td>
    </tr>
    <tr>
        <td>Nominal</td>
        <td>: <strong>Rp. <?php echo number_format($amount, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Biaya</td>
        <td>: <strong>Rp. <?php echo number_format($administrasi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Channel</td>
        <td>: <strong>Rp. <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <?php 
        if ($promoCode != '') {
            echo "<tr>";
            echo "<td>Promo "; 
            echo $promoCode;
            echo "</td>";
            echo "<td>:</td>";
            echo "<td><strong>Rp. ";
            echo number_format($promoAmount,0,',','.');
            echo "</strong></td>";
            echo "</tr>";
        }
    ?>
    <tr>
        <td>Total</td>
        <td>: <strong>Rp. <?php echo number_format($totalKewajiban+$biayaTransaksi-$discountAmount, 0, ",", ".") ?></strong></td>
    </tr>
</table>

<br /><br />
Terima kasih
