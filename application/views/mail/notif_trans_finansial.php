
Hi, <?php echo $namaNasabah ?>, status aktifasi transaksi finansial anda telah diperbaharui
<br /><br />

<table>
    <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td> <strong><?php $date = new DateTime($tanggal); echo $date->format('d/m/Y H:i:s') ?></strong></td>
    </tr>
    <tr>
        <td>CIF</td>
        <td>:</td>
        <td> <strong><?php echo $cif; ?></strong></td>
    </tr> 
    <tr>
        <td>CIF</td>
        <td>:</td>
        <td> <strong><?php echo $status; ?></strong></td>
    </tr>     
</table>
<br /> <br />
Untuk info lebih lanjut, mohon hubungi cabang pegadaian terdekat. Terima kasih
<br /> <br />
