
Hi, <?php echo $namaNasabah ?>, rekening bank berikut telah ditambahkan ke akun anda: 
<br /><br />

<table class="table table-striped">
   <thead>
        <tr>
            <th>No</th>
            <th>Nama Bank</th>
            <th>Nomor Rekening Bank</th>
            <th>Nama Nasabah Bank</th>
        </tr>
   </thead>  
   <tbody>
        <?php $x=1; foreach($rekening as $r): ?>
        <tr>
            <td><?php echo $x; ?></td>
            <td><?php echo $r['namaBank'] ?></td>
            <td><?php echo $r['norekBank'] ?></td>
            <td><?php echo $r['namaNasabahBank']?></td>
        </tr>
        <?php $x++; endforeach; ?>
   </tbody>
</table>
<br /> <br />
Untuk info lebih lanjut, mohon hubungi cabang pegadaian terdekat. Terima kasih
<br /> <br />
