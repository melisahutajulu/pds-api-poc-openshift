
<?php if ($payment == 'BNI' 
        || $payment == 'VA_BCA' 
        || $payment == 'VA_MANDIRI' 
        || $payment == 'VA_BRI' 
        || $payment == 'VA_BRI' ): ?>
    Pembelian Logam Mulia
    <br /><br />
    Segera bayar Rp <?php echo number_format($totalKewajiban+$biayaTransaksi,0,',','.');?> ke <?php echo $va; ?>
    <br /><br />
    Hi, <?php echo $namaNasabah ?>
    <br />
    Permintaan pembelian Logam Mulia Anda sudah kami terima dengan rincian sebagai berikut:
    <br /><br />

    <table class="table table-responsive">
        <tr>
            <td>Reff Id</td>
            <td>:</td>
            <td> <strong><?php echo $trxId; ?></strong></td>
        </tr>
        <tr>
            <td>Tanggal </td>
            <td>:</td>
            <td> <strong><?php
                    $date = new DateTime($realTglTransaksi);
                    echo $date->format('d/M/Y H:i:s');
                    ?></strong>
            </td>
        </tr>
        <tr>
            <td>Metode Pembelian</td>
            <td>:</td>
            <td> <strong>Angsuran</strong></td>
        </tr>
        <tr>
            <td>Status</td>
            <td>:</td>
            <td> <strong>Menunggu Pembayaran</strong></td>
        </tr>
        <tr>
            <td>Cabang</td>
            <td>:</td>
            <td>
                <strong>
                    <?php echo $namaCabang.' - '.$alamatCabang.' <br> <a href="tel:'.$telpCabang.'"><span class="fa fa-phone"></span> '.$telpCabang.'</a>' ?>
                </strong>
            </td>
        </tr>
    </table>

    <br />
    Rincian Pembelian
    <table class="table table-responsive table-bordered">
        <thead>
            <tr>
                <th width="1%" style="text-align: center">No</th>
                <th style="text-align: center">Jenis Logam Mulia</th>
                <th style="text-align: center">Denom (gram)</th>
                <th style="text-align: center">Jumlah Keping</th>
                <th style="text-align: center">Total Gram</th>
                <th style="text-align: center">Total Harga</th>
                
            </tr>
        </thead>
        <tbody>
        <?php 
            $totalKeping = 0;            
            $no=1;
            $totalGram = 0;
            $_totalHarga = 0;
            foreach($item as $i): ?>
        <tr>
            <td style="text-align: center"><?php echo $no;?></td>
            <td style="text-align: center"><?php echo $jenisLogamMulia; ?></td>
            <td style="text-align: center"><?php echo $i->denom;?></td>
            <td style="text-align: center"><?php echo $i->qty;?></td>
            <td style="text-align: center"><?php echo $i->qty * $i->denom ;?></td>
            <td style="text-align: left; white-space: nowrap">Rp <?php echo number_format($i->qty * $i->harga, 0, ',', '.') ;?></td>

            
        </tr>
        <?php 
            $no++;
            $totalGram = $totalGram + ( $i->qty * $i->denom);  
            $totalKeping = $totalKeping + $i->qty;
            $_totalHarga = $_totalHarga + ($i->qty * $i->harga);
            endforeach;
        ?>
        <tr>
            <th colspan="3">Total</th>
            <th style="text-align: center"><?php echo $totalKeping; ?></th>
            <th style="text-align: center"><?php echo $totalGram; ?></th>
            <th style="text-align: left; white-space: nowrap"">Rp <?php echo number_format($_totalHarga, 0, ',', '.');?></th>
        </tr>
        </tbody>
    </table>

    <br />

    <table class="table table-responsive">
        <tr>
            <td>Harga Dasar</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($totalHarga, 0, ',', '.'); ?></strong></td>
        </tr>
        <tr>
            <td>Sisa Pembiayaan</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($pokokPembiayaan,0,',','.'); ?></strong></td>
        </tr>
        <tr>
            <td>Jangka Waktu</td>
            <td>:</td>
            <td> <strong><?php echo $tenor; ?> Bulan</strong></td>
        </tr>
        <tr>
            <td>Angsuran/Bulan</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($angsuran, 0, ',', '.');; ?></strong></td>
        </tr>
        <tr>
            <th colspan="3" style="text-align: left">Total Bayar Awal</th>
        </tr>
        <tr>
            <td>Uang Muka</td>
            <td>:</td>
            <td><strong>Rp <?php echo number_format($uangMuka,0,',','.'); ?> </strong></td>
        </tr>
        <tr>
            <td>Administrasi</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($administrasi, 0, ',', '.');; ?></strong></td>
        </tr>
        <?php 
            if ($promoCode != '') {
                echo "<tr>";
                echo "<td>Promo "; 
                echo $promoCode;
                echo "</td>";
                echo "<td>:</td>";
                echo "<td>Rp <strong>";
                echo number_format($promoAmount,0,',','.');
                echo "</strong></td>";
                echo "</tr>";
            }
        ?>
        <!-- <tr>
            <td>Promo <?= $promoCode ?></td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($promoAmount,0,',','.'); ?></strong></td>
        </tr> -->
        <!-- <tr>
            <td>Jumlah Pembayaran</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($totalKewajiban, 0, ',', '.');; ?></strong></td>
        </tr> -->
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td>
                <strong>
                    <?php
                    if($payment == 'BNI')
                    {
                        echo 'Virtual Account BNI';
                    }
                    else if($payment == 'VA_BCA')
                    {
                        echo 'Virtual Account BCA';
                    }
                    else if($payment == 'VA_MANDIRI')
                    {
                        echo 'Virtual Account Mandiri';
                    }
                    else if($payment == 'VA_BRI')
                    {
                        echo 'Virtual Account BRI';
                    }
                    ?>
                </strong>
            </td>
        </tr>        
        <tr>
            <td>Rekening Tujuan</td>
            <td>:</td>
            <td> <strong><?php echo $va; ?></strong></td>
        </tr>
        <tr>
            <td>Jumlah Pembayaran</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($totalKewajiban, 0, ',', '.');; ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($biayaTransaksi,0,',','.'); ?></strong></td>
        </tr>
        
        <tr>
            <td>Total Tagihan</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($totalKewajiban + $biayaTransaksi,0,',','.'); ?></strong></td>
        </tr>
        <tr>
            <td>Batas Waktu</td>
            <td>:</td>
            <td> <strong><?php echo $tglExpired; ?></strong></td>
        </tr>
    </table>

    <br /><br />
    <strong>Tata Cara Pembayaran:</strong>
    <br /><br>

    <?php if($payment=='BNI'):?>

        VIA ATM BNI
        <br />
        <ol>
            <li>Masukkan Kartu Anda.</li>
            <li>Pilih Bahasa.</li>
            <li>Masukkan PIN ATM Anda.</li>
            <li>Pilih "Menu Lainnya".</li>
            <li>Pilih "Transfer".</li>
            <li>Pilih "Virtual Account Billing".</li>
            <li>Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: <?php echo $va ?>).</li>
            <li>Konfirmasi, apabila telah sesuai, lanjutkan transaksi.</li>
            <li>Transaksi telah selesai.</li>
        </ol>
        <br />

        iBank Personal
        <br />
        <ol>
            <li>Ketik alamat https://ibank.bni.co.id kemudian klik "Enter".</li>
            <li>Masukkan User ID dan Password.</li>
            <li>Klik menu "TRANSFER" kemudian pilih "TAMBAH REKENING FAVORIT". Jika menggunakan desktop untuk menambah rekening, pada menu "Transaksi" lalu pilih "Info & Administrasi Transfer" kemudian "Atur Rekening Tujuan" lalu "Tambah Rekening Tujuan".</li>
            <li>Masukkan nomor Virtual Account sebagai nomor rekening tujuan (contoh: <?php echo $va ?>).</li>
            <li>Masukkan Kode Otentikasi Token. Nomor rekening tujuan berhasil ditambahkan.</li>
            <li>Kembali ke menu "TRANSFER". Pilih "VIRTUAL ACCOUNT BILLING", kemudian pilih rekening tujuan. Sistem akan mengkonfirmasi nama pemilik Virtual Account dan nominal transfer.</li>
            <li>Lalu masukkan kode otentikasi token.</li>
            <li>Transfer Anda Telah Berhasil.</li>
        </ol>
        <br />

        Mobile Banking
        <br />
        <ol>
            <li>Buka aplikasi Mobile Banking BNI</li>
            <li>Pilih menu Transfer</li>
            <li>Pilih menu Virtual Account Billing</li>
            <li>Masukkan nomor rekening tujuan denan 16 digit Nomor Virtual Account (contoh: <?php echo $va ?>)</li>
            <li>Sistem akan secara otomatis menampilkan nominal transfer dan nama sesuai tagihan atau kewajiban Anda</li>
            <li>Pilih "Proses" kemudian "Setuju"</li>
            <li>Masukan password transaksi</li>
            <li>Transaksi berhasil</li>
        </ol>
        <br />

        SMS Banking
        <br />
        <ol>
            <li>Buka aplikasi SMS Banking BNI</li>
            <li>Pilih menu Transfer</li>
            <li>Pilih menu Trf rekening BNI</li>
            <li>Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: <?php echo $va ?>).</li>
            <li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
            <li>Pilih &ldquo;Proses&rdquo; kemudian &ldquo;Setuju&rdquo;</li>
            <li>Reply sms dengan ketik pin sesuai perintah</li>
            <li>Transaksi Berhasil</li>
        </ol>
        Atau Dapat juga langsung mengetik sms dengan format:&nbsp;<br> <strong><span>TRF[SPASI]NomorVA[SPASI]NOMINAL</strong>&nbsp;<br> dan kemudian kirim ke 3346&nbsp;<br> Contoh : TRF <?php echo $va ?> <?php echo $totalKewajiban + $biayaTransaksi ?></span>
        <br /><br />

        ATM Bersama
        <br />
        <ol>
            <li>Masukkan kartu ke mesin ATM Bersama.</li>
            <li>Pilih "Transaksi Lainnya".</li>
            <li>Pilih menu "Transfer".</li>
            <li>Pilih "Transfer ke Bank Lain".</li>
            <li>Masukkan kode bank BNI (009) dan 16 Digit Nomor Virtual Account (contoh: <?php echo $va ?>).</li>
            <li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
            <li>Konfirmasi rincian Anda akan tampil di layar, cek dan tekan 'Ya' untuk melanjutkan.</li>
            <li>Transaksi Berhasil.</li>
        </ol>
        <br />

        Transfer Dari Bank Lain
        <br />
        <ol>
            <li>Pilih menu "Transfer antar bank" atau "Transfer online antarbank".</li>
            <li>Masukkan kode bank BNI (009) atau pilih bank yang dituju yaitu BNI.</li>
            <li>Masukan 16 Digit Nomor Virtual Account pada kolom rekening tujuan, (contoh: <?php echo $va ?>).</li>
            <li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
            <li>Masukkan jumlah pembayaran : <?php echo $totalKewajiban + $biayaTransaksi ?>.</li>
            <li>Konfirmasi rincian Anda akan tampil di layar, cek dan apabila sudah sesuai silahkan lanjutkan transaksi sampai dengan selesai.</li>
            <li>Transaksi Berhasil.</li>
        </ol>
        <br /><br />
    <?php endif?>

    <?php if($payment == 'VA_BCA'): ?>
        Via ATM BCA
        <br />
        <ol>
            <li>Masukkan Kartu ATM BCA & PIN.</li>
            <li>Pilih menu Transaksi Lainnya &gt; Transfer &gt; ke Rekening BCA Virtual Account.</li>
            <li>Masukkan 5 angka kode perusahaan untuk Pegadaian (22333) dan Nomor Billing Anda (Contoh: 22333<?php echo $va; ?>).</li>
            <li>Di halaman konfirmasi, pastikan detil pembayaran sudah sesuai seperti No VA, Nama, Perus/Produk dan Total Tagihan.</li>
            <li>Pastikan nama Anda dan Total Tagihan benar.</li>
            <li>Jika sudah benar, klik Ya.</li>
            <li>Simpan struk transaksi sebagai bukti pembayaran.</li>
        </ol>
        <br />

        Via mobile banking BCA (m-BCA)

        <br />
        <ol>
            <li>Lakukan log in pada aplikasi BCA Mobile.</li>
            <li>Pilih menu m-BCA, kemudian masukkan kode akses m-BCA.</li>
            <li>Pilih m-Transfer &gt; BCA Virtual Account.</li>
            <li>Pilih dari Daftar Transfer, atau masukkan 5 angka kode perusahaan untuk Pegadaian Mikro/Digital (22333) dan Nomor Billing Anda (Contoh: 22333<?php echo $va; ?>).</li>
            <li>Pastikan nama Anda dan Total Tagihan sudah benar.</li>
            <li>Kemudian klik Ok dan masukkan pin m-BCA.</li>
            <li>Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran.</li>
        </ol>
        <br />

        Via KlikBCA Individual
        <br />
        <ol>
            <li>Lakukan log in pada aplikasi Klik BCA Individual (https://ibank.klikbca.com/).</li>
            <li>Masukkan User ID dan PIN.</li>
            <li>Pilih Transfer Dana &gt; Transfer ke BCA Virtual Account.</li>
            <li>Masukkan nomor BCA Virtual Account (22333) dan Nomor Billing Anda (Contoh: 22333<?php echo $va; ?>).</li>
            <li>Pastikan nama Anda dan Total Tagihan benar.</li>
            <li>Jika sudah benar, klik Lanjutkan.</li>
            <li>Cetak nomor referensi sebagai bukti transaksi Anda.</li>
        </ol>
        <br /><br />
    <?php endif;?>

    <?php if ($payment == 'VA_MANDIRI'): ?>
    
        Via ATM Bank Mandiri
        <br />
        <ol>
            <li>Masukkan kartu ATM dan Pin</li>
            <li>Pilih Menu "Bayar/Beli"</li>
            <li>Pilih menu "Lainnya", hingga menemukan menu "Multipayment"</li>
            <li>Masukkan kode biller Pegadaian Mikro/Digital 50025, lalu pilih Benar</li>
            <li>Masukkan "Nomor Virtual Account" Pegadaian (<?php echo $va; ?>), lalu pilih tombol Benar</li>
            <li>Masukkan Angka "1 untuk memilih tagihan, lalu pilih tombol Ya</li>
            <li>Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya</li>
            <li>Simpan struk sebagai bukti pembayaran anda</li>
        </ol>
        <br />

        Via Internet Banking atau Mandiri Online

        <br />
        <ol>
            <li>Login Mandiri Online dengan memasukkan username dan password</li>
            <li>Pilih menu "Pembayaran"</li>
            <li>Pilih menu "Multipayment"</li>
            <li>Pilih penyedia jasa "Pegadaian Mikro/Digital"</li>
            <li>Masukkan "Nomor Virtual Account" (<?php echo $va; ?>) dan "Nominal" yang akan dibayarkan , lalu pilih Lanjut</li>
            <li>setelah muncul tagihan, pilih Konfirmasi</li>
            <li>Masukkan PIN/ challange code token</li>
            <li>Transaksi selesai, simpan bukti bayar anda</li>
        </ol>
        <br /><br />
    <?php endif; ?>

    <?php if ($payment == 'VA_BRI'): ?>
    
        ATM
    <br>
    <ol>
        <li>Masukan Kartu ATM BRI anda.</li>
        <li>Pilih Bahasa yang diinginkan lalu tekan &ldquo;Lanjutkan&rdquo;.</li>
        <li>Masukan PIN Kartu ATM BRI Anda.</li>
        <li>Tekan &ldquo;Transaksi Lain&rdquo;.</li>
        <li>Tekan &ldquo;Pembayaran&rdquo;.</li>
        <li>Tekan &ldquo;Lainnya&rdquo;.</li>
        <li>Tekan &ldquo;BRIVA&rdquo;.</li>
        <li>Lalu masukan Nomor BRIVA Pelanggan (maksimum 15 Digit).</li>
        <li>Sistem akan memverifikasi data yang dimasukan pelanggan seperti tampak pada layar. Lalu tekan &ldquo;YA&rdquo;.</li>
        <li>Transaksi telah selesai.</li>
    </ol>
    <br>
    Mobile Banking BRI
    <br>
    <ol>
        <li>Masuk ke Aplikasi BRI Mobile anda dan pilih Mobile Banking BRI.</li>
        <li>Pilih menu &ldquo;Info&rdquo;.</li>
        <li>Pilih menu &ldquo;Pembayaran BRIVA&rdquo;.</li>
        <li>Masukan Nomor BRIVA anda dan Masukan Jumlah Pembayaran.</li>
        <li>Masukan PIN anda, Kirimkan kemudian tekan OK.</li>
        <li>Selanjutnya anda akan menerima &ldquo;Notifikasi SMS&rdquo;.</li>
    </ol>
    <br>
    <p>Internet Banking BRI</p>
    <br>
    <ol>
        <li>Login pada alamat Internet Banking BRI <strong>(</strong><a href="https://ib.bri.co.id/ib-bri/Login.html"><strong>https://ib.bri.co.id/ib-bri/Login.html</strong></a><strong>).</strong></li>
        <li>Pilih Menu Pembayaran &amp; Pembelian.</li>
        <li>Pilih Submenu BRIVA.</li>
        <li>Masukan Nomor BRIVA.</li>
        <li>Kemudian akan muncul konfirmasi data BRIVA, lalu masukan jumlah pembayaran (untuk open payment).</li>
        <li>Lalu masukan password dan mToken anda, dan tekan Kirim.</li>
        <li>Transaksi telah selesai.</li>
    </ol>
    <br>
    <p>Mini ATM BRI</p>
    <br>
    <ol>
        <li>Pilih Menu Mini ATM.</li>
        <li>Pilih Menu Pembayaran.</li>
        <li>Pilih Menu BRIVA.</li>
        <li>Swipe Kartu ATM anda.</li>
        <li>Masukan nomor BRIVA anda.</li>
        <li>Masukan PIN ATM anda.</li>
        <li>Akan muncul Menu Konfirmasi. Lalu pilih &ldquo;Lanjut&rdquo;.</li>
        <li>Print bukti transaksi anda.</li>
        <li>Transaksi telah selesai.</li>
    </ol>
    <br>
    <p>ATM Bank lain</p>
    <br>
    <ol>
        <li>Masukan kartu ATM dan inputkan PIN anda.</li>
        <li>Pilih &ldquo;Transaksi Lainnya&rdquo;.</li>
        <li>Tekan Menu &ldquo;Transfer&rdquo;.</li>
        <li>Pilih Menu &ldquo;Ke Rek Bank Lain&rdquo;.</li>
        <li>Masukan &ldquo;Kode BANK&rdquo;.</li>
        <li>Ketikan &ldquo;002&rdquo;, lalu tekan &ldquo;Benar&rdquo;.</li>
        <li>Masukkan jumlah nominal/tagihan yang akan dibayar. Lalu tekan &ldquo;Benar&rdquo;.</li>
        <li>Masukkan Nomor BRIVA Anda (maksimum 15 digit). Lalu tekan &ldquo;Benar&rdquo;.</li>
        <li>Pilih dari rekekning apa akan didebet (Tabungan atau Giro).</li>
        <li>Layar akan menampilkan data yang anda masukan. Jika anda sudah menyakini data sudah sesuai makan tekan &ldquo;Benar&rdquo;.</li>
        <li>Transaksi telah selesai.</li>
    </ol>
        <br /><br />        
    <?php endif; ?>

    <?php if ($payment == 'VA_PERMATA'): ?>
        Via ATM PermataBank
        <br />
        <ol>
            <li>Masukkan Kartu ATM PermataBank & PIN.</li>
            <li>Pilih menu Transaksi Lainnya > Pembayaran > Pembayaran lainnya > Virtual Account.</li>
            <li>Silahkan masukkan Kode Prefix (8470) + No Billing anda. Contoh: 8470<?php echo $va; ?>.</li>
            <li>Pastikan nama Anda dan Total Tagihan benar.</li>
            <li>Klik BENAR jika nama dan nominal pembayaran sudah sesuai.</li>
            <li>Pilih rekening yang akan didebet.</li>
            <li>Tekan YA jika ingin melanjutkan transaksi dan TIDAK jika transaksi selesai.</li>
            <li>Simpan struk transaksi sebagai bukti pembayaran.</li>
        </ol>
        <br />

        Via Internet Banking PermataBank

        <br />
        <ol>
            <li>Silahkan login internet banking kemudian pilih Menu Pembayaran</li>
            <li>Lalu pilih sub menu Pembayaran Tagihan dan klik Virtual Account</li>
            <li>Silahkan pilih rekening anda lalu masukkan Kode Prefix (8470) + No Billing anda (contoh: 8470<?php echo $va; ?>) lalu klik Lanjut</li>
            <li>Masukkan jumlah nominal tagihan pada bagian Total Pembayaran sesuai dengan invoice yang dikirimkan. Kemudian klik Submit</li>
            <li>Tunggu sebentar hingga anda memperoleh SMS notifikasi yang berisi sebuah KODE. Setelah itu masukkan KODE tersebut</li>
            <li>Proses transfer internet banking telah selesai.</li>
        </ol>
        <br />

        Via Mobile Banking PermataBank

        <br />
        <ol>
            <li>Silahkan login mobile banking yang dimiliki Permata Bank</li>
            <li>Lalu klik Menu Pembayaran Tagihan dan pilih Menu Virtual Account</li>
            <li>Kemudian pilih Tagihan Anda dan pilih Daftar Tagihan Baru</li>
            <li>Silahkan masukkan Kode Prefix (8470) + No Billing. (Contoh: 8470<?php echo $va; ?>) sebagai Nomor Tagihan. Apabila selesai silahkan klik Konfirmasi.</li>
            <li>Masukkan jumlah nominal tagihan sesuai dengan invoice. Apabila selesai silahkan klik Konfirmasi</li>
            <li>Masukkan Response Code dan klik Konfirmasi apabila telah selesai</li>
            <li>Proses transfer telah selesai.</li>
        </ol>
        <br /><br />
    <?php endif; ?>

    Terima Kasih
<?php endif; ?>

<?php if ($payment == 'WALLET' || $payment == 'GCASH'): ?>

    Pembelian Logam Mulia Berhasil
    <br /><br />
    Hi, <?php echo $namaNasabah ?>
    <br />
    Pembelian Logam Mulia Anda sudah kami terima dengan rincian sebagai berikut:
    <br /><br />
    <table class="table table-responsive">
        <tr>
            <td>Reff Id</td>
            <td>:</td>
            <td> <strong><?php echo $trxId; ?></strong></td>
        </tr>
        <tr>
            <td>Tanggal </td>
            <td>:</td>
            <td> <strong><?php
                    $date = new DateTime($tglTransaksi);
                    echo $date->format('d/M/Y');
                    ?></strong>
            </td>
        </tr>
        <tr>
            <td>Metode Pembelian</td>
            <td>:</td>
            <td> <strong>Angsuran</strong></td>
        </tr>
        <tr>
            <td>No Kredit</td>
            <td>:</td>
            <td> <strong><?php echo $noKredit; ?></strong></td>
        </tr>
        <tr>
            <td>Jumlah Pembiayaan</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($biayaTransaksi + $totalKewajiban,0,',','.'); ?></strong></td>
        </tr>
        <tr>
            <td>Status</td>
            <td>:</td>
            <td> <strong>Sukses</strong></td>
        </tr>
        <tr>
            <td>Cabang</td>
            <td>:</td>
            <td> <strong><?php echo $namaCabang.' - '.$alamatCabang.' - '.$telpCabang; ?></strong></td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td> <strong><?php echo $payment ?></strong></td>
        </tr>
    </table>

    <br />
    Rincian Pembelian
   <table class="table table-responsive table-bordered">
        <thead>
            <tr>
                <th width="1%" style="text-align: center">No</th>
                <th style="text-align: center">Denom (gram)</th>
                <th style="text-align: center">Jumlah Keping</th>
                <th style="text-align: center">Jenis Logam Mulia</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $totalKeping = 0;            
            $no=1; 
            foreach($item as $i): ?>
        <tr>
            <td style="text-align: center"><?php echo $no;?></td>
            <td style="text-align: center"><?php echo $i->denom;?></td>
            <td style="text-align: center"><?php echo $i->qty;?></td>
            <td style="text-align: center"><?php echo $jenisLogamMulia; ?></td>
        </tr>
        <?php 
            $no++; 
            $totalKeping = $totalKeping + $i->qty;
            endforeach;
        ?>
        <tr>
            <th colspan="2">Total</th>
            <th style="text-align: center"><?php echo $totalKeping; ?></th>
            <th style="text-align: center">Rp <?php echo number_format($totalHarga, 0, ',', '.');?></th>
        </tr>
        </tbody>
    </table>
    
    <br>

    <table class="table table-responsive">
        <tr>
            <td>Uang Muka</td>
            <td>:</td>
            <td><strong><?php echo number_format($uangMuka,0,',','.'); ?> %</strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($biayaTransaksi,0,',','.'); ?></strong></td>
        </tr>
        <tr>
            <td>Harga Dasar</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($totalHarga, 0, ',', '.'); ?></strong></td>
        </tr>
        <tr>
            <td>Administrasi</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($administrasi, 0, ',', '.');; ?></strong></td>
        </tr>
        <tr>
            <td>Total Harga</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($administrasi+$totalHarga, 0, ',', '.');; ?></strong></td>
        </tr>        
    </table>
    
    <br/><br/>

    Terima Kasih

<?php endif; ?>



