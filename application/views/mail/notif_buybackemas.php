
Hi, <?php echo $namaNasabah ?> 
<br /></br /><br />
Terima kasih telah melakukan Jual Tabungan Emas Pegadaian.
<br /><br /><br />
Berikut merupakan rincian transaksi Anda
<br /><br />

<table>
    <tr>
        <td>Jenis Transaksi</td>
        <td>:</td>
        <td> <strong>Jual Tabungan Emas</strong></td>
    </tr>
     <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td> <strong><?php $tglTrx = new DateTime($tglTransaksi); echo $tglTrx->format('d/M/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>No Rekening Tabungan Emas</td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</td>
        <td>:</td>
        <td> <strong><?php echo $id_transaksi; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td> <strong><?php echo $namaNasabah; ?></strong></td>
    </tr>
    <tr>
        <td>Jumlah Gram</td>
        <td>:</td>    
        <td> <strong><?php echo $gramTransaksi ?> gram</strong></td>
    </tr>
   
    <tr>
        <td>Biaya</td>
        <td>:</td>    
        <td> <strong>Rp <?php echo number_format($surcharge, 0, ",", ".") ?></strong></td>
    </tr>
    <?php if($payment != 'GCASH'): ?>
    <tr>
        <td>Total Kewajiban</td>
        <td>:</td>    
        <td> <strong>Rp <?php echo number_format($totalKewajiban, 0, ",", ".") ?></strong></td>
    </tr>
    <?php endif; ?>
    <tr>
        <td>Total Terima</td>
        <td>:</td>    
        <td> <strong>Rp <?php echo number_format($nilaiTransaksi - $totalKewajiban, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Rekening Penerima Dana</td>
        <td>:</td>
        <td> <strong><?php echo $payment ?></strong></td>
    </tr>
    
    <?php if($payment == 'BANK'):?>
    <tr>
        <td>Nama Bank</td>
        <td>:</td>
        <td> <strong><?php echo $namaBank ?></strong></td>
    </tr>    
    <tr>
        <td>Nama Nasabah Bank</td>
        <td>:</td>
        <td> <strong><?php echo $namaBankTujuan ?></strong></td>
    </tr>
    <tr>
        <td>No Rekening Bank</td>
        <td>:</td>
        <td> <strong><?php echo $norekBankTujuan ?></strong></td>
    </tr>
    
    <?php endif; ?>
 

</table>

<br /><br />
Transfer dana ke rekening BCA, Mandiri, BNI, BRI, dan BTN dilakukan maksimal 1X24 jam di hari kerja.<br>
Informasi lebih lanjut silahkan hubungi cs.digital@pegadaian.co.id atau telepon ke (021) 31925472 atau kirim pesan Whatsapp ke 082133007773.


