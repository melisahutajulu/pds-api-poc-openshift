Hi, <?php echo $nama ?>
<br /></br /><br />
Terima kasih telah melakukan pembelian <?php echo $product ?> .
<br /><br /><br />
Rincian Transaksi:
<br /><br />
<table>
    <tr>
        <td>No Jurnal</td>
        <td>:</td>
        <td> <strong><?php echo $trxId; ?></strong></td>
    </tr>
    <tr>
        <td>No Reff</td>
        <td>:</td>
        <td> <strong><?php echo $trxId; ?></strong></td>
    </tr>
    <tr>
        <td>Tgl Transaksi</td>
        <td>:</td>
        <td> <strong><?php echo DateTime::createFromFormat('Y-m-d H:i:s', $tglTransaksi)->format('d/m/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Produk</td>
        <td>:</td>
        <td> <strong><?php echo $product ?></strong></td>
    </tr>

    <?php if ($mpoGroup === 'seluler'){ ?>
    <tr>
        <td>No HP </td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>
    <?php } else { ?>
    <tr>
        <td>No Kredit</td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>    
   <?php }?>

    <tr>
        <td>Total Kewajiban</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban, 0, ",", "."); ?></strong></td>
    </tr>
    <?php if($noHp != null):?>
    <tr>
        <td>No HP</td>
        <td>:</td>
        <td> <strong><?php echo $noHp; ?></strong></td>
    </tr>
    <?php endif; ?>
    <?php if($nominalPulsa != null):?>
    <tr>
        <td>Nominal Pulsa</td>
        <td>:</td>
        <td> <strong>Rp <?php echo $nominalPulsa; ?></strong></td>
    </tr>
    <?php endif; ?>
    <?php if($mpoProduct->tipe=='data'):?>
    <tr>
        <td>Paket Data</td>
        <td>:</td>
        <td> <strong><?php echo $mpoProduct->namaLayanan; ?></strong></td>
    </tr>
    <?php endif; ?>
    <tr>
        <td>Tanggal Transaksi</td>
        <td>:</td>
        <td> <strong><?php  $tglTrx = new DateTime($tglTransaksi); echo $tglTrx->format('d/M/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Channel</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", "."); ?></strong></td>
    </tr>
    <tr>
        <td>Total Tagihan</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi, 0, ",", "."); ?></strong></td>
    </tr>

</table>
<br><br>

<?php echo $mpoFooter ?>





