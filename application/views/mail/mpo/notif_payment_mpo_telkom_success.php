Hi, <?php echo $nama ?>
<br>
Terima kasih telah melakukan pembelian layanan <?php echo $product ?>
<table>
    <tr>
        <td>No Jurnal</td>
        <td>:</td>
        <td> <strong><?php echo $reffCore; ?></strong></td>
    </tr>
    <tr>
        <td>No Reff </td>
        <td>:</td>
        <td> <strong><?php echo $trxId; ?></strong></td>
    </tr>
    <tr>
        <td>Tgl Transaksi </td>
        <td>:</td>
        <td> <strong><?php echo date('d/m/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>No Transaksi </td>
        <td>:</td>
        <td> <strong><?php echo $id ?></strong></td>
    </tr>
    <tr>
        <td>Id Pelanggan </td>
        <td>:</td>
        <td> <strong><?php echo '('.$idTambahan.')'.$norek; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Pelanggan </td>
        <td>:</td>
        <td> <strong><?php echo $namaPelanggan; ?></strong></td>
    </tr>
    <tr>
        <td>NPWP </td>
        <td>:</td>
        <td> <strong><?php echo ( $npwp === '' || $npwp == null || $npwp==" " ) ? '-' : $npwp; ?></strong></td>
    </tr>
    <tr>
        <td>No Reff Biller</td>
        <td>:</td>
        <td> <strong><?php echo $reffMpo; ?></strong></td>
    </tr>
    <tr>
        <td>Jumlah Tagihan </td>
        <td>:</td>
        <td> <strong><?php echo $jumlahBill; ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Tagihan </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($hargaJual, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Admin </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($administrasi, 0, ",", ".") ?></strong></td>
    </tr>    
</table>

Detail:
<table class="table table-responsive table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>No Reff</th>
            <th>Tagihan</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($dataTagihan as $d):?>
        <tr>
            <td><?php echo $d->no; ?></td>
            <td><?php echo $d->noref; ?></td>
            <td>Rp <?php echo number_format((int)$d->tagihan, 0, ",", "."); ?></td>
        </tr>
    <?php endforeach;?>
    
    </tbody>
</table>

<table>
    <!-- PAYMENT METHOD BNI -->
     <?php if ($paymentMethod == 'BNI' 
                || $paymentMethod == 'VA_BCA'
                || $paymentMethod == 'VA_BRI'
                || $paymentMethod == 'VA_PERMATA' 
                || $paymentMethod == 'VA_MANDIRI'): ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> 
            <strong>
                <?php 
                    if($paymentMethod == 'BNI')
                    {
                        echo 'Virtual Account BNI';
                    }
                    else if($paymentMethod == 'VA_BCA')
                    {
                        echo 'Virtual Account BCA';
                    }
                    else if($paymentMethod == 'VA_MANDIRI')
                    {
                        echo 'Virtual Account Mandiri';
                    }
                    else if($paymentMethod == 'VA_PERMATA')
                    {
                        echo 'Virtual Account Permata';
                    }
                    else if($paymentMethod == 'VA_BRI')
                    {
                        echo 'Virtual Account BRI';
                    }
                ?>
            </strong>
        </td>
    </tr>    
    <tr>
        <td>No Rekening VA</td>
        <td>:</td>
        <td> <strong><?php echo $va ?></strong></td>
    </tr>
    <tr>
        <td>Biaya </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Transfer</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi, 0, ",", ".") ?> Include PPN</strong></td>
    </tr>
    <?php endif ?>

    <!-- PAYMENT METHOD MANDIRI -->
    <?php if ($paymentMethod == 'Mandiri'): ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong>Mandiri Click Pay</strong></td>
    </tr>  
    <tr>
        <td>No Rekening VA</td>
        <td>:</td>
        <td> <strong><?php echo $va ?></strong></td>
    </tr>
    <tr>
        <td>Biaya </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Transfer</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi, 0, ",", ".") ?> Include PPN</strong></td>
    </tr>
    <tr>
        <td>Reff Biller:</td>
        <td>:</td>
        <td> <strong><?php echo $reffBiller ?></strong></td>
    </tr>
    <?php endif; ?>

    <!-- PAYMENT METHOD WALLET -->
    <?php if ($paymentMethod == 'WALLET'): ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong>WALLET</strong></td>
    </tr>
    <tr>
        <td>Reff MPO:</td>
        <td>:</td>
        <td> <strong><?php echo $reffMpo ?></strong></td>
    </tr>
    <tr>
        <td>Serial Number:</td>
        <td>:</td>
        <td> <strong><?php echo $serialNumber ?></strong></td>
    </tr>
    <tr>
        <td>SID:</td>
        <td>:</td>
        <td> <strong><?php echo $sid ?></strong></td>
    </tr>
    <?php endif; ?>

    <tr>
        <td>Waktu Pembayaran</td>
        <td>:</td>
        <td> <strong><?php echo date('d/m/Y H:i:s'); ?></strong></td>
    </tr>
    <tr>
        <td>Status </td>
        <td>:</td>
        <td> 
            <strong><?php echo $paymentStatus['label'] ?></strong>. 
            <?php echo $paymentStatus['code'] == '2' ? $paymentStatus['message'] : '' ?>
        </td>
    </tr>

</table>

<br>

<?php echo $mpoFooter ?>



