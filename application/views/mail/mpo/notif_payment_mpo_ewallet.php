Hi, <?php echo $nama ?>
<br>
Terima kasih telah melakukan pembelian layanan <?php echo $product ?> .
<br>
Silakan melakukan pembayaran untuk melanjutkan transaksi anda.
<br>
Konfirmasi Transaksi

<table>
    <tr>
        <td>No Reff </td>
        <td>:</td>
        <td> <strong><?php echo $trxId; ?></strong></td>
    </tr>
    <tr>
        <td>Tgl Transaksi </td>
        <td>:</td>
        <td> <strong><?php echo date('d/m/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Produk </td>
        <td>:</td>
        <td> <strong><?php echo $product; ?></strong></td>
    </tr>
    <tr>
        <td>No Rek </td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>
    <tr>
        <td>Harga</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban, 0, ",", ".") ?></strong></td>
    </tr>

    <br><br>

    <!-- PAYMENT METHOD BNI -->
    <?php 
        if ($paymentMethod == 'BNI' 
                || $paymentMethod == 'VA_BCA' 
                || $paymentMethod == 'VA_MANDIRI'
                || $paymentMethod == 'VA_BRI'
                || $paymentMethod == 'VA_PERMATA'): 
        ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> 
            <strong>
                <?php 
                    if($paymentMethod == 'BNI')
                    {
                        echo 'Virtual Account BNI';
                    }
                    else if($paymentMethod == 'VA_BCA')
                    {
                        echo 'Virtual Account BCA';
                    }
                    else if($paymentMethod == 'VA_MANDIRI')
                    {
                        echo 'Virtual Account Mandiri';
                    }
                    else if($paymentMethod == 'VA_PERMATA')
                    {
                        echo 'Virtual Account Permata';
                    }
                    else if($paymentMethod == 'VA_BRI')
                    {
                        echo 'Virtual Account BRI';
                    }
                ?>
            </strong>
        </td>
    </tr>    
    <tr>
        <td>No Rekening VA</td>
        <td>:</td>
        <td> <strong><?php echo $va ?></strong></td>
    </tr>
    <tr>
        <td>Biaya </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Transfer</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi, 0, ",", ".") ?> Include PPN</strong></td>
    </tr>
    <tr>
        <td>Batas Waktu Pembayaran </td>
        <td>:</td>
        <td> <strong><?php echo $tglExpired ?></strong></td>
    </tr>
    <?php endif ?>

    <!-- PAYMENT METHOD MANDIRI -->
    <?php if ($paymentMethod == 'Mandiri'): ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong>Mandiri Click Pay</strong></td>
    </tr>  
    <tr>
        <td>No Rekening VA</td>
        <td>:</td>
        <td> <strong><?php echo $va ?></strong></td>
    </tr>
    <tr>
        <td>Biaya </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Transfer</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi, 0, ",", ".") ?> Include PPN</strong></td>
    </tr>
    <tr>
        <td>Reff Biller:</td>
        <td>:</td>
        <td> <strong><?php echo $reffBiller ?></strong></td>
    </tr>
    <?php endif; ?>

    <!-- PAYMENT METHOD WALLET -->
    <?php if ($paymentMethod == 'WALLET' || $paymentMethod == 'GCASH'): ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong><?php echo $paymentMethod ?></strong></td>
    </tr>
    <tr>
        <td>Reff MPO:</td>
        <td>:</td>
        <td> <strong><?php echo $reffMpo ?></strong></td>
    </tr>
    <tr>
        <td>Serial Number:</td>
        <td>:</td>
        <td> <strong><?php echo $serialNumber ?></strong></td>
    </tr>
    <tr>
        <td>SID:</td>
        <td>:</td>
        <td> <strong><?php echo $sid ?></strong></td>
    </tr>
    <?php endif; ?>

</table>

Tata Cara Pembayaran:

<br>

<?php if(is_array($redaksi)): ?>
    <?php foreach($redaksi as $r): ?>
        <strong><?php echo $r['title']?></strong><br>
        <?php echo $r['description']; ?> 
    <?php endforeach; ?>
<?php endif; ?>



