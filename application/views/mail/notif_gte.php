Hi, <?php echo $nama ?> 
<br /></br /><br />
Transaksi anda telah kami cairkan dengan detail sebagai berikut:

<br /><br />
<table class="table table-responsive">
    <tr>
        <td>No Referensi</td>
        <td>:</td>
        <td> <strong><?php echo $reffSwitching ?></strong></td>
    </tr>
    <tr>
        <td>Produk</td>
        <td>:</td>
        <td> <strong>Gadai Tabungan Emas</strong></td>
    </tr>
    <tr>
        <td>No Kredit</td>
        <td>:</td>
        <td> <strong><?php echo $norek ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td> <strong><?php echo $nama ?></strong></td>
    </tr>
    <tr>
        <td>Jangka Waktu</td>
        <td>:</td>
        <td> <strong><?php echo $tenor ?> hari</strong></td>
    </tr>
    <tr>
        <td>Tanggal Kredit</td>
        <td>:</td>
        <td> <strong><?php $rTglKredit = new DateTime($tglKredit); echo $rTglKredit->format('d/m/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Jatuh Tempo</td>
        <td>:</td>
        <td> <strong><?php $rTglJatuhTempo = new DateTime($tglJatuhTempo); echo $rTglJatuhTempo->format('d/m/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Tarif Sewa Modal</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo $tarif; ?></strong></td>
    </tr>
    <tr>
        <td>Uang Pinjaman</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($up,0,',','.') ?></strong></td>
    </tr>
    <tr>
        <td>Administrasi</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($administrasi,0,',','.') ?></strong></td>
    </tr>
    <tr>
        <td>Asuransi</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($asuransi ,0,',','.') ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Transfer</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($surcharge,0,',','.') ?></strong></td>
    </tr>
    <tr>
        <td>Jumlah Diterima</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($hakNasabah,0,',','.') ?></strong></td>
    </tr>
    <tr>
        <td>Barang Jaminan</td>
        <td>:</td>
        <td> <strong>Titipan emas no rekening <?php echo $noKredit ?> Sebesar <?php echo $gram ?> gram </strong></td>
    </tr>

    <?php if($gcashIdTujuan != null):?>
    <tr>
        <td>Rekening Gcash</td>
        <td>:</td>
        <td> <strong><?php echo $gcashIdTujuan; ?></strong></td>
    </tr>
    <?php endif; ?>
    
    <?php if($gcashIdTujuan == null):?>
    <tr>
        <td>No Rekening</td>
        <td>:</td>
        <td> <strong><?php echo $norekBankTujuan; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Pemilik Rekening</td>
        <td>:</td>
        <td> <strong><?php echo $namaBankTujuan; ?></strong></td>
    </tr>
    <tr>
        <td>Bank Tujuan</td>
        <td>:</td>
        <td> <strong><?php echo $namaBank; ?></strong></td>
    </tr>
    <?php endif; ?>



</table>



<br /><br />
Transfer dana ke rekening BCA, Mandiri, BNI, BRI, dan BTN dilakukan maksimal 1X24 jam di hari kerja.<br>
Informasi lebih lanjut silahkan hubungi cs.digital@pegadaian.co.id atau telepon ke (021) 31925472 atau kirim pesan Whatsapp ke 082133007773.



