
Hi, <?php echo $namaNasabah ?> 
<br /></br /><br />
Terima kasih telah melakukan Gadai Tabungan Emas Pegadaian.
<br /><br /><br />
Berikut merupakan rincian transaksi Anda
<br /><br />

<table>
    <tr>
        <td>Jenis Transaksi</td>
        <td>:</td>
        <td> <strong>Gadai Tabungan Emas</strong></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td> <strong><?php $tglTrx = new DateTime($tglTransaksi); echo $tglTrx->format('d/M/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>No Rekening Tabungan Emas</td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</td>
        <td>:</td>
        <td> <strong><?php echo $reffSwitching; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td> <strong><?php echo $namaNasabah; ?></strong></td>
    </tr>
    <tr>
        <td>Gram Gadai</td>
        <td>:</td>    
        <td> <strong><?php echo $gram ?> gram</strong></td>
    </tr>
    <tr>
        <td>Uang Pinjaman</td>
        <td>:</td>    
        <td> <strong>Rp<?php echo number_format($up, 0, ",", ".") ?></strong></td>
    </tr> 
    <tr>
        <td>Hak Nasabah</td>
        <td>:</td>    
        <td> <strong>Rp<?php echo number_format($hakNasabah, 0, ",", ".") ?></strong></td>
    </tr>  
    <tr>
        <td>Biaya</td>
        <td>:</td>    
        <td> <strong>Rp <?php echo number_format($surcharge, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Total Kewajiban</td>
        <td>:</td>    
        <td> <strong>Rp <?php echo number_format($totalKewajiban, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Nama Bank</td>
        <td>:</td>
        <td> <strong><?php echo $namaBank ?></strong></td>
    </tr>    
    <tr>
        <td>Nama Nasabah Bank</td>
        <td>:</td>
        <td> <strong><?php echo $namaBankTujuan ?></strong></td>
    </tr>
    <tr>
        <td>No Rekening Bank</td>
        <td>:</td>
        <td> <strong><?php echo $norekBankTujuan ?></strong></td>
    </tr>


</table>
<br/><br/>


Terima Kasih<br>
PT Pegadaian (Persero)

<br><br>
SIMPAN TANDA TERIMA INI SEBAGAI BUKTI PENERIMAAN YANG SAH

<br><br>