<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pegadaian Digital Service</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            .banner {
                border: 1px solid #0F941A;
                background-color: #5EBD3B;
                padding: 5px;
                text-decoration: none;
                color: white;
            }

            .btn-primary-pegadaian {
                border: 1px solid #0F941A;
                background-color: #5EBD3B;
                color: white;
            }

            .btn-primary-pegadaian:hover{
                background-color: #0F941A;
                color: white !important;
            }
            .greenColor{
                color: #199f56;
                font-weight: 400;
                padding-bottom: 16px;
                border-bottom: 4px solid #dadada;
                margin-bottom: 32px;
            }
            .m-b-34{
                margin-bottom: 34px;
            }
            .width50p{
                width: 50%;
                float: left;
            }
            .width50right{
               text-align: right !important;
            }
            .text-center {
                text-align: center;
            }
            .underline {
                text-decoration: underline;
            }
            ol.c {
              list-style-type: upper-roman;
              padding-left: 20px;
            }
            .emailOrMobile{
              width: 600px;
              margin: 0 auto;
            }
            .detailGadaiContent{
              width: 300px;
              margin: 0 auto;
              border: 1px solid #CCC;
              padding: 15px;
            }
            @media (max-width: 500px){
              .emailOrMobile{
                width: 100%;
              }
              .detailGadaiContent{
                width: 100%;
              }
            }
        </style>
    </head>
    <body>
    <?php 
      function indonesian_date ($timestamp = '', $date_format = 'l, j F Y H:i') {
        if (trim ($timestamp) == '')
        {
          $timestamp = time ();
        }
        elseif (!ctype_digit ($timestamp))
        {
          $timestamp = strtotime ($timestamp);
        }
        # remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace ("/S/", "", $date_format);
        $pattern = array (
          '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
          '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
          '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
          '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
          '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
          '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
          '/April/','/June/','/July/','/August/','/September/','/October/',
          '/November/','/December/',
        );
        $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
          'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
          'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
          'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
          'Oktober','November','Desember',
        );
        $date = date ($date_format, $timestamp);
        $date = preg_replace ($pattern, $replace, $date);
        $date = "{$date}";
        return $date;
      } 
    ?>
    <div class="banner">
        <div class="container">
            <h3>PT PEGADAIAN (Persero) </h3>
              <h4>Cabang/Unit <?php print $kodeNamaCabang ?></h4>
        </div>
    </div>

    <div class="container emailOrMobile">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="margin-top: 60px; border: 0;">
                    <div class="panel-body">
                        <div class="center-block text-center">
                            <img class="center-block" src="http://pds-api.affinitydigitalasia.com/assets/logo.png">
                            <br>
                        </div>
                        <div></div>
                        <div class="detailGadaiContent">
                            <?php 
                              if($checkPayment->count > 0){
                              }else{
                                print 'Pengajuan ini belum dilakukan pembayaran.';
                              }
                            ?>
                            Nota transaksi ini merupakan satu kesatuan yang tidak dapat terpisahkan dari Perjanjian Utang Piutang dengan jaminan gadai nomor: <?php 
                              if($no_kredit==null){
                            }else{
                              print $no_kredit;
                            }
                            ?>
                            <br><br>
                            <div class="text-center">
                                NOTA TANSAKSI<br>
                                PENERIMAAN UANG - KREDIT BARU<br>
                                GADAI KCA
                            </div>
                            <hr>
                            <table>
                              <tr>
                                <td style="width: 50%;">Tanggal</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"><?php
                                if(isset($tglKredit)){
                                  $parsed_date=strtotime($tglKredit);
                                  if($parsed_date===FALSE){
                                    print '';
                                  }elseif($tglKredit=='0000-00-00'){
                                    print '';
                                  }else{
                                    print date("d-m-Y", strtotime($tglKredit)); 
                                  }
                                }
                                ?></td>
                              </tr>
                              <tr>
                                <td style="width: 50%;">Nomor Kredit</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"> 
                                <?php 
                                  if($no_kredit==null){
                                }else{
                                  print $no_kredit;
                                }
                                ?></td>
                              </tr>
                                <tr>
                                <td style="width: 50%;">Uang Pinjaman</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"> 
                                Rp. <?php 
                                if($up==0){
                                  
                                }else{
                                  print number_format($up, '0',',','.');
                                }
                                ?></td>
                              </tr>
                              <tr>
                                <td style="width: 50%;">Administrasi</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"> 
                                Rp. <?php 
                                if($administrasi==0){

                                }else{
                                  print number_format($administrasi, '0',',','.');
                                }
                                ?></td>
                              </tr>
                              <tr>
                                <td style="width: 50%;">Biaya Asuransi</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"> 
                                Rp. <?php print number_format($asuransi, '0',',','.') ?>
                                  
                                </td>
                              </tr>
                              <tr>
                                <td style="width: 50%;">Biaya Transfer</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"> 
                                <!-- TODO -->
                                Rp. <?php print number_format('2500', '0',',','.') ?>
                                  
                                </td>
                              </tr>
                            </table>
                            <div class="width50p">--------------------</div>
                            <div class="width50p">&nbsp;</div>
                            <div class="clearfix"></div>
                            <table>
                              <tr>
                                <td style="width: 50%;">Jumlah Diterima</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;">Rp. <?php 
                                if($diterimaNasabah==0){

                                }else{
                                  // TODO
                                  print number_format($diterimaNasabah-2500, '0',',','.');
                                }
                                ?>
                                </td>
                              </tr>
                            </table>
                            <br>
                            <table>
                              <tr>
                                <td style="width: 50%;">Sewa Modal</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;">Rp. <?php 
                                if ($sewaModal!=0){
                                  print number_format($sewaModal, '0',',','.');  
                                }else{
                                  print '';
                                }
                                ?>
                                </td>
                              </tr>
                              <tr>
                                <td style="width: 50%;">Tanggal Jatuh Tempo</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"><?php 
                                  if(isset($tglJatuhTempo)){
                                    $parsed_date=strtotime($tglJatuhTempo);
                                    if($parsed_date===FALSE){
                                      print '';
                                    }elseif($tglJatuhTempo=='0000-00-00'){
                                      print '';
                                    }else{
                                      print date("d-m-Y", strtotime($tglJatuhTempo)); 
                                    }
                                  }

                                  ?>
                                </td>
                              </tr>
                            </table>
                            <br>
                            <div class="m-b-34">
                                Jika kredit ini tidak dilunasi/diperpanjang sampai tanggal jatuh tempo, maka barang jaminan akan dilelang mulai<br>
                            </div>
                            <table>
                              <tr>
                                <td style="width: 50%;">Tanggal</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"><?php 
                                  if(isset($tglLelang)){
                                    $parsed_date=strtotime($tglLelang);
                                    if($parsed_date===FALSE){
                                      print '';
                                    }elseif($tglLelang=='0000-00-00'){
                                      print '';
                                    }else{
                                      print date("d-m-Y", strtotime($tglLelang)); 
                                    }
                                  }

                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <td style="width: 50%;">Biaya Proses Lelang</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"><?php print $biayaProsesLelang ?>%
                                </td>
                              </tr>
                              <tr>
                                <td style="width: 50%;">Biaya Lelang</td>
                                <td>:</td>
                                <td style="width: 50%; text-align: right;"><?php  print number_format($biayaLelang, '0',',','.') ?>%
                                </td>
                              </tr>
                            </table>
                            <br>
                            <div>
                                Jika belum terjual lelang, Barang Jaminan Dalam Proses Lelang (BJDPL) dapat diselesaikan oleh Nasabah dengan dikenakan tambahan biaya Administrasi Penyelesaian BJDPL sebesar <?php print number_format($biayaAdmBjpdl, '0',',','.') ?>% dari Uang Pinjaman dan maksimal sebesar <?php  print number_format($biayaBjdplMax, '0',',','.') ?>%
                                <br><br>
                                Terima kasih atas kepercayaan Anda
                                <br><br>
                                <div class="text-center">
                                    <div class="width50p">Nama Petugas </div>
                                    <div class="width50p">Nama Nasabah </div>
                                </div>
                                 <br><br><br><br>
                                 <div class="text-center">
                                    <?php 
                                    if($namaPinca==null){
                                      print '<div class="width50p">&nbsp;</div>';
                                    }else{
                                      print '<div class="width50p underline">'.$namaPinca.'</div>';
                                    }
                                    ?>
                                    <div class="width50p underline"><?php print $nasabah ?></div>
                                </div>
                                <br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div style="margin-top: 60px">
                    <div class="panel-body">
                        <div class="text-center">
                            <h4>Pejanjian Utang Piutang Dengan Jaminan Gadai</h4>
                            <p class="text-center">Nomor : 
                            <?php 
                              if($no_kredit==null){
                            }else{
                              print $no_kredit;
                            }
                            ?></p>
                        </div>
                        <div>
                          <?php 
                            $tanggal_pembayaran = indonesian_date(strtotime($tanggal_pembayaran), 'd-m-Y');
                            $dayname = indonesian_date(strtotime($tanggal_pembayaran), 'l');
                            $month = indonesian_date(strtotime($tanggal_pembayaran), 'F');
                            $date = indonesian_date(strtotime($tanggal_pembayaran), 'd');
                            $year = indonesian_date(strtotime($tanggal_pembayaran), 'Y');
                          ?>
                            Pada hari ini <?php print $dayname ?> tanggal <?php print $date ?>  bulan <?php print $month ?> tahun 
                            <?php
                            if($year='0000'){

                            }else{
                              print $year; 
                            } 
                            ?> bertempat di PT PEGADAIAN (Persero) CP/UPC. <?php print $kodeNamaCabang ?>, kami yang bertanda tangan
                            di bawah ini :<br>
                            <ol class="c">
                                <li>
                                    Nama <?php print $nama_group ?> Jabatan Pimpinan CP/UPC, dalam hal ini bertindak dan atas
                                    nama PT PEGADAIAN (Persero), yang selanjutnya disebut PEGADAIAN.</li>
                                <li>
                                    Nama <?php print $nasabah ?>, No. KTP.<?php print $noIdentitas ?>
                                    alamat <?php print $jalan ?> dalam hal ini bertindak untuk dan atas nama diri sendiri/perusahaan
                                </li>
                            </ol>
                            yang selanjutnya disebut NASABAH.
                            Kami yang bertandatangan pada Surat Bukti Gadai (SBG) ini, yakni PT PEGADAIAN (Persero)
                            (Penerima Gadai) dan NASABAH (pemilik barang jaminan atau kuasa dari pemilik barang
                            jaminan), sepakat membuat perjanjian sebagai berikut :
                            <br><br>
                            <ol style="padding-left: 20px">
                                <li>
                                    NASABAH menerima dan setuju terhadap uraian dan taksiran barang jaminan, penetapan
                                    Uang Pinjaman, Tarif Sewa Modal, Biaya Administrasi, Biaya Lainnya (jika ada), Bea
                                    Lelang sebagaimana yang dimaksud pada Surat Bukti Gadai (SBG) atau bukti transaksi
                                    (struk atau dokumen elektronik) dan sebagai tanda bukti yang sah penerimaan uang
                                    pinjaman​ d
                                    an Uang Kelebihan Lelang (jika ada).
                                </li>
                                <li>
                                    Barang yang diserahkan sebagai barang jaminan adalah milik NASABAH dan/atau
                                    kepemilikan sebagaimana pasal 1977 KUH Perdata dan/atau milik Pemberi Kuasa atas
                                    barang jaminan yang dikuasakan kepada NASABAH, dan menjamin bukan berasal dari
                                    hasil kejahatan, tidak dalam obyek sengketa dan/atau sita jaminan.
                                </li>
                                <li>
                                    NASABAH menyatakan telah berutang kepada PT PEGADAIAN (Persero) dan
                                    berkewajiban untuk membayar uang pinjaman ditambah sewa modal dan biaya lainnya (jika
                                    ada) pada saat pelunasan, atau membayar cicilan uang pinjaman (jika ada), sewa modal,
                                    biaya administrasi pada saat perpanjangan.
                                </li>
                                <li>
                                    Tarif Sewa Modal dihitung per 15 (lima belas) hari 1 (satu) sampai dengan 15 (lima belas)
                                    hari dihitung sama dengan 15 (lima belas hari), untuk Gadai Fleksi dihitung per hari.
                                </li>
                                <li>
                                    Sewa modal dihitung sejak tanggal pinjaman sampai dengan tanggal pelunasan dan/atau
                                    perpanjangan oleh NASABAH, hasilnya dibulatkan ke atas dengan kelipatan Rp. 100,-
                                    (seratus rupiah).
                                </li>
                                <li>
                                    Pinjaman dapat dilunasi atau diperpanjang (ulang gadai, mengangsur uang pinjaman, dan
                                    minta tambah uang pinjaman) sebelum dan/atau sampai dengan tanggal jatuh tempo.
                                </li>
                                <li>
                                    Bila transaksi pelunasan dan perpanjangan pinjaman dilakukan oleh NASABAH di Cabang
                                    /Unit Pelayanan Cabang atau secara ​ Online a
                                    tau tempat lain yang ditunjuk oleh PT
                                    PEGADAIAN (Persero), maka NASABAH menyetujui nota transaksi (struk atau dokumen
                                    elektronik) sebagai satu kesatuan dari perjanjian dari Surat Bukti Gadai ini.
                                </li>
                                <li>
                                    Dalam hal terjadi perpanjangan pinjaman maka untuk tanggal jatuh tempo, tanggal lelang,
                                    besaran uang pinjaman, besaran sewa modal, dan rincian barang jaminan tercantum dalam
                                    nota transaksi (struk atau dokumen elektronik).
                                </li>
                                <li>
                                    PT PEGADAIAN (Persero) akan memberikan ganti kerugian apabila barang jaminan yang
                                    berada dalam penguasaan PT PEGADAIAN (Persero) mengalami kerusakan atau hilang
                                    yang tidak disebabkan oleh suatu bencana alam (​ Force Majeure ​ ) yang ditetapkan
                                    pemerintah. Ganti rugi diberikan dalam bentuk uang atau barang yang memiliki nilai
                                    sama/setara sebagaimana tertera pada Surat Bukti Gadai (SBG), dengan tetap
                                    memperhitungkan kewajiban nasabah berupa Uang Pinjaman, Sewa Modal dan Biaya
                                    Lainnya (jika ada), sesuai ketentuan penggantian yang berlaku di PT PEGADAIAN
                                    (Persero).
                                </li>
                                <li>
                                    NASABAH dapat melakukan Ulang Gadai, Gadai Ulang Otomatis, dan Minta Tambah Uang
                                    Pinjaman, selama nilai taksiran masih memenuhi syarat dengan memperhitungkan Sewa
                                    Modal, Biaya Administrasi dan Biaya Lainnya (jika ada) yang masih akan dibayar. Jika
                                    terjadi penurunan nilai Taksiran Barang Jaminan pada saat Ulang Gadai, maka NASABAH
                                    wajib mengangsur Uang Pinjaman atau menambah barang jaminan agar sesuai dengan
                                    nilai taksiran yang baru.
                                </li>
                                <li>
                                    Terhadap barang jaminan yang telah dilunasi dan belum diambil oleh NASABAH, terhitung
                                    sejak terjadinya tanggal pelunasan sampai dengan sepuluh hari tidak dikenakan biaya jasa
                                    titipan. Bila telah melebihi sepuluh hari dari pelunasan, barang jaminan tetap belum diambil,
                                    maka NASABAH sepakat dikenakan biaya jasa titipan yang besarannya sesuai dengan
                                    ketentuan yang berlaku di PT PEGADAIAN (Persero) atau sebesar yang tercantum dalam
                                    Nota Transaksi.
                                </li>
                                <li>
                                    Apabila sampai dengan tanggal lelang sebagimana tertera dalam Nota Transaksi tidak
                                    dilakukan Pelunasan, Ulang Gadai atau Gadai Ulang Otomatis, maka PT PEGADAIAN
                                    (Persero) berhak melakukan penjualan barang jaminan melalui lelang.
                                </li>
                                <li>
                                    Sebelum tanggal lelang sebagaimana tercantum dalam Nota Transaksi, nasabah dapat
                                    menjual sendiri barang jaminannya atau memberikan kuasa kepada PT PEGADAIAN
                                (Persero) untuk menjualkan barang jaminannya.
                                </li>
                                <li>
                                    Hasil penjualan lelang Barang Jaminan setelah dikurangi Uang Pinjaman, Sewa Modal,
                                    Biaya lainnya (jika ada) dan Bea Lelang, merupakan kelebihan yang menjadi hak
                                    NASABAH, PT PEGADAIAN (Persero) akan memberitahukan nominal Uang Kelebihan
                                    NASABAH melalui papan pengumuman di Kantor Cabang /Unit Pelayanan Cabang
                                    Penerbit SBG atau mengirimkan surat ke alamat NASABAH atau melalui media lainnyaseperti telepon, short message service (SMS).
                                </li>
                                <li>
                                    NASABAH setuju bahwa biaya pemberitahuan Uang Kelebihan kepada NASABAH dapat
                                    diperhitungkan sebagai pengurang dari Uang Kelebihan.
                                </li>
                                <li>
                                    Jangka waktu pengambilan uang kelebihan lelang adalah selama 1 (satu) tahun sejak
                                    tanggal lelang sebagaimana dimaksud pada angka 8 Perjanjian ini.
                                </li>
                                <li>
                                    Jika lewat waktu dari jangka waktu pengambilan uang kelebihan lelang, NASABAH
                                    menyatakan setuju untuk menyalurkan uang kelebihan lelang tersebut sebagai Dana
                                    Kepedulian Sosial yang pelaksanaannya diserahkan kepada PT PEGADAIAN (Persero).
                                    Jika hasil penjualan lelang Barang Jaminan tidak mencukupi untuk melunasi kewajiban
                                    NASABAH berupa Uang Pinjaman, Sewa Modal, Biaya lainnya (jika ada) dan Bea Lelang
                                    maka NASABAH wajib membayar kekurangan tersebut.
                                </li>
                                <li>
                                    NASABAH dapat datang sendiri atau secara online untuk melakukan Ulang Gadai atau
                                    Mengangsur Uang Pinjaman atau Pelunasan, melalui Kantor Cabang/Unit Pelayanan
                                    Cabang atau melalui ​ channel ​ yang bekerjasama dengan PT PEGADAIAN (Persero).
                                </li>
                                <li>
                                    Nasabah datang sendiri untuk melakukan Minta Tambah Uang Pinjaman atau Menerima
                                    Barang Jaminan atau Menerima Ulang Kelebihan Lelang, dan/atau dapat dengan
                                    memberikan kuasa kepada orang lain dengan mekanisme yang ditetapkan oleh PT
                                    Pegadaian (Persero).
                                </li>
                                <li>
                                    Bilamana NASABAH meninggal dunia dan terdapat hak dan kewajiban terhadap PT
                                    PEGADAIAN (Persero) ataupun sebaliknya, maka hak dan kewajiban dibebankan kepada
                                    ahli waris NASABAH sesuai dengan ketentuan waris dalam hukum Republik Indonesia.
                                </li>
                                <li>
                                    NASABAH menyatakan tunduk dan mengikuti segala peraturan yang berlaku di PT
                                    PEGADAIAN (Persero) sepanjang ketentuan yang menyangkut Utang Piutang Dengan
                                    Jaminan Gadai.
                                </li>
                                <li>
                                    Apabila terjadi perselisihan dikemudian hari akan diselesaikan secara musyawarah untuk
                                    mufakat dan apabila tidak tercapai kesepakatan akan diselesaikan melalui Lembaga
                                    Alternatif Penyelesaian Sengketa di Bidang Usaha Pergadaian sesuai Undang – Undang
                                    yang berlaku.
                                </li>
                                <li>
                                    Perjanjian ini telah disesuaikan dengan ketentuan perundang-undangan termasuk
                                    ketentuan Otoritas Jasa Keuangan
                                </li>
                            </ol>
                            Demikian perjanjian ini berlaku dan mengikat PT PEGADAIAN (Persero) dengan NASABAH sejak Surat Bukti Gadai (SBG) ini ditandatangani oleh kedua belah pihak.
                            <br><br>
                            <h1> <img src="<?php echo base_url()."uploaded/user/gadai/".$barcode ?>" ></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <table class="table" style="border: 1px solid #ccc;">
                    <body>
                        <tr>
                            <td>
                               Kitir Barang Jaminan 
                            </td>
                        </tr>
                        <tr>
                            <td>
                               NAMA : <?php print $nasabah ?>
                            </td>
                        </tr>
                         <tr>
                            <td>
                               NO KREDIT : 
                              <?php 
                              if($no_kredit==null){
                              }else{
                                print $no_kredit;
                              }
                              ?>
                            </td>
                        </tr>
                         <tr>
                            <td>
                               RUBRIK : <?php print $rubrik ?><br>
                               TGL KREDIT : <?php 
                               if(isset($tglKredit)){
                                  $parsed_date=strtotime($tglKredit);
                                  if($parsed_date===FALSE){
                                    print '';
                                  }elseif($tglKredit=='0000-00-00'){
                                    print '';
                                  }else{
                                    print date("d-m-Y", strtotime($tglKredit)); 
                                  }
                                }

                               ?><br>
                               TAKSIRAN : Rp. <?php print number_format($taksiran, '0',',','.')  ?><br>
                               UP : Rp. <?php print number_format($up, '0',',','.') ?>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <br><br>
                            </td>
                        </tr>
                    </body>
                </table>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>