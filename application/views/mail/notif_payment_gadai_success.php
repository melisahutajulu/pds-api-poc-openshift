Hi, <?php echo $nama ?> 
<br /></br /><br />
Terima kasih telah melakukan pembayaran.
<br /><br /><br />
Transaksi Anda Berhasil:
<br /><br />
<table class="table table-responsive">
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong><?php echo $payment; ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</td>
        <td>:</td>
        <td> <strong><?php echo $trxId; ?></strong></td>
    </tr>
    <tr>
        <td>Jenis Transaksi</td>
        <td>:</td>
        <td> <strong><?php echo $jenisTransaksi ?></strong></td>
    </tr>
    <tr>
        <td>No Kredit</td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td> <strong><?php echo $namaNasabah; ?></strong></td>
    </tr>
    <tr>
        <td>UP Lama</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($up,0,",","."); ?></strong></td>
    </tr>
    <tr>
        <td>Hari/Tarif</td>
        <td>:</td>
        <td> <strong><?php echo $hariTarif; ?></strong></td>
    </tr>
    <tr>
        <td>UP Cicil</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($nilaiTransaksi,0,",","."); ?></strong></td>
    </tr>
    <tr>
        <td>Sewa Modal</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($sewaModal,0,",","."); ?></strong></td>
    </tr>
    <tr>
        <td>Administrasi</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($administrasi,0,",","."); ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Channel</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($biayaTransaksi,0,",","."); ?></strong></td>
    </tr>
    <tr>
        <td>Total</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($totalKewajiban + $biayaTransaksi,0,",","."); ?></strong></td>
    </tr>
    <?php if($jtCode !='TB') {?>
    <tr>
        <td>Tgl Jatuh Tempo</td>
        <td>:</td>
        <td> <strong><?php echo $tglJatuhTempo; ?></strong></td>
    </tr>    
    <tr>
        <td>Uang Pinjaman</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($up - $nilaiTransaksi,0,",","."); ?></strong></td>
    </tr> 
    <?php } ?>

</table>
<br />
<br />

Terima Kasih

<br />
<br />


