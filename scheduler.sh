#!/usr/bin/sh

while true; do
    # Get Status Gosend Logam Mulia
    /usr/bin/php /usr/share/nginx/html/v2/index.php Cron getStatusGosendLogamMulia 1

    # Get Status Gosend Perhiasan
    /usr/bin/php /usr/share/nginx/html/v2/index.php Cron getStatusGosendPerhiasan 1

    # Get status ditolak Logam Mulia
    /usr/bin/php /usr/share/nginx/html/v2/index.php Cron getStatusTolakLogamMulia

    # Get status ditolak Perhiasan
    /usr/bin/php /usr/share/nginx/html/v2/index.php Cron getStatusTolakPerhiasan

    sleep 15
done
