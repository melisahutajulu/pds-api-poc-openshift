FROM php:7.1.29-fpm

# update linux installer
RUN apt-get update && apt-get -y install git && apt-get -y install zip

# install libraries that needed for php extentions
RUN  apt-get install -y libmcrypt-dev \
        libmagickwand-dev --no-install-recommends \
        && apt-get install -y libxslt-dev \
        && apt-get install -y libssl-dev \
        && apt-get install -y gcc make autoconf libc-dev pkg-config \
        && apt-get install -y zlib1g-dev \
        && apt-get install -y libmemcached-dev

# install php extentions using docker-php-ext
RUN docker-php-ext-install pdo_mysql \
        && docker-php-ext-install mysqli \
        && docker-php-ext-install bz2 \
        && docker-php-ext-install mcrypt \
        && docker-php-ext-install calendar \
        && docker-php-ext-install exif \
        && docker-php-ext-install gd \
        && docker-php-ext-install gettext \
        && docker-php-ext-install sockets \
        && docker-php-ext-install wddx \
        && docker-php-ext-install xsl

# install php extentions using pecl
RUN pecl install crypto-0.3.1 \
        && pecl install igbinary-2.0.8 \
        && pecl install memcached-3.0.4 \
        && pecl install msgpack-2.0.2 \
        && docker-php-ext-enable crypto \
        && docker-php-ext-enable igbinary \
        && docker-php-ext-enable memcached \
        && docker-php-ext-enable msgpack

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --install-dir=/var --filename=composer \
    && php -r "unlink('composer-setup.php');"

# copy composer.json file
RUN cd /var/www
COPY composer.lock composer.json /var/www/

# install all dependencies
RUN cd /var/www && php /var/composer install

# create project directory
WORKDIR /var/www

# copy all files
COPY . /var/www